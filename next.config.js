let config = {
  images: {
    domains: [
      '5.imimg.com',
      'us.123rf.com',
      'http2.mlstatic.com',
      'www.dreaminterpretation.co',
      'vibrantprintshopimage.s3.amazonaws.com',
    ],
  },
  env: {},
};

if (process.env.MOBILE_BUILD == 'true') {
  config.images.loader = 'imgix';
  config.images.path = process.env.APP_URL;
  config.env.MOBILE_BUILD = process.env.MOBILE_BUILD;
}
module.exports = config;
