const myLoader = ({ src, width, quality }) => {
  return `${process.env.API_URL}${src}`;
};

export default myLoader;
