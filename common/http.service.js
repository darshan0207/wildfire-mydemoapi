import axios from 'axios';
const API_URL = 'https://www.shivshaktimultitraders.in:8001/api';

class HttpService {
  get(url, params = {}, token) {
    return call('GET', url, token, params);
  }

  post(url, params = {}, payload = {}, token) {
    return call('POST', url, token, params, payload);
  }

  delete(url, params = {}, token) {
    return call('DELETE', url, token, params);
  }

  put(url, params = {}, payload = {}, token) {
    return call('PUT', url, token, params, payload);
  }
}

function call(method, URL, token, params, payload = {}) {
  const opts = {
    method,
    url: API_URL + URL,
    headers: {
      'Content-Type': 'application/json',
    },
  };
  if (params) opts.params = params;
  if (payload) opts.data = payload;
  if (token) opts.headers.Authorization = `Bearer ${token}`;
  return axios(opts);
}

axios.interceptors.response.use(
  (res) => {
    return res;
  },
  (err) => {
    switch (err && err.response && err.response.status) {
      case 400:
        return err.response;
      case 401:
        return err;
      case 409:
        return err.response;
      default:
        return 'Error';
    }
  }
);

export default new HttpService();
