import Link from 'next/link';
import Image from 'next/image';
import styles from './promotion.module.scss';
import promotionJson from '../../Json/promotion.json';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import { useState } from 'react';

function Promotion({ promotions, discount }) {
  const [isCopied, setIsCopied] = useState(false);
  const [itemIndex, setItemIndex] = useState();

  const onCopyText = () => {
    setIsCopied(true);
    setTimeout(() => {
      setIsCopied(false);
    }, 1000);
  };

  const onGetCoupon = (indx) => {
    promotions[indx].ishide = true;
    setItemIndex(indx);
  };

  return (
    <>
      <div className={styles.hero_banner_sm}>
        <div className="container">
          <div className="row">
            <div className="col-lg-12 col-6 mx-auto">
              <div className="text-center py-4 d-flex justify-content-center">
                <div>
                  <div className={styles.special_promo_text}>Special Promo</div>
                  <div className="d-flex justify-content-center">
                    <div className={styles.special_promo_title}>
                      NEW ARRIVAL SALE
                    </div>
                    <div>
                      <div className={styles.special_promo_off}>
                        UPTO{' '}
                        <span className={styles.special_promo_offp}>
                          {discount}
                        </span>{' '}
                        OFF
                      </div>
                      <div className={styles.select_brand_text}>
                        Selected Brands
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className={styles.unique_feature_section}>
        <div className="container d-none d-lg-block d-md-none d-sm-none">
          {promotions.map((item, index) => {
            return (
              <div
                key={index}
                className={`row ms-0 me-0 hover ${styles.unique_feature_section_row}`}
              >
                <div className={`${styles.card} ${styles.cardLeft}`}>
                  <div className="d-flex">
                    <div className={styles.imgLeft}>
                      <div
                        style={{
                          background: item.color,
                          width: 100,
                          height: 100,
                        }}
                      >
                        <Image
                          src={item.image}
                          width="100px"
                          height="100px"
                          alt=""
                        />
                      </div>
                    </div>
                    <div className={styles.imgRight}>
                      <div className={styles.title}>{item.title}</div>
                      <div className={styles.card_text}>{item.text}</div>
                    </div>
                  </div>
                </div>
                <div className={`card ${styles.cardRight}`}>
                  <div className="m-auto">
                    {index !== itemIndex && (
                      <a
                        className="btn btn-sm btn-primary"
                        onClick={() => onGetCoupon(index)}
                      >
                        get coupon
                      </a>
                    )}
                    {index === itemIndex && item.ishide && (
                      <CopyToClipboard
                        text={item.couponcode}
                        onCopy={onCopyText}
                      >
                        <span>
                          {isCopied ? (
                            'Copied!'
                          ) : (
                            <button
                              className={`${styles.banner_ticket} btn btn-xs btn-outline-primary border-0 mb-0`}
                            >
                              {item.couponcode}
                            </button>
                          )}
                        </span>
                      </CopyToClipboard>
                    )}
                  </div>
                </div>
              </div>
            );
          })}
        </div>
        <div className="container  d-sm-block d-md-block d-lg-none">
          <div className="row row-cols-1 row-cols-md-12 g-4">
            {promotions.map((item, index) => {
              return (
                <div className="col" key={index}>
                  <div className="card p-0 rounded-0 overflow-hidden border-0">
                    <div
                      className={styles.card_content}
                      style={{ background: item?.color }}
                    >
                      <div className={styles.card_img}>
                        <Image
                          src={item.image}
                          alt="..."
                          width="100%"
                          height="120px"
                          objectFit="contain"
                        />
                      </div>
                    </div>
                    <div className="card-body text-center">
                      <div className={styles.title}>{item.title}</div>
                      <p className={`${styles.card_text} mt-2`}>{item.text}</p>
                    </div>
                    <div className={styles.holes_lower}></div>

                    <div className="m-auto mt-3">
                      {index !== itemIndex && (
                        <a
                          className="btn btn-sm btn-primary  mb-4"
                          onClick={() => onGetCoupon(index)}
                        >
                          get coupon
                        </a>
                      )}
                      {index === itemIndex && item.ishide && (
                        <CopyToClipboard
                          text={item.couponcode}
                          onCopy={onCopyText}
                        >
                          <span>
                            {isCopied ? (
                              'Copied!'
                            ) : (
                              <button
                                className={`${styles.banner_ticket} btn btn-xs btn-outline-primary border-0`}
                              >
                                {item.couponcode}
                              </button>
                            )}
                          </span>
                        </CopyToClipboard>
                      )}
                    </div>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    </>
  );
}

export async function getStaticProps() {
  return {
    props: {
      promotions: promotionJson?.promotion,
      discount: promotionJson?.discount,
    },
  };
}

export default Promotion;
