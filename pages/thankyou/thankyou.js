import { useRouter } from 'next/dist/client/router';
import Button from '../../components/Button/button';
import styles from './thankyou.module.scss';
import Link from 'next/link';
import { useEffect } from 'react';
export default function Thankyou({ isAuthenticated, recentOrder }) {
  // const rounter = useRouter();
  // console.log(recentOrder);
  // useEffect(() => {
  //   // if (!recentOrder?.orderId) {
  //   //   rounter.push('/');
  //   // }
  // }, [recentOrder]);

  return (
    <>
      <div className={styles.complete_order}>
        <div className={styles.thank_you}>Thank you</div>
        <div className={`${styles.your_order}`}>for your order !</div>
        <div className={`text-center ${styles.desc}`}>
          Your order number is <b>{recentOrder?.orderId}</b> <br /> We’ve
          emailed you an order confirmation, and we’ll notify you when the order
          been shipped.
        </div>
        <div className={` ${styles.complete_btn}`}>
          <Link href={'/'}>
            <a className={`m-3 btn ${styles.continue_shopping}`}>
              continue shopping
            </a>
          </Link>
          {isAuthenticated && (
            <Link href={`/order-details/${recentOrder?.orderId}`}>
              <a className={`m-3 btn ${styles.order_detail}`}>order details</a>
            </Link>
          )}
        </div>
      </div>
    </>
  );
}
