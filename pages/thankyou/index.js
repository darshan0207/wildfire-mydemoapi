import { connect } from 'react-redux';
import Thankyou from './thankyou';

const mapStateToProps = (state) => {
  console.log(state);
  return {
    isAuthenticated: state.auth.isAuthenticated,
    recentOrder: state.order.orderDetails,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {};
};
export default connect(mapStateToProps, mapDispatchToProps)(Thankyou);
