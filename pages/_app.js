import '../styles/globals.scss';
import Layout from '../layouts/default/index';
import { ToastContainer } from 'react-toastify';
import { wrapper } from '../redux/store/store';
import { PersistGate } from 'redux-persist/integration/react';
import { useStore } from 'react-redux';

function MyApp({ Component, pageProps }) {
  const store = useStore((state) => state);
  const getLayout = Component.getLayout || ((page) => <Layout>{page}</Layout>);

  return getLayout(
    <PersistGate persistor={store.__PERSISTOR} loading={<div>Loading...</div>}>
      <ToastContainer />
      <Component {...pageProps} />
    </PersistGate>
  );
}
export default wrapper.withRedux(MyApp);
