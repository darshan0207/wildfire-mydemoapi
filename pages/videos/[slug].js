import { connect } from 'react-redux';
import { END } from 'redux-saga';
import { useRouter } from 'next/router';
import { toast } from 'react-toastify';
import { wrapper } from '../../redux/store/store';
import {
  getVideoDetails,
  getVideoReview,
} from '../../redux/actionCreaters/vendor.actioncreater';
import VideoPage from '../../components/VideoPage';
import SEO from '../../components/seo/seo';

function Video({ videoDetails, getVideoReview, isAuthenticated }) {
  const router = useRouter();

  return (
    <>
      <VideoPage
        videoDetails={videoDetails}
        getVideoReview={getVideoReview}
        isAuthenticated={isAuthenticated}
      />
      <SEO
        description={videoDetails?.data?.attributes?.seo_description}
        title={videoDetails?.data?.attributes?.seo_title}
        keywords={videoDetails?.data?.attributes?.seo_title}
      />
    </>
  );
}

export async function getStaticPaths() {
  // const req = await fetch(`${process.env.API_URL}/api/v2/storefront/videos`);
  // const resp = await req.json();
  // let paths = [];
  // if (resp && resp.data && resp.data.length) {
  //   paths = resp.data.map((data) => ({
  //     params: { slug: data.attributes.slug },
  //   }));
  // }
  return {
    paths: [],
    fallback: process.env.MOBILE_BUILD === 'true' ? false : 'blocking',
  };
}

export const getStaticProps = wrapper.getStaticProps(
  (store) =>
    async ({ params }) => {
      store.dispatch(getVideoDetails(params.slug));
      store.dispatch(END);
      await store.sagaTask.toPromise();
      return { revalidate: 10 };
    }
);

const mapStateToProps = (state) => {
  return {
    videoDetails: state.vendor.videoDetails,
    isAuthenticated: state.auth.isAuthenticated,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    getVideoReview: (payload) => dispatch(getVideoReview(payload)),
    getVideoDetails: (payload) => dispatch(getVideoDetails(payload)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Video);
