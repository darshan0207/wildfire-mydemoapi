import { connect } from 'react-redux';
import { END } from 'redux-saga';
import { useRouter } from 'next/router';
import { wrapper } from '../../redux/store/store';
import {getVideoDetails, getVideoReview} from '../../redux/actionCreaters/vendor.actioncreater';
import VideoPage from '../../components/VideoPage';
import SEO from '../../components/seo/seo';
import { useEffect } from 'react';

function Video({getVideoDetails, videoDetails,getVideoReview,isAuthenticated }) {
  const router = useRouter();
  useEffect(() => {
    // if (!videoDetails.data) {
      const { slug } = router.query;
      getVideoDetails(slug);
    // }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [getVideoDetails, router.query]);
  if (videoDetails.data)
    return (
      <>
      <VideoPage
        videoDetails={videoDetails}
        getVideoReview={getVideoReview}
        isAuthenticated={isAuthenticated}
      />
      <SEO
        description={videoDetails?.data?.attributes?.seo_description}
        title={videoDetails?.data?.attributes?.seo_title}
        keywords={videoDetails?.data?.attributes?.seo_title}
      />
    </>
    );
  else return null;
  
}

const mapStateToProps = (state) => {
  return {
    videoDetails: state.vendor.videoDetails,
    isAuthenticated: state.auth.isAuthenticated,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    getVideoReview: (payload) => dispatch(getVideoReview(payload)),
    getVideoDetails: (payload) => dispatch(getVideoDetails(payload)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Video);
