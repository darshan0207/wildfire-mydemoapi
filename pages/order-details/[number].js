import Link from 'next/link';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import { Dropdown } from 'react-bootstrap';
import { connect } from 'react-redux';
import { toast } from 'react-toastify';
import {
  getSingleOrderDetail,
  doGetOrderReturnReasons,
  doOrderReturnUpdateItem,
  doGetOrderReturn,
  doGetInvoice,
} from '../../redux/actionCreaters/order.actioncreater';
import styles from './order-details.module.scss';
import Image from 'next/image';
import { ReactSVG } from 'react-svg';
import Loader from '../../components/Loader';
import { END } from 'redux-saga';
import { wrapper, State } from '../../redux/store/store';
import WithAuth from '../../components/WithAuth';
import myLoader from '../../common/loader';

function OrderHistory({
  orderDetails = {},
  line_items = [],
  included = [],
  getSingleOrderDetail,
  doGetOrderReturnReasons,
  orderReturnReasons,
  isloader = false,
  isHydrated,
  isAuthenticated,
  doOrderReturnUpdateItem,
  doGetOrderReturn,
  doGetInvoice,
}) {
  const router = useRouter();
  const [returnOrder, setReturnOrder] = useState(false);
  useEffect(() => {
    if (router?.query?.number) {
      getSingleOrderDetail(router?.query?.number);
      // if (!orderReturnReasons.length) doGetOrderReturnReasons();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [router, router?.query?.number]);

  const handleItem = (index, value, slug) => {
    doOrderReturnUpdateItem({ index, value, slug });
  };
  const confirmReturnOrder = () => {
    const returnItems = line_items.filter((items) => items.check == true);
    if (!returnItems.length) {
      toast.error('select item to return');
      return;
    }

    let invalidQuantity = returnItems.find(
      (item) =>
        item.attributes.quantity < item.quantity ||
        !item.quantity ||
        item.quantity === 0 ||
        (item.return_quantity &&
          item.attributes.quantity - item.return_quantity < item.quantity)
    );
    if (invalidQuantity && invalidQuantity.id) {
      toast.error('select valid quantity to return of selected item');
      return;
    }
    let invalidReason = returnItems.find((item) => !item.reason);
    if (invalidReason && invalidReason.id) {
      toast.error('select valid reason to return selected item');
      return;
    }
    doGetOrderReturn({
      order_number: orderDetails?.attributes?.number,
      return_items: returnItems,
      route: router,
    });
  };

  const getInvoice = (id) => {
    doGetInvoice({
      invoice_id: id,
    });
  };

  return (
    <>
      {isloader ? (
        <Loader />
      ) : (
        <div className="container mt-5">
          <div className="row">
            <div className="col-md-12">
              <Link href={'/account/order-history/'}>
                <a className={styles.order_details_link}>
                  Go back to Order History
                </a>
              </Link>
              <div
                className={`${styles.order_details_header} d-flex justify-content-between`}
              >
                <div>
                  <h3>Your order no. : {orderDetails?.orderId}</h3>
                  <div className={styles.order_details_status}>
                    {orderDetails?.paymentstate} /{orderDetails?.state} /
                    {orderDetails?.shipmentstate}
                  </div>
                </div>

                <div>
                  <h3>
                    {['shipped', 'partially', 'complete'].indexOf(
                      orderDetails?.shipmentstate
                    ) >= 0 && (
                      <a
                        onClick={() => setReturnOrder(!returnOrder)}
                        className="btn btn-xs btn-outline-primary h-50"
                      >
                        {returnOrder ? 'Cancel return order' : 'return order'}
                      </a>
                    )}

                    <a
                      onClick={() =>
                        getInvoice(orderDetails?.attributes?.invoice_id)
                      }
                      className="btn btn-xs btn-outline-primary h-50"
                    >
                      download invoice
                    </a>
                  </h3>
                </div>
              </div>
            </div>
          </div>
          <div className="row mt-5">
            <div
              className={`col-md-12 custom_scrollbar ${styles.custom_table} ${
                returnOrder && 'order_detail_active'
              }`}
            >
              <div className="table-responsive-sm pb-5">
                <table className="w-100 align-middle ">
                  <thead>
                    <tr>
                      <th style={{ width: 30 }}></th>
                      <th style={{ minWidth: 180 }}>Products</th>
                      <th style={{ minWidth: 300 }}>Description</th>
                      <th className="text-center" style={{ minWidth: 150 }}>
                        Price
                      </th>
                      <th className="text-center" style={{ minWidth: 150 }}>
                        Variant
                      </th>
                      <th className="text-center" style={{ minWidth: 150 }}>
                        Quantity
                      </th>
                      {returnOrder && (
                        <>
                          <th className="text-center" style={{ minWidth: 150 }}>
                            Return quantity
                          </th>
                          <th className="text-center" style={{ minWidth: 150 }}>
                            Return reason
                          </th>
                          <th className="text-center" style={{ minWidth: 150 }}>
                            Return notes
                          </th>
                        </>
                      )}
                      <th className="text-end" style={{ minWidth: 150 }}>
                        Total Price
                      </th>
                      <th style={{ width: 30 }}></th>
                    </tr>
                  </thead>
                  <tbody>
                    {line_items?.length > 0 &&
                      line_items.map((item, i) => {
                        return (
                          <tr key={i}>
                            <td>
                              {returnOrder && item.isReturnable && (
                                <input
                                  className="form-check-input me-2 pointer"
                                  name={`item${i}`}
                                  type="checkbox"
                                  checked={item?.check}
                                  onChange={(e) => {
                                    handleItem(i, e.target.checked, 'check');
                                    handleItem(
                                      i,
                                      parseInt(item.variantId),
                                      'variant_id'
                                    );
                                  }}
                                />
                              )}
                            </td>
                            <td>
                              {item?.product?.images ? (
                                <div className={styles.image_detail}>
                                  <Image
                                    // loader={myLoader}
                                    src={`${item?.product?.images[0]?.path}`}
                                    width="100px"
                                    height="100px"
                                    alt=""
                                    layout={'fill'}
                                    objectFit={'contain'}
                                  />
                                </div>
                              ) : (
                                <ReactSVG src="/assets/noimage.svg" />
                              )}
                            </td>
                            <td className="text-wrap">
                              <Link
                                href={`/products${
                                  process.env.MOBILE_BUILD == 'true'
                                    ? '?slug='
                                    : '/'
                                }${item?.product?.slug}`}
                              >
                                <a className="pointer text-decoration-none">
                                  {item?.product?.name}
                                </a>
                              </Link>
                            </td>
                            <td
                              className={`text-center ${styles.font_weight_600}`}
                            >
                              {item?.product?.price}
                            </td>
                            <td className="text-center">
                              {item?.product?.options_text}
                            </td>
                            <td className="text-center">{item?.quantity}</td>
                            {returnOrder && (
                              <>
                                <td className="text-center">
                                  <input
                                    className={styles.return_quantity}
                                    name={`item${i}`}
                                    type="number"
                                    max={
                                      item?.return_quantity
                                        ? item?.attributes?.quantity -
                                          item?.return_quantity
                                        : item?.attributes?.quantity
                                    }
                                    min="0"
                                    value={item?.quantity}
                                    disabled={!item.isReturnable}
                                    onChange={(e) =>
                                      handleItem(
                                        i,
                                        parseInt(e.target.value),
                                        'quantity'
                                      )
                                    }
                                  />
                                  <br />
                                  {item?.product?.returnable && (
                                    <sub className={styles.sub_max}>
                                      {' '}
                                      Max return quantity :{' '}
                                      {item?.return_quantity
                                        ? item?.product?.quantity -
                                          item?.return_quantity
                                        : item?.product?.quantity}
                                    </sub>
                                  )}
                                  {!item.isReturnable && (
                                    <sub className={styles.sub_max}>
                                      item is not returnable{' '}
                                    </sub>
                                  )}
                                </td>
                                <td className="text-center">
                                  <Dropdown
                                    onClick={(e) => {
                                      handleItem(
                                        i,
                                        e?.target?.textContent,
                                        'reason'
                                      );
                                      handleItem(
                                        i,
                                        parseInt(e?.target?.id),
                                        'return_authorization_reason_id'
                                      );
                                    }}
                                  >
                                    <Dropdown.Toggle
                                      className={styles.reason}
                                      disabled={!item.product?.returnable}
                                    >
                                      {item?.reason
                                        ? item?.reason
                                        : 'select reason'}
                                    </Dropdown.Toggle>
                                    <Dropdown.Menu
                                      className={styles.custom_dropdown_menu}
                                    >
                                      {orderReturnReasons.map((reason) => {
                                        return (
                                          <Dropdown.Item
                                            eventKey={reason.id}
                                            key={reason.id}
                                          >
                                            <div
                                              key={reason.id}
                                              id={reason.id}
                                              className={`pointer ${styles.custom_dropdown_item}`}
                                            >
                                              {reason.name}
                                            </div>
                                          </Dropdown.Item>
                                        );
                                      })}
                                    </Dropdown.Menu>
                                  </Dropdown>
                                </td>
                                <td className="text-center">
                                  <textarea
                                    name={`notes${i}`}
                                    placeholder={
                                      !item.product?.returnable
                                        ? 'You can not return this item'
                                        : 'enter your notes here'
                                    }
                                    rows={2}
                                    value={item?.memo}
                                    disabled={!item.product?.returnable}
                                    onChange={(e) =>
                                      handleItem(i, e?.target?.value, 'memo')
                                    }
                                  />
                                </td>
                              </>
                            )}

                            <td
                              className={`text-end ${styles.font_weight_600}`}
                            >
                              {orderDetails?.total}
                            </td>
                            <td></td>
                          </tr>
                        );
                      })}

                    <tr className={styles.table_footer}>
                      <td></td>
                      <td colSpan="4" className={`${styles.table_footer_td}`}>
                        Subtotal
                      </td>
                      <td
                        colSpan={returnOrder ? '5' : '2'}
                        className={`text-end ${styles.font_weight_600}`}
                      >
                        {orderDetails?.total}
                      </td>
                      <td></td>
                    </tr>
                    <tr className={styles.table_footer}>
                      <td></td>
                      <td colSpan="4" className={`${styles.table_footer_td}`}>
                        Shipping Cost
                      </td>
                      <td
                        colSpan={returnOrder ? '5' : '2'}
                        className={`text-end ${styles.font_weight_600}`}
                      >
                        {orderDetails?.attributes?.shippingrates > 0
                          ? orderDetails?.attributes?.display_ship_total
                          : 'Free'}
                        {/* {included &&
                          included?.map((item) => {
                            if (item?.type === 'shipment') {
                              return item?.attributes?.free === true
                                ? 'Free'
                                : parseFloat(item?.attributes?.final_price);
                            }
                          })} */}
                      </td>
                      <td></td>
                    </tr>
                    {orderDetails?.attributes?.tax_total > 0 && (
                      <tr className={styles.table_footer}>
                        <td></td>
                        <td colSpan="4" className={`${styles.table_footer_td}`}>
                          TAX COST
                        </td>
                        <td
                          colSpan={returnOrder ? '5' : '2'}
                          className={`text-end ${styles.font_weight_600}`}
                        >
                          {orderDetails?.total}
                        </td>
                        <td></td>
                      </tr>
                    )}
                    <tr className={styles.table_footer}>
                      <td></td>
                      <td colSpan="4" className={`${styles.table_footer_td}`}>
                        TOTAL Cost
                      </td>
                      <td
                        colSpan={returnOrder ? '5' : '2'}
                        className={`text-end ${styles.font_weight_600}`}
                      >
                        {orderDetails?.total}
                      </td>
                      <td></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          {returnOrder && (
            <div className="row ">
              <div className={styles.confirm_return_button}>
                <h3 onClick={() => confirmReturnOrder()}>
                  <a className="btn btn-xs btn-outline-primary h-50">
                    confirm return order
                  </a>
                </h3>
              </div>
            </div>
          )}
          <div className="row mt-5 mb-3">
            <div className="col-md-3">
              <h5 className="mb-3">Shipping address</h5>
              <div className={styles.card}>
                <div className="card-body">
                  <div>
                    <div className={styles.shipping_address}>Address</div>
                    <p className={styles.shipping_address_content}>
                      {/* {item?.attributes?.address1},{' '}
                      {item?.attributes?.state_code} {item?.attributes?.zipcode}{' '}
                      {item?.attributes?.country_name} */}
                      {orderDetails?.shipaddress?.address1}
                    </p>
                    <div className={styles.shipping_address}>Phone</div>
                    <p className={styles.shipping_address_content}>
                      {/* {item?.attributes?.phone} */}
                      {orderDetails?.shipaddress?.phone}
                    </p>
                    <div className={styles.shipping_address}>Email</div>
                    <p className={styles.shipping_address_content}>
                      {orderDetails?.email}
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="row mb-5">
            <div className="col-md-3">
              <h6 className="mb-3">Shipping Method</h6>
              <div className={styles.card}>
                <div className="card-body">
                  <div>
                    <div className={styles.shipping_address}>Method</div>
                    <div className={` mb-2 ${styles.shipping_address_content}`}>
                      {/* {item?.attributes?.name} */}
                      {orderDetails?.shipmentmethod?.name}
                    </div>
                  </div>
                  <div>
                    {/* <><div className={styles.shipping_address}>
                      Tracking number
                    </div>
                    <div className={` mb-2 ${styles.shipping_address_content}`}>
                      {item?.attributes?.tracking}
                    </div></> */}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
}

const mapStateToProps = (state) => {
  return {
    orderDetails: state.order.orderDetails,
    line_items: state.order.orderDetails.lineitems,
    // included: state.order.included,
    // isloader: state.order.isloader,
    // isHydrated: state._persist && state._persist.rehydrated ? true : false,
    // isAuthenticated: state?.auth?.isAuthenticated,
    // orderReturnReasons: state.order.orderReturnReasons,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getSingleOrderDetail: (payload) => dispatch(getSingleOrderDetail(payload)),
    // doGetOrderReturnReasons: () => dispatch(doGetOrderReturnReasons()),
    // doOrderReturnUpdateItem: (payload) =>
    //   dispatch(doOrderReturnUpdateItem(payload)),
    // doGetOrderReturn: (payload) => dispatch(doGetOrderReturn(payload)),
    // doGetInvoice: (payload) => dispatch(doGetInvoice(payload)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(OrderHistory);
