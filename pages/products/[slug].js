import {
  productVariant,
  singleProduct,
} from '../../redux/actionCreaters/product.actioncreater';
import { connect } from 'react-redux';
import Productview from '../../components/ProductView/productview';
import { END } from 'redux-saga';
import SEO from '../../components/seo/seo';
import { wrapper } from '../../redux/store/store';
import { addProductToWishlist } from '../../redux/actionCreaters/order.actioncreater';

function Product({
  productData,
  productVariant,
  isAuthenticated,
  // ratingReview,
  // getReviewRating,
  // ratingReviewmeta,
  wishlistItem,
  addProductTOWishlist,
}) {
  return (
    <>
      <Productview
        productData={productData}
        productVariant={productVariant}
        isAuthenticated={isAuthenticated}
        // ratingReview={ratingReview}
        // reviewMeta={ratingReviewmeta}
        // allReviewRating={getReviewRating}
        wishlistItem={wishlistItem}
        addProductTOWishlist={addProductTOWishlist}
      />
      <SEO
        description={productData?.data?.metadescription}
        title={productData?.data?.metatitle}
        keywords={productData?.data?.metakeywords}
      />
    </>
  );
}

export async function getStaticPaths() {
  return {
    paths: [],
    fallback: process.env.MOBILE_BUILD === 'true' ? false : 'blocking',
  };
}

export const getStaticProps = wrapper.getStaticProps(
  (store) =>
    async ({ preview, params }) => {
      store.dispatch(singleProduct(params.slug));
      // store.dispatch(
      //   getReviewRating({ slug: params.slug, page: 1, per_page: 3 })
      // );
      store.dispatch(END);
      await store.sagaTask.toPromise();
      return { revalidate: 10 };
    }
);

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.auth.isAuthenticated,
    productData: state.singleProduct,
    isAuthenticated: state.auth.isAuthenticated,
    // ratingReview: state.singleProduct.ratingReview,
    // ratingReviewmeta: state.singleProduct.meta,
    wishlistItem: state.auth?.wishlist,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    singleProduct: (payload) => dispatch(singleProduct(payload)),
    productVariant: (payload) => dispatch(productVariant(payload)),
    // getReviewRating: (payload) => dispatch(getReviewRating(payload)),
    // getWishListData: () => dispatch(getWishListData()),
    addProductTOWishlist: (payload) => dispatch(addProductToWishlist(payload)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Product);
