import styles from './contact-us.module.scss';
import { ReactSVG } from 'react-svg';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { connect } from 'react-redux';
import { doContactUs } from '../../redux/actionCreaters/contact-us.actioncreater';
import SocialLinks from '../../components/SocialLinks/sociallinks';

function ContactUs({ doContactUs, isLoader }) {
  const initialValues = {
    email: '',
    message: '',
    first_name: '',
    last_name: '',
  };

  const formik = useFormik({
    initialValues: initialValues,
    validationSchema: Yup.object({
      email: Yup.string()
        .email('Invalid email address')
        .required('This field is required.'),
      message: Yup.string().required('This field is required.'),
      first_name: Yup.string().required('This field is required.'),
      last_name: Yup.string().required('This field is required.'),
    }),
    onSubmit: (e, { resetForm }) => {
      doContactUs({
        contact: {
          email: e.email,
          message: e.message,
          first_name: e.first_name,
          last_name: e.last_name,
        },
        resetvalue: resetForm,
      });
    },
  });

  return (
    <>
      <div className={styles.tile_head}>
        <div className={`text-center`}>
          <div className={styles.wild_logo}>
            <div className={styles.main_title}>get in touch with us</div>
            <div className="row m-0 text-center">
              <div className="col-sm-12 col-md-4 col-lg-4 mt-4">
                <ReactSVG
                  src="/assets/icons/map.svg"
                  className={styles.svg_custom}
                />
                <div className={styles.title}>ADDRESS</div>
                <div className={styles.content}>
                  Quadra CL 408 Bloco A, 1807 Santa <br /> Maria-DF -72508-241
                </div>
                <div className={`mt-4 ${styles.content}`}>
                  Rua Tibúrcio Frota, 1963 Fortaleza <br /> CE - 60130-301
                </div>
              </div>
              <div className="col-sm-12 col-md-4 col-lg-4 mt-4">
                <ReactSVG
                  src="/assets/icons/phone.svg"
                  className={styles.svg_custom}
                />
                <div className={styles.title}>PHONE</div>
                <div className={styles.content}>
                  000 (61) 5805-9059
                  <br />
                  000 (61) 5805-9060
                </div>
                <div className={`mt-4 ${styles.content}`}>
                  00 (85) 7147-7233
                  <br />
                  00 (85) 7147-7233
                </div>
              </div>
              <div className="col-sm-12 col-md-4 col-lg-4 mt-4">
                <ReactSVG
                  src="/assets/icons/mail.svg"
                  className={styles.svg_custom}
                />
                <div className={styles.title}>EMAIL</div>
                <div className={styles.content}>
                  SophiaCarvalhoSouza@rhyta.com
                </div>
                <div className={styles.content}>
                  AlexRodriguesAzevedo@armyspy.com
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className={`row m-0 ${styles.bg_contact}`}>
        <div className={`col-md-10  ${styles.custom_contact}`}>
          <div className="row">
            <div className={`col-md-7 mb-4 ${styles.custom_main}`}>
              <div className={`${styles.contact_title} mb-2`}>HIT US UP!</div>
              <div className={` ${styles.contact_content}`}>
                WE’D LOVE TO HEAR FROM YOU! DROP US A NOTE WITH ANY COMMENTS,
                QUESTIONS OR TECHNICAL CONCERNS.
              </div>
              <div className={`${styles.contact_title} mt-4 mb-3`}>
                Follow us
              </div>
              <SocialLinks iscontectussvg={true} />
            </div>
            <div className={`col-md-5 mb-4`}>
              <form noValidate onSubmit={formik.handleSubmit}>
                <div className={`row ${styles.contact_from}`}>
                  <div className="col-md-12">
                    <label
                      htmlFor="first_name"
                      className={`form-label ${styles.form_label}`}
                    >
                      Name
                    </label>
                  </div>
                  <div className="col-6 col-sm-6 col-md-6 col-lg-6">
                    <input
                      className={`form-control ${styles.form_control}`}
                      type="text"
                      id="first_name"
                      placeholder="First Name"
                      name="first_name"
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                      value={formik.values['first_name']}
                    />
                    {formik.touched['first_name'] &&
                    formik.errors['first_name'] ? (
                      <div className="text-danger field-validation-valid font-16">
                        {formik.errors['first_name']}
                      </div>
                    ) : null}
                  </div>
                  <div className="col-6 col-sm-6 col-md-6 col-lg-6">
                    <input
                      className={`form-control ${styles.form_control}`}
                      type="text"
                      placeholder="Last Name"
                      id="last_name"
                      name="last_name"
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                      value={formik.values['last_name']}
                    />
                    {formik.touched['last_name'] &&
                    formik.errors['last_name'] ? (
                      <div className="text-danger field-validation-valid font-16">
                        {formik.errors['last_name']}
                      </div>
                    ) : null}
                  </div>
                </div>
                <div className={`row mt-4 ${styles.contact_from}`}>
                  <div className="col-md-12">
                    <label
                      htmlFor="email"
                      className={`form-label ${styles.form_label}`}
                    >
                      Email
                    </label>
                    <input
                      className={`form-control ${styles.form_control}`}
                      type="text"
                      placeholder="wirte your email address here"
                      id="email"
                      name="email"
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                      value={formik.values['email']}
                    />
                    {formik.touched['email'] && formik.errors['email'] ? (
                      <div className="text-danger field-validation-valid font-16">
                        {formik.errors['email']}
                      </div>
                    ) : null}
                  </div>
                </div>
                <div className={`row mt-4 ${styles.contact_from}`}>
                  <div className="col-md-12">
                    <label
                      htmlFor="message"
                      className={`form-label ${styles.form_label}`}
                    >
                      Comments
                    </label>
                    <textarea
                      rows="5"
                      className={`form-control ${styles.form_control}`}
                      type="text"
                      placeholder="wirte your comments here."
                      id="message"
                      name="message"
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                      value={formik.values['message']}
                    />
                    {formik.touched['message'] && formik.errors['message'] ? (
                      <div className="text-danger field-validation-valid font-16">
                        {formik.errors['message']}
                      </div>
                    ) : null}
                    <button className={styles.contact_btn}>
                      {isLoader && (
                        <span
                          className="me-2 spinner-border spinner-border-sm text-light"
                          role="status"
                        ></span>
                      )}
                      submit
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

const mapStateToProps = (state) => {
  return {
    isLoader: state.contactus.isloader,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    doContactUs: (payload) => dispatch(doContactUs(payload)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ContactUs);
