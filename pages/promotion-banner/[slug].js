import styles from './promotion-banner.module.scss';
import PromotionCard from '../../components/PromotionCard/PromotionCard';
import promotionJson from '../../Json/promotion.json';
import promotionbannerJson from '../../Json/promotionbanner.json';

function PromotionBanner({ promotionbanner }) {
  return (
    <>
      <div className={styles.hero_banner_sm}>
        <div className="container">
          <div className="row">
            <div className="col-lg-12 col-6 mx-auto">
              <div className="text-center py-4 d-flex justify-content-center">
                <div>
                  <div className={styles.special_promo_text}>Banner</div>
                  <div className="d-flex justify-content-center">
                    <div className={styles.special_promo_title}>Promotion</div>
                    <div className={styles.special_promo_off}>Banner</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className={styles.Promotion_Banner_section}>
        <div className="row row-cols-1 row-cols-md-4 g-4">
          {promotionbanner.map((item, index) => {
            return (
              <PromotionCard
                key={index}
                title={item.title}
                text={item.text}
                color={item.color}
                image={item.image}
              />
            );
          })}
        </div>
      </div>
    </>
  );
}

async function getPromotionCode(id) {
  const getdata = promotionbannerJson.filter((item) => item.promotionid === id);
  return getdata;
}
let getStaticPaths = null;
let getStaticProps = null;
if (process.env.MOBILE_BUILD != 'true') {
  getStaticPaths = async () => {
    let paths = [];
    if (promotionJson.length) {
      paths = promotionJson.map((data) => ({
        params: { slug: data.slug },
      }));
    }
    return {
      paths,
      fallback: 'blocking',
    };
  };

  getStaticProps = async ({ params }) => {
    const getCode = await getPromotionCode(params.slug);

    return {
      props: {
        promotionbanner: getCode,
      },
    };
  };
}
export { getStaticPaths, getStaticProps, PromotionBanner as default };
