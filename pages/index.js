import Cookie from '../components/CookieConsent/cookie';
import { getLayout } from '../layouts/default';
import Home from './home/index';
import { wrapper } from '../redux/store/store';
import { END } from 'redux-saga';

function Index() {
  return (
    <>
      <Home />
      {process.env.MOBILE_BUILD === 'true' ? null : <Cookie />}
    </>
  );
}

Index.getLayout = getLayout;
export default Index;
