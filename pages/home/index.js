import { connect } from 'react-redux';
import { getHomeDetail } from '../../redux/actionCreaters/home.actioncreater';
import Home from './home';
// import { wrapper } from '../../redux/store/store';
// import { END } from 'redux-saga';
// import { getWishListData } from '../../redux/actionCreaters/order.actioncreater';

const mapStateToProps = (state) => {
  return {
    isAuth: state.auth.isAuthenticated,
    mainvideo: state?.home?.data?.banner,
    topproducts: state?.home?.data?.topproduct,
    // newarrivalvendor: state?.home?.data?.relationships?.new_arrival_vendor,
    // newarrivalvideo: state?.home?.data?.relationships?.new_arrival_vendor_video,
    relatedproducts: state?.home?.data?.trendingitem,
    // topbrandvendor: state?.home?.data?.relationships?.top_brand_vendor,
    // topbrandvendorvideo:
    //   state?.home?.data?.relationships?.top_brand_vendor_video,
    // topbrandvendorvideoreview:
    //   state?.home?.data?.relationships?.top_brand_vendor_video_review,
    wishlistItem: state.auth?.wishlist,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getHomeDetail: () => dispatch(getHomeDetail()),
    // getWishListData: () => dispatch(getWishListData()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
