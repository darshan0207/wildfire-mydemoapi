import Banner from '../../components/Banner/Banner';
import TrendingItem from '../../components/TrendingItem';
// import TopBrand from '../../components/TopBrand/TopBrand';
// import NewArrival from '../../components/NewArrival/NewArrival';
// import SocialShopping from '../../components/SocialShopping/SocialShopping';
import Trendz from '../../components/Trendz/Trendz';
import TrendzTwo from '../../components/TrendzTwo/TrendzTwo';
// import TrendzThree from '../../components/TrendzThree/TrendzThree';
import { useEffect, useState } from 'react';
import promotionJson from '../../Json/homebanner.json';

export default function Home({
  isAuth,
  mainvideo,
  topproducts,
  newarrivalvendor,
  newarrivalvideo,
  relatedproducts,
  topbrandvendor,
  topbrandvendorvideo,
  topbrandvendorvideoreview,
  getHomeDetail,
  wishlistItem,
  getWishListData,
}) {
  let homebannerJson = promotionJson || [];
  const wished_products = wishlistItem;

  useEffect(() => {
    getHomeDetail();
    // if (isAuth) {
    //   getWishListData();
    // }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      <Banner imageurl={mainvideo} />
      <Trendz
        goldBanner={
          homebannerJson &&
          homebannerJson?.length &&
          homebannerJson.filter((item) => item.type === 'gold')
        }
      />
      <div className="treding-slider">
        {topproducts?.length > 0 && (
          <div className="container">
            <div className="row">
              <div className="col-lg-12">
                <h3 className="title-md text-uppercase text-dark fw-bold">
                  trending items
                </h3>
              </div>
            </div>
            <TrendingItem
              trendingproducts={topproducts}
              wished_products={wished_products}
              isWishlist={true}
              isAuth={isAuth}
              isproduct={true}
            />
          </div>
        )}
      </div>
      {/* <TopBrand
        isAuth={isAuth}
        vendor={topbrandvendor?.topbrandattributes}
        videourl={topbrandvendorvideo?.video_url}
        thumbnailurl={topbrandvendorvideo?.thumbnail}
        review={topbrandvendorvideoreview?.topbrandattributes}
        userimage={topbrandvendorvideoreview?.userimage}
        index={videoIndex}
        currentVideoIndex={() => setVideoIndex(1)}
      /> */}
      <TrendzTwo
        sliverBanner={
          homebannerJson &&
          homebannerJson?.length &&
          homebannerJson.filter((item) => item.type === 'silver')
        }
      />
      {/* <NewArrival
        videourl={newarrivalvideo?.video_url}
        thumbnailurl={newarrivalvideo?.thumbnail}
        attributes={newarrivalvideo?.videoattributes}
        taxons={newarrivalvideo?.taxonsdata}
        vendor={newarrivalvendor}
        index={videoIndex}
        currentVideoIndex={() => setVideoIndex(2)}
      /> */}
      <div className="treding-slider pt-lg-2 pt-3">
        {relatedproducts?.length > 0 && (
          <div className="container">
            <div className="row">
              <div className="col-lg-12">
                <h3 className="leads text-uppercase text-dark fw-normal">
                  Related Products
                </h3>
              </div>
            </div>
            <TrendingItem
              trendingproducts={relatedproducts}
              wished_products={wished_products}
              isproduct={false}
              isWishlist={false}
            />
          </div>
        )}
      </div>
      {/* <TrendzThree
        bronzeBanner={
          homebannerJson &&
          homebannerJson?.length &&
          homebannerJson.filter((item) => item.type === 'bronze')
        }
      />
      <SocialShopping isAuth={isAuth} /> */}
    </>
  );
}
