import Profile from '../account/profile/index';
import { getLayout } from '../../layouts/AccountSettingsLayout/AccountSettingsLayout';

function Account() {
  return <Profile />;
}

Account.getLayout = getLayout;
export default Account;
