import { connect } from 'react-redux';
import {
  doAccountInfo,
  doAccountUpdate,
  doProfileImageUpload,
} from '../../../redux/actionCreaters/account.actioncreater';
import AccountSettingsProfile from './profile';

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.auth.isAuthenticated,
    userProfile: state.account.accountInfo,
    // addressesList: state.account.accountInfo?.included?.filter(
    //   (res) => res.type !== 'user_image'
    // ),
    // isHydrated: state._persist && state._persist.rehydrated ? true : false,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    doAccountInfo: (payload) => dispatch(doAccountInfo(payload)),
    doAccountUpdate: (payload) => dispatch(doAccountUpdate(payload)),
    // doProfileImageUpload: (payload) => dispatch(doProfileImageUpload(payload)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AccountSettingsProfile);
