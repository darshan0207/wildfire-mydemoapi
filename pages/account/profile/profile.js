import { ReactSVG } from 'react-svg';
import InputField from '../../../components/InputField/inputField';
import styles from './profile.module.scss';
import { Formik, Form } from 'formik';
import * as Yup from 'yup';
import { useEffect, useState } from 'react';
import { getLayout } from '../../../layouts/AccountSettingsLayout/AccountSettingsLayout';
import WithAuth from '../../../components/WithAuth';

function AccountSettingsProfile({
  isAuthenticated,
  userProfile = {},
  addressesList = [],
  doAccountInfo,
  doAccountUpdate,
  doProfileImageUpload,
}) {
  const [names, setNames] = useState(false);
  useEffect(() => {
    if (isAuthenticated) {
      doAccountInfo();
    }
  }, [doAccountInfo, isAuthenticated]);

  const imageUpload = (val) => {
    if (val.target.files[0]) {
      // doProfileImageUpload(val.target.files[0]);
    }
  };

  return (
    <>
      <div className="d-none d-sm-block">
        <div className={`pl-58 mt-4 ${styles.title_name}`}> Profile </div>
      </div>
      <div className="d-block d-sm-none">
        <div className="d-flex justify-content-between mt-4">
          <div></div>
          <div className={`${styles.title_name}`}>Profile</div>
          {!names ? (
            <ReactSVG
              onClick={() => setNames(true)}
              className="ps-4"
              src="\assets\icons\edit.svg"
            />
          ) : (
            <div></div>
          )}
        </div>
      </div>
      <div className="pl-58  mt-4">
        <div className="row g-0">
          <div
            className={`col-sm-12 col-md-12 col-lg-2 col-xl-2  ${styles.mobile_image}`}
          >
            <div className="avatar-upload">
              <div className="avatar-edit">
                <input
                  type="file"
                  id="imageUpload"
                  accept=".png, .jpg, .jpeg"
                  onChange={(e) => imageUpload(e)}
                />
                <label htmlFor="imageUpload"></label>
              </div>
              <div className="avatar-preview">
                <div
                  id="imagePreview"
                  style={{
                    backgroundImage: userProfile?.attributes?.user_image
                      ? `url(${process.env.API_URL}${userProfile?.attributes?.user_image})`
                      : `url(/assets/noimage.svg)`,
                  }}
                ></div>
              </div>
            </div>
          </div>
          <div
            className={`col-sm-12 col-md-12 col-lg-9 col-xl-9 pe-0 ${styles.mobile_text}`}
          >
            {/* {userProfile?.attributes?.first_name &&
              userProfile?.attributes?.last_name && ( */}
            <div>
              {!names && (
                <div className={`mt-3 ${styles.title_name} d-flex`}>
                  <ReactSVG
                    className="pe-4"
                    src="\assets\icons\profile_user.svg"
                  />
                  <div>{`${
                    userProfile?.firstName ? userProfile?.firstName : ''
                  } ${
                    userProfile?.lastName ? userProfile?.lastName : ''
                  }`}</div>
                  <div className="d-none d-sm-block">
                    <ReactSVG
                      onClick={() => setNames(true)}
                      className="pointer ps-4"
                      src="\assets\icons\edit.svg"
                    />
                  </div>
                </div>
              )}
              {names && (
                <Formik
                  initialValues={{
                    firstname: userProfile?.firstName,
                    lastname: userProfile?.lastName,
                  }}
                  validationSchema={Yup.object({
                    lastname: Yup.string().required('This field is required'),
                    firstname: Yup.string().required('This field is required'),
                  })}
                  onSubmit={(payload, actions) => {
                    doAccountUpdate({
                      firstName: payload.firstname,
                      lastName: payload.lastname,
                    });
                    actions.resetForm({
                      values: {
                        firstname: '',
                        lastname: '',
                      },
                    });
                    setNames(false);
                  }}
                >
                  <Form>
                    <div className="row">
                      <div className="col-sm-12 col-md-12 col-lg-4 col-xl-4">
                        <InputField
                          label="First Name"
                          name="firstname"
                          type="text"
                          placeholder="Enter first name"
                          icon="fa fa-user-o fa-lg"
                        />
                      </div>
                      <div className="col-sm-12 col-md-12 col-lg-4 col-xl-4">
                        <InputField
                          label="Last Name"
                          name="lastname"
                          type="text"
                          placeholder="Enter last name"
                          icon="fa fa-user-o fa-lg"
                        />
                      </div>
                      <div className=" col-sm-12 col-md-12 col-lg-2 col-xl-2">
                        <button className={styles.save_btn} type="submit">
                          save
                        </button>
                      </div>
                      <div className=" col-sm-12 col-md-12 col-lg-2 col-xl-2">
                        <button
                          onClick={() => setNames(false)}
                          className={styles.save_btn}
                          type="button"
                        >
                          cancel
                        </button>
                      </div>
                    </div>
                  </Form>
                </Formik>
              )}
            </div>
            {/* )} */}
            {addressesList?.length > 0 && (
              <>
                <div className={`mt-4 d-flex ${styles.profile_text} `}>
                  <ReactSVG className="pe-4" src="\assets\icons\address.svg" />
                  <div className="d-none d-sm-block">
                    {addressesList[0]?.attributes?.address1},&nbsp;
                    {addressesList[0]?.attributes?.state_code}&nbsp;
                    {addressesList[0]?.attributes?.zipcode}
                  </div>
                  <div className="d-block d-sm-none">
                    {addressesList[0]?.attributes?.address1},&nbsp;
                    {addressesList[0]?.attributes?.state_code}&nbsp;
                  </div>
                </div>
                <div className={`d-block d-sm-none ${styles.profile_text} `}>
                  {addressesList[0]?.attributes?.zipcode}{' '}
                </div>
              </>
            )}
            <div className={`mt-4 d-flex ${styles.profile_text}`}>
              <ReactSVG className="pe-4" src="\assets\icons\email.svg" />
              <div>{userProfile && userProfile?.email}</div>
            </div>
            {addressesList?.length > 0 && (
              <div className={`mt-4 d-flex ${styles.profile_text}`}>
                <ReactSVG
                  className="pe-4"
                  src="\assets\icons\phonenumber.svg"
                />
                <div>{addressesList[0]?.attributes?.phone}</div>
              </div>
            )}
          </div>
        </div>
      </div>
      <div className={`${styles.rectangle}`}></div>
      <div className={`pl-58 mt-4 ${styles.title_name}`}> Privacy </div>

      <Formik
        initialValues={{
          password: '',
          password_confirmation: '',
        }}
        validationSchema={Yup.object({
          password: Yup.string().required('This field is required'),
          password_confirmation: Yup.string()
            .required('This field is required')
            .oneOf([Yup.ref('password'), null], 'Passwords must match'),
        })}
        onSubmit={(payload, actions) => {
          doAccountUpdate({
            password: payload.password,
          });
          actions.resetForm({
            values: {
              password: '',
              password_confirmation: '',
            },
          });
        }}
      >
        {({ isValid, dirty }) => (
          <Form>
            <div className={`row pl-58 ${styles.form_input}`}>
              <div className="col-sm-12 col-md-12 col-lg-3 col-xl-3">
                <InputField
                  label="Password"
                  name="password"
                  type="password"
                  placeholder="* * * * * * * *"
                  icon="fa fa-lock fa-lg"
                />
              </div>
              <div className="col-sm-12 col-md-12 col-lg-3 col-xl-3">
                <InputField
                  label="Password Confirmation"
                  name="password_confirmation"
                  type="password"
                  placeholder="* * * * * * * *"
                  icon="fa fa-lock fa-lg"
                />
              </div>
              <div className=" col-sm-12 col-md-12 col-lg-4 col-xl-4">
                <button type="submit" disabled={!(dirty && isValid)}>
                  Change password
                </button>
              </div>
            </div>
          </Form>
        )}
      </Formik>

      <div className={`${styles.rectangle}`}></div>
      {/* <div className={`pl-58 mb-5 ${styles.close_account}`}>
        Close my account
      </div> */}
    </>
  );
}
const AuthProfile = WithAuth(AccountSettingsProfile, true);
AuthProfile.getLayout = getLayout;

export default AuthProfile;
