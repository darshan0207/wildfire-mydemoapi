import { ReactSVG } from 'react-svg';
import Button from '../../../components/Button/button';
import Item from '../../../components/Item/';
import WithAuth from '../../../components/WithAuth';
import { getLayout } from '../../../layouts/AccountSettingsLayout/AccountSettingsLayout';
import styles from './my-wishlist.module.scss';
import Link from 'next/link';
import { useEffect } from 'react';
import router from 'next/router';

function MyWishlist({
  isAuth,
  getWishListData,
  wishList,
  addProductTOWishlist,
  addCartDetail,
}) {
  useEffect(() => {
    getWishListData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  const handleClick = (slug) => {
    let pathname =
      process.env.MOBILE_BUILD == 'true' ? '/products' : '/products/[slug]';
    router.push({
      pathname,
      query: { slug: slug },
    });
  };

  const handleBuyNow = (variantID, price) => {
    addItem(true, variantID, price);
    if (!isAuth) router.push('/guest');
    else router.push('/checkout/address');
  };
  const addItem = (show = false, variantID, price) => {
    addCartDetail({
      productId: variantID,
      quantity: 1,
      price: price,
    });
    if (!show) {
      document.getElementById('mycartSidenav').style.right = '0';
      document.getElementById('show-backdropcart').style.display = 'block';
      document.body.style.overflow = 'hidden';
    }
  };

  return (
    <div className="mb-5">
      <div className="d-none d-sm-block">
        <div className={`pl-58 mt-4 ${styles.title_name}`}>My Wishlist</div>
      </div>
      <div className="d-block d-sm-none">
        <div className={styles.mobile_title_name}>
          <ReactSVG src="\assets\icons\hamburger_account.svg" />
          <div className={`${styles.title_name}`}>My Wishlist</div>
        </div>
      </div>
      <div className={`${styles.rectangle} pl-58`}></div>
      {wishList.length > 0 &&
        wishList?.map((item, i) => {
          return (
            <div
              key={i}
              className={`row justify-content-end align-items-center ${styles.wishlist_product}`}
            >
              <div
                className={`p-0 col-sm-12 col-md-12 col-lg-6 ${styles.whishproduct_detail}`}
              >
                <Item
                  title={item?.name}
                  description={item?.description}
                  display_Price={item?.price}
                  original_Price={item?.price}
                  compare_Price={item?.compareatprice}
                  price={item?.price}
                  slug={item?.slug}
                  width="150px"
                  height="112px"
                  data_Image_Id={item?._id}
                  include_Image={item?.images[0]?.path}
                  handleClick={handleClick}
                  product_wishlist={'product_wishlist'}
                />
              </div>
              <div className="col-sm-12 col-md-12 col-lg-6">
                <div className={`row ${styles.all_btn}`}>
                  <Button
                    type={'button'}
                    class={`form-control me-4 ${styles.buynow_btn}`}
                    name={'buy now'}
                    // disabled={!item?.availableon || !item?.stock}
                    handleCLick={() => handleBuyNow(item?._id, item?.price)}
                  />
                  <Button
                    type={'button'}
                    class={`form-control me-4 ${styles.addtocart_btn}`}
                    name={'add to cart'}
                    // disabled={!item?.availableon || !item?.stock}
                    handleCLick={() => addItem(false, item?._id, item?.price)}
                  />
                  <button type="button" className={styles.delete_btn}>
                    <ReactSVG
                      className="pointer"
                      src="\assets\icons\wishdelete.svg"
                      onClick={() =>
                        addProductTOWishlist({
                          productId: item._id,
                          isDelete: true,
                        })
                      }
                    />
                  </button>
                </div>
              </div>
            </div>
          );
        })}
      {wishList && wishList?.length === 0 && (
        <div className="p-3 text-center"> no products in wishlist</div>
      )}
      <div className={`text-end ${styles.continue}`}>
        <Link href="/search">Continue shopping</Link>
      </div>
    </div>
  );
}

const AuthMyWishlist = WithAuth(MyWishlist, true);
AuthMyWishlist.getLayout = getLayout;

export default AuthMyWishlist;
