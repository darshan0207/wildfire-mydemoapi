import MyWishlist from './my-wishlist';
import { connect } from 'react-redux';
import {
  addProductToWishlist,
  getWishListData,
} from '../../../redux/actionCreaters/order.actioncreater';
import { addCartDetail } from '../../../redux/actionCreaters/cart.actioncreater';

const mapStateToProps = (state) => {
  return {
    wishList: state.order.wishList,
    isAuth: state.auth.isAuthenticated,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getWishListData: () => dispatch(getWishListData()),
    addProductTOWishlist: (payload) => dispatch(addProductToWishlist(payload)),
    addCartDetail: (payload) => dispatch(addCartDetail(payload)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MyWishlist);
