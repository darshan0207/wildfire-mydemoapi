import { connect } from 'react-redux';
import {
  doAccountInfo,
  doAccountUpdate,
} from '../../../redux/actionCreaters/account.actioncreater';
import {
  doAddressesList,
  doRemoveAddress,
  doUpdateAddress,
} from '../../../redux/actionCreaters/address.actioncreater';
import {
  doCountrieList,
  doRegionList,
} from '../../../redux/actionCreaters/countrie.actioncreater';
import AccountSettingsAddress from './address-book';

const mapStateToProps = (state) => {
  return {
    addressesList: state.address.addressesList.data,
    userProfile: state.account.accountInfo,
    // isHydrated: state._persist && state._persist.rehydrated ? true : false,
    isAuthenticated: state.auth.isAuthenticated,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    doAccountInfo: (payload) => dispatch(doAccountInfo(payload)),
    doAccountUpdate: (payload) => dispatch(doAccountUpdate(payload)),
    doAddressesList: (payload) => dispatch(doAddressesList(payload)),
    doRemoveAddress: (payload) => dispatch(doRemoveAddress(payload)),
    doCountrieList: (payload) => dispatch(doCountrieList(payload)),
    doUpdateAddress: (payload) => dispatch(doUpdateAddress(payload)),
    doRegionList: () => dispatch(doRegionList()),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AccountSettingsAddress);
