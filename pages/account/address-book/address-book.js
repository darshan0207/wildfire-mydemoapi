import styles from './address-book.module.scss';
import { getLayout } from '../../../layouts/AccountSettingsLayout/AccountSettingsLayout';
import { useEffect, useState } from 'react';
import { ReactSVG } from 'react-svg';
import AddressModal from '../../../components/Model/Address/index';
import WithAuth from '../../../components/WithAuth';

function AccountSettingsAddress({
  addressesList = [],
  userProfile = {
    relationships: {
      default_billing_address: {
        data: {
          id: null,
        },
      },
      default_shipping_address: {
        data: {
          id: null,
        },
      },
    },
  },
  doAddressesList,
  doCountrieList,
  doAccountInfo,
  doAccountUpdate,
  doRemoveAddress,
  doUpdateAddress,
  isHydrated,
  isAuthenticated,
  doRegionList,
}) {
  const [displayNewModal, setDisplayNewModal] = useState(false);
  const [displayEditModal, setDisplayEditModal] = useState(false);
  const [addressData, setAddressData] = useState(null);
  useEffect(() => {
    if (isAuthenticated) {
      doAddressesList();
      doAccountInfo();
      doCountrieList();
      doRegionList();
    }
  }, [doAddressesList, doAccountInfo, doCountrieList, isAuthenticated]);

  const handleChangeshippingAddress = (address) => {
    doUpdateAddress({
      payload: { shipping: true },
      id: address._id,
    });
  };

  const handleChangeBillingAddress = (address) => {
    doUpdateAddress({
      payload: { billing: true },
      id: address._id,
    });
  };

  const handleEditAddress = (address) => {
    setDisplayEditModal(true);
    setAddressData(address);
  };

  const handleDeleteAddress = (address) => {
    doRemoveAddress({
      id: address._id,
    });
  };

  return (
    <>
      <div className="d-none d-sm-block">
        <div className={`pl-58 mt-4 ${styles.title_name}`}> Address book </div>
      </div>
      <div className="d-block d-sm-none">
        <div className={styles.mobile_title_name}>
          <ReactSVG src="\assets\icons\hamburger_account.svg" />
          <div className={`${styles.title_name}`}>Address book</div>
        </div>
      </div>
      <div className="w-100 mt-4">
        <div className={styles.address_box}>
          {addressesList.length > 0 ? (
            addressesList.map((address, index) => {
              return (
                <div className={`pl-58 ${styles.address_content}`} key={index}>
                  <div className={`mb-2 ${styles.address_type_text}`}>
                    Address {index + 1}
                  </div>
                  <div className={styles.address_text}>
                    <div className={styles.address_wrapper}>
                      <div className="d-flex justify-content-between">
                        <div className={styles.address_content_pedding}>
                          <div className="me-3">
                            <input
                              className="form-check-input me-2 pointer"
                              name="shippingAddress"
                              id={index}
                              type="radio"
                              defaultChecked={address?.shipping ? true : false}
                              onChange={() =>
                                handleChangeshippingAddress(address)
                              }
                            />
                            <span className={styles.address_type_text}>
                              Default Shipping Address
                            </span>
                          </div>

                          <div>
                            <input
                              className="form-check-input me-2 pointer"
                              name="Address"
                              id={index}
                              type="radio"
                              defaultChecked={address?.billing ? true : false}
                              onChange={() =>
                                handleChangeBillingAddress(address)
                              }
                            />
                            <span className={styles.address_type_text}>
                              Default Billing Address
                            </span>
                          </div>
                        </div>
                        <ReactSVG
                          className="pointer"
                          onClick={() => handleEditAddress(address)}
                          src="\assets\icons\edit.svg"
                        />
                      </div>

                      <div className={styles.address_content_pedding}>
                        <ReactSVG
                          className="pe-2"
                          src="\assets\icons\address.svg"
                        />
                        <div>
                          <span>
                            {address?.address1},{address?.zipcode}
                          </span>
                          <br />
                          <span> P.O : {address?.city}</span>
                          <br />
                          <span>PIN : {address?.phone} </span>
                          <br />
                        </div>
                      </div>
                      <div
                        className="d-flex flex-row-reverse pointer"
                        onClick={() => handleDeleteAddress(address)}
                      >
                        <ReactSVG src="\assets\icons\delete.svg" />
                      </div>
                    </div>
                  </div>
                </div>
              );
            })
          ) : (
            <div className={`pl-58 text-danger`}>ADDRESS NOT FOUND</div>
          )}
        </div>
        <div className={styles.btn_end}>
          <button
            className={styles.btnaddaddress}
            onClick={() => setDisplayNewModal(true)}
          >
            add address
          </button>
        </div>
        <div className={`${styles.rectangle}`}></div>
        {displayNewModal && (
          <AddressModal
            show={displayNewModal}
            onHide={() => setDisplayNewModal(false)}
            title={'add new address'}
            type="add"
          />
        )}
        {displayEditModal && (
          <AddressModal
            show={displayEditModal}
            onHide={() => setDisplayEditModal(false)}
            address={addressData}
            title={'edit address'}
            type="edit"
          />
        )}
      </div>
    </>
  );
}

const AuthAddress = WithAuth(AccountSettingsAddress, true);
AuthAddress.getLayout = getLayout;

export default AuthAddress;
