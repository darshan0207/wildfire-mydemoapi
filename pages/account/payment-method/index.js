import { connect } from 'react-redux';
import {
  getCardDetails,
  removeCard,
} from '../../../redux/actionCreaters/payment.actioncreater';
import PaymentMethods from './payment-method';

const mapStateToProps = (state) => {
  return {
    isHydrated: state._persist && state._persist.rehydrated ? true : false,
    cardDetail: state.payment.cardDetail,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getCardDetails: () => dispatch(getCardDetails()),
    removeCards: (payload) => dispatch(removeCard(payload)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PaymentMethods);
