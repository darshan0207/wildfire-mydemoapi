import { useEffect } from 'react';
import { ReactSVG } from 'react-svg';
import WithAuth from '../../../components/WithAuth';
import { getLayout } from '../../../layouts/AccountSettingsLayout/AccountSettingsLayout';
import styles from './payment-method.module.scss';

function PaymentMethods({
  cardDetail,
  removeCards,
  isHydrated,
  getCardDetails,
}) {
  useEffect(() => {
    // if (isHydrated) {
    //   getCardDetails();
    // }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isHydrated]);

  const HandleDelete = (id) => {
    // removeCards(id);
  };
  return (
    <>
      <div className="d-none d-sm-block">
        <div className={`pl-58 mt-4 ${styles.title_name}`}>Payment Methods</div>
      </div>
      <div className="d-block d-sm-none">
        <div className={styles.mobile_title_name}>
          <ReactSVG src="\assets\icons\hamburger_account.svg" />
          <div className={`${styles.title_name}`}>Payment Methods</div>
        </div>
      </div>
      <div className={`${styles.rectangle} pl-58`}></div>
      <div className="w-100 pl-58">
        <div className={`row`}>
          {cardDetail?.length > 0 ? (
            cardDetail.map((item, i) => {
              return (
                <div key={i} className={`${styles.methods_payment}`}>
                  <div className={`${styles.methods}`}>
                    <span>************{item?.attributes?.last_digits}</span>
                    <ReactSVG
                      src="/assets/icons/deleteCart.svg"
                      className="pointer"
                      onClick={(e) => HandleDelete(item.id)}
                    />
                  </div>
                </div>
              );
            })
          ) : (
            <div className={`pt-3 text-danger`}>PAYMENT METHODS NOT FOUND</div>
          )}
        </div>
      </div>
    </>
  );
}

const AuthPaymentMethod = WithAuth(PaymentMethods, true);
AuthPaymentMethod.getLayout = getLayout;

export default AuthPaymentMethod;
