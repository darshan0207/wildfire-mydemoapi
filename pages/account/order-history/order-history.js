import { getLayout } from '../../../layouts/AccountSettingsLayout/AccountSettingsLayout';
import { ReactSVG } from 'react-svg';
import WithAuth from '../../../components/WithAuth';
import styles from './order-history.module.scss';
import { useEffect, useState } from 'react';
import OrderTable from '../../../components/OrderTable/ordertable';
import Loader from '../../../components/Loader';

function AccountSettingsOrdersHistory({
  doCompletedOrdersList,
  doCompletedOrdersLoadMoreList,
  orderList,
  orderMeta,
  isloader = false,
  isHydrated,
}) {
  const [count, setCount] = useState(1);

  useEffect(() => {
    if (isHydrated) {
      doCompletedOrdersList({
        page: count,
        limit: 2,
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [doCompletedOrdersList, isHydrated]);

  const handleOrderLoadMore = (count) => {
    setCount(count);
    doCompletedOrdersLoadMoreList({
      page: count,
      limit: 2,
    });
  };

  return (
    <>
      {isloader ? (
        <Loader />
      ) : (
        <>
          <div className="d-none d-sm-block">
            <div className={`pl-58 mt-4 ${styles.title_name}`}>
              Order History
            </div>
          </div>
          <div className="d-block d-sm-none">
            <div className={styles.mobile_title_name}>
              <ReactSVG src="\assets\icons\hamburger_account.svg" />
              <div className={`${styles.title_name}`}>Order History</div>
            </div>
          </div>
          <OrderTable orderList={orderList} />
          <div className="row mb-4">
            <div className="col-md-12">
              {count !== orderMeta.totalPages && count < orderMeta.totalPages && (
                <div className={styles.btn_end}>
                  <button
                    className={styles.btnaddaddress}
                    onClick={() => handleOrderLoadMore(count + 1)}
                  >
                    Load More
                  </button>
                </div>
              )}
            </div>
          </div>
        </>
      )}
    </>
  );
}

const AuthOrdersHistory = WithAuth(AccountSettingsOrdersHistory, true);
AuthOrdersHistory.getLayout = getLayout;

export default AuthOrdersHistory;
