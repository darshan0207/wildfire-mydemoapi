import { connect } from 'react-redux';
import {
  doCompletedOrdersList,
  doCompletedOrdersLoadMoreList,
} from '../../../redux/actionCreaters/order.actioncreater';

import AccountSettingsOrdersHistory from './order-history';

const mapStateToProps = (state) => {
  return {
    orderList: state.order.orderList,
    orderMeta: state.order.orderMeta,
    isloader: state.order.isloader,
    isHydrated: state._persist && state._persist.rehydrated ? true : false,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    doCompletedOrdersList: (payload) =>
      dispatch(doCompletedOrdersList(payload)),
    doCompletedOrdersLoadMoreList: (payload) =>
      dispatch(doCompletedOrdersLoadMoreList(payload)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AccountSettingsOrdersHistory);
