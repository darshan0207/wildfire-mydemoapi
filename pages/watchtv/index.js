import { useEffect, useState } from 'react';
import Image from 'next/image';
import styles from './watchtv.module.scss';
import TrendingItem from '../../components/WatchtvView/TrendingItem';
import NewArrival from '../../components/WatchtvView/NewArrival';
import NewVideo from '../../components/WatchtvView/NewVideo';
import CategoryFilter from '../../components/WatchtvView/CategoryFilter';
import TrendingItemMobile from '../../components/WatchtvView/TrendingItemMobile';
import { connect } from 'react-redux';
import { getWatchTvVideo } from '../../redux/actionCreaters/vendor.actioncreater';
import { searchProduct } from '../../redux/actionCreaters/search.actioncreater';
import Loader from '../../components/Loader';
import { useRouter } from 'next/router';
import FilterImage from '../../public/assets/icons/filter.svg';
import HamburgerMenu from '../../public/assets/icons/hamburger_account.svg';
import SearchImage from '../../public/assets/icons/search.svg';

const mapStateToProps = (state) => {
  return {
    watchTvVideo: state?.vendor?.watchTv && state?.vendor?.watchTv?.data,
    watchTvVideometa: state?.vendor?.watchTv && state?.vendor?.watchTv?.meta,
    isLoader: state?.vendor?.isLoader,
    searchdata: state.search,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getWatchTvVideo: (payload) => dispatch(getWatchTvVideo(payload)),
    searchProduct: (payload) => dispatch(searchProduct(payload)),
  };
};

function WatchTv({
  queryname,
  pageno,
  getWatchTvVideo,
  watchTvVideo = [],
  watchTvVideometa = {},
  isLoader,
  searchdata,
  searchProduct,
}) {
  const [isFilter, setIsFilter] = useState(false);
  const [videoIndex, setVideoIndex] = useState(0);
  const router = useRouter();

  useEffect(() => {
    getWatchTvVideo({
      name: queryname,
      page: pageno,
    });
    router.push(
      {
        pathname: `/watchtv`,
        query: { name: queryname ? queryname : '', page: pageno },
      },
      undefined,
      {
        shallow: true,
      }
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [queryname]);

  const onFilter = (val) => {
    setIsFilter(val);
    document.body.style.overflow = '';
  };

  const onFilterMobile = (val) => {
    setIsFilter(val);
    document.body.style.overflow = 'hidden';
  };

  const handleClick = (index) => {
    let pagination =
      watchTvVideometa.total_pages < parseInt(pageno) + 1
        ? 1
        : parseInt(pageno) + 1;
    if (index === 0) handleSearch(queryname, pagination);
    setVideoIndex(index);
  };

  const handleSearch = (name, pagenumber) => {
    getWatchTvVideo({
      name: name,
      page: pagenumber,
    });
    router.push({
      pathname: '/watchtv',
      query: { name: name, page: pagenumber },
      shallow: true,
    });

    setVideoIndex(0);
  };

  return (
    <div className={styles.search_section}>
      <div className={styles.search_section_container}>
        <div className="row">
          <div className="col-lg-12">
            <div className="search_top">
              <div className="row align-items-center">
                {watchTvVideo && watchTvVideo.length >= 0 && (
                  <div className="col-lg-2 col-auto">
                    <div className={styles.search_filter}>
                      <div className="d-lg-block d-none">
                        <button
                          type="button"
                          className="btn btn-xs btn-secondary"
                          onClick={() => onFilter(!isFilter)}
                        >
                          <sapn className="me-2"> filter </sapn>
                          <Image
                            src={FilterImage}
                            alt=""
                            className="img-fluid"
                          />
                        </button>
                      </div>
                      <div className="d-lg-none d-block">
                        <a
                          className={styles.search_filter_toggle}
                          onClick={() => onFilterMobile(!isFilter)}
                        >
                          <Image
                            src={HamburgerMenu}
                            alt=""
                            className="img-fluid"
                          />
                        </a>
                      </div>
                    </div>
                  </div>
                )}

                <div className="col-lg-6 col ps-lg-2 ps-4 ms-lg-auto">
                  <div className={styles.search_box}>
                    <div className="input-group">
                      <span
                        className={`${styles.input_group_text} input-group-text bg-transparent`}
                      >
                        <Image src={SearchImage} alt="" className="img-fluid" />
                      </span>
                      <input
                        type="text"
                        className={`${styles.form_control} form-control border-start-0`}
                        placeholder="Search"
                        aria-label="Search"
                        value={router?.query?.name}
                        onChange={(e) => handleSearch(e.target.value, 1)}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="row">
          <div className="col-lg-12">
            <div className={styles.search_wrap}>
              {isLoader ? (
                <Loader />
              ) : watchTvVideo && watchTvVideo.length == 0 ? (
                <p className="text-center p-5">
                  <b>No video found</b>
                </p>
              ) : (
                <div className="row">
                  <div className="d-lg-none d-block">
                    <div
                      className={`${styles.backdrop_overlay} ${
                        isFilter ? styles.active : ''
                      }`}
                      onClick={() => onFilter(false)}
                    ></div>
                  </div>
                  <div
                    className={`col ${styles.col_195} ${
                      isFilter ? styles.active : ''
                    }`}
                  >
                    <div
                      className={`${styles.search_filter} d-lg-none d-block text-center`}
                    >
                      <button
                        type="button"
                        className="btn btn-xs btn-secondary"
                        onClick={() => onFilter(!isFilter)}
                      >
                        <sapn className="me-2"> filter </sapn>
                        <Image src={FilterImage} alt="" className="img-fluid" />
                        {/* <img
                          src="assets/icons/filter.svg"
                          className="img-fluid ms-2"
                        /> */}
                      </button>
                    </div>
                    <CategoryFilter
                      filteroption={searchdata?.meta}
                      videoid={watchTvVideo[videoIndex].id}
                      searchProduct={searchProduct}
                    />
                  </div>

                  <div className={`col ${styles.col_250}`}>
                    <div className={styles.trending_item_search}>
                      <div className="search_trending_title mb-3">
                        <h4 className="title-sm text-uppercase fw-bold text-dark text-center">
                          Trending items
                        </h4>
                      </div>
                      <TrendingItem
                        watchTvVideo={searchdata?.data}
                        videoIndex={videoIndex}
                        videoid={watchTvVideo[videoIndex].id}
                        searchProduct={searchProduct}
                      />
                    </div>
                  </div>

                  <div className={`col ${styles.col_full}`}>
                    <div className={styles.search_video}>
                      <NewVideo
                        watchTvVideo={watchTvVideo}
                        videoIndex={videoIndex}
                        handleClick={handleClick}
                      />
                    </div>

                    <div className={`${styles.search_trending} px-0`}>
                      <div className="search_trending_title mb-3">
                        <h4 className="title-sm text-uppercase fw-bold text-dark">
                          new arrival
                        </h4>
                      </div>
                      <NewArrival
                        watchTvVideo={watchTvVideo}
                        handleClick={(index) => setVideoIndex(index)}
                      />
                    </div>
                  </div>

                  <div className="col-12 d-lg-none">
                    <TrendingItemMobile
                      watchTvVideo={watchTvVideo}
                      productData={searchdata?.data}
                      // watchTvVideo={watchTvVideo}
                      videoIndex={videoIndex}
                      handleClick={(index) => setVideoIndex(index)}
                      videoid={watchTvVideo[videoIndex].id}
                      searchProduct={searchProduct}
                    />
                  </div>
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

WatchTv.getInitialProps = async ({ query }) => {
  const { name, page } = query;
  return { queryname: name, pageno: page ? page : 1 };
};

export default connect(mapStateToProps, mapDispatchToProps)(WatchTv);
