import { Formik, Form } from 'formik';
import Button from '../../../components/Button/button';
import Styles from './change.module.scss';
import InputField from '../../../components/InputField/inputField';
import * as Yup from 'yup';
import { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import WithLogin from '../../../components/WithLogin';
import Loader from '../../../components/Loader';

function ResetPassword(props) {
  const [showpassword, setShowpassword] = useState(false);
  const [showconfirm, setShowConfirm] = useState(false);
  const router = useRouter();

  const HandleIconClick = () => {
    setShowpassword((showpassword) => !showpassword);
  };
  const HandleConfirmPassword = () => {
    setShowConfirm((showconfirm) => !showconfirm);
  };

  useEffect(() => {
    if (router.query) {
      props?.doAccountInfo({ id: router.query.uid, router: router });
    }
  }, [router.query]);

  // if (!props?.queryname) {
  //   router.push('/');
  // }
  return (
    <>
      {props.isloader ? (
        <Loader />
      ) : (
        <div className={`container-fluid m-0 ${Styles.container_main}`}>
          <div className="row m-0">
            <div className="col col-sm-12 col-md-10 col-lg-10 col-xl-5">
              <div className={`${Styles.header_main}`}>FORGOT PASSWORD ?</div>
              <Formik
                initialValues={{
                  password: '',
                  confirmPassword: '',
                }}
                validationSchema={Yup.object({
                  password: Yup.string().required('This field is required'),
                  confirmPassword: Yup.string()
                    .required('This field is required')
                    .oneOf([Yup.ref('password'), null], 'Passwords must match'),
                })}
                onSubmit={(payload) => {
                  props?.passwordReset({
                    token: props?.queryname,
                    password: {
                      user: {
                        password: payload.password,
                        password_confirmation: payload.confirmPassword,
                        uid: router.query.uid,
                      },
                    },
                    router: router,
                  });
                }}
              >
                <Form>
                  <InputField
                    label="New Password"
                    name="password"
                    type={!showpassword ? 'password' : 'text'}
                    right_icon={!showpassword ? 'fa fa-eye-slash' : 'fa fa-eye'}
                    right_icon_class={Styles.input_icon}
                    HandleIconClick={HandleIconClick}
                  />
                  <div className="mt-4">
                    <InputField
                      label="Confirm Password"
                      name="confirmPassword"
                      type={!showconfirm ? 'password' : 'text'}
                      right_icon={
                        !showconfirm ? 'fa fa-eye-slash' : 'fa fa-eye'
                      }
                      right_icon_class={Styles.input_icon}
                      HandleIconClick={HandleConfirmPassword}
                    />
                  </div>
                  <div className="mt-4">
                    <Button
                      class={Styles.send_btn}
                      type="submit"
                      name="change password"
                    />
                  </div>
                </Form>
              </Formik>

              <div className={`${Styles.remeber_password}`}>
                <a className="pointer">
                  <WithLogin title="Back to login" />
                </a>
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
}

ResetPassword.getInitialProps = async ({ query }) => {
  const name = query?.reset_password_token;
  return { queryname: name };
};

export default ResetPassword;
