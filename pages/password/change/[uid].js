import ResetPassword from './change';
import { connect } from 'react-redux';
import { doAccountInfo, doResetPassword } from '../../../redux/actionCreaters/account.actioncreater';

const mapStateToProps = (state) => {
  return {
    isloader: state.account.isloader,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    passwordReset: (payload) => dispatch(doResetPassword(payload)),
    doAccountInfo: (payload) => dispatch(doAccountInfo(payload))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ResetPassword);
