import { Formik, Form } from 'formik';
import Button from '../../../components/Button/button';
import Styles from './recover.module.scss';
import InputField from '../../../components/InputField/inputField';
import * as Yup from 'yup';
import WithLogin from '../../../components/WithLogin';
import Loader from '../../../components/Loader';

function Recover(props) {
  return (
    <>
      {props.isloader ? (
        <Loader />
      ) : (
        <div className={`container-fluid m-0 ${Styles.container_main}`}>
          <div className="row m-0">
            <div className="col col-sm-12 col-md-10 col-lg-10 col-xl-5">
              <div className={`${Styles.header_main}`}>FORGOT PASSWORD ?</div>
              <Formik
                initialValues={{
                  email: '',
                }}
                validationSchema={Yup.object({
                  email: Yup.string()
                    .email('Invalid email address')
                    .required('This field is required'),
                })}
                onSubmit={(payload, { resetForm }) => {
                  props?.getPassworInfo({
                    user: {
                      email: payload.email,
                    },
                    reset: resetForm,
                  });
                }}
              >
                <Form>
                  <InputField
                    label="Email Address"
                    name="email"
                    type="text"
                    placeholder="Enter your email address"
                  />
                  <div className={`mt-3 mb-3 ${Styles.Sign_in}`}>
                    <div className={Styles.remeber_password}>
                      Do you remember your password ? &nbsp;
                    </div>
                    <div className={`pointer ${Styles.remeber_password}`}>
                      <a>
                        <WithLogin title="Try sign in" />
                      </a>
                    </div>
                  </div>
                  <Button class={Styles.send_btn} type="submit" name="send" />
                </Form>
              </Formik>
            </div>
          </div>
        </div>
      )}
    </>
  );
}

export default Recover;
