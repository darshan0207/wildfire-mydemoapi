import Recover from './recover';
import { connect } from 'react-redux';
import { doForgotPassword } from '../../../redux/actionCreaters/account.actioncreater';

const mapStateToProps = (state) => {
  return {
    isloader: state.account.isloader,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getPassworInfo: (payload) => dispatch(doForgotPassword(payload)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Recover);
