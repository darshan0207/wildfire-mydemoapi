import styles from './search.module.scss';
import Image from 'next/image';
// import filterIcon from '../../public/assets/images/Vector.png';
import SearchIcon from '../../public/assets/icons/search.svg';
// import FilterBtn from '../../components/Button/button';
import Item from '../../components/Item';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import Loader from '../../components/Loader';
import Infinitescroll from 'react-infinite-scroll-component';
import { connect } from 'react-redux';
import {
  searchProduct,
  searchProductScroll,
} from '../../redux/actionCreaters/search.actioncreater';
import ProductFilterMenu from '../../components/ProductFilterMenu';
import { getWishListData } from '../../redux/actionCreaters/order.actioncreater';

const mapStateToProps = (state) => {
  return {
    searchdata: state.search,
    isAuth: state.auth.isAuthenticated,
    wishlistItem: state?.order?.wishList,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    searchProduct: (payload) => dispatch(searchProduct(payload)),
    searchProductScroll: (payload) => dispatch(searchProductScroll(payload)),
    getWishListData: () => dispatch(getWishListData()),
  };
};

function Search(props) {
  const queryname = props?.queryname;
  const result = props.searchdata;
  const router = useRouter();
  const [hasMore, sethasMore] = useState(false);
  const [count, setCount] = useState(1);
  let filterOptionTypes = {};

  const wished_products =
    props?.wishlistItem?.data &&
    props?.wishlistItem?.data[0]?.relationships?.wished_items?.data;

  useEffect(() => {
    if (props?.isAuth) {
      props?.getWishListData();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleFilter = async (name, count = 1) => {
    let newfilterdata = {};
    if (name) {
      newfilterdata['searchkick'] = name;
      filterOptionTypes['searchkick'] = name;
    } else {
      delete newfilterdata['searchkick'];
      delete filterOptionTypes['searchkick'];
    }
    if (Object.keys(filterOptionTypes).length) {
      newfilterdata['properties'] = {};

      newfilterdata['options'] = {};
      Object.entries(filterOptionTypes).forEach(([key, value]) => {
        if (key !== 'searchkick') {
          if (key.includes('properties')) {
            if (filterOptionTypes[key]) {
              newfilterdata['properties'][key?.replace('properties ', '')] =
                value;
            } else {
              newfilterdata['properties'][key?.replace('properties ', '')];
            }
          } else if (key.includes('vendor_ids')) {
            newfilterdata['vendor_ids'] = value;
          } else if (key.includes('taxons')) {
            newfilterdata['taxons'] = value;
          } else {
            if (filterOptionTypes[key]) {
              newfilterdata['options'][key] = value;
            } else {
              delete newfilterdata['options'][key];
            }
            // if (key === 'length') {
            //   if (filterOptionTypes['length']) {
            //     newfilterdata['options'] = {
            //       ['length']: value,
            //     };
            //   } else {
            //     delete newfilterdata['options']['length'];
            //   }
            // } else {

            // }
          }
        }
      });
    }
    props.searchProduct(
      Object.assign(
        {
          include: 'images,primary_variant',
          sort: 'updated_at',
          per_page: 15,
          page: count,
        },
        { filter: newfilterdata }
      )
    );
    const params = await objectToQueryString(filterOptionTypes);
    router.push(params ? `/search?${params}` : '/search', undefined, {
      shallow: true,
    });
  };

  useEffect(() => {
    getQueryParms();
    handleFilter(queryname);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [queryname]);

  const getQueryParms = () => {
    if (Object.keys(router.query).length) {
      filterOptionTypes = router.query;
    }
  };

  useEffect(() => {
    if (result?.meta?.total_pages <= 1) {
      sethasMore(false);
    } else {
      sethasMore(true);
    }
  }, [result, result?.meta?.total_pages]);

  const getMorePost = (count) => {
    if (result?.meta?.total_pages < count) {
      sethasMore(false);
    } else {
      sethasMore(true);
      filterOptionTypes = router.query;
      let newfilterdata = {};
      if (queryname) {
        newfilterdata['searchkick'] = queryname;
        filterOptionTypes['searchkick'] = queryname;
      }
      if (Object.keys(filterOptionTypes).length) {
        newfilterdata['properties'] = {};
        newfilterdata['options'] = {};
        Object.entries(filterOptionTypes).forEach(([key, value]) => {
          if (key !== 'searchkick') {
            if (key.includes('properties')) {
              if (filterOptionTypes[key]) {
                newfilterdata['properties'][key?.replace('properties ', '')] =
                  value;
              } else {
                newfilterdata['properties'][key?.replace('properties ', '')];
              }
            } else if (key.includes('taxons')) {
              newfilterdata['taxons'] = value;
            } else if (key.includes('vendor_ids')) {
              newfilterdata['vendor_ids'] = value;
            } else {
              if (filterOptionTypes[key]) {
                newfilterdata['options'][key] = value;
              } else {
                delete newfilterdata['options'][key];
              }
              // if (key === 'length') {
              //   if (filterOptionTypes['length']) {
              //     newfilterdata['options'] = {
              //       ['length']: value,
              //     };
              //   } else {
              //     delete newfilterdata['options']['length'];
              //   }
              // } else {
              //   if (filterOptionTypes[key]) {
              //     newfilterdata['options'][key] = value;
              //   } else {
              //     delete newfilterdata['options'][key];
              //   }
              // }
            }
          }
        });
      }
      props.searchProductScroll(
        Object.assign(
          {
            include: 'images,primary_variant',
            sort: 'updated_at',
            per_page: 15,
            page: count,
          },
          { filter: newfilterdata }
        )
      );
    }
  };
  const handleClick = (slug) => {
    let pathname = '/products/[slug]';
    if (process.env.MOBILE_BUILD == 'true') {
      pathname = '/products';
    }
    router.push({
      pathname,
      query: { slug: slug },
    });
  };

  const objectToQueryString = (initialObj) => {
    const reducer =
      (obj, parentPrefix = null) =>
        (prev, key) => {
          const val = obj[key];
          key = key;
          const prefix = parentPrefix ? `${parentPrefix}[${key}]` : key;
          if (val == null || typeof val === 'function') {
            prev.push(`${prefix}=`);
            return prev;
          }
          if (['number', 'boolean', 'string'].includes(typeof val)) {
            prev.push(`${prefix}=${val}`);
            return prev;
          }
          prev.push(Object.keys(val).reduce(reducer(val, prefix), []).join('&'));
          return prev;
        };
    return Object.keys(initialObj).reduce(reducer(initialObj), []).join('&');
  };

  const handleFilterOpition = async (data) => {
    filterOptionTypes = data;
    const searchname = data.searchkick ? data.searchkick : '';
    handleFilter(searchname, 1);
  };

  const handleInputChange = async (data, parms) => {
    if (data === '') {
      router.push('/search', { shallow: true });
    }
    filterOptionTypes = parms;
    setCount(1);
    sethasMore(true);
    handleFilter(data, 1);
  };

  const handleOptionRemove = async (key, filter) => {
    let filterdata = filter;
    delete filterdata[key];
    filterOptionTypes = filterdata;
    const searchname = filter.searchkick ? filter.searchkick : '';
    handleFilter(searchname, 1);
  };

  const handleFilterOptionTypes = () => {
    let item = [];
    for (const [key, value] of Object.entries(router.query)) {
      if (key !== 'searchkick') {
        item.push(
          <div
            keys={value}
            className={`btn mt-2 d-flex justify-content-between ${styles.filter_btn}`}
          >
            {key === 'taxons' ? (
              <div className="text-uppercase">
                {props.searchdata.meta.filters?.taxons?.length &&
                  props.searchdata.meta.filters?.taxons.find(
                    (resp) => resp.id?.toString() === value
                  )?.name}
              </div>
            ) : key === 'vendor_ids' ? (
              <div className="text-uppercase">
                {props.searchdata.meta.filters?.vendors?.length &&
                  props.searchdata.meta.filters?.vendors.find(
                    (resp) => resp.id?.toString() === value
                  )?.name}
              </div>
            ) : (
              <div className="text-uppercase">{value}</div>
            )}

            <div
              className={styles.closebtn}
              onClick={() => handleOptionRemove(key, router.query)}
            >
              <i className="fa fa-times fs-5" aria-hidden="true"></i>
            </div>
          </div>
        );
      }
    }
    return item;
  };

  return (
    <>
      <div className={styles.search_header}>
        <ProductFilterMenu
          query={router}
          filteroption={props.searchdata.meta}
          filterdata={(data) => handleFilterOpition(data)}
        />
        <div>
          {/* {Object.keys(filterOptionTypes)}
          {Object.entries(filterOptionTypes).forEach(([key, value]) => {
            return <div keys={value}>123</div>;
          })} */}
        </div>
        {/* <div>
          <div className={styles.filter_search}> 
           <FilterBtn type="" class={styles.filter_btn} name={'Filter'} />
            <span className={styles.filter_icon}>
              <Image src={filterIcon} alt="" />
            </span> 
          </div> 
           <div className={styles.filter_mobile}>
            <button className={styles.filter_btn_mobile}>
              <div className="position-absolute">
                <Image src={filterIcon} alt="" />
              </div>
            </button>
          </div> 
        </div>*/}
        <div className={styles.search_field}>
          <div className={`${styles.search_icon_size}`}>
            <Image src={SearchIcon} alt="" />
          </div>
          <input
            className={`${styles.search_input}`}
            type="text"
            placeholder="Search"
            value={router?.query?.searchkick && router?.query?.searchkick}
            onChange={(e) => {
              handleInputChange(e.target.value, router?.query);
            }}
          />
        </div>
      </div>
      <div className="container">
        <div className={`row m-0 ${styles.grid_template}`}>
          {handleFilterOptionTypes()}
        </div>
      </div>
      {result.isloader === true ? (
        <Loader />
      ) : (
        <Infinitescroll
          dataLength={result?.data.length}
          next={(e) => {
            setCount(count + 1);
            getMorePost(count + 1);
          }}
          hasMore={hasMore}
          scrollableTarget="scrollableDiv"
          loader={<Loader className="w-100" />}
          endMessage={
            <p className="text-center p-5">
              <b>No more results</b>
            </p>
          }
        >
          <div className={styles.search_card}>
            <div className={styles.grid_item}>
              {result.data.map((item, index) => {
                return (
                  <Item
                    key={index}
                    title={item?.attributes?.name}
                    description={item?.attributes?.description}
                    display_Price={item?.attributes?.display_price}
                    original_Price={item?.attributes?.display_compare_at_price}
                    compare_Price={item?.attributes?.compare_at_price}
                    price={item?.attributes?.price}
                    data_Image_Id={item?.relationships?.images?.data[0]?.id}
                    item={item}
                    include={result?.included}
                    include_Image={item && item.imagestyle && item?.imagestyle}
                    slug={item?.attributes?.slug}
                    handleClick={handleClick}
                    isAuth={props?.isAuth}
                    wished_products={
                      wished_products?.length > 0 &&
                      wished_products.filter(
                        (ele) => ele.product_id === item?.id
                      )
                    }
                  />
                );
              })}
            </div>
          </div>
        </Infinitescroll>
      )}
    </>
  );
}

Search.getInitialProps = async ({ query }) => {
  const { searchkick } = query;
  return { queryname: searchkick };
};

export default connect(mapStateToProps, mapDispatchToProps)(Search);
