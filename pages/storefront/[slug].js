import { connect } from 'react-redux';
import { END } from 'redux-saga';
import { useRouter } from 'next/router';
import { toast } from 'react-toastify';
import MerchantLayout from '../../components/Merchant/merchantLayout';
import { wrapper } from '../../redux/store/store';
import {
  getVendor,
  getVendorVideo,
  getVendorProduct,
} from '../../redux/actionCreaters/vendor.actioncreater';

function Merchant({ vendor, getVendorVideo, getVendorProduct }) {
  console.log(vendor);
  const router = useRouter();
  if (!vendor.data) {
    toast.error('no vendor found');
    router.push('/');
  }
  return (
    <>
      <MerchantLayout
        vendor={vendor}
        getVendorVideo={getVendorVideo}
        getVendorProduct={getVendorProduct}
      />
    </>
  );
}

export async function getStaticPaths() {
  return {
    paths: [],
    fallback: process.env.MOBILE_BUILD === 'true' ? false : 'blocking',
  };
}

export const getStaticProps = wrapper.getStaticProps(
  (store) =>
    async ({ params }) => {
      store.dispatch(getVendor(params.slug));
      store.dispatch(END);
      await store.sagaTask.toPromise();
      return { revalidate: 10 };
    }
);

const mapStateToProps = (state) => {
  return {
    vendor: state.vendor.vendor,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    getVendorVideo: (payload) => dispatch(getVendorVideo(payload)),
    getVendorProduct: (payload) => dispatch(getVendorProduct(payload)),
    getVendor: (payload) => dispatch(getVendor(payload)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Merchant);
