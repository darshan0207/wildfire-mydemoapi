import { connect } from 'react-redux';
import { END } from 'redux-saga';
import { useRouter } from 'next/router';
import { toast } from 'react-toastify';
import MerchantLayout from '../../components/Merchant/merchantLayout';
import { wrapper } from '../../redux/store/store';
import {
  getVendor,
  getVendorVideo,
  getVendorProduct,
} from '../../redux/actionCreaters/vendor.actioncreater';
import { useEffect } from 'react';

function Merchant({ getVendor,vendor, getVendorVideo, getVendorProduct }) {
  const router = useRouter();
  useEffect(() => {
    // if (!vendor.data) {
      const { slug } = router.query;
      getVendor(slug);
    // }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [getVendor, router.query]);
  if (vendor.data)
    return (
      <>
        <MerchantLayout
          vendor={vendor}
          getVendorVideo={getVendorVideo}
          getVendorProduct={getVendorProduct}
        />
      </>
    );
  else return null;
}

const mapStateToProps = (state) => {
  return {
    vendor: state.vendor.vendor,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    getVendorVideo: (payload) => dispatch(getVendorVideo(payload)),
    getVendorProduct: (payload) => dispatch(getVendorProduct(payload)),
    getVendor: (payload) => dispatch(getVendor(payload)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Merchant);
