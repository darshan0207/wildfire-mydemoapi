import { useRouter } from 'next/router';
import { Formik, Form } from 'formik';
import Link from 'next/link';
import * as Yup from 'yup';
import InputField from '../../components/InputField/inputField';
import styles from './guest.module.scss';
import ButtonRegister from '../../components/Button/button';
import WithRegistration from '../../components/WithRegistration';

export default function Guest(props) {
  const router = useRouter();
  return (
    <div className="container">
      <div className={`row m-0  ${styles.guest_container}`}>
        <div className={`col-sm-12 col-md-5 col-lg-5 ${styles.guest_info}`}>
          <div className={styles.login_info}>
            <div className={`mb-4 ${styles.login_header}`}>
              continue as a guest
            </div>
            {/* <div className={`mt-3 mb-3 ${styles.description}`}>
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard dummy text.
            </div> */}

            <Formik
              initialValues={{
                email: '',
              }}
              validationSchema={Yup.object({
                email: Yup.string()
                  .email('Invalid email address')
                  .required('This field is required'),
              })}
              onSubmit={(payload) => {
                props.doGuestUser({
                  email: payload.email,
                  isguest: true,
                  route: router,
                });
              }}
            >
              <Form className={'text-start'}>
                <InputField
                  label="Email Address"
                  name="email"
                  type="text"
                  placeholder="email"
                />
                <div className="text-end mt-4">
                  <button type="submit" className={styles.continue_btn}>
                    continue as a guest
                  </button>
                </div>
              </Form>
            </Formik>
            <div className={`mt-4 ${styles.create_account}`}>
              <span className="d-inline-block"> or you can </span>{' '}
              <a className="d-inline-block">
                {' '}
                <WithRegistration title="create an account" />
              </a>
            </div>
          </div>
        </div>
        <div className={`col-sm-12 col-md-1 col-lg-1 p-0`}>
          <div className={`d-block d-sm-none  ${styles.line}`}>
            <span className={styles.word}>OR</span>
          </div>
          <div className={`d-none d-sm-block ${styles.wrapper}`}>
            <div className={styles.line}></div>
            <div className={styles.wordwrapper}>
              <div className={styles.word}>or</div>
            </div>
          </div>
        </div>
        <div className={`col-sm-12 col-md-6 col-lg-6 ${styles.login_detail}`}>
          <div className={`mb-4 ${styles.login_header}`}>REGISTERED USER</div>
          <Formik
            initialValues={{
              email: '',
              password: '',
            }}
            validationSchema={Yup.object({
              email: Yup.string()
                .email('Invalid email address')
                .required('This field is required'),
              password: Yup.string().required('This field is required'),
            })}
            onSubmit={(payload) => {
              props.doLoginUser({
                email: payload.email,
                password: payload.password,
                route: router,
              });
            }}
          >
            <Form>
              <InputField
                label="Email Address"
                name="email"
                type="text"
                placeholder="email"
              />
              <InputField
                label="Password"
                name="password"
                type="password"
                placeholder="password"
              />
              <div className={styles.btn_singin}>
                <ButtonRegister
                  type={'submit'}
                  class={`form-control btn btn-danger ${styles.login_btn}`}
                  name={'SIGN IN'}
                />
              </div>
            </Form>
          </Formik>

          <div className={styles.forgot_password}>
            Have you forgotten your password ?{' '}
            <span>
              <Link href={'/password/recover'}>
                <a>Click Here</a>
              </Link>
            </span>
          </div>
        </div>
      </div>
    </div>
  );
}
