import Guest from './guest';
import { connect } from 'react-redux';
import {
  doLoginUser,
  doGuestUser,
} from '../../redux/actionCreaters/auth.actioncreater';

const mapStateToProps = (state) => {
  console.log(state);
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    doLoginUser: (payload) => dispatch(doLoginUser(payload)),
    doGuestUser: (payload) => dispatch(doGuestUser(payload)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Guest);
