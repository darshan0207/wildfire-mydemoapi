import { errors } from '@spree/storefront-api-v2-sdk';

const checkError = (error) => {
  if (error instanceof errors.BasicSpreeError) {
    return error.summary;
  } else if (error instanceof errors.MisconfigurationError) {
    return 'Client is misconfigured';
  } else if (error instanceof errors.NoResponseError) {
    return 'Store unreachable';
  } else if (error instanceof errors.SpreeError) {
    return error.serverResponse.statusText;
  } else {
    return error.errors;
  }
};

export { checkError };
