import styles from './ordertable.module.scss';
import Link from 'next/link';
import Image from 'next/image';
import userimage from '../../public/assets/images/userimage.jpg';

export default function OrderTable({ orderList }) {
  return (
    <>
      <div
        className={`w-100 mt-4 mb-4 pl-58 custom_scrollbar ${styles.custom_table}`}
      >
        <div className="table-responsive-sm">
          <table className="w-100 align-middle">
            <thead>
              <tr>
                <th className={styles.minwidth_100}>Index Number</th>
                {/* <th className="text-center">
                  Products Ordered
                </th> */}
                <th className="text-center">Date of Order</th>
                <th className="text-center">Value</th>
                <th className={`text-center ${styles.minwidth_120}`}>
                  Shipment State
                </th>
                <th className={`text-end ${styles.minwidth_120}`}>Status</th>
              </tr>
            </thead>
            <tbody>
              {orderList && orderList?.length > 0 ? (
                orderList?.map((order, index) => {
                  return (
                    <tr key={index}>
                      <td>
                        <Link href={`/order-details/${order?.orderId}`}>
                          <a>{order?.orderId}</a>
                        </Link>
                      </td>
                      {/* <td className="text-wrap text-center">
                        <Image
                          src={userimage}
                          alt=""
                          width="100px"
                          height="100px"
                        />
                      </td> */}
                      <td className="text-center">
                        {new Date(order?.orderdate)?.toLocaleDateString(
                          'en-US'
                        )}
                      </td>
                      <td className={`text-center ${styles.font_weight_600}`}>
                        {order?.itemtotal}
                      </td>
                      <td className="text-center text-capitalize">
                        {order?.shipmentstate}
                      </td>
                      <td className="text-end text-capitalize">
                        {order?.state}
                      </td>
                    </tr>
                  );
                })
              ) : (
                <tr>
                  <td colSpan="5" className="text-center text-danger">
                    No Orders Found
                  </td>
                </tr>
              )}
            </tbody>
          </table>
        </div>
      </div>
    </>
  );
}
