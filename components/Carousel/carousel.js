import Carousel from 'react-multi-carousel';

export default function CarouselItem(props) {
  return (
    <>
      {props?.item?.length !== 0 && (
        <Carousel
          arrows={props?.arrows}
          ssr
          autoPlay={props?.autoPlay}
          autoPlaySpeed={props?.autoPlaySpeed}
          infinite={props?.infinite}
          containerClass={props?.containerClass}
          responsive={{
            desktop: {
              breakpoint: {
                max: 3000,
                min: 1024,
              },
              items: props?.desktop,
              partialVisibilityGutter: 10,
            },
            mobile: {
              breakpoint: {
                max: 464,
                min: 0,
              },
              items: props?.mobile,
              partialVisibilityGutter: 10,
            },
            tablet: {
              breakpoint: {
                max: 1024,
                min: 464,
              },
              items: props?.tablet,
              partialVisibilityGutter: 10,
            },
          }}
          showDots={props?.showDots}
          slidesToSlide={props?.slidesToSlide}
          swipeable={props?.swipeable}
          partialVisible
        >
          {props?.item}
        </Carousel>
      )}
    </>
  );
}
