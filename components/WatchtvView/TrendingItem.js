import styles from '../../pages/watchtv/watchtv.module.scss';
import Image from 'next/image';
import router from 'next/router';
import { useEffect } from 'react';

export default function TrendingItem({
  watchTvVideo,
  videoIndex,
  videoid,
  searchProduct,
}) {
  useEffect(() => {
    searchProduct({
      include: 'images,primary_variant',
      filter: {
        video_ids: videoid,
      },
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [videoid]);

  const HandleClick = (slug) => {
    router.push({
      pathname: '/products/[slug]',
      query: { slug: slug },
    });
  };
  return (
    <>
      {watchTvVideo && watchTvVideo.length > 0 ? (
        watchTvVideo?.map((item, index) => {
          return (
            <div
              key={index}
              className={`pointer ${styles.search_trending_item}`}
              onClick={() => HandleClick(item?.attributes?.slug)}
            >
              <div className={styles.search_trending_item_image}>
                <Image
                  src={`${process.env.API_URL}${item?.imagestyle}`}
                  alt=""
                  width="200px"
                  height="150px"
                  objectFit={'contain'}
                  className="img-fluid w-100"
                />

                {item?.attributes?.display_compare_at_price &&
                  parseFloat(item?.attributes?.compare_at_price) !== 0 && (
                    <span className={styles.search_trending_item_discount}>
                      {(
                        ((item?.attributes?.compare_at_price -
                          item?.attributes?.price) /
                          item?.attributes?.compare_at_price) *
                        100
                      ).toFixed(0)}
                      % OFF
                    </span>
                  )}

                {/* <a href="#" className={styles.search_trending_item_like}>
                  <img
                    src="assets/icons/heart.svg"
                    className="img-fluid"
                    alt=""
                  />
                </a> */}
              </div>
              {item?.attributes?.name && (
                <h6 className={styles.search_trending_item_title}>
                  {item?.attributes?.name?.slice(0, 20).concat('...')}
                </h6>
              )}

              {item?.attributes?.description && (
                <p
                  className={styles.search_trending_item_description}
                  dangerouslySetInnerHTML={{
                    __html: item?.attributes?.description
                      ?.slice(0, 28)
                      .concat('...'),
                  }}
                ></p>
              )}

              {item?.attributes?.display_price && (
                <p className={styles.search_trending_item_price}>
                  <span className={styles.search_trending_item_price_label}>
                    Price:
                  </span>
                  <span className={styles.search_trending_item_price_sale}>
                    {item?.attributes?.display_price}
                  </span>
                  <span className={styles.search_trending_item_price_old}>
                    {item?.attributes?.compare_at_price &&
                    item?.attributes?.compare_at_price > 0
                      ? item?.attributes?.display_compare_at_price
                      : ''}
                  </span>
                </p>
              )}
            </div>
          );
        })
      ) : (
        <p className="text-center p-4">
          <b>No item found</b>
        </p>
      )}
    </>
  );
}
