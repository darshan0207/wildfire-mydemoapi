import styles from '../../pages/watchtv/watchtv.module.scss';
import Image from 'next/image';
import VideoPlayer from '../VideoPlayer/VideoPlayer';

export default function NewArrival({ watchTvVideo, handleClick }) {
  return (
    <>
      <div className="row g-0">
        {watchTvVideo.length > 0 &&
          watchTvVideo?.map((item, index) => {
            return (
              <div
                key={index}
                className={`pointer ${styles.search_trending_item}`}
                onClick={() => handleClick(index)}
              >
                <div className={styles.search_trending_item_image}>
                  {/* <Image
                    src={`${process.env.API_URL}${item?.relationships?.upload_video?.data?.video_url?.original_url}`}
                    alt=""
                    width="200px"
                    height="150px"
                    objectFit={'contain'}
                    className="img-fluid w-100"
                  /> */}
                  <VideoPlayer
                    url={item?.video_url}
                    thumbnail={item?.thumbnail}
                    iscontrols={false}
                    height={'150px'}
                    width={'200px'}
                    isplaying={false}
                  />
                  {/* <a href="#" className={styles.search_trending_item_like}>
                    <img
                      src="assets/icons/heart.svg"
                      className="img-fluid"
                      alt=""
                    />
                  </a> */}
                </div>
                {item?.attributes?.name && (
                  <h6 className={styles.search_trending_item_title}>
                    {item?.attributes?.name?.slice(0, 20).concat('...')}
                  </h6>
                )}

                {item?.attributes?.description && (
                  <div
                    className={styles.search_trending_item_description}
                    dangerouslySetInnerHTML={{
                      __html: item?.attributes?.description
                        ?.slice(0, 28)
                        .concat('...'),
                    }}
                  ></div>
                )}
              </div>
            );
          })}
      </div>
    </>
  );
}
