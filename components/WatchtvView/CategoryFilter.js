import { useEffect, useState } from 'react';
import styles from '../../pages/watchtv/watchtv.module.scss';

export default function CategoryFilter({
  filteroption,
  videoid,
  searchProduct,
}) {
  const [filterOptionobj, setFilterOptionobj] = useState({});
  const [filterManufacturerobj, setFilterManufacturerobj] = useState({});
  // const [filterPrototypesobj, setFilterPrototypesobj] = useState({});
  const [filterTaxonsobj, setFilterTaxonsobj] = useState({});
  const [filterVendorsobj, setFilterVendorsobj] = useState({});
  let filterOptionTypes = {};

  const handleOptionTypes = (name, val) => {
    if (filterOptionobj[name] === val) {
      delete filterOptionobj[name];
    } else {
      filterOptionobj[name] = val;
    }
    setFilterOptionobj((filterOptionobj) => ({
      ...filterOptionobj,
      ...filterOptionTypes,
    }));

    searchProduct({
      include: 'images,primary_variant',
      filter: {
        video_ids: videoid,
        options: { ...filterOptionobj, ...filterOptionTypes },
        properties: filterManufacturerobj,
        // prototypes: filterPrototypesobj?.prototypes,
        taxons: filterTaxonsobj && filterTaxonsobj.taxons,
        vendor_ids: filterVendorsobj?.vendor_ids,
      },
    });
  };

  const handlePropertiesTypes = (name, val) => {
    if (filterManufacturerobj[name] === val) {
      delete filterManufacturerobj[name];
    } else {
      filterManufacturerobj[name] = val;
    }

    setFilterManufacturerobj((filterManufacturerobj) => ({
      ...filterManufacturerobj,
      ...filterOptionTypes,
    }));

    searchProduct({
      include: 'images,primary_variant',
      filter: {
        video_ids: videoid,
        options: filterOptionobj,
        properties: { ...filterManufacturerobj, ...filterOptionTypes },
        // prototypes: filterPrototypesobj?.prototypes,
        taxons: filterTaxonsobj && filterTaxonsobj.taxons,
        vendor_ids: filterVendorsobj?.vendor_ids,
      },
    });
  };

  // const handlePrototypesTypes = (name, val) => {
  //   if (filterPrototypesobj[name] === val) {
  //     delete filterPrototypesobj[name];
  //   } else {
  //     filterPrototypesobj[name] = val;
  //   }

  //   setFilterPrototypesobj((filterPrototypesobj) => ({
  //     ...filterPrototypesobj,
  //     ...filterOptionTypes,
  //   }));

  //   let prototypesdata = { ...filterPrototypesobj, ...filterOptionTypes };
  //   searchProduct({
  //     include: 'images,primary_variant',
  //     filter: {
  //       video_ids: videoid,
  //       options: filterOptionobj,
  //       properties: filterManufacturerobj,
  //       prototypes: prototypesdata?.prototypes,
  //       taxons: filterTaxonsobj && filterTaxonsobj.taxons,
  //       vendors: filterVendorsobj?.vendors,
  //     },
  //   });
  // };

  const handleTaxonsTypes = (name, val) => {
    if (filterTaxonsobj[name] === val) {
      delete filterTaxonsobj[name];
    } else {
      filterTaxonsobj[name] = val;
    }

    setFilterTaxonsobj((filterTaxonsobj) => ({
      ...filterTaxonsobj,
      ...filterOptionTypes,
    }));

    let taxondata = { ...filterTaxonsobj, ...filterOptionTypes };
    searchProduct({
      include: 'images,primary_variant',
      filter: {
        video_ids: videoid,
        options: filterOptionobj,
        properties: filterManufacturerobj,
        // prototypes: filterPrototypesobj?.prototypes,
        taxons: taxondata?.taxons,
        vendor_ids: filterVendorsobj?.vendor_ids,
      },
    });
  };

  const handleVendorsTypes = (name, val) => {
    if (filterVendorsobj[name] === val) {
      delete filterVendorsobj[name];
    } else {
      filterVendorsobj[name] = val;
    }

    setFilterVendorsobj((filterVendorsobj) => ({
      ...filterVendorsobj,
      ...filterOptionTypes,
    }));

    let vendorsdata = { ...filterVendorsobj, ...filterOptionTypes };
    console.log(vendorsdata);
    searchProduct({
      include: 'images,primary_variant',
      filter: {
        video_ids: videoid,
        options: filterOptionobj,
        properties: filterManufacturerobj,
        // prototypes: filterPrototypesobj?.prototypes,
        taxons: filterTaxonsobj && filterTaxonsobj.taxons,
        vendor_ids: vendorsdata?.vendor_ids,
      },
    });
  };

  const handleClearAll = () => {
    setFilterOptionobj({});
    setFilterManufacturerobj({});
    // setFilterPrototypesobj({});
    setFilterTaxonsobj({});
    setFilterVendorsobj({});
    searchProduct({
      include: 'images,primary_variant',
      filter: {
        video_ids: videoid,
      },
    });
  };

  useEffect(() => {
    setFilterOptionobj({});
    setFilterManufacturerobj({});
    // setFilterPrototypesobj({});
    setFilterTaxonsobj({});
    setFilterVendorsobj({});
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [videoid]);

  return (
    <>
      <div className={styles.search_filter_list}>
        <div className={styles.search_filter_title}></div>
        <span
          className={styles.search_filter_clear}
          onClick={() => handleClearAll()}
        >
          Clear ALL
        </span>
        <div className="search_filter_list_item">
          {filteroption?.products?.length > 0 &&
            filteroption?.products?.map((item, index) => {
              return (
                <div className="form-check" key={index}>
                  <input
                    id={item.name}
                    className="form-check-input"
                    name={item?.name}
                    type="checkbox"
                    value={item.id}
                    checked={item.id === filterOption['product_ids']}
                    onChange={() => handleOptionTypes('product_ids', item.id)}
                  />
                  <label
                    htmlFor={item.name}
                    className={styles.form_check_label}
                  >
                    {item.name}
                  </label>
                </div>
              );
            })}
        </div>
      </div>
      {filteroption?.filters?.option_types?.length > 0 &&
        filteroption?.filters?.option_types.map((item, index) => (
          <div className={styles.search_filter_list} key={index}>
            <div className={styles.search_filter_title}>{item?.name}</div>
            {item?.name?.toLowerCase() !== 'color' ? (
              <div className="search_filter_list_item">
                {item?.option_values?.length > 0 &&
                  item?.option_values.map((optiontypes, index) => {
                    return (
                      <div className="form-check" key={index}>
                        <input
                          className="form-check-input"
                          type="checkbox"
                          id={optiontypes.name}
                          name={item?.name}
                          value={optiontypes.id}
                          checked={
                            optiontypes?.name === filterOptionobj[item?.name]
                          }
                          onChange={() =>
                            handleOptionTypes(item.name, optiontypes.name)
                          }
                        />
                        <label
                          className={styles.form_check_label}
                          htmlFor={optiontypes.name}
                        >
                          {optiontypes.name}
                        </label>
                      </div>
                    );
                  })}
              </div>
            ) : (
              <div className="search_filter_list_item">
                <div className="col-md-12">
                  <div className={`${styles.grid_template}`}>
                    {item?.option_values?.length > 0 &&
                      item?.option_values.map((optiontypes, index) => {
                        return (
                          <button
                            key={index}
                            type="checkbox"
                            name="group0"
                            className={`mb-2 ${styles.product_size_btn} ${
                              optiontypes?.name === filterOptionobj[item?.name]
                                ? styles.active
                                : ''
                            }`}
                            style={{ background: optiontypes.presentation }}
                            value={optiontypes.id}
                            onClick={() =>
                              handleOptionTypes(item.name, optiontypes.name)
                            }
                          ></button>
                        );
                      })}
                  </div>
                </div>
              </div>
            )}
          </div>
        ))}

      {filteroption?.filters?.product_properties?.length > 0 &&
        filteroption?.filters?.product_properties.map((item, index) => (
          <div className={styles.search_filter_list} key={index}>
            <div className={styles.search_filter_title}>{item?.name}</div>
            <div className="search_filter_list_item">
              {item?.values?.length > 0 &&
                item?.values.map((optiontypes, index) => {
                  return (
                    <div className="form-check" key={index}>
                      <input
                        className="form-check-input"
                        type="checkbox"
                        id={optiontypes.value}
                        name={item?.name}
                        value={optiontypes.filter_param}
                        checked={
                          optiontypes?.value ===
                          filterManufacturerobj[item?.name]
                        }
                        onChange={() =>
                          handlePropertiesTypes(item.name, optiontypes.value)
                        }
                      />
                      <label
                        className={styles.form_check_label}
                        htmlFor={optiontypes.value}
                      >
                        {optiontypes.value}
                      </label>
                    </div>
                  );
                })}
            </div>
          </div>
        ))}

      {/* {filteroption?.filters?.prototypes?.length > 0 && (
        <div className={styles.search_filter_list}>
          <div className={styles.search_filter_title}>Proto Types</div>
          <div className="search_filter_list_item">
            {filteroption?.filters?.prototypes.map((optiontypes, index) => {
              return (
                <div className="form-check" key={index}>
                  <input
                    className="form-check-input"
                    type="checkbox"
                    id={optiontypes.name}
                    name="prototypes"
                    value={optiontypes.id}
                    checked={
                      optiontypes?.id === filterPrototypesobj['prototypes']
                    }
                    onChange={() =>
                      handlePrototypesTypes('prototypes', optiontypes.id)
                    }
                  />
                  <label
                    className={styles.form_check_label}
                    htmlFor={optiontypes.name}
                  >
                    {optiontypes.name}
                  </label>
                </div>
              );
            })}
          </div>
        </div>
      )} */}
      {filteroption?.filters?.taxons?.length > 0 && (
        <div className={styles.search_filter_list}>
          <div className={styles.search_filter_title}>Taxons</div>
          <div className="search_filter_list_item">
            {filteroption?.filters?.taxons.map((optiontypes, index) => {
              return (
                <div className="form-check" key={index}>
                  <input
                    className="form-check-input"
                    type="checkbox"
                    id={optiontypes.name}
                    name="taxons"
                    value={optiontypes.id}
                    checked={optiontypes?.name === filterTaxonsobj['taxons']}
                    onChange={() =>
                      handleTaxonsTypes('taxons', optiontypes.name)
                    }
                  />
                  <label
                    className={styles.form_check_label}
                    htmlFor={optiontypes.name}
                  >
                    {optiontypes.name}
                  </label>
                </div>
              );
            })}
          </div>
        </div>
      )}
      {filteroption?.filters?.vendors?.length > 0 && (
        <div className={styles.search_filter_list}>
          <div className={styles.search_filter_title}>Vendors</div>
          <div className="search_filter_list_item">
            {filteroption?.filters?.vendors.map((optiontypes, index) => {
              return (
                <div className="form-check" key={index}>
                  <input
                    className="form-check-input"
                    type="checkbox"
                    id={optiontypes.id}
                    name="vendor_ids"
                    value={optiontypes.id}
                    checked={optiontypes?.id === filterVendorsobj['vendor_ids']}
                    onChange={() =>
                      handleVendorsTypes('vendor_ids', optiontypes.id)
                    }
                  />
                  <label
                    className={styles.form_check_label}
                    htmlFor={optiontypes.name}
                  >
                    {optiontypes.name}
                  </label>
                </div>
              );
            })}
          </div>
        </div>
      )}
    </>
  );
}
