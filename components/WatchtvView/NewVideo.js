import VideoPlayer from '../VideoPlayer/VideoPlayer';
import ReactPlayer from 'react-player';

export default function NewVideo({ handleClick, videoIndex, watchTvVideo }) {
  const continuewVideo = (index) => {
    handleClick(index === watchTvVideo?.length ? 0 : index);
  };
  return (
    <>
      <div className="ratio ratio-16x9">
        <VideoPlayer
          url={
            watchTvVideo &&
            watchTvVideo?.length &&
            watchTvVideo[videoIndex]?.video_url
          }
          // thumbnail={
          //   watchTvVideo &&
          //   watchTvVideo?.length &&
          //   watchTvVideo[videoIndex]?.thumbnail
          // }
          index={videoIndex}
          isplaying={true}
          isloop={watchTvVideo?.length === 1 ? true : false}
          continuewVideo={continuewVideo}
          ismuted={true}
          islight={true}
        />
      </div>
    </>
  );
}
