import styles from '../../pages/watchtv/watchtv.module.scss';
import { Tabs, Tab } from 'react-bootstrap';
import Image from 'next/image';
import VideoPlayer from '../VideoPlayer/VideoPlayer';
import Link from 'next/link';
import { useEffect } from 'react';

export default function TrendingItemMobile({
  watchTvVideo,
  videoIndex,
  handleClick,
  videoid,
  searchProduct,
  productData,
}) {
  // useEffect(() => {
  //   searchProduct({
  //     include: 'images,primary_variant',
  //     filter: {
  //       video_ids: videoid,
  //     },
  //   });
  //   // eslint-disable-next-line react-hooks/exhaustive-deps
  // }, [videoid]);

  return (
    <>
      <div className={styles.search_trending_mobile}>
        <div className="search_trending_mobile">
          <Tabs defaultActiveKey="0">
            <Tab eventKey={0} title="Trending items">
              <div className="row mt-4">
                {productData && productData?.length > 0 ? (
                  productData?.map((item, index) => {
                    return (
                      <Link
                        key={index}
                        passHref
                        href={`/products${
                          process.env.MOBILE_BUILD == 'true' ? '?slug=' : '/'
                        }${item?.attributes?.slug}`}
                      >
                        <div className={styles.search_trending_item}>
                          <div className={styles.search_trending_item_image}>
                            <Image
                              src={`${process.env.API_URL}${item?.imagestyle}`}
                              alt=""
                              width="200px"
                              height="150px"
                              objectFit={'contain'}
                              className={`img-fluid w-100 ${styles.product_image}`}
                            />

                            {item?.attributes?.display_compare_at_price &&
                              parseFloat(item?.attributes?.compare_at_price) !==
                                0 && (
                                <span
                                  className={
                                    styles.search_trending_item_discount
                                  }
                                >
                                  {(
                                    ((item?.attributes?.compare_at_price -
                                      item?.attributes?.price) /
                                      item?.attributes?.compare_at_price) *
                                    100
                                  ).toFixed(0)}
                                  % OFF
                                </span>
                              )}

                            {/* <a
                            href="#"
                            className={styles.search_trending_item_like}
                          >
                            <img
                              src="assets/icons/heart.svg"
                              className="img-fluid"
                              alt=""
                            />
                          </a> */}
                          </div>
                          {item?.attributes?.name && (
                            <h6 className={styles.search_trending_item_title}>
                              {item?.attributes?.name
                                ?.slice(0, 20)
                                .concat('...')}
                            </h6>
                          )}

                          {item?.attributes?.description && (
                            <p
                              className={
                                styles.search_trending_item_description
                              }
                              dangerouslySetInnerHTML={{
                                __html: item?.attributes?.description
                                  ?.slice(0, 28)
                                  .concat('...'),
                              }}
                            ></p>
                          )}

                          {item?.attributes?.display_price && (
                            <p className={styles.search_trending_item_price}>
                              <span
                                className={
                                  styles.search_trending_item_price_label
                                }
                              >
                                Price:
                              </span>
                              <span
                                className={
                                  styles.search_trending_item_price_sale
                                }
                              >
                                {item?.attributes?.display_price}
                              </span>
                              <span
                                className={
                                  styles.search_trending_item_price_old
                                }
                              >
                                {item?.attributes?.compare_at_price &&
                                item?.attributes?.compare_at_price > 0
                                  ? item?.attributes?.display_compare_at_price
                                  : ''}
                              </span>
                            </p>
                          )}
                        </div>
                      </Link>
                    );
                  })
                ) : (
                  <p className="text-center p-4">
                    <b>No item found</b>
                  </p>
                )}
              </div>
            </Tab>
            <Tab eventKey={1} title="New Arrival">
              <div className="row mt-4">
                {watchTvVideo.length > 0 &&
                  watchTvVideo?.map((item, index) => {
                    return (
                      <div
                        key={index}
                        className={`pointer ${styles.search_trending_item}`}
                        onClick={() => handleClick(index)}
                      >
                        <div className={styles.search_trending_item_image}>
                          {/* <Image
                    src={`${process.env.API_URL}${item?.relationships?.upload_video?.data?.video_url?.original_url}`}
                    alt=""
                    width="200px"
                    height="150px"
                    objectFit={'contain'}
                    className="img-fluid w-100"
                  /> */}
                          <VideoPlayer
                            url={item?.video_url}
                            iscontrols={false}
                            height={'72px'}
                            width={'96px'}
                          />
                          {/* <a
                            href="#"
                            className={styles.search_trending_item_like}
                          >
                            <img
                              src="assets/icons/heart.svg"
                              className="img-fluid"
                              alt=""
                            />
                          </a> */}
                        </div>
                        {item?.attributes?.name && (
                          <h6 className={styles.search_trending_item_title}>
                            {item?.attributes?.name?.slice(0, 20).concat('...')}
                          </h6>
                        )}

                        {item?.attributes?.description && (
                          <p
                            className={styles.search_trending_item_description}
                            dangerouslySetInnerHTML={{
                              __html: item?.attributes?.description
                                ?.slice(0, 28)
                                .concat('...'),
                            }}
                          ></p>
                        )}
                      </div>
                    );
                  })}
              </div>
            </Tab>
          </Tabs>
        </div>

        {/* <div className="tab-content" id="nav-tabContent">
          <div
            className="tab-pane fade show active"
            id="nav-trendingitem"
            role="tabpanel"
            aria-labelledby="nav-trendingitem-tab"
          >
            <div className="col-sm-6 col-12">
                  <div className={styles.search_trending_item}>
                    <div className={styles.search_trending_item_image}>
                      <img
                        src="assets/images/dummy.jpg"
                        width="200"
                        height="150"
                        className="img-fluid w-100"
                        alt=""
                      />

                      <span className={styles.search_trending_item_discount}>
                        37% off
                      </span>

                      <a href="#" className={styles.search_trending_item_like}>
                        <img
                          src="assets/icons/heart.svg"
                          className="img-fluid"
                          alt=""
                        />
                      </a>
                    </div>
                    <h6 className={styles.search_trending_item_title}>
                      Lorem IPSUm
                    </h6>

                    <p className={styles.search_trending_item_description}>
                      Lorem Ipsum is simply dummy
                    </p>

                    <p className={styles.search_trending_item_price}>
                      <span className={styles.search_trending_item_price_label}>
                        Price:
                      </span>
                      <span className={styles.search_trending_item_price_sale}>
                        $120
                      </span>
                      <span className={styles.search_trending_item_price_old}>
                        $149
                      </span>
                    </p>
                  </div>
                </div>
          </div>

          <div
            className="tab-pane fade"
            id="nav-newarrival"
            role="tabpanel"
            aria-labelledby="nav-newarrival-tab"
          >
            
          </div>
        </div> */}
      </div>
    </>
  );
}
