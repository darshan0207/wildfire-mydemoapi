import styles from './productrating.module.scss';

export default function ProductRating({ productReview }) {
  return (
    <>
      <div className={`mt-3 mb-2 ${styles.product_reviw_rating}`}>
        <div>
          {[...Array(5)].map((e, i) => (
            <span
              key={i}
              className={`me-1 fa ${i >= Math.round(productReview)
                ? 'fa-star-o'
                : 'fa-star'
                }  ${styles.star_icon}`}
            />
          ))}
          <span className={`pe-2 ${styles.product_rating_star}`}>
            {Math.round(productReview)}/5
          </span>
        </div>
        <div>
          <span className={`pe-2 ${styles.product_rating} `}>
            {productReview} Reviews
          </span>
          {/* <span className={`ps-2 ${styles.product_review}`}>
                  15 Reviews
                </span> */}
        </div>
      </div>
    </>
  );
}
