export default function Button(props) {
  const handleClick = () => {
    if (props && props.type === 'button') {
      props?.handleCLick();
    }
  };
  return (
    <button
      className={props.class}
      type={props.type}
      onClick={handleClick}
      disabled={props.disabled}
    >
      {props?.name}
    </button>
  );
}
