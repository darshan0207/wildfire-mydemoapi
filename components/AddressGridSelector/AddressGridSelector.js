import styles from './AddressGridSelector.module.scss';
import AddressModal from '../../components/Model/Address/index';
import { useState } from 'react';

export default function AddressGridSelector({
  id,
  userAddresses,
  setShippingAddress,
  setBillingAddress,
}) {
  const [displayNewModal, setDisplayNewModal] = useState(false);
  const AddNewAddress = () => {
    return (
      <div className="col-md-6 mt-4" onClick={() => setDisplayNewModal(true)}>
        <div className={styles.new_Address}>
          <div className={styles.new_address_padding}>
            <div className={styles.new_address_content}>
              <p>
                <svg height="24" viewBox="0 0 32 32" width="24">
                  <path
                    d="M18 14v-12c0-1.104-0.896-2-2-2s-2 0.896-2 2v12h-12c-1.104 0-2 0.896-2 2s0.896 2 2 2h12v12c0 1.104 0.896 2 2 2s2-0.896 2-2v-12h12c1.104 0 2-0.896 2-2s-0.896-2-2-2h-12z"
                    fill="#d73939"
                  ></path>
                </svg>
              </p>
              <p>Add new address</p>
            </div>
          </div>
        </div>
      </div>
    );
  };
  return (
    <>
      <div className="row">
        <AddNewAddress />
        {userAddresses.length > 0 &&
          userAddresses.map((item, i) => (
            <div className="col-md-6 mt-4" key={`${id}${i}`}>
              <input
                type="radio"
                value="1"
                name={id}
                id={`${id}${i}`}
                className="d-none"
                onChange={() =>
                  id === 'shhiping' || id === 'shhipingmobile'
                    ? setShippingAddress(item)
                    : setBillingAddress(item)
                }
              />
              <label htmlFor={`${id}${i}`} className="w-100">
                <div className={styles.view_address}>
                  <div>
                    <span>
                      {item.firstname} {item.lastname}
                    </span>
                    {item.address1},{item.country}
                    <br />
                    P.O : {item.city}
                    <br />
                    PIN : {item.zipcode}
                    <br />
                    PHONE : {item.phone}
                  </div>
                </div>
              </label>
            </div>
          ))}
      </div>
      {displayNewModal && (
        <AddressModal
          show={displayNewModal}
          onHide={() => setDisplayNewModal(false)}
          title={'add new address'}
          type="add"
        />
      )}
    </>
  );
}
