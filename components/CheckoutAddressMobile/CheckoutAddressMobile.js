import styles from './CheckoutAddressMobile.module.scss';
import AddressForm from '../AddressForm/addressform';
import { Tabs, Tab } from 'react-bootstrap';
import { useEffect, useState } from 'react';

export default function CheckoutAddressMobile({
  billingAsShippingAddress,
  setBillingAsShippingAddress,
  countrielist,
  statelist,
  country,
}) {
  return (
    <Tabs
      defaultActiveKey="home"
      transition={false}
      id="noanim-tab-example"
      className="mb-3"
    >
      <Tab eventKey="home" title="SHIPPING ADDRESS">
        <AddressForm
          countrielist={countrielist}
          country={country}
          statelist={statelist}
        />
      </Tab>
      <Tab eventKey="profile" title=" BILLING ADDRESS" id="billing">
        <div className={`mt-4 ${styles.same_address}`}>
          <input
            className="form-check-input me-2 pointer"
            name="shippingAddress"
            type="checkbox"
            checked={billingAsShippingAddress}
            onChange={() =>
              setBillingAsShippingAddress(!billingAsShippingAddress)
            }
          />
          Same as shipping address
        </div>
        {!billingAsShippingAddress && (
          <AddressForm
            type={'billing'}
            countrielist={countrielist}
            country={country}
            statelist={statelist}
          />
        )}
      </Tab>
    </Tabs>
  );
}
