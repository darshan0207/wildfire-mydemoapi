import {
  doAddressesList,
  doCreateAddress,
} from '../../redux/actionCreaters/address.actioncreater';
import { doCountrieList } from '../../redux/actionCreaters/countrie.actioncreater';
import { connect } from 'react-redux';
import CheckoutAddressMobile from './CheckoutAddressMobile';

const mapStateToProps = (state) => {
  return {
    addressesList: state.address.addressesList.data,
    isloader: state.address.isloader,
    countrielist: state.countrie.data,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    doAddressesList: (payload) => dispatch(doAddressesList(payload)),
    doCreateAddress: (payload) => dispatch(doCreateAddress(payload)),
    doCountrieList: (payload) => dispatch(doCountrieList(payload)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CheckoutAddressMobile);
