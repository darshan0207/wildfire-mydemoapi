import Link from 'next/link';
import Image from 'next/image';
import styles from './PromotionCard.module.scss';

export default function PromotionCard({
  title,
  text,
  color,
  image,
  isPromotion,
  slug,
}) {
  return (
    <>
      <div className="col">
        <div className="card p-0 rounded-0 overflow-hidden border-0">
          <div className={styles.card_content} style={{ background: color }}>
            <div className={styles.card_img}>
              <Image
                src={image}
                alt="..."
                width="100%"
                height="120px"
                objectFit="contain"
              />
            </div>
          </div>
          <div className="card-body text-center">
            <div className={styles.title}>{title}</div>
            <p className={`${styles.card_text} mt-2`}>{text}</p>
          </div>
          <div className={styles.holes_lower}></div>

          <div className="m-auto mt-3">
            {isPromotion && (
              <Link href={`/promotion-banner/${slug}`}>
                <a className="btn btn-sm btn-primary  mb-4">get coupon</a>
              </Link>
            )}
            {!isPromotion && (
              <>
                <div className={styles.promo_code_text}>Promo Code</div>
                <button
                  className={`${styles.banner_ticket} btn btn-xs btn-outline-primary border-0`}
                >
                  SHB145863klsk
                </button>
              </>
            )}
          </div>
        </div>
      </div>
    </>
  );
}
