import styles from './sociallinks.module.scss';
import { ReactSVG } from 'react-svg';

export default function SocialLinks({ iscontectussvg }) {
  return (
    <div className="d-flex">
      <a
        className={`ps-0 ${
          iscontectussvg ? styles.contect_icon : styles.social_icon
        } `}
        href={`#`}
        target="_blank"
        rel="noreferrer"
      >
        <ReactSVG src="/assets/socialmediaicon/facebook.svg" />
      </a>
      <a
        className={iscontectussvg ? styles.contect_icon : styles.social_icon}
        href={`#`}
        target="_blank"
        rel="noreferrer"
      >
        <ReactSVG src="/assets/socialmediaicon/instagram.svg" />
      </a>
      {/* <a
        className={iscontectussvg ? styles.contect_icon : styles.social_icon}
        href={`https://twitter.com`}
        target="_blank"
        rel="noreferrer"
      >
        <ReactSVG src="/assets/socialmediaicon/twitter.svg" />
      </a> */}
      {/* <a
        className={styles.social_icon}
        href="#" // target={target || "_blank"}
      >
        <ReactSVG src="/assets/socialmediaicon/whatsapp.svg" />
      </a> */}
      {/* <a
        className={iscontectussvg ? styles.contect_icon : styles.social_icon}
        href={`https://linkedin.com`}
        target="_blank"
        rel="noreferrer"
      >
        <ReactSVG src="/assets/socialmediaicon/linkedin.svg" />
      </a> */}
    </div>
  );
}
