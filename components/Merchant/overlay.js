import Rating from './rating';
import Styles from './storefront.module.scss';
import Link from 'next/link';

export default function Overlay(props) {
  return (
    <div
      className={`${Styles.store_list_item_content} ${
        props.product === true ? 'isProduct' : ''
      }`}
    >
      <h4 className="title-md fw-semi text-white">
        <span> {props?.name}</span>
      </h4>
      <div className={`${Styles.store_list_info_detail_rating} mt-0 d-none`}>
        <Link
          href={`/videos${process.env.MOBILE_BUILD == 'true' ? '?slug=' : '/'}${
            props?.slug
          }`}
        >
          <a className="btn btn-sm btn-primary">view DETAILS</a>
        </Link>
        {/* <Rating rating={props?.avg_rating }/> */}
      </div>
    </div>
  );
}
