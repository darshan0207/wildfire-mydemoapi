
import { Tabs, Tab } from 'react-bootstrap';
export default function MerchantTabs(props) {
  const titles = ["Trending videos", "Top products", "About Us"];
    return (
      <Tabs defaultActiveKey="0" onSelect={(k) => props.selectTab(k)}>
        {
          titles?.map((title, index) => <Tab eventKey={index} title={title} key={index}/>)
        }
      </Tabs>
    )
}