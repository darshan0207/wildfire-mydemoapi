import Styles from './storefront.module.scss';
import Header from './header';
import MerchantTabs from './merchantTabs';
import MerchantVideo from './merchantVideo';
import { useEffect, useState } from 'react';
import Image from 'next/image';
import MerchantProduct from './merchantProduct';

export default function MerchantLayout(props) {
  const [tab, setTab] = useState(0);
  const [videopage, setVideoPage] = useState(1);
  const [productpage, setProductPage] = useState(1);
  const [videoIndex, setVideoIndex] = useState();
  var vendorData = {};
  const titles = ['Trending videos', 'Top products', 'About Us'];
  props?.vendor?.included?.map((items) => {
    if (items?.type == 'vendor_image') {
      vendorData['logo'] = items?.attributes?.original_url;
    }
    if (items?.type == 'vendor_banner') {
      vendorData['banner'] = items?.attributes?.original_url;
    }
  });

  useEffect(() => {
    props.getVendorVideo({ page: videopage, id: props?.vendor?.data?.id });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [videopage]);

  useEffect(() => {
    props.getVendorProduct({ page: productpage, id: props?.vendor?.data?.id });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [productpage]);
  return (
    <>
      <div
        className={Styles.store_banner_section}
        style={{
          backgroundImage: `url(${process.env.API_URL}${vendorData?.banner})`,
        }}
      ></div>
      <div className={Styles.store_list_section}>
        <div className="container">
          <Header
            selectTab={(tab) => setTab(tab)}
            vendor={props?.vendor?.data?.attributes}
            images={vendorData}
          />
          <div className="row">
            <div className="col-12">
              <div
                className={`d-lg-none d-block store_list_info_detail_category `}
              >
                <MerchantTabs selectTab={(tab) => setTab(tab)} />
              </div>
              <div className="tab-content">
                <div className="tab-pane fade show active">
                  <div className="row">
                    <div className="col-lg-12">
                      <div className="d-flex align-items-center justify-content-between mb-lg-0 mb-2">
                        <h3 className="title-md text-uppercase text-dark fw-bold">
                          {titles[tab]}
                        </h3>
                        {/* <a href="#" className={Styles.store_list_filter}>
                                 <img src="/assets/icons/filter-alt.svg" className="img-fluid" alt="" />
                                 </a> */}
                      </div>
                    </div>
                  </div>
                  {tab == 0 && (
                    <div className="row">
                      {props?.vendor &&
                        props?.vendor?.videos?.data?.length > 0 &&
                        props?.vendor?.videos?.data?.map((item, index) => (
                          <MerchantVideo
                            data={item}
                            key={index}
                            index={index}
                            videoIndex={videoIndex}
                            currentVideoIndex={(id) => setVideoIndex(id)}
                          />
                        ))}
                      {videopage < props?.vendor?.videos?.meta?.total_pages && (
                        <div className="col-lg-12 custom_button">
                          <div className="text-center mt-lg-5 mt-4 pt-3">
                            <button
                              onClick={() =>
                                setVideoPage((prevPage) => prevPage + 1)
                              }
                              className="btn btn-sm btn-info"
                            >
                              Load more
                            </button>
                          </div>
                        </div>
                      )}
                    </div>
                  )}
                  {tab == 1 && (
                    <div className="row">
                      {props?.vendor &&
                        props?.vendor?.product?.data?.length > 0 &&
                        props?.vendor?.product?.data?.map((item, index) => (
                          <MerchantProduct data={item} key={index} />
                        ))}
                      {productpage <
                        props?.vendor?.product?.meta?.total_pages && (
                        <div className="col-lg-12 custom_button">
                          <div className="text-center mt-lg-5 mt-4 pt-3">
                            <button
                              onClick={() =>
                                setProductPage((prevPage) => prevPage + 1)
                              }
                              className="btn btn-sm btn-info"
                            >
                              Load more
                            </button>
                          </div>
                        </div>
                      )}
                    </div>
                  )}
                  {tab == 2 && (
                    <div className="row">
                      <div
                        dangerouslySetInnerHTML={{
                          __html: props?.vendor?.data?.attributes?.about_us,
                        }}
                      ></div>
                    </div>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="about_short_section">
        <div className="container">
          <div className="row g-0">
            <div className="col-lg-7">
              <h4 className="lead fw-bold text-dark text-uppercase my-3">
                ABOUT {props?.vendor?.data?.attributes?.name}
              </h4>
              <div className={Styles.about_short_left}>
                <p>{props?.vendor?.data?.attributes?.meta_about_us}</p>
                <a href="#" id="store-logo">
                  <Image
                    src="/assets/icons/arrow-right-light.svg"
                    alt="logo"
                    height={78}
                    width={78}
                  />
                </a>
              </div>
            </div>
            <div className="col-lg-5">
              <div className="about_short_right">
                <p>OUR CUSTOMERS LOVE US!</p>
                <h6 className="title-md mt-3">
                  {props?.vendor?.data?.attributes?.dummy_review}{' '}
                </h6>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
