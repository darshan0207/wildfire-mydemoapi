import Image from 'next/image';
import { useRouter } from 'next/router';
import Overlay from './overlay';
import Taxonomy from './taxonomy';
import Styles from './storefront.module.scss';
import myLoader from '../../common/loader';

export default function MerchantProduct(props) {
  const router = useRouter();
  const handleClick = (slug) => {
    let pathname = '/products/[slug]';
    if (process.env.MOBILE_BUILD == 'true') {
      pathname = '/products';
    }
    router.push({
      pathname: `/products`,
      query: { slug: slug },
    });
  };
  return (
    <div className="col-lg-4">
      <div className={Styles.store_list_item}>
        <div
          className={Styles.store_list_item_image}
          onClick={() => handleClick(props?.data?.attributes?.slug)}
        >
          <div className={Styles.image_product}>
            <Image
              loader={myLoader}
              src={`${props?.data?.attributes?.image}`}
              layout={'fill'}
              objectFit={'contain'}
              alt="product"
            />
          </div>
          <Overlay
            product={true}
            avg_rating={props?.data?.attributes?.avg_rating}
            name={props?.data?.attributes?.name}
          />
        </div>
        <Taxonomy taxons={props?.data?.attributes?.taxon} />
      </div>
    </div>
  );
}
