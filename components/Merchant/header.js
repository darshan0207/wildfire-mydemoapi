import Image from 'next/image';
import Styles from './storefront.module.scss';
import MerchantTabs from './merchantTabs';
import Rating from './rating';
import myLoader from '../../common/loader';

export default function Header(props) {
  return (
    <div className="row">
      <div className="col-lg-12">
        <div className={Styles.store_list_info}>
          <div className={Styles.store_list_info_image}>
            <Image
              loader={myLoader}
              src={`${props?.images?.logo}`}
              alt="logo"
              height={236}
              width={236}
            />
          </div>
          <div className={Styles.store_list_info_detail}>
            <div className={Styles.store_list_info_detail_rating}>
              <p>Overall Rating</p>
              <Rating rating={props?.vendor?.avg_rating} />
            </div>
            <div className={Styles.store_list_info_detail_bottom}>
              <div
                className={`d-lg-block d-none store_list_info_detail_category ${Styles.store_list_info_detail_category}`}
              >
                <MerchantTabs selectTab={props.selectTab} />
              </div>
              <div className={Styles.store_list_info_detail_social}>
                <a
                  target="_blank"
                  rel="noreferrer"
                  href={`https://facebook.com${props?.vendor?.facebook}`}
                >
                  <Image
                    src="/assets/icons/facebook-dark.svg"
                    alt="logo"
                    height={28}
                    width={28}
                  />
                </a>
                <a
                  target="_blank"
                  rel="noreferrer"
                  href={`https://twitter.com${props?.vendor?.twitter}`}
                >
                  <Image
                    src="/assets/icons/twitter-dark.svg"
                    alt="logo"
                    height={28}
                    width={28}
                  />
                </a>
                <a
                  target="_blank"
                  rel="noreferrer"
                  href={`https://instagram.com${props?.vendor?.instagram}`}
                >
                  <Image
                    src="/assets/icons/instagram-dark.svg"
                    alt="logo"
                    height={28}
                    width={28}
                  />
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
