import Image from 'next/image';

export default function Rating(props) {
    return (
        <div className="storefront_store_rating_wrap">
        <Image src="/assets/icons/rating.svg" alt="logo" layout={'intrinsic'} width={8} height={16 }/>
            <span>{ props?.rating}/5</span>
       </div>

    )
}

