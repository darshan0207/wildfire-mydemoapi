import Styles from './storefront.module.scss';

export default function Taxonomy(props) {
    return (
        <div className={Styles.store_list_item_category}>
            {
                props?.taxons && props?.taxons?.map((taxon,index) => <a href="#" key={index}>{ taxon}</a>)
            }
    </div>
    )
}