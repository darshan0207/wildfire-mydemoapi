import Styles from './storefront.module.scss';
import Overlay from './overlay';
import Taxonomy from './taxonomy';
import VideoPlayer from '../VideoPlayer/VideoPlayer';
import { useState } from 'react';
import Rating from './rating';
import Image from 'next/image';

export default function MerchantVideo(props) {
  const [controls, setControls] = useState(false);
  return (
    <div className="col-lg-6" id="store_list_id">
      <div className={Styles.store_list_item}>
        <div
          className={Styles.store_list_item_image}
          onClick={() => setControls(true)}
        >
          <div className={Styles.store_list_item_content_review}>
            <h4 className="title-md fw-semi text-white"></h4>
            <div className={`${Styles.store_list_info_detail_rating} mt-0`}>
              <Rating rating={props?.data?.attributes?.avg_rating} />
            </div>
          </div>

          <Image
            src={`${process.env.API_URL}${props?.data?.attributes?.thumbnail}`}
            alt=""
            width="360px"
            height="360px"
            // objectFit="contain"
          />
          {/* <img
            src={`${process.env.API_URL}${props?.data?.attributes?.thumbnail}`}
            className="img-fluid"
          /> */}
          {/* <VideoPlayer
            url={props?.data?.attributes?.video}
            thumbnail={props?.data?.attributes?.thumbnail}
            index={props?.index}
            isCurrentPlay={(id) => props.currentVideoIndex(id)}
            isplaying={props?.videoIndex === props?.index ? true : false}
            iscontrols={controls}
          /> */}
          {/* {props?.videoIndex !== props?.index && ( */}
          <Overlay
            slug={props?.data?.attributes?.slug}
            name={props?.data?.attributes?.name}
          />
          {/* )} */}
        </div>
        <Taxonomy taxons={props?.data?.attributes?.taxon} />
      </div>
    </div>
  );
}
