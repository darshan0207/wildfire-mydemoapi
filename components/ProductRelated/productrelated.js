import styles from './productrelated.module.scss';
import Carousel from '../../components/Carousel';
import { useRouter } from 'next/router';
import Image from 'next/image';
import myLoader from '../../common/loader';

export default function ProductRelated({ relatedProduct }) {
  const router = useRouter();
  let pathname = '/products/[slug]';
  if (process.env.MOBILE_BUILD == 'true') {
    pathname = '/products';
  }
  const handleProduct = (slug) => {
    router.push({
      pathname,
      query: { slug },
    });
  };

  const footerCarousel = relatedProduct?.map((item, index) => {
    return (
      <div
        key={index}
        className="me-4 pointer"
        onClick={(e) => handleProduct(item?.slug)}
      >
        <div className={styles.related_item_image}>
          <Image
            src={`${item?.images[0]?.path}`}
            alt=""
            layout={'fill'}
            objectFit={'contain'}
          />
        </div>
        <div>
          <span className={styles.related_item_title}>
            {item?.name?.slice(0, 15).concat('...')}
          </span>
          <p className={styles.related_item_dsc}>
            {item?.meta_description?.slice(0, 20).concat('...')}
          </p>
        </div>
      </div>
    );
  });

  return (
    <>
      {relatedProduct?.length > 0 && (
        <div className="row m-0 mb-4">
          <div className="col">
            <hr />
            <div className="mt-3 mb-3">
              <span className={styles.product_detail_header}>
                related items
              </span>
            </div>
            <div>
              <Carousel
                arrows={false}
                autoPlay
                autoPlaySpeed={2000}
                infinite
                showDots={false}
                slidesToSlide={1}
                swipeable
                mobile={2}
                tablet={6}
                desktop={8}
                item={footerCarousel}
              />
            </div>
          </div>
        </div>
      )}
    </>
  );
}
