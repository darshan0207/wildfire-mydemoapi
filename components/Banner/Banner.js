import styles from './Banner.module.scss';
import Image from 'next/image';
import Carousel from '../../components/Carousel';

export default function Banner({ imageurl }) {
  let newBannner = imageurl?.map((item, index) => {
    return (
      <div className={styles.related_item_image} key={index}>
        <Image src={item} alt="" layout={'fill'} objectFit={'cover'} />
      </div>
    );
  });

  return (
    <div>
      {imageurl?.length && (
        <Carousel
          arrows={true}
          autoPlay={false}
          infinite
          showDots={false}
          slidesToSlide={1}
          containerClass={''}
          swipeable
          mobile={1}
          tablet={1}
          desktop={1}
          item={newBannner}
        />
      )}
    </div>
  );
}
