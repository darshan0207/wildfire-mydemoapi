import InputField from '../InputField/inputField';
import { CountryDropdown, RegionDropdown } from 'react-country-region-selector';
import { Form } from 'react-bootstrap';
import styles from '../AddressForm/addressform.module.scss';
import { useField, useFormikContext } from 'formik';
import { useEffect, useState } from 'react';

export default function AddressForm(props) {
  const [country, setCountry] = useState(props?.country);
  const newState = props?.statelist?.filter((data) => data.country === country);
  function SelectCountry({ label, ...props }) {
    const [field, meta, { setValue }] = useField(props);
    const [inputColor, setInputColor] = useState();
    const { setFieldValue } = useFormikContext();
    const onChangeCountry = (val) => {
      setValue(val);
      setCountry(val);
      if (props && props.name === 'billing.countryid')
        setFieldValue('billing.stateid', '');
      else if (props && props.name === 'countryid')
        setFieldValue('stateid', '');
    };
    return (
      <>
        <Form.Group className="mb-3">
          <Form.Label></Form.Label>
          <select
            className={`${styles.inputField}`}
            {...field}
            {...props}
            onChange={(e) => onChangeCountry(e.target.value)}
            onFocus={() => setInputColor(true)}
            onBlur={() => setInputColor(false)}
            style={{
              border: inputColor
                ? '1px solid #2A2B2A'
                : meta.touched && meta.error
                ? '1px solid #d73939'
                : '1px solid #d4d4d4',
            }}
          >
            {props?.countrielist?.length > 0 &&
              props?.countrielist?.map((element, index) => (
                <option key={index} value={element?._id}>
                  {element?.name}
                </option>
              ))}
          </select>
          {/* <CountryDropdown
            className={`${styles.inputField}`}
            {...field}
            {...props}
            onChange={(val) => onChangeCountry(val)}
            onFocus={() => setInputColor(true)}
            onBlur={() => setInputColor(false)}
            valueType="short"
            style={{
              border: inputColor
                ? '1px solid #2A2B2A'
                : meta.touched && meta.error
                ? '1px solid #d73939'
                : '1px solid #d4d4d4',
            }}
          /> */}
          {meta.touched && meta.error ? (
            <div className="text-danger">{meta.error}</div>
          ) : null}
        </Form.Group>
      </>
    );
  }

  function SelectRegion({ label, ...props }) {
    const [field, meta, { setValue }] = useField(props);
    const [inputColor, setInputColor] = useState();

    const onChangeRegion = (val) => {
      setValue(val);
    };
    return (
      <>
        <Form.Group className="mb-3">
          <Form.Label></Form.Label>
          <select
            className={`${styles.inputField}`}
            {...field}
            {...props}
            onChange={(e) => onChangeRegion(e.target.value)}
            onFocus={() => setInputColor(true)}
            onBlur={() => setInputColor(false)}
            style={{
              border: inputColor
                ? '1px solid #2A2B2A'
                : meta.touched && meta.error
                ? '1px solid #d73939'
                : '1px solid #d4d4d4',
            }}
          >
            <option value="''">Select Region</option>
            {newState?.length > 0 &&
              newState?.map((element, index) => (
                <option key={index} value={element?._id}>
                  {element?.name}
                </option>
              ))}
          </select>
          {/* <RegionDropdown
            country={country}
            value={newstate}
            className={`${styles.inputField}`}
            {...field}
            {...props}
            onChange={(e) => onChangeRegion(e)}
            onFocus={(e) => setInputColor(true)}
            onBlur={(e) => setInputColor(false)}
            countryValueType="short"
            valueType="short"
            style={{
              border: inputColor
                ? '1px solid #2A2B2A'
                : meta.touched && meta.error
                ? '1px solid #d73939'
                : '1px solid #d4d4d4',
            }}
          /> */}
          {meta.touched && meta.error ? (
            <div className="text-danger">{meta.error}</div>
          ) : null}
        </Form.Group>
      </>
    );
  }
  return (
    <>
      <div className={`row ${styles.custom_addreess}`}>
        <div className="col-md-6">
          <InputField
            name={props?.type ? `${props?.type}.firstname` : 'firstname'}
            type="text"
            placeholder="First Name"
          />
        </div>
        <div className="col-md-6">
          <InputField
            name={props?.type ? `${props?.type}.lastname` : 'lastname'}
            type="text"
            placeholder="Last Name"
          />
        </div>
        <div className="col-md-6">
          <InputField
            name={props?.type ? `${props?.type}.company` : 'company'}
            type="text"
            placeholder="Company name (optional)"
          />
        </div>
        <div className="col-md-6">
          <InputField
            name={props?.type ? `${props?.type}.phone` : 'phone'}
            type="text"
            placeholder="Phone"
          />
        </div>
        <div className="col-md-12">
          <InputField
            name={props?.type ? `${props?.type}.address1` : 'address1'}
            type="text"
            placeholder="Address line 1"
          />
        </div>
        <div className="col-md-12">
          <InputField
            name={props?.type ? `${props?.type}.address2` : 'address2'}
            type="text"
            placeholder="Address line 2"
          />
        </div>
        <div className="col-md-6">
          <InputField
            name={props?.type ? `${props?.type}.city` : 'city'}
            type="text"
            placeholder="City"
          />
        </div>
        <div className="col-md-6">
          <InputField
            name={props?.type ? `${props?.type}.zipcode` : 'zipcode'}
            type="text"
            placeholder="ZIP / Postal code"
            maxLength="5"
          />
        </div>
        <div className="col-md-6">
          <SelectCountry
            name={props?.type ? `${props?.type}.countryid` : 'countryid'}
            type="text"
            placeholder="Country"
            countrielist={props?.countrielist}
          />
        </div>
        <div className="col-md-6">
          <SelectRegion
            name={props?.type ? `${props?.type}.stateid` : 'stateid'}
            type="text"
            placeholder="State / Province"
          />
        </div>
      </div>
    </>
  );
}
