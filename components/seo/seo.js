import Head from 'next/head';

export default function SEO({
  description,
  title,
  siteTitle = 'Wildfire+',
  keywords,
}) {
  return (
    <Head>
      <title>{`${title} | ${siteTitle}`}</title>
      <meta property="title" content={title} />
      <meta name="keywords" content={keywords} />
      <meta name="description" content={description} />
      <meta property="og:title" content={title} />
      <meta name="og:keywords" content={keywords} />
      <meta property="og:description" content={description} />
      <meta property="og:site_name" content={siteTitle} />
      <meta property="og:type" content="product" />
    </Head>
  );
}
