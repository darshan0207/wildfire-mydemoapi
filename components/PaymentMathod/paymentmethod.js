import { Elements } from '@stripe/react-stripe-js';
import { useEffect, useRef, useState } from 'react';
import * as Yup from 'yup';
import Button from '../Button/button';
import styles from './paymentmethod.module.scss';
import { loadStripe } from '@stripe/stripe-js';
import SplitForm from '../SplitForm/splitform';
import Loader from '../Loader';
import { useFormik } from 'formik';
import { ReactSVG } from 'react-svg';

export default function PaymentMethod(props) {
  const result = props?.payment;
  const promotion = props?.promotions?.promotion;
  const [check, setCheck] = useState(false);
  const [methodId, setMethodId] = useState();
  const [cardDetails, setCardDetails] = useState({});
  const [isNewCard, setIsNewCard] = useState(true);
  const stripeRef = useRef();
  const stripePromise = loadStripe(
    `pk_test_51HFZKmE4pgx94R1PjZaClJN7rxqCJElOlGKjgHdEVzSroXTKTXaOZkZT3I6MtOudViLszzU4xzWlIa011H35sahw00TpHlkCB3`
  );

  useEffect(() => {
    if (props?.isHydrated) {
      props?.getPaymentDetail();
      // if (props?.isAuth) props?.getCardDetails();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props?.isHydrated]);

  useEffect(() => {
    if (result?.data?.length) {
      setMethodId(result?.data[0]);
      // props.doPaymentMethod({
      //   paymentMethodName: result?.data[0]?.attributes?.name,
      // });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [result?.data]);

  useEffect(() => {
    // cardDefaultData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props?.cardDetail]);

  const cardDefaultData = () => {
    if (props?.cardDetail?.length) {
      let defaultCard = props?.cardDetail?.filter(
        (item) => item?.attributes?.default === true
      );
      if (defaultCard?.length) {
        setMethodId(defaultCard[0]?.relationships?.payment_method?.data);
        // props.doPaymentMethod({ paymentMethodName: defaultCard[0]?.type });
        // setCardDetails(defaultCard[0]);
        setIsNewCard(false);
      } else {
        setMethodId(props?.cardDetail[0]?.relationships?.payment_method?.data);
        // props.doPaymentMethod({
        //   paymentMethodName: props?.cardDetail[0]?.type,
        // });
        // setCardDetails(props?.cardDetail[0]);
        setIsNewCard(false);
      }
    } else {
      // setCardDetails({});
      setIsNewCard(true);
    }
  };

  const selectPaymentMethod = (item, type) => {
    if (
      item?.attributes?.name?.toLowerCase() === 'credit card' &&
      type !== 'newcard' &&
      !isNewCard
    ) {
      // cardDefaultData();
    } else {
      // setCardDetails({});
      setMethodId(item);
    }
    // props.doPaymentMethod({ paymentMethodName: item?.attributes?.name });
  };

  const formik = useFormik({
    initialValues: {
      promocode: '',
    },
    validationSchema: Yup.object({
      promocode: Yup.string().required('This field is required'),
    }),
    onSubmit: (payload, { resetForm }) => {
      if (promotion?.name) {
        // props?.removePromoCode({ promocode: promotion?.code });
        resetForm({
          values: {
            promocode: '',
          },
        });
      } else {
        // props?.setPromoCode({ coupon_code: payload.promocode });
      }
    },
  });

  useEffect(() => {
    if (promotion?.name) {
      setCheck(true);
      formik.setFieldValue('promocode', `Promotion (${promotion?.name})`);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [promotion]);

  const handleRemoveCode = () => {
    if (check && promotion?.name) {
      // props?.removePromoCode({ promocode: promotion?.code });
      formik.values.promocode = '';
    }
  };

  const HandlePaymentDefaultCard = () => {
    // props.doPaymentMethod({ paymentMethodName: cardDetails?.type });
    props?.doUpdateOrder({
      // order: {
      //   payments_attributes: [
      //     {
      //       payment_method_id: methodId,
      //     },
      //   ],
      //   existing_card: cardDetails?.id,
      // },
      paymentid: paymentId,
      nextStep: props?.changeSubmitProgress,
      state: 'payment',
    });
  };

  const HandleDelete = (id) => {
    props.removeCards(id);
  };

  const onHandlecontinue = (method) => {
    // if (method?.type === 'stripe') {
    // if (
    //   (method?.attributes?.name?.toLowerCase() === 'credit card' ||
    //     cardDetails?.type === 'credit_card') &&
    //   !isNewCard &&
    //   Object?.keys(cardDetails)?.length
    // ) {
    //   HandlePaymentDefaultCard();
    // } else if (
    //   (method?.attributes?.name?.toLowerCase() === 'credit card' ||
    //     cardDetails?.type === 'credit_card') &&
    //   isNewCard
    // ) {
    stripeRef.current.stripeSubmit();
    // }
    // else if(method?.attributes?.name?.toLowerCase() === 'check') {

    // }
    // }
  };

  return (
    <>
      <div className={`pb-5 ${styles.shipping_header}`}>Payment method</div>
      <form onSubmit={formik.handleSubmit}>
        <div className={styles.checkbox_first}>
          <input
            name="checkbox"
            type="checkbox"
            id="checkbox"
            className="form-check-input pointer"
            checked={check}
            onChange={(e) => {
              setCheck((check) => !check);
              handleRemoveCode();
            }}
          />
          <label htmlFor="checkbox" className={`ms-3 ${styles.checkbox_label}`}>
            Do you have a gift card voucher or discount code ?
          </label>
        </div>
        {check && (
          <div>
            <div className={`mb-3 ${styles.discount_code}`}>
              <input
                id="promocode"
                name="promocode"
                type="text"
                className="w-100"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.promocode}
                disabled={promotion ? true : false}
              />
              <Button
                class={`d-none d-sm-block ms-3 ${styles.apply_button}`}
                type="submit"
                name={promotion ? 'Remove' : 'Apply'}
              />
            </div>
            <div>
              {formik.errors['promocode'] ? (
                <span className="text-danger">
                  {formik.errors['promocode']}
                </span>
              ) : null}
            </div>
            <Button
              class={`d-sm-none d-block ${styles.apply_button}`}
              name={promotion ? 'Remove' : 'Apply'}
              type="submit"
            />
            <hr className={styles.separator} />
          </div>
        )}
      </form>
      {props?.pay_loader === true ? (
        <Loader />
      ) : (
        <>
          {result &&
            result?.data.length > 0 &&
            result?.data?.map((item, i) => {
              return (
                <>
                  <div key={i} className={`mb-3 ps-4 ${styles.radioButton}`}>
                    <input
                      className="m-0 form-check-input form-select-input pointer"
                      name="group1"
                      id={`radio${i}`}
                      type="radio"
                      checked={item?._id === methodId?._id}
                      onChange={(e) => {
                        selectPaymentMethod(item);
                      }}
                    />
                    <label htmlFor="radio1" className="ms-3">
                      {item?.name}
                    </label>
                  </div>
                  {item?.name?.toLowerCase() === 'default' &&
                    (item?._id === methodId?._id ||
                      methodId?.type === 'credit_card') && (
                      <div className="row">
                        <div className="col-md-12">
                          <div
                            className={`mb-3 ps-4 ${styles.cardRadioButton}`}
                          >
                            <div className="d-flex align-items-center">
                              <input
                                className="m-0 form-check-input form-select-input pointer"
                                name="newcredit"
                                id="newcredit"
                                type="radio"
                                checked={isNewCard}
                                onClick={(e) => {
                                  setIsNewCard(true),
                                    selectPaymentMethod(item, 'newcard');
                                }}
                              />
                              <label htmlFor="newcredit" className="ms-3">
                                Add New Credit Card
                              </label>
                            </div>
                          </div>
                        </div>
                        {(props?.cardDetail?.length === 0 || isNewCard) && (
                          <Elements stripe={stripePromise}>
                            <SplitForm
                              doUpdateOrder={props?.doUpdateOrder}
                              methodId={methodId?._id}
                              changeSubmitProgress={props?.changeSubmitProgress}
                              ref1={stripeRef}
                            />
                          </Elements>
                        )}
                        {/* <div className="col-md-12">
                          {props?.cardDetail &&
                            props?.cardDetail.map((item, i) => {
                              return (
                                <div
                                  key={i}
                                  className={`mb-3 ps-4 ${styles.cardRadioButton}`}
                                >
                                  <div className="d-flex align-items-center">
                                    <input
                                      className="m-0 form-check-input form-select-input pointer"
                                      name="credit"
                                      id="credit"
                                      type="radio"
                                      onChange={(e) => {
                                        setIsNewCard(false),
                                          setCardDetails(item);
                                      }}
                                      checked={cardDetails?.id === item.id}
                                    />
                                    <label htmlFor="credit" className="ms-3">
                                      ************
                                      {item?.attributes?.last_digits}
                                    </label>
                                  </div>
                                  <div className="me-4">
                                    <ReactSVG
                                      src="/assets/icons/deleteCart.svg"
                                      className={`pointer ${styles.transLogo}`}
                                      onClick={(e) => HandleDelete(item.id)}
                                    />
                                  </div>
                                </div>
                              );
                            })}
                        </div> */}
                      </div>
                    )}
                </>
              );
            })}
        </>
      )}
      <div className="col-md-12">
        <button
          className={styles.payment_button}
          type="button"
          onClick={() => onHandlecontinue(methodId)}
          // onClick={(event) => stripeSubmit(event)}
          disabled={props?.dis_Loader ? true : false}
        >
          Continue to REVIEW
        </button>
      </div>
    </>
  );
}
