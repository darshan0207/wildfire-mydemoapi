import PaymentMethod from './paymentmethod';
import { connect } from 'react-redux';
import {
  doCardDetails,
  getCardDetails,
  getPaymentDetail,
  removeCard,
  removePromoCode,
  setPromoCode,
} from '../../redux/actionCreaters/payment.actioncreater';
import { doPaymentMethod } from '../../redux/actionCreaters/cart.actioncreater';
import { doUpdateOrder } from '../../redux/actionCreaters/order.actioncreater';

const mapStateToProps = (state) => {
  return {
    payment: state.payment,
    pay_loader: state.payment.isLoader,
    isHydrated: state._persist && state._persist.rehydrated ? true : false,
    cardDetail: state.payment.cardDetail,
    isAuth: state.auth.isAuthenticated,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getPaymentDetail: (payload) => dispatch(getPaymentDetail(payload)),
    setPromoCode: (payload) => dispatch(setPromoCode(payload)),
    doUpdateOrder: (payload) => dispatch(doUpdateOrder(payload)),
    doPaymentMethod: (payload) => dispatch(doPaymentMethod(payload)),
    removePromoCode: (payload) => dispatch(removePromoCode(payload)),
    getCardDetails: () => dispatch(getCardDetails()),
    removeCards: (payload) => dispatch(removeCard(payload)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PaymentMethod);
