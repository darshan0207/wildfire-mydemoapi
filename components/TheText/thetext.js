import styles from './thetext.module.scss';

export default function thetext() {
  return (
    <>
      <ul className={styles.main_content}>
        <li className={styles.sub_content}>
          We enhance brands and influencers engagement with shoppers in
          real-time with interactive shoppable videos and graphics. Viewers can
          find products directly next to the video player with deals and special
          offer pricing reflected in the product carousel for viewers.
        </li>
        <li className={styles.sub_content}>
          <button className={styles.button}>KNOW MORE</button>
        </li>
      </ul>
    </>
  );
}
