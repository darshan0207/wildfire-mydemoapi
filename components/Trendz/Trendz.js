import styles from './Trendz.module.scss';
import Image from 'next/image';
import WildfireTv from '../../public/assets/images/logo-white.png';
// import BiyomaImage from '../../public/assets/images/biyoma.png';
import Link from 'next/link';

export default function Trendz({ goldBanner }) {
  return (
    <>
      {goldBanner?.length && (
        <div className="trendz-section">
          <div className="container">
            <div>
              <div
                className={`row ${styles.trendz_image} trendz-image`}
                style={{
                  backgroundImage: `url('assets/ecommerce-banner.jpg')`,
                }}
              >
                {/* <div className="col-lg-6">
                  <div
                    className="trendz-image"
                    style={{
                      backgroundImage: `url(${goldBanner[0]?.banner})`,
                    }}
                  >
                    <div className="trendz-logo">
                      <Image
                        src={goldBanner[0]?.logo}
                        width="110px"
                        height="110px"
                        alt=""
                        className="img-fluid"
                      />
                    </div>
                  </div>
                </div> */}
                {/* <div className="col-lg-6">
                  <div
                    className="trendz-content"
                    style={{ backgroundImage: "url('assets/images/fire.png')" }}
                  >
                    <h2 className="title-lg text-white">
                      Get into <br />
                      New <span className="fw-bold">Trendz</span> Now!
                    </h2>

                    <div className="d-sm-inline-flex d-flex flex-sm-row flex-column gap-lg-3 gap-2">
                      {goldBanner[0]?.slug && (
                        <Link
                          href={`/storefront${
                            process.env.MOBILE_BUILD == 'true' ? '?slug=' : '/'
                          }${goldBanner[0]?.slug}`}
                        >
                          <a className="btn btn-sm btn-dark me-auto">Buy now</a>
                        </Link>
                      )}
                      <Link href={`/watchtv`}>
                        <a className="btn btn-sm btn-light me-auto">
                          watch & shop
                        </a>
                      </Link>
                    </div>
                    <div className="mt-lg-4 mt-3 text-end ms-auto">
                      <Image src={WildfireTv} alt="" className="img-fluid " />
                    </div>
                  </div>
                </div> */}
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
}
