import WithLogin from '../WithLogin';
import styles from './SocialShopping.module.scss';
import Image from 'next/image';
import WildfireTv from '../../public/assets/images/logo-text.png';
import SocialImage from '../../public/assets/images/social.png';

export default function SocialShopping({ isAuth }) {
  return (
    <div className="social-shopping-section">
      <div className="container">
        <div className="row social-shopping-border align-items-center g-lg-0">
          <div className="col-lg-6">
            <div className="social-shopping-content">
              {/* <p className="social-shopping-content-text text-primary">
                The network designed for...
              </p> */}

              <h3 className="title-lg text-capitalize fw-bold text-dark">
                The Ultimate <br />
                Social Shopping Experience
              </h3>
            </div>
          </div>

          <div className="col-lg-6">
            <div className="row">
              <div className="col-lg-6">
                <div className="social-shopping-image">
                  <div className="ms-lg-auto mx-auto img-fluid text-center">
                    <Image src={SocialImage} alt="" />
                  </div>
                </div>
              </div>

              <div className="col-lg-6 align-self-center">
                <div className="social-shopping-action">
                  <div className="text-center">
                    <div className="img-fluid mx-auto">
                      <Image src={WildfireTv} alt="" />
                    </div>
                    {isAuth === false && (
                      <a className="btn btn-primary">
                        <WithLogin title="Join the network" />
                      </a>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
