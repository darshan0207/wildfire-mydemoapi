import styles from './productslider.module.scss';
import Image from 'next/image';
import Carousel from '../../components/Carousel';
import heart from '../../public/assets/icons/singleProductHeart.svg';
import { useEffect, useState } from 'react';
import myLoader from '../../common/loader';

function ProductSlider({
  wished_products,
  product_ID,
  variantID,
  variantData = [],
  addProductTOWishlist,
  isAuthenticated,
}) {
  const [mainImage, setMainImage] = useState(0);
  useEffect(() => {
    setMainImage(0);
  }, [variantData, variantData?.length]);

  // const addWishlistProduct = (varID) => {
  //   addProductTOWishlist({
  //     wished_product: {
  //       variant_id: varID,
  //       // remark: 'I want this Product too',
  //       quantity: 1,
  //     },
  //   });
  // };

  const caros =
    variantData?.length > 0 &&
    variantData?.map((item, index) => {
      return (
        <div key={index} className={styles.slider_image}>
          <Image
            // loader={myLoader}
            src={`${item.path}`}
            alt=""
            layout={'fill'}
            objectFit={'contain'}
            onClick={(e) => setMainImage(index)}
          />
        </div>
      );
    });
  return (
    <>
      <div className={styles.product_main_image}>
        {isAuthenticated && (
          <div className={styles.product_icon_heart}>
            <i
              className={`pointer fa ${
                wished_products?.length > 0 && wished_products[0] === product_ID
                  ? 'fa-heart'
                  : 'fa-heart-o'
              }`}
              aria-hidden="true"
              onClick={() => addProductTOWishlist({ productId: product_ID })}
            ></i>
          </div>
        )}

        {variantData && variantData.length > 0 && (
          <Image
            // loader={myLoader}
            src={`${variantData[mainImage].path}`}
            alt=""
            layout={'fill'}
            objectFit={'contain'}
          />
        )}
      </div>
      <div className={`mt-5 mb-3 ${styles.carousel_images}`}>
        <Carousel
          arrows={true}
          autoPlay={false}
          infinite
          showDots={false}
          slidesToSlide={1}
          containerClass={styles.custom_dot_with_class}
          swipeable
          mobile={3}
          tablet={3}
          desktop={4}
          item={caros}
        />
      </div>
    </>
  );
}

export default ProductSlider;
