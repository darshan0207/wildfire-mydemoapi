import styles from './productmoredetails.module.scss';
import { useState } from 'react';

export default function ProductMoreDetails({ productProperty }) {
  const [viewmore, setViewMore] = useState(false);
  return (
    <>
      <span
        className={styles.product_viewmore}
        onClick={(e) => setViewMore((viewmore) => !viewmore)}
      >
        {viewmore ? 'view less details' : 'view more details'}
      </span>
      <div>
        {viewmore &&
          productProperty &&
          productProperty.map((item, index) => {
            return (
              <small key={index}>
                <b className="text-capitalize"> {item?.name} </b> :{' '}
                {item?.value}
                <br />
              </small>
            );
          })}
      </div>
    </>
  );
}
