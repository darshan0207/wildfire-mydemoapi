import styles from './uniqueFeature.module.scss';
import Image from 'next/image';
import Feature1 from '../../public/assets/images/feature01.jpg';
import Feature2 from '../../public/assets/images/feature02.jpg';
import Feature3 from '../../public/assets/images/feature03.jpg';
import Play from '../../public/assets/icons/play.svg';

export default function UniqueFeature() {
  return (
    <div
      className={styles.unique_feature_section}
      style={{ backgroundImage: 'url(/assets/images/bg01.jpg)' }}
    >
      <div className={`container ${styles.contain_box}`}>
        <div className="row no-gutter">
          <div className="col-lg-12 text-center">
            <h3
              className={`${styles.title_lg} text-dark text-capitalize fw-normal`}
            >
              Unique Features
            </h3>
          </div>
        </div>

        <div className="row">
          <div className="col-lg-12">
            <div className={styles.unique_feature_item}>
              <div className="row">
                <div className="col-lg-6">
                  <div className={styles.unique_feature_image}>
                    <Image src={Feature1} className="img-fluid" alt="" />
                  </div>
                </div>

                <div className="col-lg-6">
                  <div className={styles.unique_feature_content}>
                    <span>
                      01. <i></i>
                    </span>

                    <h4
                      className={`${styles.title_md} fw-bold text-dark text-uppercase`}
                    >
                      Live tv
                    </h4>

                    <p>
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                      sed do eiusmod tempor incididunt ut labore et dolore magna
                      aliqua. Ut enim ad minim veniam
                    </p>

                    <button className={styles.whatch_btn}>Watch now</button>
                  </div>
                </div>
              </div>
            </div>

            <div className={styles.unique_feature_item}>
              <div className="row">
                <div className="col-lg-6 order-lg-2 order-1">
                  <div className={styles.unique_feature_video}>
                    <Image src={Feature2} className="img-fluid" alt="" />

                    <a href="#" className={styles.top_brand_video_play}>
                      <Image src={Play} className="img-fluid" alt="" />
                    </a>
                  </div>
                </div>

                <div className="col-lg-6 order-lg-1 order-2">
                  <div className={styles.unique_feature_content}>
                    <span>
                      02. <i></i>
                    </span>

                    <h4
                      className={`${styles.title_md} fw-bold text-dark text-uppercase`}
                    >
                      Interviews
                    </h4>

                    <p>
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                      sed do eiusmod tempor incididunt ut labore et dolore magna
                      aliqua. Ut enim ad minim veniam
                    </p>

                    <button
                      className={`${styles.btn_view} ${styles.whatch_btn}`}
                    >
                      View all
                    </button>
                  </div>
                </div>
              </div>
            </div>

            <div className={styles.unique_feature_item}>
              <div className="row">
                <div className="col-lg-6">
                  <div className={styles.unique_feature_image}>
                    <Image src={Feature3} className="img-fluid" alt="" />
                  </div>
                </div>

                <div className="col-lg-6">
                  <div className={styles.unique_feature_content}>
                    <span>
                      01. <i></i>
                    </span>

                    <h4
                      className={`${styles.title_md} fw-bold text-dark text-uppercase`}
                    >
                      PROMOTION
                    </h4>

                    <p>
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                      sed do eiusmod tempor incididunt ut labore et dolore magna
                      aliqua. Ut enim ad minim veniam
                    </p>

                    <button className={styles.whatch_btn}>know more</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
