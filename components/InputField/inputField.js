import { Form } from 'react-bootstrap';
import styles from './inputField.module.scss';
import { useField } from 'formik';
import { useState } from 'react';

export default function InputField({ label, ...props }) {
  const [field, meta] = useField(props);
  const [inputColor, setInputColor] = useState();

  const HandleIconClick = () => {
    if (props && props.right_icon) {
      props?.HandleIconClick();
    }
  };

  return (
    <>
      <Form.Group className="mb-3">
        <Form.Label
          htmlFor={props.id || props.name}
          className={styles.form_label}
        >
          {label}
        </Form.Label>
        {props.icon && (
          <i
            className={`${props.icon} ${styles.input_left_icon}`}
            style={{ color: inputColor ? '#3a4468' : '' }}
          />
        )}
        <Form.Control
          className={styles.inputField}
          {...field}
          {...props}
          onFocus={(e) => setInputColor(true)}
          onBlur={(e) => setInputColor(false)}
          style={{
            border: inputColor
              ? '1px solid #2A2B2A'
              : meta.touched && meta.error
              ? '1px solid #3a4468'
              : '1px solid #d4d4d4',
          }}
        />
        {props.right_icon && (
          <i
            className={`${props.right_icon} ${props.right_icon_class}`}
            style={{ color: inputColor ? '#3a4468' : '' }}
            onClick={HandleIconClick}
          />
        )}
        <div>
          {meta.touched && meta.error ? (
            <div className="text-danger">{meta.error}</div>
          ) : null}
        </div>
      </Form.Group>
    </>
  );
}
