import styles from './TrendzTwo.module.scss';
import Image from 'next/image';
import WildfireTv from '../../public/assets/images/logo-red.png';
// import DebunkImage from '../../public/assets/images/debunk.png';
import Link from 'next/link';

export default function TrendzTwo({ sliverBanner }) {
  return (
    <>
      {sliverBanner?.length && (
        <div className="trendz-section trendz-style2">
          <div className="container">
            <div className="bg-secondary">
              <div
                className={`row ${styles.trendz_image}`}
                style={{
                  backgroundImage: `url('assets/banner-ecommerce.jpg')`,
                }}
              >
                {/* <div className="col-lg-6">
                  <div
                    className="trendz-image"
                    style={{
                      backgroundImage: `url(${sliverBanner[0]?.banner})`,
                    }}
                  >
                    <div className="trendz-logo">
                      <Image
                        src={sliverBanner[0]?.logo}
                        width="156px"
                        height="44px"
                        alt=""
                        className="img-fluid"
                      />
                    </div>
                  </div>
                </div> */}
                {/* <div className="col-lg-6">
                  <div
                    className="trendz-content"
                    style={{
                      backgroundImage: "url('assets/images/fire02.png')",
                    }}
                  >
                    <h2 className="title-lg text-primary">
                      Get into <br />
                      New <span className="fw-bold">Trendz</span> Now!
                    </h2>

                    <div className="d-sm-inline-flex d-flex flex-sm-row flex-column gap-lg-3 gap-2">
                      {sliverBanner[0]?.slug && (
                        <Link
                          href={`/storefront${
                            process.env.MOBILE_BUILD == 'true' ? '?slug=' : '/'
                          }${sliverBanner[0]?.slug}`}
                        >
                          <a className="btn btn-sm btn-dark me-auto">Buy now</a>
                        </Link>
                      )}
                      <Link href={`/watchtv`}>
                        <a className="btn btn-sm btn-light me-auto">
                          watch & shop
                        </a>
                      </Link>
                    </div>

                    <div className="mt-lg-n3 mt-n4 text-end">
                      <Image
                        src={WildfireTv}
                        alt=""
                        className="img-fluid ms-auto"
                      />
                    </div>
                  </div>
                </div> */}
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
}
