import filterIcon from '../../public/assets/images/Vector.png';
import Image from 'next/image';
import styles from './ProductFilterMenu.module.scss';
import { createRef, useEffect, useState } from 'react';

export default function ProductFilterMenu({ query, filteroption, filterdata }) {
  let filterOptionTypes = query.query || {};
  const navRef = createRef();
  const backRef = createRef();
  const navClose = () => {
    navRef.current.style.left = '-320px';
    backRef.current.style.display = 'none';
  };

  const navOpen = () => {
    navRef.current.style.left = '0';
    backRef.current.style.display = 'block';
  };

  const handleOptionTypes = (name, val) => {
    if (filterOptionTypes[name] === val) {
      delete filterOptionTypes[name];
    } else {
      filterOptionTypes[name] = val;
    }
    filterdata(filterOptionTypes);
  };
  const handleClearAll = () => {
    filterdata({});
  };

  const checkFilterOption = (name, val) => {
    if (Object?.keys(query.query)?.length) {
      return query?.query[name] === val ? true : false;
    } else {
      return false;
    }
  };

  return (
    <>
      <div className={styles.filter_search}>
        <div className="d-flex">
          <button
            type="button"
            className={styles.filter_btn}
            name={'Filter'}
            onClick={() => navOpen()}
          >
            Filter
            <span className={styles.filter_icon}>
              <Image src={filterIcon} alt="" />
            </span>
          </button>
          {Object?.keys(query.query)?.length > 0 && (
            <div
              className={styles.filter_clear_all}
              onClick={() => handleClearAll()}
            >
              Clear ALL
            </div>
          )}
        </div>
      </div>
      <div className={styles.filter_mobile}>
        <button className={styles.filter_btn_mobile} onClick={() => navOpen()}>
          <div className="position-absolute">
            <Image src={filterIcon} alt="" />
          </div>
        </button>
      </div>

      <div
        id="show-backdrop"
        ref={backRef}
        onClick={() => navClose()}
        className={styles.menu_backdrop}
      ></div>
      <div ref={navRef} id="FilterSidebar" className={styles.sidenav}>
        <div className="d-flex justify-content-between mb-3">
          <div className={styles.filter_text}>Filter</div>
          <div className={styles.closebtn} onClick={() => navClose()}>
            &times;
          </div>
        </div>
        {filteroption?.length > 0 &&
          filteroption?.map((item, index) => (
            <div className="row m-0 w-75" key={index}>
              <div
                className={`${styles.filter_title} col-md-12 mb-2 text-capitalize`}
              >
                {item?.name}
              </div>

              {item?.name?.toLowerCase() !== 'color' ? (
                <div className="col-md-12">
                  {item?.optionvalues?.length > 0 &&
                    item?.optionvalues.map((optiontypes, index) => (
                      <div className="d-flex mt-2 mb-2" key={index}>
                        <div className="filter_radio">
                          <input
                            id={optiontypes.name}
                            name={item?.name}
                            type="checkbox"
                            value={optiontypes._id}
                            checked={checkFilterOption(
                              item?.name,
                              optiontypes._id
                            )}
                            onChange={() =>
                              handleOptionTypes(item.name, optiontypes._id)
                            }
                          />
                          <label
                            htmlFor={optiontypes.name}
                            className={`radio-label ${styles.filter_name}`}
                          >
                            {optiontypes.name}
                          </label>
                        </div>
                      </div>
                    ))}
                  <div className={styles.line_verticel}></div>
                </div>
              ) : (
                <div className="col-md-12">
                  <div className={`${styles.grid_template}`}>
                    {item?.optionvalues?.length > 0 &&
                      item?.optionvalues.map((optiontypes, index) => (
                        <button
                          key={index}
                          type="checkbox"
                          name="group0"
                          className={`mb-2 ${styles.product_size_btn} ${
                            checkFilterOption(item?.name, optiontypes._id)
                              ? styles.active
                              : ''
                          }`}
                          style={{ background: optiontypes.presentation }}
                          value={optiontypes._id}
                          onClick={() =>
                            handleOptionTypes(item.name, optiontypes._id)
                          }
                        ></button>
                      ))}
                  </div>
                  <div className={styles.line_verticel}></div>
                </div>
              )}
            </div>
          ))}
      </div>
    </>
  );
}
