import { Step, Stepper } from 'react-form-stepper';
import styles from './Stepper.module.scss';
import { ReactSVG } from 'react-svg';
import CheckoutAddress from '../CheckoutAddress/index';
import { useEffect, useState } from 'react';
import CartSummary from '../CartSummary/cartsummary';
import { useRouter } from 'next/router';
import ShippingMethod from '../ShippingMethod/index';
import PaymentMethod from '../PaymentMathod/index';
import ReviewOrder from '../ReviewOrder/index';

function StepperPage({
  cart,
  activeIndex,
  updateCartQuantity,
  dis_Loader,
  doOrderComplete,
}) {
  const router = useRouter();
  const [stepIndex, setStepIndex] = useState(activeIndex || 0);
  const handleStepper = (val) => {
    if (val === 0) {
      router.push('/checkout/address');
    } else if (val === 1) {
      router.push('/checkout/shipping');
    } else if (val === 2) {
      router.push('/checkout/payment');
    } else if (val === 3) {
      router.push('/checkout/review');
    }
  };

  const handleCount = (itme_id, quantity) => {};

  const handlePayment = () => {};

  return (
    <>
      <div className="container pb-5">
        <div className="row">
          <div className={`col-sm-12 col-md-7 col-lg-8 ${styles.stepper_main}`}>
            <Stepper
              activeStep={stepIndex}
              className={styles.StepperContainer}
              connectorStateColors={true}
              connectorStyleConfig={{
                size: 4,
                disabledColor: '#adb9c7',
                activeColor: '#272727',
                completedColor: '#272727',
              }}
              styleConfig={{
                activeBgColor: '#3a4468',
                activeTextColor: '#ffffff',
                completedBgColor: '#272727',
                completedTextColor: '#ffffff',
                inactiveBgColor: '#adb9c7',
                inactiveTextColor: '#3a4468',
                circleFontSize: '18px',
                fontWeight: 500,
              }}
            >
              <Step label="Address" onClick={(e) => handleStepper(0)}>
                {stepIndex > 0 && <ReactSVG src="/assets/icons/check.svg" />}
              </Step>
              <Step label="Shipping" onClick={(e) => handleStepper(1)}>
                {stepIndex > 1 && <ReactSVG src="/assets/icons/check.svg" />}
              </Step>
              <Step label="Payment" onClick={(e) => handleStepper(2)}>
                {stepIndex > 2 && <ReactSVG src="/assets/icons/check.svg" />}
              </Step>
              <Step label="Review" onClick={(e) => handleStepper(3)}>
                {stepIndex > 3 && <ReactSVG src="/assets/icons/check.svg" />}
              </Step>
            </Stepper>
            {
              {
                0: (
                  <CheckoutAddress
                    changeSubmitProgress={() => handleStepper(1)}
                    dis_Loader={dis_Loader}
                    // line_items={newdata}
                  />
                ),
                1: (
                  <ShippingMethod
                    changeSubmitProgress={() => handleStepper(2)}
                    dis_Loader={dis_Loader}
                  />
                ),
                2: (
                  <PaymentMethod
                    changeSubmitProgress={() => handleStepper(3)}
                    dis_Loader={dis_Loader}
                    promotions={cart?.data?.relationships?.promotions?.data[0]}
                  />
                ),
                3: <ReviewOrder />,
              }[stepIndex]
            }
          </div>
          <div className={`col-sm-12 col-md-5 col-lg-4`}>
            {cart && cart?.data && (
              <CartSummary
                line_item={cart?.data}
                subtotal={cart?.itemtotal}
                // promo={
                //   cart?.data?.attributes?.promo_total > 0
                //     ? cart?.data?.attributes?.display_promo_total
                //     : ''
                // }
                // total={cart?.data?.attributes?.display_total}
                // compareTotal={
                //   cart?.data?.attributes?.pre_tax_total > 0
                //     ? cart?.data?.attributes?.display_pre_tax_total
                //     : ''
                // }
                // Shipping={
                //   cart?.data?.attributes?.ship_total > 0
                //     ? cart?.data?.attributes?.display_ship_total
                //     : ''
                // }
                // taxTotal={
                //   cart?.data?.attributes?.tax_total > 0 ?
                //   cart?.data?.attributes?.display_tax_total
                //   : ''
                // }
                IsLoader={cart?.isLoader}
                handleCount={handleCount}
              />
            )}
          </div>
        </div>
      </div>
    </>
  );
}
export default StepperPage;
