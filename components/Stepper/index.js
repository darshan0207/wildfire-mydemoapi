import StepperPage from './Stepper';
import { connect } from 'react-redux';
import { updateCartQuantity } from '../../redux/actionCreaters/cart.actioncreater';
import { doOrderComplete } from '../../redux/actionCreaters/order.actioncreater';

const mapStateToProps = (state) => {
  return {
    cart: state?.cart,
    dis_Loader: state.order.isloader,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateCartQuantity: (payload) => dispatch(updateCartQuantity(payload)),
    doOrderComplete: (payload) => dispatch(doOrderComplete(payload)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(StepperPage);
