import {
  doAddressesList,
  doCreateAddress,
} from '../../redux/actionCreaters/address.actioncreater';
import {
  doCountrieList,
  doRegionList,
} from '../../redux/actionCreaters/countrie.actioncreater';
import { connect } from 'react-redux';
import CheckoutAddress from './CheckoutAddress';
import { doUpdateOrder } from '../../redux/actionCreaters/order.actioncreater';

const mapStateToProps = (state) => {
  return {
    addressesList: state.address.addressesList.data,
    isloader: state.address.isloader,
    isAuthenticated: state.auth.isAuthenticated,
    isHydrated: state._persist && state._persist.rehydrated ? true : false,
    nextStep: state?.order?.orderDetails?.attributes?.state,
    countrielist: state.countrie.data,
    statelist: state.countrie.stateList,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    doAddressesList: (payload) => dispatch(doAddressesList(payload)),
    doCreateAddress: (payload) => dispatch(doCreateAddress(payload)),
    doUpdateOrder: (payload) => dispatch(doUpdateOrder(payload)),
    doCountrieList: (payload) => dispatch(doCountrieList(payload)),
    doRegionList: () => dispatch(doRegionList()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CheckoutAddress);
