import styles from './CheckoutAddress.module.scss';
import AddressForm from '../AddressForm/addressform';
import { Formik, Form } from 'formik';
import * as Yup from 'yup';
import { useEffect, useState } from 'react';
import AddressGridSelector from '../AddressGridSelector/AddressGridSelector';
import Loader from '../Loader';
import CheckoutAddressMobile from '../CheckoutAddressMobile/CheckoutAddressMobile';
import { Tabs, Tab } from 'react-bootstrap';
import { toast } from 'react-toastify';

function CheckoutAddress({
  doCreateAddress,
  doAddressesList,
  addressesList = [],
  isloader,
  isAuthenticated,
  isHydrated,
  changeSubmitProgress,
  line_items,
  nextStep,
  doUpdateOrder,
  dis_Loader,
  doCountrieList,
  countrielist,
  doRegionList,
  statelist,
}) {
  const [billingAsShippingAddress, setBillingAsShippingAddress] =
    useState(false);
  const [shippingAddress, setshippingAddress] = useState({});
  const [billingAddress, setBillingAddress] = useState({});
  const [isShippingError, setIsShippingError] = useState(false);
  const [isBillingError, setIsBillingError] = useState(false);
  const [defaultcty, setdefaultcty] = useState();
  // let defaultcty;
  // useEffect(() => {
  //   if (nextStep?.toLowerCase() === 'delivery') {

  //   }
  // }, [nextStep]);

  // useEffect(() => {
  //   if (isnextpage) {
  //     changeSubmitProgress({
  //       shippingAddress: shippingAddress,
  //       billingAddress: billingAddress,
  //     });
  //   }
  // }, [isnextpage]);

  useEffect(() => {
    if (isHydrated && isAuthenticated) {
      doAddressesList();
    }
    doCountrieList();
    doRegionList();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [doAddressesList, isHydrated]);

  let addressFields = {
    firstname: '',
    lastname: '',
    address1: '',
    address2: '',
    city: '',
    phone: '',
    zipcode: '',
    countryid: '',
    stateid: '',
    company: '',
  };

  useEffect(() => {
    if (countrielist?.length) {
      // let defaultcountry = countrielist && countrielist;
      // if (defaultcountry) {
      addressFields.countryid = countrielist[0]?._id;
      setdefaultcty(countrielist[0]?._id);
      // }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    // }, [countrielist, defaultcty]);
  }, [countrielist?.length]);

  let addressWithPickedFields = Object.assign(
    {
      billing: addressFields,
    },
    addressFields
  );

  let addressWitvalidationSchema = {
    firstname: Yup.string().required('This field is required'),
    lastname: Yup.string().required('This field is required'),
    address1: Yup.string().required('This field is required'),
    address2: Yup.string(),
    city: Yup.string().required('This field is required'),
    phone: Yup.string().required('This field is required'),
    zipcode: Yup.string()
      .required('This field is required')
      .matches(/^[0-9]+$/, 'Must be only digits')
      .min(2, 'Must be exactly 2 digits')
      .max(5, 'Must be exactly 5 digits'),
    stateid: Yup.string().required('This field is required'),
    countryid: Yup.string().required('This field is required'),
    company: Yup.string(),
  };

  const handleContinueShipping = (shipping, billing, sameas) => {
    if (Object.keys(shipping).length === 0) {
      setIsShippingError(true);
    }
    if (Object.keys(billing).length === 0 && !sameas) {
      setIsBillingError(true);
    }
    if (
      Object.keys(shipping).length !== 0 &&
      Object.keys(billing).length === 0 &&
      !sameas
    ) {
      toast.error('Please provide billing address.');
    }
    if (
      Object.keys(shipping).length > 0 &&
      (Object.keys(billing).length > 0 || sameas)
    ) {
      toast.info('Getting Shipping rates');
      setshippingAddress(shipping);
      setBillingAddress(sameas ? shipping : billing);
      doUpdateOrder({
        // line_items: line_items,
        shipaddress: sameas ? shipping : billing,
        billaddress: shipping,
        // state: 'complete',
        // bill_address_attributes: sameas ? shipping : billing,
        // ship_address_attributes: shipping,
        nextStep: changeSubmitProgress,
        state: 'address',
      });
      // changeSubmitProgress({
      //   shippingAddress: shipping,
      //   billingAddress: sameas ? shipping : billing,
      // });
      setIsShippingError(false);
      setIsBillingError(false);
    }
  };

  return (
    <>
      {isloader && isAuthenticated ? (
        <Loader />
      ) : (
        <div className={`${styles.custom_address}`}>
          <Formik
            initialValues={addressWithPickedFields}
            validationSchema={
              billingAsShippingAddress
                ? Yup.object({
                    ...addressWitvalidationSchema,
                  })
                : Yup.object({
                    ...addressWitvalidationSchema,
                    billing: Yup.object().shape({
                      ...addressWitvalidationSchema,
                    }),
                  })
            }
            onSubmit={(value, { setSubmitting, resetForm }) => {
              let shippingAddress = { ...value };
              delete shippingAddress.billing;
              let billingAddress = value.billing;
              // setshippingAddress(shippingAddress);
              // setBillingAddress(billingAddress);
              // doCreateAddress({
              //   payload: shippingAddress,
              // });
              // if (
              //   !billingAsShippingAddress &&
              //   JSON.stringify(shippingAddress) !==
              //     JSON.stringify(billingAddress)
              // ) {
              //   doCreateAddress({
              //     payload: billingAddress,
              //   });
              // }
              doUpdateOrder({
                shipaddress: shippingAddress,
                billaddress:
                  !billingAsShippingAddress &&
                  JSON.stringify(shippingAddress) !==
                    JSON.stringify(billingAddress)
                    ? billingAddress
                    : shippingAddress,
                nextStep: changeSubmitProgress,
                state: 'address',
              });
              // doUpdateOrder({
              //   shipaddress: shippingAddress,
              //   billaddress: billingAddress,
              //   nextStep: changeSubmitProgress(),
              //   state: 'address',
              // });
            }}
          >
            {function MyForm({ setFieldValue, handleSubmit, values, isValid }) {
              const onerrorhandle = () => {
                if (
                  values['address1'] &&
                  values['city'] &&
                  values['countryid'] &&
                  values['firstname'] &&
                  values['lastname'] &&
                  values['phone'] &&
                  values['stateid'] &&
                  values['zipcode'] &&
                  !values['billing']['address1'] &&
                  !values['billing']['city'] &&
                  !values['billing']['countryid'] &&
                  !values['billing']['firstname'] &&
                  !values['billing']['lastname'] &&
                  !values['billing']['phone'] &&
                  !values['billing']['stateid'] &&
                  !values['billing']['zipcode'] &&
                  !isValid &&
                  !billingAsShippingAddress
                ) {
                  toast.error('Please provide billing address.');
                }
              };

              useEffect(() => {
                if (countrielist?.length) {
                  setFieldValue('countryid', defaultcty);
                }
                // eslint-disable-next-line react-hooks/exhaustive-deps
              }, [countrielist?.length, defaultcty]);

              useEffect(() => {
                if (!billingAsShippingAddress) {
                  setFieldValue('billing.countryid', defaultcty);
                  setFieldValue('billing.stateid', '');
                }
                // eslint-disable-next-line react-hooks/exhaustive-deps
              }, [billingAsShippingAddress, defaultcty]);
              return (
                <Form>
                  {addressesList.length > 0 ? (
                    <>
                      <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12 d-none d-sm-block">
                        <div className={styles.title}>shipping address</div>
                        <AddressGridSelector
                          id="shhiping"
                          userAddresses={addressesList}
                          setShippingAddress={(data) => {
                            setshippingAddress(data), setIsShippingError(false);
                          }}
                        />
                        {isShippingError &&
                          Object.keys(shippingAddress).length === 0 && (
                            <div className="text-danger">
                              Please provide shipping address.
                            </div>
                          )}

                        <hr className={styles.address_line} />
                        <div className={styles.title}>Billing address</div>
                        <div className={styles.same_address}>
                          <input
                            className="form-check-input me-2 pointer"
                            name="shippingAddress"
                            type="checkbox"
                            checked={billingAsShippingAddress}
                            onChange={() =>
                              setBillingAsShippingAddress(
                                !billingAsShippingAddress
                              )
                            }
                          />
                          Same as shipping address
                        </div>
                        {!billingAsShippingAddress && (
                          <>
                            <AddressGridSelector
                              id="billing"
                              userAddresses={addressesList}
                              setBillingAddress={(data) => {
                                setBillingAddress(data),
                                  setIsBillingError(false);
                              }}
                            />
                            {isBillingError &&
                              Object.keys(billingAddress).length === 0 && (
                                <div className="text-danger">
                                  Please provide billing address.
                                </div>
                              )}
                          </>
                        )}
                        <div className="pt-5 pb-5">
                          <button
                            type="button"
                            className={styles.custom_shipping}
                            onClick={() =>
                              handleContinueShipping(
                                shippingAddress,
                                billingAddress,
                                billingAsShippingAddress
                              )
                            }
                          >
                            Continue to shipping
                          </button>
                        </div>
                      </div>
                      <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12 d-block d-sm-none address_mobile_tabs">
                        <Tabs
                          defaultActiveKey="home"
                          transition={false}
                          id="noanim-tab-example"
                          className="mb-3"
                        >
                          <Tab eventKey="home" title="SHIPPING ADDRESS">
                            <AddressGridSelector
                              id="shhipingmobile"
                              userAddresses={addressesList}
                              setShippingAddress={(data) => {
                                setshippingAddress(data),
                                  setIsShippingError(false);
                              }}
                            />
                            {isShippingError &&
                              Object.keys(shippingAddress).length === 0 && (
                                <div className="text-danger">
                                  Please provide shipping address.
                                </div>
                              )}
                          </Tab>
                          <Tab
                            eventKey="profile"
                            title=" BILLING ADDRESS"
                            id="billing"
                          >
                            <div className={`mt-4 ${styles.same_address}`}>
                              <input
                                className="form-check-input me-2 pointer"
                                name="shippingAddress"
                                type="checkbox"
                                checked={billingAsShippingAddress}
                                onChange={() =>
                                  setBillingAsShippingAddress(
                                    !billingAsShippingAddress
                                  )
                                }
                              />
                              Same as shipping address
                            </div>
                            {!billingAsShippingAddress && (
                              <>
                                <AddressGridSelector
                                  id="billingmobile"
                                  userAddresses={addressesList}
                                  setBillingAddress={(data) => {
                                    setBillingAddress(data),
                                      setIsBillingError(false);
                                  }}
                                />
                                {isBillingError &&
                                  Object.keys(billingAddress).length === 0 && (
                                    <div className="text-danger">
                                      Please provide billing address.
                                    </div>
                                  )}
                              </>
                            )}
                          </Tab>
                        </Tabs>
                        <div className="pt-5 pb-5">
                          <button
                            type="button"
                            className={styles.custom_shipping}
                            onClick={() =>
                              handleContinueShipping(
                                shippingAddress,
                                billingAddress,
                                billingAsShippingAddress
                              )
                            }
                            disabled={dis_Loader === true ? true : false}
                          >
                            Continue to shipping
                          </button>
                        </div>
                      </div>
                    </>
                  ) : (
                    <>
                      <div className="col-md-12 d-none d-sm-block">
                        <div className={styles.title}>shipping address</div>
                        {defaultcty?.length > 0 && (
                          <AddressForm
                            country={defaultcty}
                            countrielist={countrielist}
                            statelist={statelist}
                          />
                        )}
                        <hr className={styles.address_line} />
                        <div className={styles.title}>Billing address</div>
                        <div className={styles.same_address}>
                          <input
                            className="form-check-input me-2 pointer"
                            name="shippingAddress"
                            type="checkbox"
                            checked={billingAsShippingAddress}
                            onChange={() =>
                              setBillingAsShippingAddress(
                                !billingAsShippingAddress
                              )
                            }
                          />
                          Same as shipping address
                        </div>
                        {!billingAsShippingAddress &&
                          defaultcty?.length > 0 && (
                            <AddressForm
                              country={defaultcty}
                              countrielist={countrielist}
                              statelist={statelist}
                              type={'billing'}
                            />
                          )}
                        <button
                          type="Submit"
                          className={styles.custom_shipping}
                          disabled={dis_Loader === true ? true : false}
                        >
                          Continue to shipping
                        </button>
                      </div>
                      <div className="col-md-12 d-block d-sm-none address_mobile_tabs">
                        {defaultcty?.length > 0 && (
                          <CheckoutAddressMobile
                            billingAsShippingAddress={billingAsShippingAddress}
                            countrielist={countrielist}
                            setBillingAsShippingAddress={(data) => {
                              setBillingAsShippingAddress(data);
                            }}
                            statelist={statelist}
                            country={defaultcty}
                          />
                        )}
                        <button
                          type="submit"
                          className={styles.custom_shipping}
                          disabled={dis_Loader === true ? true : false}
                          onClick={() => [handleSubmit(), onerrorhandle()]}
                        >
                          Continue to shipping
                        </button>
                      </div>
                    </>
                  )}
                </Form>
              );
            }}
          </Formik>
        </div>
      )}
    </>
  );
}

export default CheckoutAddress;
