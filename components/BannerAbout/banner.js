import styles from './banner.module.scss';
import Image from 'next/image';

export default function BannerAbout() {
  return (
    <div
      className={styles.hero_banner_sm}
      style={{ backgroundImage: 'url(/assets/images/banner08.jpg)' }}
    >
      <div className="container">
        <div className="row no-gutter">
          <div className="col-lg-12 col-6 ">
            <h1 className="text-center">Shiv Shakti</h1>
            {/* <Image
                src="/assets/W2.png"
                className="mx-auto"
                height="120px"
                width="277px"
                objectFit={'contain'}
                alt=""
              /> */}
          </div>
        </div>
      </div>
    </div>
  );
}
