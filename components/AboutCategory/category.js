import styles from './category.module.scss';
import Image from 'next/image';
import Category1 from '../../public/assets/images/category01.png';
import Category2 from '../../public/assets/images/category02.png';
import Category3 from '../../public/assets/images/category03.png';
import Category4 from '../../public/assets/images/category04.png';
import { Nav, Tab } from 'react-bootstrap';

export default function Category() {
  return (
    <div className={styles.category_section}>
      <div className="container ">
        <div className="row no-gutter">
          <div className="col-lg-12 text-center">
            <h2
              className={`${styles.title_lg} text-uppercase fw-bold text-dark`}
            >
              Category
            </h2>
          </div>
        </div>
        <Tab.Container id="left-tabs-example" defaultActiveKey="first">
          <div className="row category-menu">
            <div className="col-lg-10 mx-auto">
              <Nav className={`nav-tabs ${styles.NavTabs}`}>
                <Nav.Item>
                  <Nav.Link
                    eventKey="first"
                    className={`pointer ${styles.TabMenu}`}
                  >
                    Beauty
                  </Nav.Link>
                </Nav.Item>
                <Nav.Item>
                  <Nav.Link
                    eventKey="second"
                    className={`pointer ${styles.TabMenu}`}
                  >
                    FASHION
                  </Nav.Link>
                </Nav.Item>
                <Nav.Item>
                  <Nav.Link
                    eventKey="third"
                    className={`pointer ${styles.TabMenu}`}
                  >
                    FOOTWEAR
                  </Nav.Link>
                </Nav.Item>
              </Nav>
              <Tab.Content>
                <Tab.Pane eventKey="first">
                  <div className="row row-cols-2 row-cols-md-2 row-cols-lg-4 justify-content-lg-between justify-content-evenly">
                    <div className={styles.category_list_item}>
                      <div className={styles.category_list_item_image}>
                        <Image src={Category2} className="img-fluid" alt="" />
                      </div>
                      <p>Skincare</p>
                    </div>

                    <div className={styles.category_list_item}>
                      <div className={styles.category_list_item_image}>
                        <Image src={Category1} className="img-fluid" alt="" />
                      </div>
                      <p>Accessories</p>
                    </div>

                    <div className={styles.category_list_item}>
                      <div className={styles.category_list_item_image}>
                        <Image src={Category4} className="img-fluid" alt="" />
                      </div>
                      <p>Hair</p>
                    </div>

                    <div className={styles.category_list_item}>
                      <div
                        className={`img-fluid  ${styles.category_list_item_image}`}
                      >
                        <Image src={Category3} className="img-fluid " alt="" />
                      </div>
                      <p>Makeup</p>
                    </div>
                  </div>
                </Tab.Pane>
                <Tab.Pane eventKey="second">
                  <div className="row row-cols-2 row-cols-md-2 row-cols-lg-4 justify-content-lg-between justify-content-evenly">
                    <div className={styles.category_list_item}>
                      <div className={styles.category_list_item_image}>
                        <Image src={Category1} className="img-fluid" alt="" />
                      </div>
                      <p>Accessories</p>
                    </div>
                    <div className={styles.category_list_item}>
                      <div className={styles.category_list_item_image}>
                        <Image src={Category2} className="img-fluid" alt="" />
                      </div>
                      <p>Skincare</p>
                    </div>
                    <div className={styles.category_list_item}>
                      <div
                        className={`img-fluid  ${styles.category_list_item_image}`}
                      >
                        <Image src={Category3} className="img-fluid " alt="" />
                      </div>
                      <p>Makeup</p>
                    </div>
                    <div className={styles.category_list_item}>
                      <div className={styles.category_list_item_image}>
                        <Image src={Category4} className="img-fluid" alt="" />
                      </div>
                      <p>Hair</p>
                    </div>
                  </div>
                </Tab.Pane>
                <Tab.Pane eventKey="third">
                  <div className="row row-cols-2 row-cols-md-2 row-cols-lg-4 justify-content-lg-between justify-content-evenly">
                    <div className={styles.category_list_item}>
                      <div className={styles.category_list_item_image}>
                        <Image src={Category4} className="img-fluid" alt="" />
                      </div>
                      <p>Hair</p>
                    </div>

                    <div className={styles.category_list_item}>
                      <div
                        className={`img-fluid  ${styles.category_list_item_image}`}
                      >
                        <Image src={Category3} className="img-fluid " alt="" />
                      </div>
                      <p>Makeup</p>
                    </div>
                    <div className={styles.category_list_item}>
                      <div className={styles.category_list_item_image}>
                        <Image src={Category2} className="img-fluid" alt="" />
                      </div>
                      <p>Skincare</p>
                    </div>

                    <div className={styles.category_list_item}>
                      <div className={styles.category_list_item_image}>
                        <Image src={Category1} className="img-fluid" alt="" />
                      </div>
                      <p>Accessories</p>
                    </div>
                  </div>
                </Tab.Pane>
              </Tab.Content>
            </div>
          </div>
        </Tab.Container>
      </div>
    </div>
  );
}
