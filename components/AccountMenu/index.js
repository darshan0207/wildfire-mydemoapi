import AccountMenu from './accountmenu';
// export default accountmenu;
import { connect } from 'react-redux';
import { doLogoutUser } from '../../redux/actionCreaters/auth.actioncreater';
import { doAccountInfo } from '../../redux/actionCreaters/account.actioncreater';

const mapStateToProps = (state) => {
  return {
    state: state,
    userProfile: state.account.accountInfo,
    // isHydrated: state._persist && state._persist.rehydrated ? true : false,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    doLogoutUser: () => dispatch(doLogoutUser()),
    doAccountInfo: (payload) => dispatch(doAccountInfo(payload)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AccountMenu);
