import styles from './accountmenu.module.scss';
import { ReactSVG } from 'react-svg';
import Link from 'next/link';
import { useEffect } from 'react';

function AccountMenu({
  items = [],
  active = null,
  doLogoutUser,
  userProfile,
  doAccountInfo,
  isHydrated,
}) {
  useEffect(() => {
    if (isHydrated && !userProfile?.firstName && !userProfile?.lastName) {
      doAccountInfo();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [doAccountInfo, isHydrated]);
  return (
    <>
      <div className={styles.wrapper}>
        <div className={styles.menu_header}>
          <div>
            <div className={styles.menu_welcome}>Welcome</div>
            {userProfile?.firstName && userProfile?.lastName && (
              <div
                className={styles.menu_name}
              >{`${userProfile?.firstName} ${userProfile?.lastName}`}</div>
            )}
          </div>
          <div>
            <button
              className={styles.btnlogout}
              type="button"
              onClick={() => doLogoutUser()}
            >
              logout
              <ReactSVG className="ps-1" src="\assets\icons\logout.svg" />
            </button>
          </div>
        </div>
        {items.map((item, index) => {
          return (
            <Link
              href={item.path}
              data-test="accountMenuLink"
              data-test-id={item.path}
              key={index}
            >
              <a className="text-decoration-none">
                <div
                  className={`${styles.menuitem} ${
                    item.path === active ? styles.active : ''
                  }`}
                >
                  {item.title}
                </div>
              </a>
            </Link>
          );
        })}
      </div>
    </>
  );
}

export default AccountMenu;
