import styles from './cartItem.module.scss';
import Image from 'next/image';
import { ReactSVG } from 'react-svg';
import router from 'next/router';
import myLoader from '../../common/loader';
export default function CartItem(props) {
  const handleClick = (itme_id, quantity) => {
    props?.handleCount(itme_id, quantity);
  };

  const HandleDelete = (itme_id) => {
    props?.HandleDeleteCart(itme_id);
  };

  const HandlePage = (slug) => {
    props?.handlePageRoute(slug);
  };

  return (
    <>
      {props &&
        props?.line_item?.map((item, index) => {
          let option_array = item?.productId?.variants;
          return (
            <div
              key={index}
              className={`row p-0 m-0 ${styles.product_detail_box}`}
            >
              <div className={`col-3 p-0`}>
                {item?.productId?.images ? (
                  <div className={styles.product_image}>
                    <Image
                      src={`${item?.productId?.images[0]?.path}`}
                      width="88px"
                      height="60px"
                      objectFit={'cover'}
                      alt=""
                    />
                  </div>
                ) : (
                  <ReactSVG src="/assets/noimage.svg" />
                )}
              </div>
              <div className={`col-9 p-0 ps-3`}>
                <div
                  className={`pointer ${styles.product_name}`}
                  onClick={() => HandlePage(item?.productId?.slug)}
                >
                  {item?.productId?.name}
                </div>
                <div className="d-flex flex-nowrap justify-content-between">
                  <div>
                    {option_array?.length &&
                      option_array?.map((ele, i) => {
                        return (
                          <div key={i} className={styles.product_variant}>
                            <></>
                            <br />
                          </div>
                        );
                      })}
                  </div>
                  <div>
                    <ReactSVG
                      src="/assets/icons/deleteCart.svg"
                      className={`pointer ${styles.transLogo}`}
                      onClick={(e) => HandleDelete(item._id)}
                    />
                  </div>
                </div>
                <div className="d-flex justify-content-between align-items-center">
                  <div className="d-flex align-items-center">
                    <button
                      className={`${styles.counter_btn} ms-0`}
                      onClick={() => handleClick(item._id, item?.quantity - 1)}
                      disabled={item?.quantity === 1}
                    >
                      -
                    </button>
                    <span className={styles.count}>{item?.quantity}</span>
                    <button
                      className={`${styles.counter_btn}`}
                      onClick={() => handleClick(item._id, item?.quantity + 1)}
                    >
                      +
                    </button>
                    <span className={`ms-2 me-2 ${styles.count_x}`}>x</span>
                    <span className={` ${styles.product_price}`}>
                      {item?.newprice ? item?.newprice : item?.productId?.price}
                    </span>
                  </div>
                  <span className={` ${styles.product_total}`}>
                    {item?.total?.toFixed(2)}
                  </span>
                </div>
              </div>
            </div>
          );
        })}
    </>
  );
}
