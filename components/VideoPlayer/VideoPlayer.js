import ReactPlayer from 'react-player';
import PlayImage from '../../public/assets/icons/play.svg';
import Image from 'next/image';

export default function VideoPlayer({
  url,
  height,
  width,
  isplaysinline = false,
  iscontrols = true,
  isplaying = false,
  islight = false,
  ismuted = false,
  isloop = false,
  isonPlay,
  isCurrentPlay,
  index,
  continuewVideo,
  thumbnail = '',
}) {
  return (
    <ReactPlayer
      config={{
        file: { attributes: { controlsList: 'nodownload' } },
      }}
      onContextMenu={(e) => e.preventDefault()}
      controls={iscontrols}
      playing={isplaying}
      muted={ismuted}
      loop={isloop}
      url={`${process.env.API_URL}${url}`}
      height={height || '100%'}
      width={width || '100%'}
      playsinline={isplaysinline}
      onPlay={() => [
        isCurrentPlay && isCurrentPlay(index),
        isonPlay && isonPlay(true),
      ]}
      onEnded={() => {
        continuewVideo && continuewVideo(index + 1),
          isonPlay && isonPlay(false);
      }}
      onPause={() => isonPlay && isonPlay(false)}
      onStart={() => isCurrentPlay && isCurrentPlay(index)}
      // onPause={() => setPause(false)}
      // onPause={() => setIsPlay(false)}
      // onPlay={() => isonPlay && isonPlay(true)}
      // onEnded={() => isonPlay && isonPlay(false)}
      // onPause={() => isonPlay && isonPlay(false)}
      playIcon={
        <Image
          src={PlayImage}
          alt=""
          className="img-fluid"
          onClick={() => [
            isCurrentPlay && isCurrentPlay(index),
            isonPlay && isonPlay(true),
          ]}
        />
      }
      light={!islight ? `${process.env.API_URL}${thumbnail}` : false}
    />
  );
}
