import Overlay from './overlay';
import { connect } from 'react-redux';
import { searchProduct } from '../../redux/actionCreaters/search.actioncreater';

const mapStateToProps = (state) => {
  return {
    searchdata: state.search,
    isAuth: state.auth.isAuthenticated,
    wishlistItem: state.auth?.wishlist,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    searchProduct: (payload) => dispatch(searchProduct(payload)),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Overlay);
