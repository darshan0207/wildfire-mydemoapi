import Image from 'next/image';
import styles from './overlay.module.scss';
import SearchIcon from '../../public/assets/icons/search.svg';
import Item from '../Item';
import ViewAllResult from '../Button/button';
import { useRouter } from 'next/router';
import { createRef, useState } from 'react';
import Loader from '../Loader';

export default function Overlay(props) {
  const result = props.searchdata;
  const router = useRouter();
  const [nameValue, setNameValue] = useState();
  let backRef = createRef();
  const wished_products =
    props?.wishlistItem?.length > 0 && props?.wishlistItem;
  let display = router.pathname === '/search' ? 'none' : '';
  const openNav = (value) => {
    setNameValue(value);
    document.body.style.overflow = value ? 'hidden' : '';
    if (backRef?.current?.style) backRef.current.style.display = 'block';
    if (value) {
      props.searchProduct({
        page: 1,
        limit: 8,
        name: value,
      });
    }
    // props.searchProduct({
    //   include: 'images,primary_variant',
    //   filter: {
    //     ['searchkick']: value,
    //   },
    //   sort: 'updated_at',
    //   page: 1,
    //   per_page: 8,
    // });
  };

  const handleClick = (slug) => {
    let pathname = '/products/[slug]';
    if (process.env.MOBILE_BUILD == 'true') {
      pathname = '/products';
    }
    router.push({
      pathname: slug ? pathname : '/search',
      query: slug ? { slug } : { searchkick: nameValue },
    });
    document.body.style.overflow = '';
    setNameValue('');
  };

  const navClose = () => {
    document.body.style.overflow = '';
    if (backRef?.current?.style) backRef.current.style.display = 'none';
    setNameValue('');
    display = 'none';
    // document.body.style.overflow = '';
  };

  return (
    <>
      <div
        id="input"
        className={styles.search_input}
        style={{ display: `${display}` }}
      >
        <div className={`${styles.search_content}`}>
          <input
            className={`${styles.input}`}
            type="text"
            value={nameValue || ''}
            placeholder="Search"
            onChange={(event) => openNav(event.target.value)}
          />
          <div className={`${styles.search_icon_size}`}>
            <Image src={SearchIcon} alt="" />
          </div>
        </div>
      </div>

      {nameValue ? (
        <>
          <div
            id="show-backdrop"
            ref={backRef}
            onClick={() => navClose()}
            className={styles.menu_backdrop}
          ></div>
          <div
            id="product"
            className={styles.search_product}
            style={{ display: `${display}` }}
          >
            {result && result.data && result?.meta?.count === 0 ? (
              <div className="p-3">
                <p className="text-uppercase fw-bold h4">
                  SORRY, BUT WE COULDN&apos;T MATCH ANY SEARCH RESULTS
                </p>
              </div>
            ) : result.isloader === true ? (
              <Loader />
            ) : (
              <>
                {result.data.map((item, index) => {
                  const newArray =
                    wished_products &&
                    wished_products?.filter((ele) => ele === item?._id);
                  return (
                    <Item
                      key={index}
                      title={item?.name}
                      description={item?.description}
                      display_Price={item?.price}
                      original_Price={item?.price}
                      compare_Price={item?.compareatprice}
                      price={item?.price}
                      data_Image_Id={
                        item && item.images.length && item?.images[0]?._id
                      }
                      item={item}
                      include={result?.included}
                      include_Image={
                        item && item.images.length && item?.images[0].path
                      }
                      oveerly_serach={'oveerly_serach'}
                      slug={item?.slug}
                      handleClick={handleClick}
                      isAuth={props?.isAuth}
                      wished_products={newArray}
                    />
                  );
                })}
                {/* {result?.data?.map((item, index) => {
                  return (
                    <Item
                      key={index}
                      title={item?.attributes?.name}
                      description={item?.attributes?.description}
                      display_Price={item?.attributes?.display_price}
                      original_Price={
                        item?.attributes?.display_compare_at_price
                      }
                      compare_Price={item?.attributes?.compare_at_price}
                      price={item?.attributes?.price}
                      data_Image_Id={item?.relationships?.images?.data[0]?.id}
                      item={item}
                      include={result?.included}
                      include_Image={
                        item && item.imagestyle && item?.imagestyle
                      }
                      oveerly_serach={'oveerly_serach'}
                      slug={item?.attributes?.slug}
                      handleClick={handleClick}
                      isAuth={props?.isAuth}
                      wished_products={
                        wished_products?.length > 0 &&
                        wished_products.filter(
                          (ele) => ele.product_id === item?.id
                        )
                      }
                    />
                  );
                })} */}

                <div className={styles.search_product_footer}>
                  <ViewAllResult
                    type={'button'}
                    class={`btn ${styles.search_product_btn}`}
                    name={'View all search results'}
                    handleCLick={handleClick}
                  />
                </div>
              </>
            )}
          </div>
        </>
      ) : null}
    </>
  );
}
