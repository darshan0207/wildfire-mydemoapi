import styles from './TrendzThree.module.scss';
import Image from 'next/image';
import WildfireTv from '../../public/assets/images/logo-red.png';
import FunnImage from '../../public/assets/images/funn.png';
import Link from 'next/link';

export default function TrendzThree({ bronzeBanner }) {
  return (
    <>
      {bronzeBanner?.length && (
        <div className="trendz-section trendz-style3">
          <div className="container">
            <div className="bg-secondary">
              <div className="row">
                <div className="col-lg-6">
                  <div
                    className="trendz-image"
                    style={{
                      backgroundImage: `url(${bronzeBanner[0]?.banner})`,
                    }}
                  >
                    <div className="trendz-logo">
                      <Image
                        src={bronzeBanner[0]?.logo}
                        width="110px"
                        height="40px"
                        alt=""
                        className="img-fluid"
                      />
                    </div>
                  </div>
                </div>
                <div className="col-lg-6">
                  <div
                    className="trendz-content"
                    style={{
                      backgroundImage: "url('assets/images/fire02.png')",
                    }}
                  >
                    <h2 className="title-md text-primary">
                      Get into <br />
                      New <span className="fw-bold">Trendz</span> Now!
                    </h2>

                    <div className="d-sm-inline-flex d-flex flex-sm-row flex-column gap-lg-3 gap-2">
                      {bronzeBanner[0]?.slug && (
                        <Link
                          href={`/storefront${
                            process.env.MOBILE_BUILD == 'true' ? '?slug=' : '/'
                          }${bronzeBanner[0]?.slug}`}
                        >
                          <a className="btn btn-sm btn-dark me-auto">Buy now</a>
                        </Link>
                      )}
                      <Link href={`/watchtv`}>
                        <a className="btn btn-sm btn-light me-auto">
                          watch & shop
                        </a>
                      </Link>
                    </div>

                    <div className="mt-n4 text-end">
                      <Image
                        src={WildfireTv}
                        alt=""
                        className="img-fluid ms-auto"
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
}
