import styles from './producttag.module.scss';

export default function ProductTag({ name }) {
  return <div className={styles.product_header}>{name}</div>;
}
