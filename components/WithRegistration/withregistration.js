import withAuth from '../WithAuth';
import React from 'react';

function WithRegistration(props) {
  return <span>{props.title}</span>;
}
export default withAuth(WithRegistration, false, false,true);
