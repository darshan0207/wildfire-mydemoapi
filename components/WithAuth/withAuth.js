import React, { Component } from 'react';
import Login from '../Model/Login';
import Onboarding from '../Model/Onboarding';
import Register from '../Model/Register';
import { withRouter } from 'next/router';

export default function WithAuth(
  AuthComponent,
  isRoute = false,
  isLogin = false,
  isRegistration = false
) {
  class Authenticated extends Component {
    static async getInitialProps(ctx) {
      // Ensures material-ui renders the correct css prefixes server-side
      let userAgent;
      if (process.browser) {
        userAgent = navigator.userAgent;
      } else {
        userAgent = ctx.req.headers['user-agent'];
      }

      // Check if Page has a `getInitialProps`; if so, call it.
      const pageProps =
        AuthComponent.getInitialProps &&
        (await AuthComponent.getInitialProps(ctx));
      // Return props.
      return { ...pageProps, userAgent };
    }

    constructor(props) {
      super(props);
      this.checkAuth = this.checkAuth.bind(this);
      this.checkRoute = this.checkRoute.bind(this);
      this.state = {
        showLogin: false,
        showRegister: false,
        showOnboarding: false,
      };
    }

    checkRoute() {
      if (!this.props.isAuthenticated && isRoute && this.props.isHydrated) {
        this.props.router.push('/');
      }
    }

    componentDidMount() {
      this.checkRoute();
    }

    componentDidUpdate(oldProps) {
      if (
        this.props.isAuthenticated &&
        !oldProps.isAuthenticated &&
        this.props.isHydrated
      ) {
        this.setShow('showLogin', false);
        if (isLogin) {
          this.props.router.push('/');
        }
      }
      this.checkRoute();
    }

    checkAuth(event) {
      if (!this.props.isAuthenticated) {
        if (isRoute) {
          this.props.router.push('/');
        } else if (isRegistration) {
          event.stopPropagation();
          this.setShow('showRegister', true);
        } else {
          event.stopPropagation();
          this.setShow('showLogin', true);
        }
      }
    }

    setShow(variable, value) {
      this.setState({
        showLogin: false,
        showRegister: false,
        showOnboarding: false,
        [variable]: value,
      });
    }

    render() {
      return (
        <>
          <Login
            show={this.state.showLogin}
            setShow={(value) => {
              this.setShow('showLogin', value);
            }}
            setShowOnboarding={(value) => this.setShow('showOnboarding', value)}
          />
          <Onboarding
            show={this.state.showOnboarding}
            setShow={(value) => this.setShow('showOnboarding', value)}
            setShowLogin={(value) => this.setShow('showLogin', value)}
            setshowRegister={(value) => this.setShow('showRegister', value)}
          />
          <Register
            show={this.state.showRegister}
            setShow={(value) => this.setShow('showRegister', value)}
            setShowLogin={(value) => this.setShow('showLogin', value)}
            setShowOnboarding={(value) => this.setShow('showOnboarding', value)}
            checkout={isRegistration}
          />
          <div onClickCapture={this.checkAuth}>
            <AuthComponent {...this.props} />
          </div>
        </>
      );
    }
  }

  return withRouter(Authenticated);
}
