import styles from './productreview.module.scss';
import Button from '../Button/button';
import Review from '../CustomerReviewDetail';
import { useState } from 'react';
import Addreview from '../Model/AddReview';

export default function ProductReview({
  ratingReview,
  isAuthenticated,
  productSlug,
  reviewMeta,
  allReviewRating,
}) {
  const [show, setShow] = useState(false);

  const getallReview = () => {
    allReviewRating({
      slug: productSlug,
      page: 1,
      per_page: reviewMeta?.total_count,
    });
  };

  return (
    <>
      <div className="mt-5">
        <div className={`align-items-center ${styles.rating_header}`}>
          <span className={styles.product_detail_header}>Customer Reviews</span>
          {isAuthenticated && (
            <Button
              type={'button'}
              class={`${styles.product_review_btn}`}
              name={'add a review'}
              handleCLick={(e) => setShow(true)}
            />
          )}
        </div>
        <Addreview
          show={show}
          setShow={setShow}
          ratingReview={ratingReview}
          productSlug={productSlug}
        />
      </div>
      <div className="custom_scrollbar">
        {ratingReview?.length > 0 ? (
          <>
            <div className={` ${styles.product_h}`}>
              {ratingReview?.map((e, i) => {
                return <Review key={i} ratingReview={e} />;
              })}
            </div>
            {reviewMeta?.total_count >= 3 &&
              reviewMeta?.total_count !== reviewMeta?.count && (
                <div className="">
                  <Button
                    type={'button'}
                    class={styles.viewallreview_btn}
                    name={'view all reviews'}
                    handleCLick={() => getallReview()}
                  />
                </div>
              )}
          </>
        ) : (
          <div className="text-center">No Review Found </div>
        )}
      </div>
    </>
  );
}
