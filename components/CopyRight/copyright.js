import styles from './copyright.module.scss';

export default function CopyRight(props) {
  return (
    <div className={styles[props.className]}>
      <span>Copyright {new Date().getFullYear()} Shiv Shakti</span>
    </div>
  );
}
