import { useEffect, useState } from 'react';
import { Form } from 'react-bootstrap';
import Button from '../Button/button';
import Loader from '../Loader';
import styles from './shippingmethod.module.scss';

export default function ShippingMethod({
  shippingMethod,
  isHydrated,
  shippingDetail,
  doUpdateOrder,
  changeSubmitProgress,
  dis_Loader,
}) {
  const [shipping, setShiping] = useState({});
  useEffect(() => {
    if (isHydrated) shippingDetail();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isHydrated]);

  let defaultData = shippingMethod?.data[0];

  useEffect(() => {
    if (defaultData) {
      setShiping(shippingMethod?.data[0]?._id);
    }
  }, [shippingMethod]);

  const handleUpdateOrder = () => {
    doUpdateOrder({
      //   shipments_attributes: [shipping],
      shipmentmethod: shipping,
      shipmenttotal: 0,
      nextStep: changeSubmitProgress,
      state: 'delivery',
    });
  };

  return (
    <>
      <div className={styles.shipping_header}>Shipping Method</div>
      {shippingMethod?.isloader === true ? (
        <Loader />
      ) : (
        <Form>
          <div key="1" className="mb-3">
            {shippingMethod &&
              shippingMethod?.data?.map((resp, i) => {
                return (
                  <Form.Check
                    key={i}
                    name="group1"
                    type="radio"
                    id={`radio`}
                    label={resp?.name}
                    className={styles.radioButton}
                    defaultChecked={
                      defaultData?._id === resp?._id ? true : false
                    }
                    onChange={(e) => setShiping(resp._id)}
                  />
                );
              })}
          </div>
        </Form>
      )}
      <Button
        class={styles.payment_button}
        type="button"
        name="Continue to Payment"
        handleCLick={handleUpdateOrder}
        disabled={dis_Loader === true ? true : false}
      />
    </>
  );
}
