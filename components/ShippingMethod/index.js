import { connect } from 'react-redux';
import { doUpdateOrder } from '../../redux/actionCreaters/order.actioncreater';
import { shippingDetail } from '../../redux/actionCreaters/shipping.actioncreater';
import ShippingMethod from './shippingmethod';

const mapStateToProps = (state) => {
  return {
    shippingMethod: state.shipping,
    isHydrated: state._persist && state._persist.rehydrated ? true : false,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    shippingDetail: () => dispatch(shippingDetail()),
    doUpdateOrder: (payload) => dispatch(doUpdateOrder(payload)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ShippingMethod);
