import Image from 'next/image';

export default function Logo(props) {
  return (
    <Image
      src={props?.src}
      alt="logo"
      height={props.height || 65}
      width={props.width || 154}
    />
  );
}
