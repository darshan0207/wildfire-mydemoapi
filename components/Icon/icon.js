import styles from './icon.module.scss';
import Profile from '../Profile';
import SidebarMenu from '../SidebarMenu/sidebarmenu';
import Overlay from '../Overlay';
import MyCart from '../Cart';

export default function Icon() {
  return (
    <>
      <Overlay />
      <Profile iconstyle={styles.icon_size} />
      <MyCart iconstyle={styles.icon_size} />
      <SidebarMenu iconstyle={styles.icon_size} />
    </>
  );
}
