import User from '../../public/assets/icons/user.svg';
import { forwardRef } from 'react';
import Image from 'next/image';
import styles from './profile.module.scss';
import { connect } from 'react-redux';

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.auth.isAuthenticated,
  };
};

const ProfileToggle = forwardRef(
  ({ isAuthenticated, children, onClick }, ref) => (
    <div
      className={`${styles.icon_size} ${
        isAuthenticated ? `${styles.active}` : ''
      }`}
    >
      <Image className="pointer" src={User} alt="" />
    </div>
  )
);

ProfileToggle.displayName = 'ProfileToggle';
export default connect(mapStateToProps, null, null, { forwardRef: true })(
  ProfileToggle
);
