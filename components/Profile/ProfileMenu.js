import Image from 'next/image';
import React, { forwardRef } from 'react';
import styles from './profile.module.scss';
import { connect } from 'react-redux';
import { doLogoutUser } from '../../redux/actionCreaters/auth.actioncreater';

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    doLogoutUser: () => dispatch(doLogoutUser()),
  };
};

const ProfileMenu = forwardRef(
  ({ children, style, className, doLogoutUser }, ref) => (
    <div style={style} className={className} ref={ref}>
      <ul className="list-unstyled">
        {React.Children.toArray(children).filter(
          (child) => child.props.children
        )}
        <li>
          <a className={styles.dropdown_logout} onClick={() => doLogoutUser()}>
            Log Out
          </a>
        </li>
      </ul>
    </div>
  )
);

ProfileMenu.displayName = 'ProfileMenu';
export default connect(mapStateToProps, mapDispatchToProps, null, {
  forwardRef: true,
})(ProfileMenu);
