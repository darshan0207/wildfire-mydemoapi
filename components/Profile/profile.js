import withAuth from '../WithAuth';
import { Dropdown } from 'react-bootstrap';
import React, { useState } from 'react';
import styles from './profile.module.scss';
import ProfileMenu from './ProfileMenu';
import ProfileToggle from './ProfileToggle';
import Link from 'next/link';
const MenuItems = [
  {
    title: 'My Account',
    path: '/account/profile',
  },
  {
    title: 'Order History',
    path: '/account/order-history',
  },
  {
    title: 'Address Book',
    path: '/account/address-book',
  },
];
function Profile(props) {
  const [show, setShow] = useState(false);
  return (
    <>
      <div className="dropdown">
        <Dropdown show={show} onClick={() => setShow(!show)}>
          <Dropdown.Toggle as={ProfileToggle} className={props.iconstyle}>
            &bull;&bull;&bull;
          </Dropdown.Toggle>
          <Dropdown.Menu
            show={show}
            as={ProfileMenu}
            className={styles.custom_dropdown_menu}
          >
            {MenuItems.map((item, index) => {
              return (
                <Link href={item.path} key={index} passHref>
                  <li key={item.title}>
                    <a href={item.path}>{item.title}</a>
                  </li>
                </Link>
              );
            })}
          </Dropdown.Menu>
        </Dropdown>
      </div>
    </>
  );
}
export default withAuth(Profile);
