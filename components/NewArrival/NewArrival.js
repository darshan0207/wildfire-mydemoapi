import styles from './NewArrival.module.scss';
import Image from 'next/image';
import PlayImage from '../../public/assets/icons/play.svg';
import { useRef, useState } from 'react';
import VideoPlayer from '../VideoPlayer/VideoPlayer';
import Link from 'next/link';

export default function NewArrival({
  videourl,
  attributes,
  taxons,
  vendor,
  index,
  currentVideoIndex,
  thumbnailurl,
}) {
  const [isplay, setIsPlay] = useState(false);
  const elementRef = useRef(null);
  return (
    <div className="new-arrival-section">
      <div className="container">
        <div className="row">
          <div className="col-lg-6 order-lg-1 order-2">
            <div className="new-arrival-video">
              <div
                className="new-arrival-video-image ratio ratio-16x9"
                ref={elementRef}
              >
                <VideoPlayer
                  url={videourl}
                  isplaying={isplay && (index === 2 ? true : false)}
                  isonPlay={(val) => setIsPlay(val)}
                  index={index}
                  isCurrentPlay={() => currentVideoIndex(2)}
                  thumbnail={thumbnailurl}
                />

                {!isplay &&
                  elementRef?.current?.children &&
                  elementRef?.current?.children[0]?.children &&
                  elementRef?.current?.children[0]?.children[0] &&
                  elementRef?.current?.children[0]?.children[0].className !==
                    'react-player__preview' && (
                    <div className="new-arrival-video-play pointer">
                      <Image
                        src={PlayImage}
                        alt=""
                        className="img-fluid"
                        onClick={() => [setIsPlay(true), currentVideoIndex(2)]}
                      />
                    </div>
                  )}
              </div>
            </div>

            <div className="d-lg-none text-center">
              {vendor?.attributes?.slug && (
                <Link
                  href={`/storefront${
                    process.env.MOBILE_BUILD == 'true' ? '?slug=' : '/'
                  }${vendor?.attributes?.slug}`}
                >
                  <a className="btn btn-outline-primary">view similar video</a>
                </Link>
              )}
            </div>
          </div>

          <div className="col-lg-6 order-lg-2 order-1">
            <div className="new-arrival-content">
              <h4 className="title-md text-uppercase text-dark fw-bold">
                New Arrival
              </h4>

              <p className="small new-arrival-content-text">
                {attributes?.attributes?.seo_description}
              </p>

              <p className="new-arrival-content-category">
                {taxons?.pretty_name}
              </p>

              <div className="new-arrival-content-action">
                {vendor?.attributes?.slug && (
                  <Link
                    href={`/storefront${
                      process.env.MOBILE_BUILD == 'true' ? '?slug=' : '/'
                    }${vendor?.attributes?.slug}`}
                  >
                    <a className="btn btn-outline-primary">
                      view similar video
                    </a>
                  </Link>
                )}
                <p>{vendor?.attributes?.name}</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
