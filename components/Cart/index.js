import MyCart from './cart';
import { connect } from 'react-redux';
import {
  deleteCartDetail,
  getCartDetail,
  updateCartQuantity,
} from '../../redux/actionCreaters/cart.actioncreater';

const mapStateToProps = (state) => {
  return {
    auth: state?.auth?.isAuthenticated,
    guest: state?.auth?.isGuestUser,
    cart: state?.cart,
    isHydrated: state._persist && state._persist.rehydrated ? true : false,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getCartDetail: () => dispatch(getCartDetail()),
    updateCartQuantity: (payload) => dispatch(updateCartQuantity(payload)),
    deleteCartDetail: (payload) => dispatch(deleteCartDetail(payload)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MyCart);
