import styles from './cart.module.scss';
import Bag from '../../public/assets/icons/bag.svg';
import Image from 'next/image';
import { createRef, useEffect } from 'react';
import CartItem from '../CartItem';
import { useRouter } from 'next/dist/client/router';

function MyCart(props) {
  const router = useRouter();
  const isGuest = props?.guest;
  const isAuth = props?.auth;
  const result = props?.cart;
  const navRef = createRef();
  const backRef = createRef();

  const navClose = () => {
    navRef.current.style.right = '-780px';
    backRef.current.style.display = 'none';
    document.body.style.overflow = '';
  };

  const navOpen = () => {
    // if (isAuth) {
    navRef.current.style.right = '0';
    backRef.current.style.display = 'block';
    document.body.style.overflow = 'hidden';
    // }
  };

  useEffect(() => {
    if (props.isHydrated) props?.getCartDetail();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.isHydrated]);

  const handleCount = (itme_id, quantity) => {
    props?.updateCartQuantity({
      line_item_id: itme_id,
      quantity: quantity,
    });
  };

  const HandleDeleteCart = (itme_id) => {
    props?.deleteCartDetail(itme_id);
  };

  const proceedToCheckout = () => {
    navClose();
    let route = isAuth || isGuest ? '/checkout/address' : '/guest';
    router.push(route);
  };

  const HandleProductPage = (slug) => {
    navClose();
    let pathname = '/products/[slug]';
    if (process.env.MOBILE_BUILD == 'true') {
      pathname = '/products';
    }
    router.push({
      pathname,
      query: { slug: slug },
    });
  };
  return (
    <>
      <div className={props?.iconstyle} onClick={() => navOpen()}>
        <span>
          {result?.data?.length > 0 && (
            <span className={`badge-light ${styles.badge_count}`}>
              {result?.data?.length}
            </span>
          )}
          <Image
            className={`pointer`}
            src={Bag}
            alt=""
            height="30px"
            width="24px"
          />
        </span>
      </div>
      <div
        id="show-backdropcart"
        ref={backRef}
        onClick={() => navClose()}
        className={styles.menu_backdrop}
      ></div>
      <div ref={navRef} id="mycartSidenav" className={styles.sidenav}>
        <div className={styles.cart_sidebar}>
          <div className="row p-0 m-0 align-items-center">
            <span className={`col-10 p-0 d-flex`}>
              <span className={styles.cart_header}>My Cart</span>
              <span className={`badge-light ${styles.badge}`}>
                {result?.data ? result?.data?.length : '0'}
              </span>
              <span className={styles.items}>items</span>
            </span>
            <span
              className={`col-2 p-0 text-end align-items-center ${styles.closebtn}`}
              onClick={() => navClose()}
            >
              &times;
            </span>
          </div>
          {result?.data?.length > 0 ? (
            <>
              <CartItem
                line_item={result?.data}
                handleCount={handleCount}
                HandleDeleteCart={HandleDeleteCart}
                handlePageRoute={HandleProductPage}
              />
              <div className="row p-0 m-0 mt-4 pb-2">
                <span className={`col-10 p-0 ${styles.total}`}>Sub Total</span>
                <span className={`col-2 p-0 text-end ${styles.total}`}>
                  ${result?.itemtotal?.toFixed(2)}
                </span>
              </div>
              <div className="row p-0 m-0 pb-2">
                <span className={`col-10 p-0 ${styles.total}`}>
                  Promo Discount
                </span>
                <span className={`col-2 p-0 text-end ${styles.total}`}>
                  {result?.data?.attributes?.display_promo_total}
                </span>
              </div>
              <div className="row p-0 m-0 pb-2">
                <span className={`col-10 p-0 ${styles.total}`}>Shipping</span>
                <span className={`col-2 p-0 text-end ${styles.total}`}>
                  {result?.data?.attributes?.display_ship_total}
                </span>
              </div>
              <div className="row p-0 m-0 pb-2">
                <span className={`col-10 p-0 ${styles.total}`}>Tax</span>
                <span className={`col-2 p-0 text-end ${styles.total}`}>
                  {result?.data?.attributes?.display_tax_total}
                </span>
              </div>
            </>
          ) : (
            <div className="text-center pt-4">
              <h3>Your cart is empty</h3>
            </div>
          )}
        </div>
        {result?.data?.length > 0 && (
          <div className="row p-0 m-0">
            <div className={`col-12 ${styles.sidenav_footer}`}>
              <hr className={styles.custom_hr} />
              <button
                className={`${styles.chekout_btn}`}
                onClick={proceedToCheckout}
              >
                <div className="d-flex justify-content-between">
                  <div className={` ${styles.btn_title}`}>
                    Proceed to check out
                  </div>
                  <div>
                    <span className={`${styles.button_price}`}>
                      ${result?.itemtotal?.toFixed(2)}
                    </span>
                    <i
                      className={`fa fa-angle-right ${styles.btn_icon}`}
                      aria-hidden="true"
                    ></i>
                  </div>
                </div>
              </button>
            </div>
          </div>
        )}
      </div>
    </>
  );
}
export default MyCart;
