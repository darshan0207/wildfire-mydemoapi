import {
  CardNumberElement,
  CardCvcElement,
  CardExpiryElement,
  useElements,
  useStripe,
} from '@stripe/react-stripe-js';
import styles from './splitform.module.scss';
import React, { useImperativeHandle, useRef } from 'react';
const SplitForm = ({ doUpdateOrder, methodId, changeSubmitProgress, ref1 }) => {
  const stripeformRef = useRef(null);
  const elements = useElements();
  const stripe = useStripe();

  const handleSubmit = async (event) => {
    event.preventDefault();
    if (!stripe || !elements) {
      return;
    }
    const cardNumberElement = elements?.getElement(CardNumberElement);
    const { error, paymentMethod } = await stripe.createPaymentMethod({
      type: 'card',
      card: cardNumberElement,
    });
    if (!error) {
      console.log('Stripe 23 | token generated!', paymentMethod);
      //send token to backend here

      try {
        const { id } = paymentMethod;
        doUpdateOrder({
          paymentmethod: methodId,
          paymentid: id,
          nextStep: changeSubmitProgress,
          state: 'payment',
        });
        console.log('Stripe 35 | data', response.data.success);
        if (response.data.success) {
          console.log('CheckoutForm.js 25 | payment successful!');
        }
      } catch (error) {
        console.log('CheckoutForm.js 28 | ', error);
      }
    } else {
      console.log(error.message);
    }
    // const res = await stripe.createToken(cardNumberElement);
    // console.log(res);
    // if (res.token) {
    //   doUpdateOrder({
    //     // order: {
    //     //   payments_attributes: [
    //     //     {
    //     //       payment_method_id: methodId,
    //     //     },
    //     //   ],
    //     // },
    //     // payment_source: {
    //     //   [methodId]: {
    //     //     number: res.token.card.last4,
    //     //     month: res.token.card.exp_month,
    //     //     year: res.token.card.exp_year,
    //     //     name: event.target.username.value,
    //     //     gateway_payment_profile_id: res.token.id,
    //     //   },
    //     // },
    //     paymentid: res.token.id,
    //     nextStep: changeSubmitProgress,
    //     state: 'payment',
    //   });
    //   changeSubmitProgress();
    // }
  };
  const stripeSubmit = () => {
    stripeformRef.current.click();
  };

  useImperativeHandle(ref1, () => ({
    stripeSubmit: stripeSubmit,
  }));

  return (
    <>
      <div className="col-md-12">
        <div className={styles.payment_form}>
          <form onSubmit={(e) => handleSubmit(e)}>
            <label>Name</label>
            <input
              name="username"
              type="text"
              placeholder="Enter your name"
              required
              className={`${styles.card_input_Field} ${styles.input_Field}`}
            />
            <label>Card number</label>
            <CardNumberElement className={styles.card_input_Field} />
            <div className="row">
              <div className="col-md-6">
                <label>Expiration date</label>
                <CardExpiryElement className={styles.card_input_Field} />
              </div>
              <div className="col-md-6">
                <label>CVC</label>
                <CardCvcElement className={styles.card_input_Field} />
              </div>
            </div>
            <input
              type="submit"
              style={{ display: 'none' }}
              ref={stripeformRef}
            />
          </form>
        </div>
      </div>
    </>
  );
};

export default SplitForm;
