import Image from 'next/image';
import styles from './about.module.scss';
import AboutImage from '../../public/assets/images/about.png';
import AboutImageMoblie from '../../public/assets/images/about-mobile.png';
import FeatureImage from '../../public/assets/icons/feature.svg';

export default function About() {
  return (
    <div className="about_section">
      <div className="container">
        <div className="row no-gutter">
          <div className="col-lg-6 col-8">
            <h1
              className={`${styles.title_xl} fw-bold text-capitalize text-dark`}
            >
              The Ultimate <br />
              Social Shopping Experience!
            </h1>
          </div>

          <div className="col-lg-5">
            <div className={styles.about_content}>
              <h2>ABOUT US</h2>

              <p>
                MOVE OVER HSN & QVC, WILDFIRE+ IS THE ULTIMATE VIDEO DRIVEN
                SOCIAL E-COMMERCE EXPERIENCE DESIGNED AS THE PREMIERE
                DESTINATION TO WATCH AND SHOP UNIQUE AND INNOVATIVE BRANDS VIA A
                CONSTANT STREAM OF SHOPPABLE LIFESTYLE VIDEO CONTENT. WATCH
                ENGAGING TV SHOWS THAT EXPLORE THE ORIGINS OF THE COMPANIES,
                INTRODUCE YOU TO THE BRILLIANT MINDS THAT HAVE CREATED SOME OF
                YOUR FAVORITE PRODUCTS, SHOW HOW PRODUCTS ARE MADE, AND PROVIDE
                EXPERT ADVICE FROM INDUSTRY PROFESSIONALS AND MORE.
              </p>
              <p>
                AS A WILDFIRE+ MEMBER, INDULGE IN DEMONSTRATIONS OF THE PRODUCTS
                VIA POP-UP SHOWS, ETC. PURCHASE, COMMENT, LIKE AND SHARE YOUR
                FAVORITES WITH A CLICK OF A BUTTON.
              </p>
              <p>
                FOR BRAND OWNERS, WILDFIRE+ ALLOWS ENTREPRENEURS THE ABILITY TO
                POSITION THEIR PRODUCTS ON A GLOBAL PLATFORM IN A SECURE
                ENVIRONMENT AND SCALE THEIR BUSINESS BY PROVIDING THE TECHNOLOGY
                TO THRIVE, THE DATA TO MARKET AND SELL EFFECTIVELY AND A
                PLATFORM TO CONNECT WITH A GLOBAL AUDIENCE.WELCOME TO THE NEW
                WAVE.
              </p>
            </div>
          </div>
        </div>

        <div className="col-lg-12">
          <div className={styles.about_image}>
            <div className="img-fluid d-lg-block d-none mx-auto text-center">
              <Image src={AboutImage} alt="" />
            </div>
            <div className="img-fluid d-lg-none d-block mx-auto text-center">
              <Image src={AboutImageMoblie} alt="" />
            </div>
          </div>
        </div>

        <div className="col-lg-12">
          <div className="row">
            <div className="col-lg-4">
              <div className={styles.about_feature}>
                <div className="about_feature_image img-fluid mx-auto">
                  <Image src={FeatureImage} alt="" />
                </div>
                <div className="about_feature_content">
                  <h4>Trendy Products</h4>
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                    do eiusmod tempor incididunt ut labore et dolore magna
                    aliqua. Ut enim ad minim veniam
                  </p>
                </div>
              </div>
            </div>

            <div className="col-lg-4">
              <div className={styles.about_feature}>
                <div className="about_feature_image img-fluid mx-auto">
                  <Image src={FeatureImage} alt="" />
                </div>
                <div className="about_feature_content">
                  <h4>Top BRANDS</h4>
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                    do eiusmod tempor incididunt ut labore et dolore magna
                    aliqua. Ut enim ad minim veniam
                  </p>
                </div>
              </div>
            </div>

            <div className="col-lg-4">
              <div className={styles.about_feature}>
                <div className="about_feature_image img-fluid mx-auto">
                  <Image src={FeatureImage} alt="" />
                </div>
                <div className="about_feature_content">
                  <h4>UNIQUE SHOPPING EXPERIENCE</h4>
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                    do eiusmod tempor incididunt ut labore et dolore magna
                    aliqua. Ut enim ad minim veniam
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
