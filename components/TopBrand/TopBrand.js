import WithLogin from '../WithLogin';
import styles from './TopBrand.module.scss';
import Image from 'next/image';
import star from '../../public/assets/icons/star.svg';
// import UserImage from '../../public/assets/images/user.jpg';
import banner03 from '../../public/assets/images/banner03.jpg';
import PlayImage from '../../public/assets/icons/play.svg';
import VideoPlayer from '../VideoPlayer/VideoPlayer';
import { useRef, useState } from 'react';
import myLoader from '../../common/loader';

export default function TopBrand({
  isAuth,
  vendor,
  videourl,
  review,
  index,
  currentVideoIndex,
  thumbnailurl,
  userimage,
}) {
  const [isplay, setIsPlay] = useState(false);
  const elementRef = useRef(null);
  return (
    <div className="top-brand-section">
      <div className="container">
        <div className="row">
          <div className="col-lg-5">
            <div className="top-brand-content">
              <h4 className="title-md text-uppercase text-dark fw-bold">
                Top brands
              </h4>

              <p className="small top-brand-text">{vendor?.name}</p>
              <div className="top-brand-celebrity">
                <div className="top-brand-celebrity-rating">
                  <div className="img-fluid d-inline-block align-sub">
                    <Image src={star} alt="" className="img-fluid" />
                  </div>
                  <span className="ms-2">{review?.rating}/5</span>
                </div>

                <h4 className="top-brand-celebrity-comment title-md text-dark">
                  {review?.review}
                  {/* Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod tempor incididunt ut labore et dolore aliqua.{' '} */}
                </h4>
              </div>
              <div className="top-brand-card">
                <div className="top-brand-card-image">
                  {userimage ? (
                    <Image
                      loader={myLoader}
                      src={userimage}
                      alt=""
                      className="img-fluid"
                      height={48}
                      width={48}
                    />
                  ) : (
                    <div className={styles.rounded_img}></div>
                  )}
                </div>
                <div className="top-brand-card-content text-truncate">
                  <h4 className="text-truncate"> {review?.name}</h4>
                  {/* <p className="text-truncate">Fashion Desinger/ ELBARTO </p> */}
                </div>
              </div>
            </div>
          </div>

          <div className="col-lg-7">
            <div className="top-brand-video">
              <div
                className="top-brand-video-image  ratio ratio-16x9"
                ref={elementRef}
              >
                {/* <img
                  src="assets/images/banner03.jpg"
                  className="img-fluid"
                  alt=""
                /> */}
                <VideoPlayer
                  url={videourl}
                  isplaying={isplay && (index === 1 ? true : false)}
                  isonPlay={(val) => setIsPlay(val)}
                  index={index}
                  isCurrentPlay={() => currentVideoIndex(1)}
                  thumbnail={thumbnailurl}
                />
                {!isplay &&
                  elementRef?.current?.children &&
                  elementRef?.current?.children[0]?.children &&
                  elementRef?.current?.children[0]?.children[0] &&
                  elementRef?.current?.children[0]?.children[0].className !==
                    'react-player__preview' && (
                    <div className="new-arrival-video-play pointer">
                      <Image
                        src={PlayImage}
                        alt=""
                        className="img-fluid"
                        onClick={() => [setIsPlay(true), currentVideoIndex(1)]}
                      />
                    </div>
                  )}
                {/* <a href="#" className="top-brand-video-play">
                  <Image src={PlayImage} alt="" className="img-fluid" />
                </a> */}
              </div>

              {isAuth === false && !isplay && (
                <div className="top-brand-video-content">
                  <p>Join The Best network!</p>
                  <a className="btn btn-sm btn-primary">
                    <WithLogin title="join now" />
                  </a>
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
