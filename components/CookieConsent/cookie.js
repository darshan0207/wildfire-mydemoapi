import CookieConsent from 'react-cookie-consent';
import styles from './cookie.module.scss';
import Image from 'next/image';
import CookieIcon from '../../public/assets/icons/cookie.svg';
import fireMbolie from '../../public/assets/icons/cookiemobile.svg';
import fireIcon from '../../public/assets/icons/fire.svg';

export default function Cookie() {
  return (
    <>
      <div className="d-none d-md-block d-lg-block d-xl-block d-sm-none">
        <div className="row">
          <div className="col-md-8">
            <CookieConsent
              location="bottom"
              buttonText="ALLOW"
              cookieName="myAwesomeCookieName2"
              containerClasses={styles.content_container}
              buttonClasses={styles.content_allow_button}
              declineButtonClasses={styles.content_decline_button}
              expires={150}
              enableDeclineButton
              flipButtons
              declineButtonText="DECLINE"
              contentStyle={{ margin: 8 }}
            >
              <div className="row">
                <div
                  className={`col-md-2 position-relative ${styles.content_img_top}`}
                >
                  <Image src={CookieIcon} alt="" width={54} height={54} />
                </div>
                <div
                  className={`col-md-6 ${styles.content_text} ps-0 pe-0 position-relative pt-4`}
                >
                  We use third-party{' '}
                  <span className={styles.content_span}>cookies</span> in order
                  to personalize your site experience.
                </div>
                <div className="col-md-4 d-flex justify-content-center">
                  <div className={styles.content_fire}>
                    <Image src={fireIcon} alt="" />
                  </div>
                </div>
              </div>
            </CookieConsent>
          </div>
        </div>
      </div>
      <div className="d-block d-sm-block d-md-none d-lg-none d-xl-none">
        <div className="row">
          <div className="col-sm-12">
            <CookieConsent
              location="bottom"
              buttonText="ALLOW"
              cookieName="myAwesomeCookieName1"
              containerClasses={styles.content_container_mobile}
              buttonClasses={styles.content_allow_button_mobile}
              contentStyle={{ margin: '0' }}
              expires={150}
            >
              <div className={`${styles.content_mobile} d-flex`}>
                <div className="me-3">
                  <Image src={CookieIcon} alt="" />
                </div>
                <div className={styles.content_text_mobile}>
                  <div className={styles.content_fire_mobile}>
                    <Image src={fireMbolie} alt="" />
                  </div>
                  We use third-party{' '}
                  <span className={styles.content_span}>cookies</span> in order
                  to personalize your site experience.
                </div>
              </div>
            </CookieConsent>
          </div>
        </div>
      </div>
    </>
  );
}
