import styles from './accountmenumobile.module.scss';
import Link from 'next/link';
import { createRef, useEffect } from 'react';
import { ReactSVG } from 'react-svg';

export default function AccountMenuMobile({
  items = [],
  active = null,
  doLogoutUser,
  userProfile,
  doAccountInfo,
  isHydrated,
}) {
  const navRef = createRef();
  const backRef = createRef();

  const navClose = () => {
    navRef.current.style.left = '-276px';
    backRef.current.style.display = 'none';
  };

  const navOpen = () => {
    navRef.current.style.left = '0';
    backRef.current.style.display = 'block';
  };

  useEffect(() => {
    if (
      isHydrated &&
      !userProfile?.attributes?.first_name &&
      !userProfile?.attributes?.last_name
    ) {
      doAccountInfo();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [doAccountInfo, isHydrated]);

  return (
    <>
      <ReactSVG
        onClick={() => navOpen()}
        className="position-absolute mt-4"
        src="\assets\icons\hamburger_account.svg"
      />
      <div
        id="show-backdrop"
        ref={backRef}
        onClick={() => navClose()}
        className={styles.menu_backdrop}
      ></div>
      <div ref={navRef} id="myAccountSidenav" className={styles.sidenav}>
        <div className={`${styles.rectangle} d-flex justify-content-between`}>
          <div className="mt-3">
            <div className={styles.menu_welcome}>Welcome</div>
            {userProfile?.attributes?.first_name &&
              userProfile?.attributes?.last_name && (
                <div
                  className={styles.menu_name}
                >{`${userProfile?.attributes?.first_name} ${userProfile?.attributes?.last_name}`}</div>
              )}
          </div>
          <div className={styles.btnclose} onClick={() => navClose()}>
            &times;
          </div>
        </div>

        {items.map((item, index) => {
          return (
            <Link
              href={item.path}
              data-test="accountMenuLink"
              data-test-id={item.path}
              key={index}
            >
              <a className="text-decoration-none" onClick={() => navClose()}>
                <div
                  className={`${styles.menuitem} ${
                    item.path === active ? styles.active : ''
                  }`}
                >
                  {item.title}
                </div>
              </a>
            </Link>
          );
        })}
        <div className={`${styles.rectangle}`}></div>
        <div className="d-grid gap-2">
          <button
            className={styles.btnlogout}
            type="button"
            onClick={() => doLogoutUser()}
          >
            logout
            <ReactSVG className="ps-1" src="\assets\icons\logout.svg" />
          </button>
        </div>
      </div>
    </>
  );
}
