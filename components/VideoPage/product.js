import Image from 'next/image';
import { useRouter } from 'next/router';
import myLoader from '../../common/loader';
import ProductDescription from '../ProductDescription/productdescription';
import Styles from './index.module.scss';

export default function Product(props) {
    const router = useRouter();
    const handleClick = (slug) => {
        let pathname =process.env.MOBILE_BUILD == 'true'? '/products': '/products/[slug]';
        router.push({
          pathname,
          query: { slug: slug },
        });
    };
    
    return (
        <div className={Styles.product_image}>
        <Image  loader={myLoader} src={`${props?.data?.imageURL}`} height={200} width={269} onClick={() => handleClick(props?.data?.attributes?.slug)}  objectFit={ 'contain'} alt={props?.data?.attributes?.name}/>
        <div  className={Styles.related_item_title} >
            {props?.data?.attributes?.name}
            </div>
            <div className={Styles.related_item_desc}>
             <ProductDescription description={props?.data?.attributes?.description} count={50} />
            </div>
      </div>
    )
}
