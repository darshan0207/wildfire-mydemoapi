import { useState, useEffect } from 'react';
import { Tabs, Tab } from 'react-bootstrap';
import Taxonomy from '../Merchant/taxonomy';
import VideoPlayer from '../VideoPlayer/VideoPlayer';
import Styles from './index.module.scss';
import Product from './product';
import Review from './Review';
import {
  FacebookShareButton,
  TwitterShareButton,
  LinkedinShareButton,
} from 'next-share';
import { useRouter } from 'next/router';
import Addreview from '../Model/AddReview/index';
import Image from 'next/image';
import myLoader from '../../common/loader';
import ProductDescription from '../ProductDescription/productdescription';

export default function VideoPage(props) {
  const [show, setShow] = useState(false);
  const router = useRouter();
  const URL = window.location.href;
  useEffect(() => {
    viewAll();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  const viewAll = (perpage) => {
    props.getVideoReview({
      per_page: perpage,
      id: props?.videoDetails?.data?.id,
    });
  };
  const handleClick = () => {
    let pathname =
      process.env.MOBILE_BUILD == 'true' ? '/storefront' : '/storefront/[slug]';
    router.push({
      pathname,
      query: { slug: props?.videoDetails?.data?.attributes?.slug },
    });
  };

  const handleProductClick = (slug) => {
    let pathname =
      process.env.MOBILE_BUILD == 'true' ? '/products' : '/products/[slug]';
    router.push({
      pathname,
      query: { slug: slug },
    });
  };

  return (
    <>
      <div
        className={Styles.store_banner_section}
        onClick={handleClick}
        style={{
          backgroundImage: `url(${process.env.API_URL}${props?.videoDetails?.data?.attributes?.banner})`,
        }}
      ></div>
      <div className={`${Styles.container_section}`}>
        <div className="row ms-0 me-0">
          <div className="col-lg-6">
            <div className={Styles.store_list_item_image}>
              <VideoPlayer
                url={props?.videoDetails?.data?.attributes?.video}
                // thumbnail={props?.videoDetails?.data?.attributes?.thumbnail}
                isplaying={true}
                islight={true}
                ismuted={false}
              />
              <div className={Styles.video_section}>
                <div className={Styles.video_caption}>
                  {props?.videoDetails?.data?.attributes?.name}
                </div>
                <div className={Styles.store_list_info_detail_social}>
                  <FacebookShareButton url={URL}>
                    <Image
                      src="/assets/icons/facebook-dark.svg"
                      alt="logo"
                      height={23}
                      width={23}
                    />
                  </FacebookShareButton>
                  <TwitterShareButton url={URL}>
                    <Image
                      src="/assets/icons/twitter-dark.svg"
                      alt="logo"
                      height={23}
                      width={23}
                    />
                  </TwitterShareButton>
                  <LinkedinShareButton url={URL}>
                    <Image
                      src="/assets/icons/linkedin-dark.svg"
                      alt="logo"
                      height={23}
                      width={23}
                    />
                  </LinkedinShareButton>
                </div>
              </div>
              <div className={Styles.taxons}>
                <Taxonomy
                  taxons={props?.videoDetails?.data?.attributes?.taxon}
                />
              </div>
              <div
                className={Styles.video_desc}
                dangerouslySetInnerHTML={{
                  __html: props?.videoDetails?.data?.attributes?.description,
                }}
              ></div>
              <div className="store_list_info_detail_category">
                <Tabs defaultActiveKey="0" className={Styles.spacebetween}>
                  <Tab eventKey={0} title={'PRODUCT DETAILS'}>
                    <div className="d-block d-sm-none">
                      {props?.videoDetails?.data?.relationships?.primary_product
                        ?.data?.id && (
                        <div className={Styles.product_panel}>
                          {props?.videoDetails &&
                            props?.videoDetails?.data?.attributes?.products
                              .length > 0 &&
                            props?.videoDetails?.data?.attributes?.products
                              ?.filter(
                                (data) =>
                                  data.id ===
                                  props?.videoDetails?.data?.relationships
                                    ?.primary_product?.data?.id
                              )
                              .map((item, index) => (
                                <div
                                  className={Styles.product_image}
                                  key={index}
                                >
                                  <div className="row mb-3">
                                    <div className="col-md-4 d-flex justify-content-center">
                                      <Image
                                        loader={myLoader}
                                        src={item?.imageURL}
                                        height={200}
                                        width={200}
                                        objectFit={'contain'}
                                        alt=""
                                        onClick={() =>
                                          handleProductClick(
                                            item?.attributes?.slug
                                          )
                                        }
                                      />
                                    </div>
                                    <div className="col-md-5 mt-2">
                                      <h4 className={Styles.related_item_title}>
                                        {item?.attributes?.name}
                                      </h4>
                                      <div
                                        className={`${Styles.related_item_desc} w-100`}
                                      >
                                        <ProductDescription
                                          description={
                                            item?.attributes?.description
                                          }
                                          count={150}
                                        />
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              ))}
                        </div>
                      )}
                    </div>
                    <div className="d-none d-sm-block">
                      {props?.videoDetails?.data?.relationships?.primary_product
                        ?.data?.id && (
                        <div className={Styles.product_panel}>
                          {props?.videoDetails &&
                            props?.videoDetails?.data?.attributes?.products
                              .length > 0 &&
                            props?.videoDetails?.data?.attributes?.products
                              ?.filter(
                                (data) =>
                                  data.id ===
                                  props?.videoDetails?.data?.relationships
                                    ?.primary_product?.data?.id
                              )
                              .map((item, index) => (
                                <div
                                  className={Styles.product_image}
                                  key={index}
                                >
                                  {index % 2 === 0 && (
                                    <div className="row mb-3">
                                      <div className="col-md-4">
                                        <Image
                                          loader={myLoader}
                                          src={item?.imageURL}
                                          height={200}
                                          width={200}
                                          objectFit={'contain'}
                                          alt=""
                                          onClick={() =>
                                            handleProductClick(
                                              item?.attributes?.slug
                                            )
                                          }
                                        />
                                      </div>
                                      <div className="col-md-5 mt-4">
                                        <h4
                                          className={Styles.related_item_title}
                                        >
                                          {item?.attributes?.name}
                                        </h4>
                                        <div
                                          className={`${Styles.related_item_desc} w-100`}
                                        >
                                          <ProductDescription
                                            description={
                                              item?.attributes?.description
                                            }
                                            count={150}
                                          />
                                        </div>
                                      </div>
                                    </div>
                                  )}
                                  {index % 2 === 1 && (
                                    <div className="row mb-3">
                                      <div className="col-md-5 mt-4">
                                        <h4
                                          className={Styles.related_item_title}
                                        >
                                          {item?.attributes?.name}
                                        </h4>
                                        <div
                                          className={`${Styles.related_item_desc} w-100`}
                                        >
                                          <ProductDescription
                                            description={
                                              item?.attributes?.description
                                            }
                                            count={150}
                                          />
                                        </div>
                                      </div>
                                      <div className="col-md-4">
                                        <Image
                                          loader={myLoader}
                                          src={item?.imageURL}
                                          height={200}
                                          width={200}
                                          objectFit={'contain'}
                                          alt=""
                                          onClick={() =>
                                            handleProductClick(
                                              item?.attributes?.slug
                                            )
                                          }
                                        />
                                      </div>
                                    </div>
                                  )}
                                </div>
                              ))}
                        </div>
                      )}
                    </div>
                  </Tab>
                  <Tab eventKey={1} title={'COMMENTS'}>
                    <div className={Styles.product_panel}>
                      <div className={Styles.video_section}>
                        <div className={Styles.customer_comment}>
                          Customer COMMENTS
                        </div>
                        {props?.isAuthenticated && (
                          <div
                            className={Styles.customer_comment_btn}
                            onClick={(e) => setShow(true)}
                          >
                            add a COMMENT
                          </div>
                        )}
                        <Addreview
                          show={show}
                          setShow={setShow}
                          video={router?.query?.slug}
                        />
                      </div>
                      {props?.videoDetails &&
                        props?.videoDetails?.review?.data?.length > 0 &&
                        props?.videoDetails?.review?.data?.map((item) => (
                          <Review data={item} key={item.id} />
                        ))}

                      {props?.videoDetails?.review?.meta?.total_count !==
                        props?.videoDetails?.review?.meta?.count && (
                        <div className={Styles.view_all_section}>
                          <span
                            className={Styles.view_all}
                            onClick={() =>
                              viewAll(
                                props?.videoDetails?.review?.meta?.total_count
                              )
                            }
                          >
                            {' '}
                            VIEW ALL COMMENTS{' '}
                          </span>
                        </div>
                      )}
                    </div>
                  </Tab>
                </Tabs>
              </div>
            </div>
          </div>
          <div className="col-lg-6">
            <div className={Styles.related_item}>RELATED ITEMS</div>
            <div className={Styles.grid_container}>
              {props?.videoDetails &&
                props?.videoDetails?.data?.attributes?.products.length > 0 &&
                props?.videoDetails?.data?.attributes?.products?.map(
                  (item, index) => <Product data={item} key={index} />
                )}
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
