import Image from 'next/image';
import Styles from './index.module.scss';
import customer from '../../public/assets/icons/user-profile.svg';
import myLoader from '../../common/loader';
export default function Review(props) {
  return (
    <div className={Styles.d_flex}>
      <div className={`${Styles.custom_images}`}>
        {props?.data?.original_url ? (
          <Image
            loader={myLoader}
            src={props?.data?.original_url}
            alt=""
            height="100%"
            width="100%"
            className="rounded-circle"
          />
        ) : (
          <Image
            src={customer}
            alt=""
            height="100%"
            width="100%"
            className="rounded-circle"
          />
        )}
      </div>
      <div className={Styles.comment_section}>
        <span className={Styles.comment_name}>
          {props?.data?.attributes?.name}
        </span>
        {/* <span className={Styles.comment_sirname}>(Ghana)</span> */}
        <div className={Styles.comment}>{props?.data?.attributes?.review}</div>
        {/* <div  className={`${Styles.video_section} pt-2`} >
              <div  className={Styles.video_section}>
                  <div  >
                    <Image src="/assets/icons/thumsup.svg" height={15} width={15} />
                    <span className={Styles.likes}>15</span>
                  </div>
                  <div>
                    <Image src="/assets/icons/thumsdown.svg" height={15} width={15} />
                    <span className={Styles.likes}>15</span>
                  </div>
              </div>
              <div className={Styles.view_more}>
                  View 12 replies
              </div>
            </div> */}
      </div>
    </div>
  );
}
