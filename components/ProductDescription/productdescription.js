import styles from './productdescription.module.scss';
import { useState } from 'react';

export default function ProductDescription({ description,count=130 }) {
  const [isHidden, setIsHidden] = useState(false);
  return (
    <>
      {description && (
        <>
          <div
            dangerouslySetInnerHTML={{
              __html: !isHidden
                ? description?.slice(0, count).concat('...')
                : description,
            }}
          ></div>
          <span
            className={styles.read_more}
            onClick={(e) => setIsHidden((isHidden) => !isHidden)}
          >
            Read {!isHidden ? 'More' : 'Less'}
          </span>
        </>
      )}
    </>
  );
}
