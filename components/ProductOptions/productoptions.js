import styles from './productoptions.module.scss';
import { useEffect, useState } from 'react';
import { OverlayTrigger, Tooltip } from 'react-bootstrap';
export default function ProductOptions({
  name,
  optionType,
  onOptionTypeChange,
}) {
  const [optionFilter, setOptionFilter] = useState({});
  useEffect(() => {
    setOptionFilter({});
  }, [name]);

  useEffect(() => {
    if (optionType?.length) {
      optionType.map((data, index) => {
        if (data.optionvalue.length === 1) {
          optionFilter[data.name] = data.optionvalue[0].id;
          setOptionFilter({ ...optionFilter });
          optionFilter['position'] = data.position;
          onOptionTypeChange(optionFilter);
        } else {
          if (!data.optionvalue.length) {
            onOptionTypeChange(optionFilter);
          }
        }
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [optionType]);

  const handleOptionClick = (name, id, position, index) => {
    optionFilter[name] = id;
    Object.keys(optionFilter).map((key, index) => {
      if (index + 1 > position) {
        delete optionFilter[key];
      }
    });
    setOptionFilter({ ...optionFilter });
    optionFilter['position'] = position;
    onOptionTypeChange(optionFilter);
  };

  const handleProductVariants = (name, index) => {
    if (optionType[index - 1] && optionType[index - 1].name) {
      if (!optionFilter[optionType[index - 1].name]) {
        delete optionFilter[name];
        return true;
      }
      return false;
    }
    return false;
  };

  return (
    <>
      {optionType?.map((item, index) => {
        return (
          <div key={index} className="mb-2">
            {optionType?.length > 0 && (
              <div key={index} className={styles.product_detail_header}>
                {item?.name} :
              </div>
            )}

            <fieldset
              id={`group${index}`}
              key={`d${index}`}
              className={`${styles.fieldset_option}`}
            >
              {item?.optionvalue?.map((element, i) => {
                return item?.name?.toLowerCase() === 'color' ? (
                  <OverlayTrigger
                    key={i}
                    placement="top"
                    overlay={
                      <Tooltip id={`tooltip-${i}`}>{element?.name}</Tooltip>
                    }
                  >
                    <button
                      key={i}
                      type="radio"
                      name={`group${index}`}
                      className={`btn me-2 ${styles.product_size_btn} ${
                        optionFilter[item.name] === element?._id
                          ? styles.active
                          : ''
                      }`}
                      style={{
                        background: `${element?.presentation}`,
                      }}
                      onClick={(e) =>
                        handleOptionClick(
                          item?.name,
                          element?._id,
                          item.position,
                          index
                        )
                      }
                      disabled={handleProductVariants(item?.name, index)}
                      // disabled={element.isactive}
                    ></button>
                  </OverlayTrigger>
                ) : (
                  <button
                    key={i}
                    type="radio"
                    name={`group${index}`}
                    className={`btn me-2 ${styles.counter_btn} ${
                      optionFilter[item.name] === element?._id
                        ? styles.counter_btn_active
                        : ''
                    }`}
                    onClick={(e) =>
                      handleOptionClick(
                        item?.name,
                        element?._id,
                        item.position,
                        index
                      )
                    }
                    // disabled={element.isactive}
                    disabled={handleProductVariants(item?.name, index)}
                  >
                    {element?.presentation}
                  </button>
                );
              })}
            </fieldset>
          </div>
        );
      })}
    </>
  );
}
