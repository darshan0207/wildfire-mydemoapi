import Link from 'next/link';
import styles from './usefullinks.module.scss';

export default function UsefulLinks() {
  return (
    <div className={styles.nav_section}>
      <Link href={'/about'} passHref>
        <p className={styles.nav_section_content}>About us</p>
      </Link>
      <Link href={'/contact-us'} passHref>
        <p className={styles.nav_section_content}>Contact Us</p>
      </Link>
      <Link href={'/terms'} passHref>
        <p className={styles.nav_section_content}>Terms & Conditions</p>
      </Link>
      <Link href={'/privacy'} passHref>
        <p className={styles.nav_section_content}>Privacy Policy</p>
      </Link>
    </div>
  );
}
