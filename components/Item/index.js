import Item from './Item';
import { connect } from 'react-redux';
import {
  addProductToWishlist,
  removeProductToWishlist,
} from '../../redux/actionCreaters/order.actioncreater';

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    addProductTOWishlist: (payload) => dispatch(addProductToWishlist(payload)),
    removeProductTowishlist: (payload) =>
      dispatch(removeProductToWishlist(payload)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Item);