import styles from './Item.module.scss';
import Image from 'next/image';
import { Badge } from 'react-bootstrap';
import myLoader from '../../common/loader';

export default function Item(props) {
  const handleProductView = () => {
    props?.handleClick(props?.slug);
  };
  const variantId = props?.item?.relationships?.primary_variant?.data?.id;
  // const addWishlistProduct = (varID) => {
  //   props?.addProductTOWishlist({
  //     wished_product: {
  //       variant_id: varID,
  //       // remark: 'I want this Product too',
  //       quantity: 1,
  //     },
  //   });
  // };

  return (
    <>
      <div
        className={
          props?.oveerly_serach
            ? styles[props?.oveerly_serach]
            : styles[props?.product_wishlist]
        }
      >
        {!props?.product_wishlist && props?.isAuth && (
          <div
            className={styles.product_icon_heart}
            onClick={() => {
              props?.addProductTOWishlist({ productId: props?.item?._id });
            }}
          >
            <i
              className={`pointer fa ${
                props?.wished_products?.length > 0 &&
                props?.wished_products[0] === props?.item?._id
                  ? 'fa-heart'
                  : 'fa-heart-o'
              }`}
              aria-hidden="true"
            ></i>
          </div>
        )}
        <div className={styles.search_product_item} onClick={handleProductView}>
          <div className={styles.product_image}>
            {props?.data_Image_Id && (
              <Image
                // loader={myLoader}
                src={`${props?.include_Image}`}
                alt=""
                width={props?.width || '200px'}
                height={props?.height || '166px'}
                objectFit={'contain'}
              />
            )}
            {/* {props && props?.original_Price && (
              <div className={styles.badge_box}>
                {parseFloat(props?.compare_Price) !== 0 && (
                  <Badge bg={`danger ${styles.badge}`}>
                    {(
                      ((props?.compare_Price - props?.price) /
                        props?.compare_Price) *
                      100
                    ).toFixed(0)}
                    % OFF
                  </Badge>
                )}
              </div>
            )} */}
          </div>
          <div className={styles.product_detail}>
            {props?.title && (
              <p className={styles.product_title}>
                {props?.title?.slice(0, 20).concat('...')}
              </p>
            )}
            {props?.description && (
              <p
                className={styles.product_text}
                dangerouslySetInnerHTML={{
                  __html: props?.description?.slice(0, 28).concat('...'),
                }}
              ></p>
            )}
            {props?.display_Price && (
              <div className={styles.product_price}>
                <span>
                  Price :
                  <span className={styles.price}>{props?.display_Price}</span>
                </span>
                {props?.compare_Price && props?.compare_Price > 0 ? (
                  <small>{props?.original_Price}</small>
                ) : (
                  ''
                )}
              </div>
            )}
          </div>
        </div>
      </div>
    </>
  );
}
