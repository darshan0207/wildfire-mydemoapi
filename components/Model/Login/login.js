import { Modal, Button } from 'react-bootstrap';
import styles from './login.module.scss';
import Image from 'next/image';
import WildfireTv from '../../../public/assets/images/WildfireTV.png';
import Facebook from '../../../public/assets/images/Facebook.png';
import Google from '../../../public/assets/images/Google.png';
import { Formik, Form } from 'formik';
import * as Yup from 'yup';
import InputField from '../../InputField/inputField';
import LoginButton from '../../Button/button';
import Link from 'next/link';
import { useRouter } from 'next/router';

export default function Login(props) {
  const router = useRouter();
  const handleForgetPassword = () => {
    props.setShow(false);
    router.push('/password/recover');
  };

  return (
    <Modal
      show={props.show}
      onHide={(e) => props.setShow(false)}
      className="ps-0"
    >
      <Modal.Header closeButton>
        <Modal.Title></Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Formik
          initialValues={{
            email: '',
            password: '',
            // email: 'customer@gmail.com',
            // password: 'customer',
          }}
          validationSchema={Yup.object({
            email: Yup.string()
              .email('Invalid email address')
              .required('This field is required'),
            password: Yup.string().required('This field is required'),
          })}
          onSubmit={(payload) => {
            props.doLoginUser({
              email: payload.email,
              password: payload.password,
            });
          }}
        >
          <Form>
            <InputField
              label="Email"
              name="email"
              type="text"
              placeholder="email"
              icon="fa fa-at fa-lg"
            />
            <InputField
              label="Password"
              name="password"
              type="password"
              placeholder="password"
              icon="fa fa-lock fa-lg"
            />
            <div className={`mb-5 ${styles.forget_pwd}`}>
              <Link href={'/password/recover'}>
                <a onClick={() => handleForgetPassword()}>Forget Password?</a>
              </Link>
            </div>
            <LoginButton
              type={'submit'}
              class={`form-control btn btn-danger ${styles.login_btn}`}
              name={'LOGIN'}
            />
          </Form>
        </Formik>
        <div className={styles.line}>
          <span>or Login with</span>
        </div>
        <div className={`row ${styles.social_media} justify-content-center`}>
          <Button className={`m-2 ${styles.social_btn}`}>
            <Image src={Facebook} alt="" /> FACEBOOK
          </Button>
          <Button className={`m-2 ${styles.social_btn}`}>
            <Image src={Google} alt="" /> GOOGLE
          </Button>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <div className={styles.signup_Account}>
          <span>{`Don't have an account?`}</span>
          <u
            className="login"
            onClick={() => {
              props.setShowOnboarding(true);
            }}
          >
            Signup
          </u>
        </div>
        <div className={styles.icon_WildFire}>
          {/* <Image src={WildfireTv} alt="" /> */}
        </div>
      </Modal.Footer>
    </Modal>
  );
}
