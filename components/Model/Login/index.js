import login from './login';
import { connect } from 'react-redux';
import { doLoginUser } from '../../../redux/actionCreaters/auth.actioncreater';

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    doLoginUser: (payload) => dispatch(doLoginUser(payload)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(login);
