import { Modal } from 'react-bootstrap';
import { useState } from 'react';
import Image from 'next/image';
import styles from './onBoarding.module.scss';
import wlogo from '../../../public/assets/icons/frame.svg';
import Button from '../../Button/button';
import { ReactSVG } from 'react-svg';
import router from 'next/router';

export default function OnBoarding(props) {
  const [userType, setUserType] = useState('Merchant');

  const handleCLick = () => {
    if (userType === 'Shopper') {
      props.setshowRegister(true);
    } else {
      router.push(`${process.env.API_URL}`);
    }
  };

  const HandleUser = (props) => (
    <div
      className={`${styles.user_circle} ${
        userType === props.userType
          ? `${styles.m_circle}`
          : `${styles.m_circle_unchecked}`
      }`}
      onClick={() => setUserType(props.userType)}
    >
      <ReactSVG
        src={
          props.userType === 'Merchant'
            ? '/assets/icons/merchant.svg'
            : '/assets/icons/buyer.svg'
        }
      />
      <div className={styles.label_text}>
        <span>{props.userType}</span>
      </div>
    </div>
  );

  return (
    <>
      <Modal
        show={props.show}
        backdrop="static"
        onHide={(e) => props.setShow(false)}
        className="ps-0"
        contentClassName={styles.popup_content}
      >
        <div className="main-container">
          <div className="d-flex justify-content-center">
            <div className="position-absolute">
              {/* <Image src={wlogo} alt="" /> */}
            </div>
          </div>
          <Modal.Header closeButton></Modal.Header>
          <div className={styles.title_bar}>
            <div className={styles.popup_title}>JOIN THE NETWORK</div>
          </div>
          <Modal.Body>
            <div className={styles.separator_board}>
              <small className={styles.saprator_text}>Are you a</small>
            </div>
            <div
              className={`d-flex justify-content-between ${styles.user_mar}`}
            >
              <HandleUser userType={'Merchant'} />
              <div className={styles.or_text}>OR</div>
              <HandleUser userType={'Shopper'} />
            </div>
            <div className={styles.btn_wrapper}>
              <Button
                type={'button'}
                handleCLick={handleCLick}
                class={styles.next_btn}
                name={'next'}
              />
            </div>
          </Modal.Body>
          <Modal.Footer>
            <div className={styles.bottom_line}>
              Already have an account? &nbsp;&nbsp;
              <b
                className="login"
                onClick={() => {
                  props.setShowLogin(true);
                }}
              >
                <u>Login</u>
              </b>
            </div>
          </Modal.Footer>
        </div>
      </Modal>
    </>
  );
}
