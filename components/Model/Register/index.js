import Register from './register';
import { connect } from 'react-redux';
import { doRegisterUser } from '../../../redux/actionCreaters/auth.actioncreater';

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    doRegisterUser: (payload) => dispatch(doRegisterUser(payload)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Register);
