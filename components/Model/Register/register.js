import { Modal } from 'react-bootstrap';
import Image from 'next/image';
import styles from './register.module.scss';
import ArrowLeft from '../../../public/assets/icons/arrow-left.svg';
import HeaderArror from '../../../public/assets/icons/arrow-down.svg';
import Button from '../../Button/button';
import { Formik, Form } from 'formik';
import * as Yup from 'yup';
import InputField from '../../InputField/inputField';
import { useRouter } from 'next/router';

export default function Register(props) {
  const router = useRouter();

  return (
    <>
      <Modal
        show={props.show}
        backdrop="static"
        onHide={(e) => props.setShow(false)}
        className="ps-0"
        contentClassName={styles.popup_content}
      >
        <div className={styles.title_bar}>
          <div className={styles.popup_title}>JOIN THE NETWORK</div>
        </div>
        <Modal.Header closeButton></Modal.Header>
        <div className={styles.header_title}>
          <div>
            <Image src={HeaderArror} alt="" />
          </div>
          <b className={styles.header_Info}>Personal Information</b>
        </div>
        <Modal.Body>
          <Formik
            initialValues={{
              firstname: '',
              lastname: '',
              email: '',
              password: '',
              password_confirmation: '',
            }}
            validationSchema={Yup.object({
              firstname: Yup.string().required('This field is required'),
              lastname: Yup.string().required('This field is required'),
              email: Yup.string()
                .email('Invalid email address')
                .required('This field is required'),
              password: Yup.string().required('This field is required'),
              password_confirmation: Yup.string()
                .required('This field is required')
                .oneOf([Yup.ref('password'), null], 'Passwords must match'),
            })}
            onSubmit={(payload) => {
              props.doRegisterUser({
                user: {
                  firstName: payload.firstname,
                  lastName: payload.lastname,
                  email: payload.email,
                  password: payload.password,
                },
                onHide: props,
                route: router,
                checkout: props.checkout,
              });
            }}
          >
            <Form>
              <InputField
                label="First Name"
                name="firstname"
                type="text"
                placeholder="first name"
                icon="fa fa-user-o fa-lg"
              />
              <InputField
                label="Last Name"
                name="lastname"
                type="text"
                placeholder="last name"
                icon="fa fa-user-o fa-lg"
              />
              <InputField
                label="Email"
                name="email"
                type="text"
                placeholder="email"
                icon="fa fa-at fa-lg"
              />
              <InputField
                label="Password"
                name="password"
                type="password"
                placeholder="password"
                icon="fa fa-lock fa-lg"
              />
              <InputField
                label="Password Confirmation"
                name="password_confirmation"
                type="password"
                placeholder="password"
                icon="fa fa-lock fa-lg"
              />
              <div className={styles.btn_group}>
                <div
                  className={styles.btn_box}
                  onClick={(e) => {
                    props.setShowOnboarding(true);
                  }}
                >
                  <div className={styles.btn_Image}>
                    <Image src={ArrowLeft} alt="" />
                  </div>
                  <div className={`${styles.btn_back} pointer`}>Back</div>
                </div>
                <div>
                  <Button
                    type={'submit'}
                    class={styles.next_btn}
                    name={'next'}
                  />
                </div>
              </div>
            </Form>
          </Formik>
        </Modal.Body>
        <Modal.Footer>
          <div className={styles.bottom_line}>
            Already have an account? &nbsp;&nbsp;
            <b
              className="login"
              onClick={() => {
                props.setShowLogin(true);
              }}
            >
              <u>Login</u>
            </b>
          </div>
        </Modal.Footer>
      </Modal>
    </>
  );
}
