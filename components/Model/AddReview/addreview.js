import { Modal } from 'react-bootstrap';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import styles from './addreview.module.scss';
import SubmitButton from '../../Button/button';
import ReactStars from 'react-rating-stars-component';

export default function Addreview({
  show,
  setShow,
  addReviewRating,
  ratingReview,
  productSlug,
  video
}) {
  const initialValues = {
    username: '',
    review: '',
    rating: '',
  };

  const ratingChanged = (newRating) => {
    formik.setFieldValue('rating', newRating);
  };

  const formik = useFormik({
    initialValues: initialValues,
    validationSchema: Yup.object({
      username: Yup.string().required('This field is required.'),
      review: Yup.string().required('This field is required.'),
      rating: Yup.string().required('This field is required.'),
    }),
    onSubmit: (e, { resetForm }) => {
      addReviewRating({
        review: {
          title: '',
          rating: e.rating,
          product: productSlug,
          review: e.review,
          name: e.username,
          show_identifier: true,
          video: video,
        },
        Show: setShow,
        resetvalue: resetForm,
      });
    },
  });

  const handleClose = () => {
    setShow(false);
    formik.resetForm({
      values: initialValues,
    });
  };

  return (
    <Modal
      show={show}
      onHide={(e) => handleClose()}
      className="ps-0 modal_review"
    >
      <Modal.Header closeButton>
        <Modal.Title></Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <form noValidate onSubmit={formik.handleSubmit}>
          <div className="row">
            <div className="col-md-12 mb-3">
              <label className={styles.form_label}>Name</label>
              <input
                type="text"
                className="form-control input-text-custom"
                name="username"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values['username']}
              />
              {formik.touched['username'] && formik.errors['username'] ? (
                <div className="text-danger field-validation-valid font-16">
                  {formik.errors['username']}
                </div>
              ) : null}
            </div>
            <div className="col-md-12 mb-3">
              <label className={styles.form_label}>Review</label>
              <textarea
                name="review"
                className="form-control input-text-custom"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values['review']}
              />
              {formik.touched['review'] && formik.errors['review'] ? (
                <div className="text-danger field-validation-valid font-16">
                  {formik.errors['review']}
                </div>
              ) : null}
            </div>
            <div className="col-md-12">
              <label className={styles.form_label}>Rating</label>
              <ReactStars
                type="text"
                count={5}
                onChange={ratingChanged}
                size={34}
                activeColor="#ffd700"
                id="rating"
                name="rating"
                value={formik.values['rating'] || 0}
              />
              {formik.touched['rating'] && formik.errors['rating'] ? (
                <div className="text-danger field-validation-valid font-16">
                  {formik.errors['rating']}
                </div>
              ) : null}
            </div>
          </div>
          <div className="pb-5">
            <SubmitButton
              type={'submit'}
              class={styles.viewallreview_btn}
              name={'submit'}
            />
          </div>
        </form>
      </Modal.Body>
      <Modal.Footer></Modal.Footer>
    </Modal>
  );
}
