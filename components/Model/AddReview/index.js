import Addreview from './addreview';
import { connect } from 'react-redux';
import { addReviewRating } from '../../../redux/actionCreaters/product.actioncreater';

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    addReviewRating: (payload) => dispatch(addReviewRating(payload)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Addreview);
