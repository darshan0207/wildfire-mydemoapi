import { Formik, Form } from 'formik';
import { Modal } from 'react-bootstrap';
import * as Yup from 'yup';
import AddressForm from '../../AddressForm/addressform';
import styles from './address.module.scss';

function Address(props) {
  let addressWithPickedFields = {
    firstname: '',
    lastname: '',
    address1: '',
    address2: '',
    city: '',
    phone: '',
    zipcode: '',
    stateid: '',
    countryid: '',
    company: '',
  };
  let country;
  if (props.type === 'add') {
    let defaultcountry = props?.countrielist;
    if (defaultcountry?.length) {
      country = defaultcountry[0]._id;
      addressWithPickedFields.countryid = defaultcountry[0]._id;
    }
  }

  if (props.address) {
    (country = props.address.countryid),
      (addressWithPickedFields.firstname = props.address.firstname),
      (addressWithPickedFields.lastname = props.address.lastname),
      (addressWithPickedFields.address1 = props.address.address1),
      (addressWithPickedFields.address2 = props.address.address2),
      (addressWithPickedFields.city = props.address.city),
      (addressWithPickedFields.phone = props.address.phone),
      (addressWithPickedFields.zipcode = props.address.zipcode),
      (addressWithPickedFields.stateid = props.address.stateid),
      (addressWithPickedFields.countryid = props.address.countryid),
      (addressWithPickedFields.company = props.address.company);
    // addressWithPickedFields = props.address.attributes;
  }

  return (
    <>
      <Modal {...props} size="lg" contentClassName={styles.modal_width}>
        <Modal.Header closeButton>
          <Modal.Title className={styles.popup_title}>
            {props.title}
          </Modal.Title>
        </Modal.Header>
        <Formik
          initialValues={addressWithPickedFields}
          validationSchema={Yup.object({
            firstname: Yup.string().required('This field is required'),
            lastname: Yup.string().required('This field is required'),
            address1: Yup.string().required('This field is required'),
            address2: Yup.string(),
            city: Yup.string().required('This field is required'),
            phone: Yup.string().required('This field is required'),
            zipcode: Yup.string()
              .required('This field is required')
              .matches(/^[0-9]+$/, 'Must be only digits')
              .min(2, 'Must be exactly 2 digits')
              .max(6, 'Must be exactly 5 digits'),
            countryid: Yup.string().required('This field is required'),
            stateid: Yup.string().required('This field is required'),
            company: Yup.string(),
          })}
          onSubmit={(payload) => {
            if (props.type === 'add') {
              props.doCreateAddress({
                payload,
                hide: props.onHide,
              });
            } else {
              props.doUpdateAddress({
                id: props.address._id,
                payload,
                hide: props.onHide,
              });
            }
          }}
        >
          <Form>
            <Modal.Body>
              <AddressForm
                country={country}
                countrielist={props?.countrielist}
                statelist={props?.statelist}
              />
            </Modal.Body>
            <Modal.Footer>
              <button type="submit" className={styles.btnaddaddress}>
                Save Address
              </button>
            </Modal.Footer>
          </Form>
        </Formik>
      </Modal>
    </>
  );
}

export default Address;
