import {
  doCreateAddress,
  doUpdateAddress,
} from '../../../redux/actionCreaters/address.actioncreater';
import { connect } from 'react-redux';
import Address from './address';

const mapStateToProps = (state) => {
  return {
    countrielist: state.countrie.data,
    statelist: state.countrie.stateList,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    doCreateAddress: (payload) => dispatch(doCreateAddress(payload)),
    doUpdateAddress: (payload) => dispatch(doUpdateAddress(payload)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Address);
