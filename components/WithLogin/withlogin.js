import withAuth from '../WithAuth';
import React from 'react';

function WithLogin(props) {
  return <span>{props.title}</span>;
}
export default withAuth(WithLogin, false, true);
