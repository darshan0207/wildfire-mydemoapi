import styles from './cartsummary.module.scss';
import Image from 'next/image';
import { ReactSVG } from 'react-svg';
import Loader from '../Loader';
import { useEffect } from 'react';
import router from 'next/router';
import myLoader from '../../common/loader';

export default function CartSummary(props) {
  const handleClick = (itme_id, quantity) => {
    props?.handleCount(itme_id, quantity);
  };

  useEffect(() => {
    if (props?.line_item?.length === 0) router.push('/');
  }, [props?.line_item]);

  const HandleProductPage = (slug) => {
    let pathname = '/products/[slug]';
    if (process.env.MOBILE_BUILD == 'true') {
      pathname = '/products';
    }
    router.push({
      pathname: '/products',
      query: { slug: slug },
    });
  };

  return (
    <div className={`${styles.cart_detail}`}>
      <div className={`p-4 ${styles.shipping_header}`}>cart Summary</div>
      {props?.IsLoader === true ? (
        <Loader />
      ) : (
        <>
          {props && props?.line_item && props?.line_item.length > 0 ? (
            <>
              <div className={styles.cartItem}>
                {props?.line_item?.map((item, index) => {
                  return (
                    <div
                      key={index}
                      className={`row p-0 m-0 ${styles.product_detail_box}`}
                    >
                      <div className={`col-3 p-0`}>
                        {item?.productId ? (
                          <div className={styles.product_image}>
                            <Image
                              src={`${item?.productId?.images[0].path}`}
                              width="100px"
                              height="100px"
                              objectFit="contain"
                              alt=""
                            />
                          </div>
                        ) : (
                          <ReactSVG src="/assets/noimage.svg" />
                        )}
                      </div>
                      <div className={`col-9 p-0 ps-3`}>
                        <div
                          className={`pointer ${styles.product_name}`}
                          onClick={() =>
                            HandleProductPage(item?.productId?.slug)
                          }
                        >
                          {item?.productId?.name &&
                            item?.productId?.name.slice(0, 20).concat('...')}
                        </div>
                        <div className={styles.product_variant}>
                          {item?.attributes?.options_text &&
                            item?.attributes?.options_text
                              .slice(0, 20)
                              .concat('...')}
                        </div>
                        <div className={styles.product_variant}>QUANTITY</div>
                        <div className="d-flex justify-content-between align-items-center">
                          <div className="d-flex align-items-center">
                            <button
                              className={`${styles.counter_btn} ms-0`}
                              onClick={() =>
                                handleClick(item._id, item?.quantity - 1)
                              }
                              disabled={item?.quantity === 1}
                            >
                              -
                            </button>
                            <span className={styles.count}>
                              {item?.quantity}
                            </span>
                            <button
                              className={`${styles.counter_btn}`}
                              onClick={() =>
                                handleClick(item._id, item?.quantity + 1)
                              }
                            >
                              +
                            </button>
                          </div>
                        </div>
                        <div className="w-100 text-end">
                          <span className={` ${styles.product_total}`}>
                            {item?.total?.toFixed(2)}
                          </span>
                          {/* <br />
                          {item?.attributes?.pre_tax_amount > 0 && (
                            <span>
                              {item?.attributes?.display_pre_tax_amount}
                            </span>
                          )} */}
                          <br />
                          {item?.promo_total > 0 && (
                            <span>{item?.display_promo_total}</span>
                          )}
                        </div>
                      </div>
                    </div>
                  );
                })}
              </div>
              <div className={styles.subtotal}>
                <div className="d-flex justify-content-between pt-3 pb-1">
                  <div className={styles.subtotal_title}>SUBTOTAL</div>
                  <div className={styles.subtotal_price}>
                    ${props?.subtotal?.toFixed(2)}
                  </div>
                </div>
                {props?.promo ? (
                  <div className="d-flex justify-content-between pb-1">
                    <div className={styles.subtotal_title}>promo discount</div>
                    <div className={styles.subtotal_price}>{props?.promo}</div>
                  </div>
                ) : (
                  ''
                )}
                <div className="d-flex justify-content-between pb-2">
                  <div className={styles.subtotal_title}>SHIPPING</div>
                  {/* {props?.Shipping && ( */}
                  <div className={styles.subtotal_price}>
                    {!props?.Shipping ? 'Free' : props?.Shipping}
                  </div>
                  {/* )} */}
                </div>
                {/* {props?.taxTotal ? ( */}
                <div className="d-flex justify-content-between pb-3">
                  <div className={styles.subtotal_title}>TAX</div>
                  <div className={styles.subtotal_price}>{props?.taxTotal}</div>
                </div>
                {/* ) : (
                  ''
                )} */}
              </div>
              <div className={styles.total}>
                <div className={styles.total_title}>TOTAL</div>
                <div className={styles.total_price}>
                  {/* <s>{props?.compareTotal}</s> */}$
                  {props?.subtotal?.toFixed(2)}
                </div>
              </div>
            </>
          ) : (
            <div className="text-center pt-4">
              <h3>Your cart is empty</h3>
            </div>
          )}
        </>
      )}
    </div>
  );
}
