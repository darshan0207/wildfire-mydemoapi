import ReviewOrder from './revieworder';
import { connect } from 'react-redux';
import {
  doOrderComplete,
  doUpdateOrder,
} from '../../redux/actionCreaters/order.actioncreater';

const mapStateToProps = (state) => {
  console.log(state);
  return {
    cart: state?.cart,
    orderDetails: state.order.orderDetails,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    doOrderComplete: (payload) => dispatch(doUpdateOrder(payload)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ReviewOrder);
