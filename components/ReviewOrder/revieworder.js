import Button from '../Button/button';
import styles from './revieworder.module.scss';
import { useRouter } from 'next/router';
import { useState } from 'react';
import { toast } from 'react-toastify';

export default function ReviewOrder(props) {
  const [submit, setSubmit] = useState(false);
  const router = useRouter();
  let order = {};
  // console.log(props);
  // props?.cart?.included?.find((element, i) => {
  //   if (
  //     element.id ===
  //       props?.cart?.data?.relationships?.shipping_address?.data?.id &&
  //     element?.type == 'address'
  //   ) {
  //     order['shipping_address'] = element?.attributes;
  //   }
  //   if (
  //     element.id ===
  //       props?.cart?.data?.relationships?.billing_address?.data?.id &&
  //     element?.type == 'address'
  //   ) {
  //     order['billing_address'] = element?.attributes;
  //   }
  //   if (
  //     element.id === props?.cart?.data?.relationships?.payments?.data[0]?.id &&
  //     element?.type == 'payment'
  //   ) {
  //     order['payment'] = element?.attributes;
  //   }
  //   if (element?.attributes?.selected && element?.type == 'shipping_rate') {
  //     order['shipment'] = element?.attributes;
  //   }
  // });
  const placeOrder = (paymentid) => {
    toast.info('Payment in progress');
    setSubmit(true);
    props.doOrderComplete({
      route: router,
      paymentid: paymentid,
      state: 'complete',
    });
  };
  return (
    <>
      <div className={styles.header_review}>Review Order</div>
      <div className="row mt-3">
        <div className="col-sm-12 col-md-6 col-lg-6">
          <div className={styles.address_info}>Shipping Address</div>
          <div className={`mt-3 p-3 ${styles.detail_address}`}>
            <div className={`pb-1 ${styles.address}`}>Address</div>
            <div className={`pb-4 ${styles.detail}`}>
              {props?.orderDetails?.shipaddress?.address1}
              {props?.orderDetails?.shipaddress?.city}
              {props?.orderDetails?.shipaddress?.zipcode}
            </div>
            <div className={`pb-1 ${styles.address}`}>Phone</div>
            <div className={`pb-4 ${styles.detail}`}>
              {props?.orderDetails?.shipaddress?.phone}
            </div>
            <div className={`pb-1 ${styles.address}`}>Email</div>
            <div className={`pb-4 ${styles.detail}`}>
              {props?.orderDetails?.email}
            </div>
          </div>
          <div className={`mt-4 ${styles.pay_method}`}>Shipping Method</div>
          <div className={`mt-2 p-3 ${styles.detail_address}`}>
            <div className={`p-1 ${styles.pay_charge}`}>
              {props?.orderDetails?.shipmentmethod?.name}
            </div>
          </div>
          <hr className={`mt-5 mb-4 d-sm-none d-block ${styles.separator}`} />
        </div>

        <div className="col-sm-12 col-md-6 col-lg-6">
          <div className={styles.address_info}>Billing Address</div>
          <div className={`mt-3 p-3 ${styles.detail_address}`}>
            <div className={`pb-1 ${styles.address}`}>Address</div>
            <div className={`pb-4 ${styles.detail}`}>
              {props?.orderDetails?.billaddress?.address1}
              {props?.orderDetails?.billaddress?.city}
              {props?.orderDetails?.billaddress?.zipcode}
            </div>
            <div className={`pb-1 ${styles.address}`}>Phone</div>
            <div className={`pb-10 ${styles.billing_phone} ${styles.detail}`}>
              {props?.orderDetails?.billaddress?.phone}
            </div>
            {/* <div className={`pb-1 ${styles.address}`}>Email</div>
            <div className={`pb-4 ${styles.detail}`}>
              domingahlongstreet@armyspy.com
            </div> */}
          </div>
          <div className={`mt-4 ${styles.pay_method}`}>Payment Method</div>
          <div className={`mt-2 p-3 ${styles.detail_address}`}>
            <div className={`p-1 ${styles.pay_charge}`}>
              {props?.orderDetails?.paymentmethod?.name}
            </div>
          </div>
        </div>
        <div className="mt-4 mb-4">
          <Button
            class={styles.order_button}
            type="button"
            name="place order"
            handleCLick={() => placeOrder(props?.orderDetails?.paymentid)}
            disabled={submit}
          />
        </div>
      </div>
    </>
  );
}
