import Image from 'next/image';
import styles from './review.module.scss';
import like from '../../public/assets/icons/thumsup.svg';
import dislike from '../../public/assets/icons/thumsdown.svg';
import customer from '../../public/assets/icons/user-profile.svg';
import myLoader from '../../common/loader';

export default function Review({ ratingReview }) {
  return (
    <>
      <div className={`pt-3 pb-3 m-0 ${styles.ratings}`}>
        <div className={`${styles.custom_images}`}>
          {ratingReview?.original_url ? (
            <Image
              loader={myLoader}
              src={ratingReview?.original_url}
              alt=""
              height="100%"
              width="100%"
              className="rounded-circle"
            />
          ) : (
            <Image
              src={customer}
              alt=""
              height="100%"
              width="100%"
              className="rounded-circle"
            />
          )}
        </div>
        <div className="w-100 ms-2">
          <div className="row">
            <div className={`col-9 ${styles.customer_name}`}>
              {ratingReview?.attributes?.name}
              {/* <small>(Ghana)</small> */}
            </div>
            {/* <div className="col-3 d-block d-sm-none d-md-none">
              <Image src={like} alt="" />
              <Image src={dislike} alt="" />
            </div> */}
          </div>
          <div className="row">
            <div className="col">
              {[...Array(5)].map((e, i) => (
                <span
                  key={i}
                  className={`me-1 fa ${
                    i >= ratingReview?.attributes?.rating
                      ? 'fa-star-o'
                      : 'fa-star'
                  }  ${styles.customer_star_icon}`}
                />
              ))}
            </div>
          </div>
          <div className="row">
            <div className={`col-9  ${styles.customer_details} `}>
              <p>{ratingReview?.attributes?.review}</p>
            </div>
            {/* <div className={`col-3 d-none  d-sm-block`}>
              <Image src={like} alt="" />
              <Image src={dislike} alt="" />
            </div> */}
          </div>
        </div>
      </div>
    </>
  );
}
