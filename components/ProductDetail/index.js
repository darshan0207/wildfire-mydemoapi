import { connect } from 'react-redux';
import { addCartDetail } from '../../redux/actionCreaters/cart.actioncreater';
import ProductDetail from './productdetail';

const mapStateToProps = (state) => {
  return {};
};
const mapDispatchToProps = (dispatch) => {
  return {
    addCartDetail: (payload) => dispatch(addCartDetail(payload)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ProductDetail);
