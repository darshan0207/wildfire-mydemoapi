import { useRouter } from 'next/router';
import styles from './productdetail.module.scss';
import ButtonSubmit from '../Button/button';
import ProductRating from '../ProductRating/productrating';
import ProductTag from '../ProductTag/productTag';
import ProductDescription from '../ProductDescription/productdescription';
import ProductPrice from '../ProductPrice/productprice';
import ProductMoreDetails from '../ProductMoreDetails/productmoredetails';
import ProductOptions from '../ProductOptions/productoptions';
import { useState } from 'react';

export default function ProductDetail(props) {
  const propsData = props?.productData;
  const router = useRouter();
  const handleBuyNow = () => {
    addItem(true);
    if (!props.isAuthenticated) router.push('/guest');
    else router.push('/checkout/address');
  };
  const [quantityCount, setQuantityCount] = useState(1);
  const addItem = (show = false) => {
    props?.addCartDetail({
      productId: propsData?.data?._id,
      quantity: quantityCount,
      price: propsData?.data?.price,
      variantId: props?.variantID,
    });
    if (!show) {
      document.getElementById('mycartSidenav').style.right = '0';
      document.getElementById('show-backdropcart').style.display = 'block';
      document.body.style.overflow = 'hidden';
    }
  };

  return (
    <>
      <div className={styles.single_product_detail}>
        <ProductTag name={propsData?.data?.name} />
        <ProductRating productReview={5} />
        <div
          className={`${styles.product_details} d-none  d-sm-block mt-3 mb-2`}
        >
          <ProductDescription description={propsData?.data?.description} />
        </div>
        <div className="mt-4 d-block d-sm-none d-md-none">
          <ProductPrice
            display_price={props?.display_price}
            compare_price={props?.compare_price}
            setquantity={(data) => {
              setQuantityCount(data);
            }}
          />
        </div>

        <div className="mt-2">
          <div className={`mt-3 mb-3 ${styles.product_detail_header}`}>
            availability :
            <span className={`ms-1 ${styles.instock}`}>
              {/* {propsData?.data?.stock && propsData?.data?.availableon
                ? 'IN STOCK'
                : propsData?.isBackOrderable
                ? 'BACKORDERED'
                : 'OUT OF STOCK'} */}
              {props?.purchasable ? 'IN STOCK' : 'OUT OF STOCK'}
            </span>
          </div>
          <div>
            <span className={`${styles.instock} text-capitalize`}>
              {props?.return_days
                ? `returnable for ${props?.return_days} days`
                : 'No returns'}
            </span>
          </div>
          <ProductOptions
            optionType={props?.optionTypeArr}
            onOptionTypeChange={props.onOptionTypeChange}
            name={propsData?.data?.name}
          />
          <div
            className={`mt-3 ${styles.product_details}  d-block d-sm-none d-md-none `}
          >
            <ProductDescription description={propsData?.data?.description} />
          </div>
          <div className=" d-block d-sm-none d-md-none">
            <ProductMoreDetails productProperty={propsData?.data?.propertys} />
          </div>
        </div>
        <div className="mt-4  d-none  d-sm-block">
          <ProductPrice
            display_price={props?.display_price}
            compare_price={props?.compare_price}
            setquantity={(data) => {
              setQuantityCount(data);
            }}
          />
        </div>
        <div className="mt-4">
          <div className="row d-none  d-sm-block m-0">
            <ButtonSubmit
              type={'button'}
              class={`col-6 ${styles.buynow_btn} me-3`}
              name={'buy now'}
              handleCLick={() => handleBuyNow()}
              disabled={!props?.purchasable}
              // disabled={
              //   !props?.isAvailable ||
              //   !props?.purchasable ||
              //   (props?.in_stock ? !props?.in_stock : !props?.isBackOrderable)
              // }
            />
            <ButtonSubmit
              type={'button'}
              class={`col-6 ${styles.add_btn}`}
              name={'add to cart'}
              disabled={!props?.purchasable}
              // disabled={
              //   !props?.isAvailable ||
              //   !props?.purchasable ||
              //   (props?.in_stock ? !props?.in_stock : !props?.isBackOrderable)
              // }
              handleCLick={addItem}
            />
          </div>
          <div className="row mb-4 d-block d-sm-none d-md-none">
            <ButtonSubmit
              type={'button'}
              class={`col-6 ${styles.add_btn}`}
              name={'add to cart'}
              disabled={!props?.purchasable}
              // disabled={
              //   !propsData?.data?.availableon ||
              //   !propsData?.data?.purchasable ||
              //   (propsData?.data?.stock
              //     ? !propsData?.data?.stock
              //     : !propsData?.data?.isBackOrderable)
              // }
              handleCLick={addItem}
            />
            <ButtonSubmit
              type={'button'}
              class={`col-6 ${styles.buynow_btn} `}
              name={'buy now'}
              handleCLick={() => handleBuyNow()}
              disabled={!props?.purchasable}
              // disabled={
              //   !propsData?.data?.isAvailable ||
              //   !propsData?.data?.purchasable ||
              //   (propsData?.data?.stock
              //     ? !propsData?.data?.stock
              //     : !propsData?.data?.isBackOrderable)
              // }
            />
          </div>
        </div>
        <div className="mt-4 mb-4 d-none d-sm-block">
          <ProductMoreDetails productProperty={propsData?.data?.propertys} />
        </div>
      </div>
    </>
  );
}
