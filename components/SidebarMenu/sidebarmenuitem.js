import styles from './sidebarmenu.module.scss';
import { ReactSVG } from 'react-svg';
import { useRouter } from 'next/router';
import Link from 'next/link';

export default function Sidebarmenuitem(props) {
  const router = useRouter();

  let sidebarcontentStyle = `pointer ${styles.sidebar_content}`;
  if (router.pathname === props.item.path) {
    sidebarcontentStyle += ` ${styles.active}`;
  }
  return (
    <>
      <li
        onClick={() => props.navClose(props.item.ref)}
        className={sidebarcontentStyle}
      >
        <Link href={props.item.path} passHref>
          <a>
            <span className={`header-sidebar-menu-icon`}>
              <ReactSVG src={props.item.image} />
            </span>

            <span className={`header-sidebar-menu-title ${styles.title}`}>
              {props.item.title}
              {props.item.text && <small>{props.item.text}</small>}
            </span>
          </a>
        </Link>
      </li>
    </>
  );
}
