import styles from './sidebarmenu.module.scss';
import WildfireTv from '../../public/assets/images/Wildfiretv_sidebar.png';
import Image from 'next/image';
import Sidebarmenuitem from './sidebarmenuitem';
import Link from 'next/link';
import { createRef } from 'react';
import CopyRight from '../CopyRight/copyright';
import Hamburger from '../../public/assets/icons/hamburger.svg';

const MenuItems = [
  {
    path: '/',
    image: '/assets/icons/home.svg',
    title: 'Home',
  },
  // {
  //   path: '/watchtv',
  //   image: '/assets/icons/watchtv.svg',
  //   title: 'Watch TV',
  //   text: 'All in one place...',
  // },
  {
    path: '/search',
    image: '/assets/icons/feature.svg',
    title: 'Products',
  },
  // {
  //   path: '/promotion',
  //   image: '/assets/icons/promotions.svg',
  //   title: 'Promotions',
  // },
  {
    path: '/about',
    image: '/assets/icons/about.svg',
    title: 'About Us',
  },
  {
    path: '/contact-us',
    image: '/assets/icons/contactus.svg',
    title: 'Contact Us',
  },
];

const UsefulLink = [
  {
    path: '/terms',
    title: 'Terms & Conditions',
  },
  {
    path: '/privacy',
    title: 'Privacy Policy',
  },
];

const SidebarMenu = (props) => {
  const navRef = createRef();
  const backRef = createRef();
  const navClose = () => {
    navRef.current.style.right = '-320px';
    backRef.current.style.display = 'none';
    document.body.style.overflow = '';
  };

  const navOpen = () => {
    navRef.current.style.right = '0';
    backRef.current.style.display = 'block';
    document.body.style.overflow = 'hidden';
  };

  return (
    <>
      <div className={props.iconstyle} onClick={() => navOpen()}>
        <Image
          className={styles.pointer}
          height="30px"
          width="24px"
          src={Hamburger}
          alt=""
        />
      </div>

      <div
        id="show-backdrop"
        ref={backRef}
        onClick={() => navClose()}
        className={styles.menu_backdrop}
      ></div>
      <div ref={navRef} id="mySidenav" className={`${styles.sidenav} `}>
        <div className="header-sidebar" id="sidebar-menu">
          <span
            className={`pointer ${styles.closebtn}`}
            onClick={() => navClose()}
          >
            &times;
          </span>
          <ul className="header-sidebar-menu">
            {MenuItems.map((item, index) => {
              return (
                <Sidebarmenuitem navClose={navClose} key={index} item={item} />
              );
            })}
          </ul>
          {/* <ul className={styles.icon_WildFire}>
            <Image src={WildfireTv} alt="" />
          </ul> */}
          <ul className="header-sidebar-link">
            {UsefulLink.map((item, index) => {
              return (
                <li key={index} onClick={() => navClose(navRef)}>
                  <Link href={item.path}>{item.title}</Link>
                </li>
              );
            })}
          </ul>
          <div className="header-sidebar-copyright">
            <CopyRight />
          </div>
        </div>
      </div>
    </>
  );
};

export default SidebarMenu;
