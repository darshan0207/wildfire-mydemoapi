import styles from './product.module.scss';
// import { useCallback, useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import ProductDetail from '../../components/ProductDetail';
import ProductSlider from '../ProductSlider/productslider';
import ProductRelated from '../ProductRelated/productrelated';
import ProductReview from '../ProductReview/productreview';

function ProductView({
  productData,
  productVariant,
  isAuthenticated,
  wishlistItem,
  addProductTOWishlist,
}) {
  const [variantsType, setVariantsType] = useState([]);
  const [isPurchasable, setIsPurchasable] = useState(false);
  const [variantId, setVariantId] = useState();
  const [optionType, setOptionType] = useState([]);
  const [displayPrice, setDisplayPrice] = useState(0);
  const [comparePrice, setComparePrice] = useState(0);

  useEffect(() => {
    if (productData && productData.data && productData.data._id) {
      // setproductproperties(
      //   productData.data.relationships.product_properties.data
      // );
      setVariantsType(productData.data.variants);
      if (!productData?.data?.variants?.length) {
        setVariantId(productData?.data?._id);
        setIsPurchasable(productData?.data?.purchasable);
      } else {
        setIsPurchasable(false);
      }
      setDisplayPrice(productData?.data?.price);
      setComparePrice(productData?.data?.compareatprice);
      // setVariantsImages(
      //   productData?.data?.relationships?.primary_variant?.data?.imageurl
      // );
      // setInStock(productData?.data?.attributes?.in_stock);
      // setIsBackOrderable(productData?.data?.attributes?.backorderable);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [productData?.data?.id]);

  useEffect(() => {
    if (productData && productData.data && productData.data._id) {
      setOptionType(productData.data.optiontype);
    }
  }, [productData]);

  function handleOptionTypeChange(optionsData) {
    let positionindex = optionsData.position;
    delete optionsData.position;
    let newoptiontype = [];

    if (variantsType.length) {
      let variantname = [...variantsType];

      Object.values(optionsData).map((item, index) => {
        variantname = variantname.filter(
          (obj) =>
            obj.optionvalueids.findIndex((option) => option._id === item) > -1
        );
      });

      if (variantname.length) {
        variantname.map((item) => {
          item.optionvalueids.map((options) => {
            if (newoptiontype.indexOf(options?._id) === -1) {
              newoptiontype.push(options);
            }
          });
        });
      }
      productVariant({
        optiontype: newoptiontype,
        position: positionindex,
      });
      if (
        Object.values(optionsData)?.length ===
        variantname[0]?.optionvalueids?.length
      ) {
        let productprice = variantname[0];
        setDisplayPrice(productprice?.price);
        setComparePrice(
          productprice?.compareatprice > 0 ? productprice?.compareatprice : 0
        );
        setIsPurchasable(true);
      } else setIsPurchasable(false);
      // if (
      //   Object.values(optionsData).length === variantname[0]?.options?.length &&
      //   variantname[0]?.imageurl.length
      // ) {
      //   setImageUrlCallback(variantname[0]?.imageurl);
      //   // setTimeout(() => {
      //   //   setVariantsImages(variantname[0]?.imageurl);
      //   // }, 500);
      // } else {
      //   setImageUrlCallback(
      //     productData?.data?.relationships?.primary_variant?.data?.imageurl
      //   );
      //   // setTimeout(() => {
      //   //   setVariantsImages(
      //   //     productData?.data?.relationships?.primary_variant?.data?.imageurl
      //   //   );
      //   // }, 500);
      // }
      setVariantId(variantname[0]?._id);
      // setInStock(variantname[0]?.value?.attributes?.in_stock);
      // setIsBackOrderable(variantname[0]?.value?.attributes?.backorderable);
    } else setIsPurchasable(true);
  }

  return (
    <>
      <div className={`container ${styles.single_product_info}`}>
        <div className={`${styles.single_product} row`}>
          <div className="col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <ProductSlider
              variantData={productData?.data?.images}
              // variantID={variantId}
              addProductTOWishlist={addProductTOWishlist}
              wished_products={
                wishlistItem?.length > 0 &&
                wishlistItem.filter((ele) => ele === productData?.data?._id)
              }
              product_ID={productData?.data?._id}
              isAuthenticated={isAuthenticated}
            />
          </div>
          <div className="col-12 col-sm-12 col-md-6 col-lg-6">
            {productData?.data !== 0 && (
              <ProductDetail
                productData={productData}
                name={productData?.data?.name}
                description={productData?.data?.description}
                display_price={displayPrice}
                compare_price={comparePrice}
                // in_stock={productData?.data?.stock}
                // ProductProperty={productData?.data?.propertys}
                optionTypeArr={optionType}
                newVariantArr={variantsType}
                onOptionTypeChange={handleOptionTypeChange}
                purchasable={isPurchasable}
                // isBackOrderable={''}
                variantID={variantId}
                isAuthenticated={isAuthenticated}
                return_days={productData?.data?.returndays}
                returnable={productData?.data?.returnable}
                isAvailable={productData?.data?.availableon}
                // productReview={1}
              />
            )}
            {/* <ProductReview
              ratingReview={ratingReview}
              isAuthenticated={isAuthenticated}
              productSlug={productData?.data?.attributes?.slug}
              reviewMeta={reviewMeta}
              allReviewRating={allReviewRating}
            /> */}
          </div>
        </div>
        <ProductRelated relatedProduct={productData?.data?.relatedproduct} />
      </div>
    </>
  );
}
export default ProductView;
