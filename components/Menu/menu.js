import Link from 'next/link';
import { useRouter } from 'next/router';
import { NavLink } from 'react-bootstrap';
import styles from './menu.module.scss';

export default function Menu() {
  const { pathname } = useRouter();
  return (
    <>
      <ul className={`navbar-nav m-auto menu  ${styles.navbar_center}`}>
        <li className="nav-item">
          <Link href="/">
            <a
              className={`nav-link ${
                pathname === '/' || pathname === '/home' ? 'active' : ''
              } `}
            >
              Home
            </a>
          </Link>
        </li>
        {/* <li className="nav-item">
          <Link href="/watchtv">
            <a
              className={`nav-link  ${pathname === '/watchtv' ? 'active' : ''}`}
            >
              Watch TV
            </a>
          </Link>
        </li> */}
        <li className="nav-item">
          <Link href="/search">
            <a
              className={`nav-link  ${pathname === '/search' ? 'active' : ''} `}
            >
              Products
            </a>
          </Link>
        </li>
        {/* <li className="nav-item">
          <Link href="/promotion">
            <a
              className={`nav-link  ${
                pathname === '/promotion' ||
                pathname === '/promotion-banner/[slug]'
                  ? 'active'
                  : ''
              } `}
            >
              Promotions
            </a>
          </Link>
        </li> */}
      </ul>
    </>
  );
}
