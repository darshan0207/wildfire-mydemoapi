import styles from './TrendingItem.module.scss';
import Image from 'next/image';
import { Navigation, Pagination, Autoplay } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
import ArrowLeft from '../../public/assets/icons/slider-arrow-left.svg';
import ArrowRight from '../../public/assets/icons/slider-arrow-right.svg';
import Link from 'next/link';
import myLoader from '../../common/loader';
export default function TrendingItem({
  trendingproducts,
  isproduct,
  isWishlist,
  wished_products,
  addProductTOWishlist,
  isAuth,
}) {
  // const addWishProduct = (id) => {
  //   addProductTOWishlist({
  //     wished_product: {
  //       variant_id: id,
  //       // remark: 'I want this Product too',
  //       quantity: 1,
  //     },
  //   });
  // };

  return (
    <>
      <div className="row">
        <div className="col-lg-12">
          <Swiper
            loop={false}
            slidesPerView="auto"
            breakpoints={{
              300: {
                spaceBetween: 16,
                slidesPerView: 1.2,
              },
              576: {
                spaceBetween: 40,
                slidesPerView: 4.8,
              },
            }}
            navigation={{
              nextEl: '.treding-slider-swiper .swiper-button-next',
              prevEl: '.treding-slider-swiper .swiper-button-prev',
            }}
            pagination={{
              el: '.treding-slider-swiper .swiper-pagination',
            }}
            modules={[Navigation, Pagination, Autoplay]}
            className="treding-slider-swiper"
            autoplay={{
              delay: 2500,
            }}
          >
            {trendingproducts?.map((item, index) => {
              let wishData =
                wished_products?.length > 0 &&
                wished_products.filter((ele) => ele === item?._id);
              return (
                <SwiperSlide key={index}>
                  {isWishlist && isAuth && (
                    <div
                      className={`treding-like ${styles.heart_like}`}
                      onClick={() => {
                        addProductTOWishlist({ productId: item?._id });
                      }}
                    >
                      {
                        <i
                          className={`pointer fa ${
                            wishData?.length > 0 && wishData[0] === item?._id
                              ? 'fa-heart'
                              : 'fa-heart-o'
                          }`}
                          aria-hidden="true"
                        ></i>
                      }
                    </div>
                  )}
                  <Link
                    passHref
                    href={`/products${
                      process.env.MOBILE_BUILD == 'true' ? '?slug=' : '/'
                    }${item?.slug}`}
                  >
                    <div className="treding-item pointer">
                      <div className={styles.treding_image}>
                        <Image
                          // loader={myLoader}
                          src={item?.images[0]?.path}
                          // loader={myLoader}
                          alt=""
                          className="img-fluid"
                          layout={'fill'}
                          objectFit={'contain'}
                        />
                      </div>
                      {isproduct && (
                        <div className="treding-title">{item?.name}</div>
                      )}
                    </div>
                  </Link>
                </SwiperSlide>
              );
            })}
            <div className="swiper-pagination"></div>
            <div className="swiper-button-next">
              <Image src={ArrowRight} alt="" className="img-fluid" />
            </div>
            <div className="swiper-button-prev">
              <Image src={ArrowLeft} alt="" className="img-fluid" />
            </div>
          </Swiper>
        </div>
      </div>
    </>
  );
}
