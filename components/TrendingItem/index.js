import TrendingItem from './TrendingItem';
import { connect } from 'react-redux';
import {
  addProductToWishlist,
  removeProductToWishlist,
} from '../../redux/actionCreaters/order.actioncreater';

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    addProductTOWishlist: (payload) => dispatch(addProductToWishlist(payload)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(TrendingItem);
