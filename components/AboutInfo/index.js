import AboutInfo from './aboutInfo';
import { connect } from 'react-redux';

const mapStateToProps = (state) => {
  return {
    isAuth: state.auth.isAuthenticated,
  };
};

export default connect(mapStateToProps)(AboutInfo);
