import styles from './aboutInfo.module.scss';
import Image from 'next/image';
import User from '../../public/assets/images/user.jpg';
import Withlogin from '../WithLogin/withlogin';

export default function AboutInfo({ isAuth }) {
  return (
    <div className={styles.about_info_section}>
      <div className="container">
        <div className="row no-gutter">
          <div className="col-lg-6">
            <div className={styles.about_info_left}>
              <p>EXPLORE MORE ABOUT WILDFIRE</p>
              <h5>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et .
              </h5>

              <div className="d-flex gap-3">
                {isAuth === false && (
                  <button className={styles.button_join}>
                    <Withlogin title="join now" />
                  </button>
                )}
                <button className={styles.button_watch}>watch & shop</button>
              </div>
            </div>
          </div>

          <div className="col-lg-6">
            <div className={styles.about_info_right}>
              <p>OUR CUSTOMERS LOVE US!</p>

              <h4>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore aliqua.
              </h4>

              <div className="about_info_user">
                <div className="d-flex align-items-center">
                  <div className="flex-shrink-0">
                    <Image
                      src={User}
                      className="img-fluid"
                      height="44"
                      width="44"
                      alt=""
                    />
                  </div>

                  <div className="flex-grow-1 ms-3">
                    <h6 className="fw-semi text-dark">Andrey Paris</h6>
                    <p>Fashion Designer</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
