import styles from './productprice.module.scss';
import { useState } from 'react';

export default function ProductPrice({
  display_price,
  compare_price,
  setquantity,
}) {
  const [count, setCount] = useState(1);
  const handleCount = (value) => {
    setCount((count) => count + value);
    setquantity(count + value);
  };
  return (
    <>
      <div className={styles.product_price}>Price</div>
      <div className="d-flex align-items-center">
        <div>
          <div className={styles.price_count}>
            {display_price}
            <s>{compare_price > 0 ? compare_price : ''}</s>
          </div>
        </div>
        <div>
          <button
            type="button"
            className={`${styles.counter_btn} m-3`}
            onClick={(e) => handleCount(-1)}
            disabled={count === 1 ? true : false}
          >
            -
          </button>
          <span className="text-muted text-light">{count}</span>
          <button
            type="button"
            className={`${styles.counter_btn} m-3`}
            onClick={(e) => handleCount(+1)}
          >
            +
          </button>
        </div>
      </div>
    </>
  );
}
