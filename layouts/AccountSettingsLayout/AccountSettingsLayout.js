import { getLayout as getSiteLayout } from '../default/index';
import { useRouter } from 'next/router';
import styles from './accountsettingslayout.module.scss';
import AccountMenu from '../../components/AccountMenu/index';
import AccountMenuMobile from '../../components/AccountMenuMobile/index';
const AccountSettingsLayout = ({ children }) => {
  const { pathname } = useRouter();
  const MenuItems = [
    {
      title: 'Profile',
      path: '/account/profile',
    },
    {
      title: 'Payment Methods',
      path: '/account/payment-method',
    },
    {
      title: 'My Order',
      path: '/account/order-history',
    },
    {
      title: 'My Wishlist',
      path: '/account/my-wishlist',
    },
    // {
    //   title: 'Account Setting',
    //   path: '/account/account-setting',
    // },
    // {
    //   title: 'Account info',
    //   path: '/account/account-information',
    // },
    // {
    //   title: 'Billing',
    //   path: '/account/billing',
    // },
    // {
    //   title: 'Manage Categories',
    //   path: '/account/manage-categories',
    // },
    {
      title: 'Address Book',
      path: '/account/address-book',
    },
  ];
  return (
    <div className="container-fluid">
      <div className={styles.account}>
        <div className="d-none d-sm-block">
          <div className={styles.account_menu}>
            <AccountMenu items={MenuItems} active={pathname} />
          </div>
        </div>
        <div className="d-block d-sm-none">
          <AccountMenuMobile items={MenuItems} active={pathname} />
        </div>
        <div className={styles.account_content}>{children}</div>
      </div>
    </div>
  );
};

export const getLayout = (page) =>
  getSiteLayout(<AccountSettingsLayout>{page}</AccountSettingsLayout>);

export default AccountSettingsLayout;
