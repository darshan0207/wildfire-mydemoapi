import Header from '../../widgets/header/index';
import Footer from '../../widgets/footer/index';
import Head from 'next/head';
import React, { useEffect, useState } from 'react';

function Layout({ children }) {
  const [innerHeight, setInnerHeight] = useState(0);

  const updateDimensions = () => {
    setInnerHeight(
      window.innerHeight -
        document.querySelector('nav')?.offsetHeight -
        document.querySelector('footer')?.offsetHeight
    );
  };

  useEffect(() => {
    updateDimensions();
    window.addEventListener('resize', updateDimensions);
    return () => window.removeEventListener('resize', updateDimensions);
  }, []);

  return (
    <>
      <Head>
        <title>shiv shakti</title>
        <link rel="icon" type="image/jpg" href="/assets/pp.jpg" />
      </Head>
      <Header />
      <main style={{ minHeight: innerHeight }}>{children}</main>
      <Footer />
    </>
  );
}

export const getLayout = (page) => <Layout>{page}</Layout>;

export default Layout;
