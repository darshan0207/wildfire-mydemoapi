import styles from './footer.module.scss';
import Logo from '../../components/Logo/logo';
import UsefulLinks from '../../components/UsefulLinks/usefullinks';
import SocialLinks from '../../components/SocialLinks/sociallinks';
import CopyRight from '../../components/CopyRight/copyright';
import TheText from '../../components/TheText/thetext';

function Footer() {
  return (
    <footer className={styles.footer}>
      <div className="row pe-0 ps-0 me-0 ms-0">
        <div className="col-lg-6 col-md-6 mb-4">
          <div className="row mr-0 ml-0">
            <div className="col-lg-4 col-md-6">
              <Logo src={'/assets/pp.jpg'} height={100} width={100} />
            </div>
            <div className="col-lg-6 col-md-12">
              <TheText />
            </div>
          </div>
        </div>
        <div className="col-lg-3 col-md-6 mb-4">
          <h5 className={styles.footer_usefullink}>Useful Links</h5>
          <UsefulLinks />
        </div>
        <div className="col-lg-3 col-md-6 mb-4">
          <h5 className={styles.footer_usefullink}>Social Links</h5>
          <SocialLinks />
        </div>
      </div>
      <hr className={styles.footer_hrline} />
      <CopyRight className="footer_copyright" />
    </footer>
  );
}

export default Footer;
