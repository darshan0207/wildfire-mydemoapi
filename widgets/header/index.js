import styles from './header.module.scss';
import { Navbar } from 'react-bootstrap';
import Logo from '../../components/Logo/logo';
import Menu from '../../components/Menu/menu';
import Icon from '../../components/Icon/icon';

function Header() {
  return (
    <>
      <Navbar className={`${styles.header} pt-0 pb-0 header header-top`}>
        <div className={`navbar-brand me-0 p-0 ${styles.header_link}`}>
          {/* <Logo src={'/assets/pp.jpg'} height={65} width={65} /> */}
          Shiv Shakti
        </div>
        <div className={`collapse navbar-collapse header-menu-center`}>
          <Menu />
        </div>
        <div className="d-flex form-inline my-2 my-lg-0">
          <Icon />
        </div>
      </Navbar>
    </>
  );
}

export default Header;
