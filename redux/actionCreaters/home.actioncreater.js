import { createAction } from 'redux-actions';
import * as actionTypes from '../actionTypes/home.actiontypes';

export const getHomeDetail = createAction(actionTypes.HOME_DETAIL_REQUESTED);
export const getHomeDetailSuccess = createAction(
  actionTypes.HOME_DETAIL_REQUEST_SUCCESS
);
export const getHomeDetailFailed = createAction(
  actionTypes.HOME_DETAIL_REQUEST_FAILED
);
