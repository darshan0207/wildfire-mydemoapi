import { createAction } from 'redux-actions';

import * as actionTypes from '../actionTypes/order.actiontypes';

export const doCompletedOrdersList = createAction(
  actionTypes.COMPLETED_ORDER_LIST_REQUESTED
);
export const doCompletedOrdersLoadMoreList = createAction(
  actionTypes.COMPLETED_MORE_ORDER_REQUESTED
);

export const doCompletedOrdersListSuccess = createAction(
  actionTypes.COMPLETED_ORDER_LIST_SUCCEEDED
);
export const doCompletedOrdersListFailure = createAction(
  actionTypes.COMPLETED_ORDER_LIST_FAILED
);
export const getSingleOrderDetail = createAction(
  actionTypes.GET_SINGLE_ORDER_REQUESTED
);
export const getSingleOrderDetailSuccess = createAction(
  actionTypes.GET_SINGLE_ORDER_SUCCEEDED
);
export const getSingleOrderDetailFailed = createAction(
  actionTypes.GET_SINGLE_ORDER_FAILED
);

export const doUpdateOrder = createAction(actionTypes.UPDATE_ORDER_REQUESTED);

export const doUpdateOrderSuccess = createAction(
  actionTypes.UPDATE_ORDER_SUCCEEDED
);
export const doUpdateOrderFailure = createAction(
  actionTypes.UPDATE_ORDER_FAILED
);

export const doOrderNextSuccess = createAction(
  actionTypes.ORDER_NEXT_SUCCEEDED
);
export const doOrderNextFailure = createAction(actionTypes.ORDER_NEXT_FAILED);

export const doOrderComplete = createAction(
  actionTypes.ORDER_COMPLETE_REQUESTED
);

export const doOrderCompleteSuccess = createAction(
  actionTypes.ORDER_COMPLETE_SUCCEEDED
);
export const doOrderCompleteFailure = createAction(
  actionTypes.ORDER_COMPLETE_FAILED
);
export const doGetOrderReturnReasons = createAction(
  actionTypes.ORDER_RETURN_REASONS_REQUESTED
);
export const doOrderReturnReasonsSuccess = createAction(
  actionTypes.ORDER_RETURN_REASONS_SUCCEEDED
);
export const doOrderReturnReasonsFailure = createAction(
  actionTypes.ORDER_RETURN_REASONS_FAILED
);
export const doOrderReturnUpdateItem = createAction(
  actionTypes.ORDER_RETURN_UPDATE_ITEM
);
export const doGetOrderReturn = createAction(
  actionTypes.ORDER_RETURN_REQUESTED
);
export const doOrderReturnSuccess = createAction(
  actionTypes.ORDER_RETURN_SUCCEEDED
);
export const doOrderReturnFailure = createAction(
  actionTypes.ORDER_RETURN_FAILED
);

export const doGetReturnedItems = createAction(
  actionTypes.ORDER_RETURN_ITEMS_REQUESTED
);
export const doGetReturnedItemsSuccess = createAction(
  actionTypes.ORDER_RETURN_ITEMS_SUCCEEDED
);
export const doGetReturnedItemsFailure = createAction(
  actionTypes.ORDER_RETURN_ITEMS_FAILED
);

export const doGetInvoice = createAction(actionTypes.GET_INVOICE_REQUESTED);
export const doGetInvoiceSuccess = createAction(
  actionTypes.GET_INVOICE_SUCCEEDED
);
export const doGetInvoiceFailure = createAction(actionTypes.GET_INVOICE_FAILED);
export const getWishListData = createAction(
  actionTypes.GET_WISHLIST_DATALIST_REQUESTED
);
export const getWishListDataSuccess = createAction(
  actionTypes.GET_WISHLIST_DATALIST_SUCCEEDED
);
export const getWishListDataFailure = createAction(
  actionTypes.GET_WISHLIST_DATALIST_FAILED
);

export const addProductToWishlist = createAction(
  actionTypes.ADD_PRODUCT_TO_WISHLIST_REQUESTED
);
export const addProductToWishlistSuccess = createAction(
  actionTypes.ADD_PRODUCT_TO_WISHLIST_SUCCEEDED
);
export const addProductToWishlistFailure = createAction(
  actionTypes.ADD_PRODUCT_TO_WISHLIST_FAILED
);
export const removeProductToWishlist = createAction(
  actionTypes.REMOVE_PRODUCT_TO_WISHLIST_REQUESTED
);
export const removeProductToWishlistSuccess = createAction(
  actionTypes.REMOVE_PRODUCT_TO_WISHLIST_SUCCEEDED
);
export const removeProductToWishlistFailed = createAction(
  actionTypes.REMOVE_PRODUCT_TO_WISHLIST_FAILED
);

export const doWishListEmpty = createAction(actionTypes.EMPTY_WISHLIST);
