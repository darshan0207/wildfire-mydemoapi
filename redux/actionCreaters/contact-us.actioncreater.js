import { createAction } from 'redux-actions';
import * as actionTypes from '../actionTypes/contact-us.actiontypes';

export const doContactUs = createAction(actionTypes.CONTACT_US_REQUESTED);
export const doContactUsSuccess = createAction(actionTypes.CONTACT_US_SUCCESS);
export const doContactUsFailure = createAction(actionTypes.CONTACT_US_FAILED);
