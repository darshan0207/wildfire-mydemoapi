import { createAction } from 'redux-actions';

import * as actionTypes from '../actionTypes/countrie.actiontypes';

export const doCountrieList = createAction(actionTypes.GET_COUNTRIE_REQUESTED);
export const doCountrieSuccess = createAction(
  actionTypes.GET_COUNTRIE_SUCCEEDED
);
export const doCountrieFailure = createAction(actionTypes.GET_COUNTRIE_FAILED);

export const doRegionList = createAction(actionTypes.GET_REGION_REQUESTED);
export const doRegionSuccess = createAction(actionTypes.GET_REGION_SUCCEEDED);
export const doRegionFailure = createAction(actionTypes.GET_REGION_FAILED);
