import { createAction } from 'redux-actions';
import * as actionTypes from '../actionTypes/product.actiontypes.js';

export const singleProduct = createAction(
  actionTypes.GET_SINGLE_PRODUCT_REQUESTED
);
export const singleProductSuccess = createAction(
  actionTypes.GET_SINGLE_PRODUCT_SUCCESS
);
export const singleProductFailure = createAction(
  actionTypes.GET_SINGLE_PRODUCT_FAILED
);

export const productVariant = createAction(
  actionTypes.PRODUCT_VARIANT_REQUESTED
);
export const productVariantSuccess = createAction(
  actionTypes.PRODUCT_VARIANT_SUCCESS
);

export const getRelatedProduct = createAction(
  actionTypes.RELATED_PRODUCT_REQUESTED
);
export const getRelatedProductSuccess = createAction(
  actionTypes.RELATED_PRODUCT_SUCCESS
);
export const getRelatedProductFailed = createAction(
  actionTypes.RELATED_PRODUCT_FAILED
);

export const getReviewRating = createAction(
  actionTypes.GET_PRODUCT_RATING_REVIEW_REQUESTED
);

export const getReviewRatingSuccess = createAction(
  actionTypes.GET_PRODUCT_RATING_REVIEW_SUCCESS
);

export const getReviewRatingFailed = createAction(
  actionTypes.GET_PRODUCT_RATING_REVIEW_FAILED
);

export const addReviewRating = createAction(
  actionTypes.ADD_REVIEW_RATING_REQUESTED
);

export const addReviewRatingSuccess = createAction(
  actionTypes.ADD_REVIEW_RATING_SUCCESS
);

export const addReviewRatingFailed = createAction(
  actionTypes.ADD_REVIEW_RATING_FAILED
);
