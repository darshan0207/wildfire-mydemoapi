import { createAction } from 'redux-actions';

import * as actionTypes from '../actionTypes/auth.actiontypes.js';

export const doRegisterUser = createAction(actionTypes.AUTH_REGISTER_REQUESTED);
export const doRegisterUserSuccess = createAction(
  actionTypes.AUTH_REGISTER_SUCCEEDED
);
export const doRegisterUserFailure = createAction(
  actionTypes.AUTH_REGISTER_FAILED
);

export const doLoginUser = createAction(actionTypes.AUTH_LOGIN_REQUESTED);
export const doLoginUserSuccess = createAction(
  actionTypes.AUTH_LOGIN_SUCCEEDED
);
export const doLoginUserFailure = createAction(actionTypes.AUTH_LOGIN_FAILED);

export const doLogoutUser = createAction(actionTypes.AUTH_LOGOUT_REQUESTED);
export const doLogoutUserSuccess = createAction(
  actionTypes.AUTH_LOGOUT_SUCCEEDED
);
export const doLogoutUserFailure = createAction(actionTypes.AUTH_LOGOUT_FAILED);

export const doRefreshTokenGenerate = createAction(
  actionTypes.AUTH_REFRESH_TOKEN_REQUESTED
);
export const doRefreshTokenGenerateSuccess = createAction(
  actionTypes.AUTH_REFRESH_TOKEN_SUCCESS
);
export const doRefreshTokenGenerateFailure = createAction(
  actionTypes.AUTH_REFRESH_TOKEN_ERROR
);

export const doGuestUser = createAction(
  actionTypes.GUEST_USER
);
export const doGuestUserSuccess = createAction(
  actionTypes.GUEST_USER_SUCCESS
);
