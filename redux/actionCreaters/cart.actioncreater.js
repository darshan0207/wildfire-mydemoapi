import { createAction } from 'redux-actions';
import * as actionTypes from '../actionTypes/cart.actiontypes';

export const getCartDetail = createAction(
  actionTypes.GET_CART_DETAILS_REQUESTED
);
export const getCartDetailSuccess = createAction(
  actionTypes.GET_CART_DETAILS_SUCCEEDED
);
export const getCartDetailFailed = createAction(
  actionTypes.GET_CART_DETAILS_FAILED
);
export const updateCartQuantity = createAction(
  actionTypes.UPDATE_CART_QUANTITY_REQUESTED
);
export const addCartDetail = createAction(
  actionTypes.ADD_CART_DETAILS_REQUESTED
);
export const doOrderTokenSuccess = createAction(
  actionTypes.GET_ORDER_TOKEN_SUCCEEDED
);
export const doOrderTokenFailed = createAction(
  actionTypes.GET_ORDER_TOKEN_FAILED
);
export const doRemoveOrderToken = createAction(
  actionTypes.DO_REMOVE_ORDER_TOKEN
);
export const doCartEmpty = createAction(actionTypes.EMPTY_CART);
export const doPaymentMethod = createAction(actionTypes.PAYMENT_METHOD);
export const deleteCartDetail = createAction(
  actionTypes.DELETE_CART_DETAILS_REQUESTED
);

export const updateCartQuantitysucess = createAction(
  actionTypes.GET_UPDATE_QUANTITY_SUCCESS
);

export const cartItemDeleteSucess = createAction(
  actionTypes.CART_ITEM_DELETE_SUCCESS
);
