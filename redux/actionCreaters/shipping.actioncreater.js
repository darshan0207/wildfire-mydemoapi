import { createAction } from 'redux-actions';
import * as actionTypes from '../actionTypes/shipping.actiontypes';

export const shippingDetail = createAction(
  actionTypes.GET_SHIPPING_DETAIL_REQUESTED
);
export const shippingDetailSuccess = createAction(
  actionTypes.GET_SHIPPING_DETAIL_SUCCESS
);
export const shippingDetailFailed = createAction(
  actionTypes.GET_SHIPPING_DETAIL_FAILED
);
