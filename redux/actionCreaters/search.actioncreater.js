import { createAction } from 'redux-actions';
import * as actionTypes from '../actionTypes/search.actiontypes.js';

export const searchProduct = createAction(actionTypes.SEARCH_PRODUCT_REQUESTED);
export const searchProductScroll = createAction(
  actionTypes.SEARCH_MORE_PRODUCT_REQUESTED
);
export const searchProductSuccess = createAction(
  actionTypes.SEARCH_PRODUCT_SUCCEEDED
);
export const searchProductFailure = createAction(
  actionTypes.SEARCH_PRODUCT_FAILED
);

export const productFilter = createAction(actionTypes.PRODUCT_FILTER_REQUESTED);

export const productFilterSuccess = createAction(
  actionTypes.PRODUCT_FILTER_SUCCEEDED
);
export const productFilterFailure = createAction(
  actionTypes.PRODUCT_FILTER_FAILED
);
