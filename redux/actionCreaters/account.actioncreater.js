import { createAction } from 'redux-actions';

import * as actionTypes from '../actionTypes/account.actiontypes';

export const doAccountInfo = createAction(actionTypes.ACCOUNT_INFO_REQUESTED);
export const doAccountInfoSuccess = createAction(
  actionTypes.ACCOUNT_INFO_SUCCEEDED
);
export const doAccountInfoFailure = createAction(
  actionTypes.ACCOUNT_INFO_FAILED
);

export const doAccountUpdate = createAction(
  actionTypes.ACCOUNT_UPDATE_REQUESTED
);
export const doAccountUpdateSuccess = createAction(
  actionTypes.ACCOUNT_UPDATE_SUCCEEDED
);
export const doAccountUpdateFailure = createAction(
  actionTypes.ACCOUNT_UPDATE_FAILED
);

export const doForgotPassword = createAction(
  actionTypes.FORGOT_PASSWORD_REQUESTED
);
export const doForgotPasswordSuccess = createAction(
  actionTypes.FORGOT_PASSWORD_SUCCEEDED
);
export const doForgotPasswordFailed = createAction(
  actionTypes.FORGOT_PASSWORD_FAILED
);
export const doResetPassword = createAction(
  actionTypes.RESET_PASSWORD_REQUESTED
);
export const doResetPasswordSuccess = createAction(
  actionTypes.RESET_PASSWORD_SUCCEEDED
);
export const doResetPasswordFailed = createAction(
  actionTypes.RESET_PASSWORD_FAILED
);

export const doProfileImageUpload = createAction(
  actionTypes.ADD_PROFILE_IMAGE_REQUESTED
);
export const doProfileImageUploadSuccess = createAction(
  actionTypes.ADD_PROFILE_IMAGE_SUCCEEDED
);
export const doProfileImageUploadFailed = createAction(
  actionTypes.ADD_PROFILE_IMAGE_FAILED
);
