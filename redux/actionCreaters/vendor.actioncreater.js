import { createAction } from 'redux-actions';
import * as actionTypes from '../actionTypes/vendor.actiontypes';

export const getVendor = createAction(actionTypes.GET_VENDOR_REQUESTED);
export const getVendorSuccess = createAction(actionTypes.GET_VENDOR_SUCCESS);
export const getVendorFailure = createAction(actionTypes.GET_VENDOR_FAILED);
export const getVendorVideo = createAction(
  actionTypes.GET_VENDOR_VIDEO_REQUESTED
);
export const getVendorVideoSuccess = createAction(
  actionTypes.GET_VENDOR_VIDEO_SUCCESS
);
export const getVendorVideoFailure = createAction(
  actionTypes.GET_VENDOR_VIDEO_FAILED
);

export const getVendorProduct = createAction(
  actionTypes.GET_VENDOR_PRODUCTS_REQUESTED
);
export const getVendorProductSuccess = createAction(
  actionTypes.GET_VENDOR_PRODUCTS_SUCCESS
);
export const getVendorProductFailure = createAction(
  actionTypes.GET_VENDOR_PRODUCTS_FAILED
);

export const getVideoDetails = createAction(
  actionTypes.GET_VIDEODETAILS_REQUESTED
);
export const getVideoDetailsSuccess = createAction(
  actionTypes.GET_VIDEODETAILS_SUCCESS
);
export const getVideoDetailsFailure = createAction(
  actionTypes.GET_VIDEODETAILS_FAILED
);

export const getVideoReview = createAction(
  actionTypes.GET_VIDEO_REVIEW_REQUESTED
);
export const getVideoReviewSuccess = createAction(
  actionTypes.GET_VIDEO_REVIEW_SUCCESS
);
export const getVideoReviewFailure = createAction(
  actionTypes.GET_VIDEO_REVIEW_FAILED
);
export const getWatchTvVideo = createAction(
  actionTypes.GET_WATCH_TV_VIDEOS_REQUESTED
);
export const getWatchTvVideoSuccess = createAction(
  actionTypes.GET_WATCH_TV_VIDEOS_SUCCESS
);
export const getWatchTvVideoFailure = createAction(
  actionTypes.GET_WATCH_TV_VIDEOS_FALIED
);
