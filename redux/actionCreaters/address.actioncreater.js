import { createAction } from 'redux-actions';

import * as actionTypes from '../actionTypes/address.actiontypes';

export const doAddressesList = createAction(
  actionTypes.ADDRESSES_LIST_REQUESTED
);
export const doAddressesListSuccess = createAction(
  actionTypes.ADDRESSES_LIST_SUCCEEDED
);
export const doAddressesListFailure = createAction(
  actionTypes.ADDRESSES_LIST_FAILED
);

export const doCreateAddress = createAction(
  actionTypes.CREATE_ADDRESSES_REQUESTED
);
export const doCreateAddressSuccess = createAction(
  actionTypes.CREATE_ADDRESSES_SUCCEEDED
);
export const doCreateAddressFailure = createAction(
  actionTypes.CREATE_ADDRESSES_FAILED
);

export const doUpdateAddress = createAction(
  actionTypes.UPDATE_ADDRESSES_REQUESTED
);
export const doUpdateAddressSuccess = createAction(
  actionTypes.UPDATE_ADDRESSES_SUCCEEDED
);
export const doUpdateAddressFailure = createAction(
  actionTypes.UPDATE_ADDRESSES_FAILED
);

export const doRemoveAddress = createAction(
  actionTypes.REMOVE_ADDRESSES_REQUESTED
);
export const doRemoveAddressSuccess = createAction(
  actionTypes.REMOVE_ADDRESSES_SUCCEEDED
);
export const doRemoveAddressFailure = createAction(
  actionTypes.REMOVE_ADDRESSES_FAILED
);
