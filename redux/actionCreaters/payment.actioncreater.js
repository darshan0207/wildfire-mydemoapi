import { createAction } from 'redux-actions';
import * as actionTypes from '../actionTypes/payment.actionstypes';

export const getPaymentDetail = createAction(
  actionTypes.GET_PAYMENT_DETAILED_REQUESTED
);
export const getPaymentDetailSuccess = createAction(
  actionTypes.GET_PAYMENT_DETAILED_SUCCESS
);
export const getPaymentDetailFailed = createAction(
  actionTypes.GET_PAYMENT_DETAILED_FAILED
);
export const setPromoCode = createAction(actionTypes.SET_COUPONCODE_REQUESTED);
export const doPayment = createAction(actionTypes.PAYMENT_REQUESTED);
export const doCardDetails = createAction(actionTypes.CARD_DETAILS);
export const removePromoCode = createAction(
  actionTypes.REMOVE_COUPONCODE_REQUESTED
);
export const getCardDetails = createAction(
  actionTypes.GET_CARD_DETAILS_REQUESTED
);
export const getCardDetailsSuccess = createAction(
  actionTypes.GET_CARD_DETAILS_SUCCESS
);
export const getCardDetailsFailed = createAction(
  actionTypes.GET_CARD_DETAILS_FAILED
);
export const removeCard = createAction(actionTypes.REMOVE_CARD_REQUESTED);
export const removeCardSuccess = createAction(actionTypes.REMOVE_CARD_SUCCESS);
export const removeCardFailed = createAction(actionTypes.REMOVE_CARD_FAILED);
