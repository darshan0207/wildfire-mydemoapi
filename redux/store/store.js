import { createStore, applyMiddleware, compose } from 'redux';
import { createWrapper, HYDRATE } from 'next-redux-wrapper';
import rootReducer from '../reducers/rootReducer';
import rootSaga from '../sagas/rootSaga';
import createSagaMiddleware from 'redux-saga';
import { composeWithDevTools } from 'redux-devtools-extension/logOnlyInProduction';
import { persistStore } from 'redux-persist';

const bindMiddleware = (middleware) => {
  if (process.env.NODE_ENV !== 'production') {
    const { composeWithDevTools } = require('redux-devtools-extension');
    return composeWithDevTools(applyMiddleware(...middleware));
  }
  return applyMiddleware(...middleware);
};

const reducer = (state, action) => {
  if (action.type === HYDRATE) {
    const nextState = {
      ...state, // use previous state
      ...action.payload, // apply delta from hydration
    };
    if (state.auth) nextState.auth = state.auth;
    if (state.cart) nextState.cart = state.cart;
    return nextState;
  } else {
    return rootReducer(state, action);
  }
};

export const makeStore = (initialState) => {
  let store;
  const sagaMiddleware = createSagaMiddleware();

  // const store = createStore(rootReducer, applyMiddleware(sagaMiddleware));
  const isServer = typeof window === 'undefined';
  if (isServer) {
    store = createStore(reducer, initialState, applyMiddleware(sagaMiddleware));
  } else {
    const { persistReducer } = require('redux-persist');
    const storage = require('redux-persist/lib/storage').default;

    const persistConfig = {
      key: 'root',
      whitelist: ['auth'], // make sure it does not clash with server keys
      storage,
      manualPersist: true,
    };

    store = createStore(
      persistReducer(persistConfig, reducer),
      initialState,
      bindMiddleware([sagaMiddleware])
    );
    store.__PERSISTOR = persistStore(store); // Nasty hack
  }

  store.sagaTask = sagaMiddleware.run(rootSaga);

  return store;
};

export const wrapper = createWrapper(makeStore, { debug: true });

// store.js

// create your reducer
