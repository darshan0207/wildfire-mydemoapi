export const ACCOUNT_INFO_REQUESTED = 'account/ACCOUNT_INFO_REQUESTED';
export const ACCOUNT_INFO_SUCCEEDED = 'account/ACCOUNT_INFO_SUCCEEDED';
export const ACCOUNT_INFO_FAILED = 'account/ACCOUNT_INFO_FAILED';

export const ACCOUNT_UPDATE_REQUESTED = 'account/ACCOUNT_UPDATE_REQUESTED';
export const ACCOUNT_UPDATE_SUCCEEDED = 'account/ACCOUNT_UPDATE_SUCCEEDED';
export const ACCOUNT_UPDATE_FAILED = 'account/ACCOUNT_UPDATE_FAILED';

export const FORGOT_PASSWORD_REQUESTED = 'recover/FORGOT_PASSWORD_REQUESTED';
export const FORGOT_PASSWORD_SUCCEEDED = 'recover/FORGOT_PASSWORD_SUCCEEDED';
export const FORGOT_PASSWORD_FAILED = 'recover/FORGOT_PASSWORD_FAILED';

export const RESET_PASSWORD_REQUESTED = 'reset/RESET_PASSWORD_REQUESTED';
export const RESET_PASSWORD_SUCCEEDED = 'reset/RESET_PASSWORD_SUCCEEDED';
export const RESET_PASSWORD_FAILED = 'reset/RESET_PASSWORD_FAILED';

export const ADD_PROFILE_IMAGE_REQUESTED = 'image/ADD_PROFILE_IMAGE_REQUESTED';
export const ADD_PROFILE_IMAGE_SUCCEEDED = 'image/ADD_PROFILE_IMAGE_SUCCEEDED';
export const ADD_PROFILE_IMAGE_FAILED = 'image/ADD_PROFILE_IMAGE_FAILED';
