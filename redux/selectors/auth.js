const getToken = (state) => {
  if (state.auth.accessToken) {
    return { bearerToken: state.auth.accessToken };
  } else if (state.auth.orderToken) {
    return { orderToken: state.auth.orderToken };
  } else {
    return {};
  }
};

const getRefreshToken = (state) => state.auth.refreshToken;

const getTokenExpiry = (state) => {
  return new Date(
    (state.auth.createdAt + state.auth.expiresIn) * 1000
  ).getTime();
};

const isAuthentication = (state) => state.auth.isAuthenticated;

const getAccessHash = (state) => state.auth.hashToken;

export {
  getToken,
  getRefreshToken,
  getTokenExpiry,
  getAccessHash,
  isAuthentication,
};
