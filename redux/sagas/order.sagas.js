import { call, put, retry, select } from 'redux-saga/effects';
import {
  doCompletedOrdersListSuccess,
  doCompletedOrdersListFailure,
  getSingleOrderDetailSuccess,
  getSingleOrderDetailFailed,
  doUpdateOrderSuccess,
  doUpdateOrderFailure,
  doOrderNextSuccess,
  doOrderCompleteSuccess,
  doOrderCompleteFailure,
  doOrderReturnReasonsSuccess,
  doGetReturnedItemsFailure,
  doGetInvoiceFailure,
  doGetInvoiceSuccess,
  getWishListDataSuccess,
  getWishListDataFailure,
  removeProductToWishlistSuccess,
  removeProductToWishlistFailed,
  addProductToWishlistSuccess,
  addProductToWishlistFailure,
} from '../actionCreaters/order.actioncreater';
import { client } from '../../common/client';
import { checkError } from '../../services/spreeerrors';
import { getAccessHash, getToken, isAuthentication } from '../selectors/auth';
import { toast } from 'react-toastify';
import { getUser } from '../selectors/user';
import {
  doCartEmpty,
  doOrderTokenFailed,
  doRemoveOrderToken,
  getCartDetailSuccess,
} from '../actionCreaters/cart.actioncreater';
// import { getdata } from './cart.sagas';
import { getCard } from '../selectors/card';
import router from 'next/router';
import httpService from '../../common/http.service';
async function doCompletedOrdersList(token, payload) {
  const tokens = token.bearerToken;
  const data = await httpService.get('/order', payload, tokens);
  return data.data;
  // return client.account.completedOrdersList(token, {
  //   page: payload.payload,
  //   per_page: 5,
  //   sort: '-completed_at',
  // });
}

async function getReasons() {
  const data = await fetch(
    `${process.env.API_URL}api/v2/storefront/return_authorization_reasons`,
    { method: 'GET' }
  );
  return await data.json();
}
async function returnOrder(token, payload) {
  const data = await fetch(
    `${process.env.API_URL}api/v2/storefront/return_authorizations`,
    {
      method: 'POST',
      body: JSON.stringify(payload),
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token.bearerToken}`,
      },
    }
  );
  return await data.json();
}

export function* completedOrdersListSaga({ payload }) {
  try {
    const token = yield select(getToken);
    const response = yield call(doCompletedOrdersList, token, payload);
    if (response.status) {
      yield put(doCompletedOrdersListSuccess(response));
    } else {
      const failed = response.fail();
      toast.error(checkError(failed));
      yield put(doCompletedOrdersListFailure(failed));
    }
  } catch (error) {
    toast.error(error);
    yield put(doCompletedOrdersListFailure(error));
  }
}

async function getSingleOrderDetail(payload, token) {
  const tokens = token.bearerToken || token.orderToken;
  const data = await httpService.get(`/order/${payload.payload}`, {}, tokens);
  return data.data;

  // return client.account.completedOrder(token, payload.payload, {
  //   include:
  //     'user,variants,shipments,shipping_address,payments,line_items,variants.option_values,variants.images,variants.product.images,shipments.shipping_rates,bookkeeping_documents',
  // });
}

async function returnedOrderItems(payload, token) {
  const data = await fetch(
    `${process.env.API_URL}api/v2/storefront/order_returned_items/?order_number=${payload.payload}`,
    {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token.bearerToken}`,
      },
    }
  );
  return await data.json();
}

export function* getSingleOrderDetailSaga(payload) {
  try {
    const token = yield select(getToken);
    const response = yield call(getSingleOrderDetail, payload, token);
    console.log(response);
    if (response.status) {
      const data = response.data;
      yield put(getSingleOrderDetailSuccess(data));
    }
  } catch (error) {
    toast.error(error);
    yield put(getSingleOrderDetailFailed(error));
  }
  //   if (response.isSuccess()) {
  //     const data = response.success();
  //     if (data) {
  //       if (data.data.attributes) {
  //         let attributesData = data.data.attributes;
  //         let order_status = ['shipped', 'partially', 'complete'];
  //         if (order_status.indexOf(attributesData.shipment_state) >= 0)
  //           var returnedOrder = yield call(returnedOrderItems, payload, token);
  //       }

  //       let lineitem = data.data.relationships.line_items;
  //       if (lineitem.data.length) {
  //         lineitem.data.map((item, i) => {
  //           let lineitemindex = data.included.findIndex(
  //             (resp) => resp.id === item.id && resp.type === 'line_item'
  //           );
  //           let variantdata = data.included[lineitemindex];
  //           item.attributes = variantdata.attributes;
  //           let variantindex = data.included.findIndex(
  //             (resp) =>
  //               resp.id === variantdata.relationships.variant.data.id &&
  //               resp.type === 'variant'
  //           );
  //           let productindex;
  //           if (variantindex !== -1) {
  //             item.variantId = data.included[variantindex].id;
  //             let productdata =
  //               data.included[variantindex].relationships.product.data.id;
  //             productindex = data.included.findIndex(
  //               (resp) => resp.id === productdata && resp.type === 'product'
  //             );
  //             if (productindex !== -1) {
  //               item.isReturnable =
  //                 data.included[productindex].attributes.returnable;
  //               item.returnDays =
  //                 data.included[productindex].attributes.return_days;
  //             }
  //             if (
  //               returnedOrder &&
  //               returnedOrder.data &&
  //               returnedOrder.data.returned_items &&
  //               returnedOrder.data.returned_items.length
  //             ) {
  //               let itemData = returnedOrder.data.returned_items;
  //               let totalQuantity = itemData
  //                 .filter((resp) => resp.line_item_id.toString() === item.id)
  //                 .reduce((count, item) => {
  //                   return count + item.quantity;
  //                 }, 0);
  //               item.return_quantity = totalQuantity;
  //             }
  //           }

  //           let lineitemrelationships =
  //             data.included[variantindex].relationships;
  //           let imageindex;
  //           if (
  //             lineitemrelationships &&
  //             lineitemrelationships?.images?.data &&
  //             lineitemrelationships?.images?.data?.length
  //           ) {
  //             let variantimageid = lineitemrelationships?.images?.data[0].id;
  //             imageindex = data.included.findIndex(
  //               (resp) => resp.id === variantimageid && resp.type === 'image'
  //             );
  //           } else {
  //             if (
  //               data.included[productindex].relationships.images.data.length
  //             ) {
  //               let imagesdata =
  //                 data.included[productindex].relationships.images.data[0].id;
  //               imageindex = data.included.findIndex(
  //                 (resp) => resp.id === imagesdata && resp.type === 'image'
  //               );
  //             }
  //           }
  //           if (imageindex)
  //             item.original_url =
  //               data.included[imageindex].attributes.original_url;
  //         });
  //       }
  //       if (data && data.included && data.included.length) {
  //         let userdata = data.included.filter((resp) => resp.type === 'user');
  //         if (userdata?.length) {
  //           data.data.attributes.email = userdata[0].attributes.email;
  //         }

  //         let shipmentsitem = data.data.relationships.shipments;
  //         if (shipmentsitem.data.length) {
  //           let shippingrates = 0;
  //           shipmentsitem.data.map((item, i) => {
  //             let shipmentindex = data.included.findIndex(
  //               (resp) => resp.id === item.id && resp.type === 'shipment'
  //             );
  //             shippingrates += parseFloat(
  //               data.included[shipmentindex].attributes.final_price
  //             );
  //           });
  //           data.data.attributes.shippingrates = shippingrates;
  //         }
  //         let bookkeepingdata = data.included.filter(
  //           (resp) => resp.type === 'bookkeeping_documents'
  //         );
  //         if (bookkeepingdata?.length) {
  //           let templatedata = data.included.filter(
  //             (resp) => resp?.attributes?.template === 'invoice'
  //           );
  //           if (templatedata?.length) {
  //             data.data.attributes.invoice_id = templatedata[0].id;
  //           }
  //         }
  //       }
  //     }
  //     yield put(getSingleOrderDetailSuccess(data));
  //   } else {
  //     const failed = response.fail();
  //     toast.error(checkError(failed));
  //     yield put(getSingleOrderDetailFailed(checkError(failed)));
  //     router.push('/account/order-history');
  //   }
  // } catch (error) { }
}

async function doUpdateOrder(payload, token) {
  const tokens = token.bearerToken || token.orderToken;
  const data = await httpService.post('/order', {}, payload.payload, tokens);
  return data.data;

  // return client.checkout.orderUpdate(
  //   token,
  //   payload.payload.state === 'payment'
  //     ? {
  //         order: payload.payload.order,
  //         payment_source: payload.payload.payment_source,
  //       }
  //     : { order: payload.payload }
  // );
}

async function doOrderNext(token) {
  // return client.checkout.orderNext(token, {
  //   include:
  //     'variants,shipments,billing_address,shipping_address,payments,line_items,variants.option_values,variants.images,variants.product.images,shipments.shipping_rates,promotions',
  // });
}
async function doOrderComplete(token, payload) {
  const tokens = token.bearerToken || token.orderToken;
  const data = await httpService.post('/order', {}, payload, tokens);
  return data.data;
  // return fetch(`http://192.168.1.7:8001/api/order`, {
  //   method: 'POST',
  //   headers: {
  //     'Content-Type': 'application/json',
  //     Authorization: `Bearer ${tokens}`,
  //   },
  //   body: JSON.stringify(payload.payload),
  // })
  //   .then((response) => response.json())
  //   .then((data) => {
  //     return data;
  //   })
  //   .catch((error) => {
  //     return error;
  //   });
  // return client.checkout.complete(token);
}
export function* updateOrdersListSaga(payload) {
  console.log(payload);
  try {
    const token = yield select(getToken);
    // if (payload.payload.state === 'address') {
    //   const userEmail = yield select(getUser);
    //   payload.payload.email = userEmail;
    // }
    const response = yield call(doUpdateOrder, payload, token);
    if (response) {
      const data = response;
      yield put(doUpdateOrderSuccess(data));
      //   if (payload.payload.state === data.data.attributes.state) {
      //     const orderNextresponse = yield call(doOrderNext, token);
      //     if (orderNextresponse.isSuccess()) {
      //       const data = orderNextresponse.success();
      //       const cartData = yield call(getdata, data);
      //       yield put(getCartDetailSuccess(cartData));
      //       payload.payload.nextStep();
      //       yield put(doOrderNextSuccess(data));
      //     } else {
      //       const failed = orderNextresponse.fail();
      //       toast.error(checkError(failed));
      //       yield put(doOrderNextFailure(failed));
      //     }
      //   } else {
      if (payload.payload.nextStep) {
        payload.payload.nextStep();
      } else if (payload.payload.state === 'complete') {
        yield put(doCartEmpty());
        payload.payload.route.push('/thankyou');
      }
      //   }
    } else {
      const failed = response.message;
      toast.error(failed);
      yield put(doUpdateOrderFailure(failed));
    }
  } catch (error) {
    toast.error('Server error');
    yield put(doUpdateOrderFailure(error));
  }
}

export function* completeOrderSaga({ payload }) {
  try {
    const token = yield select(getToken);
    const response = yield call(doOrderComplete, token, payload);
    console.log(response);
    if (response) {
      yield put(doOrderCompleteSuccess(response.data));
      const auth = yield select(isAuthentication);
      if (!auth) {
        yield put(doOrderTokenFailed());
      } else {
        yield put(doRemoveOrderToken());
      }
      yield put(doCartEmpty());
      payload.route.push('/thankyou');
    }
  } catch (error) {
    toast.error(error);
  }
  // try {
  //   const card = yield select(getCard);
  //   if (!card.order && payload.payload.route) {
  //     payload.payload.route.push('/checkout/payment');
  //     toast.error('Payment method is required');
  //     return;
  //   }
  //   const payloadData = {
  //     payload: {
  //       state: 'payment',
  //       order: card.order,
  //       payment_source: card.payment_source,
  //     },
  //   };
  //   const token = yield select(getToken);
  //   const response = yield call(doUpdateOrder, payloadData, token);
  //   if (response.isSuccess()) {
  //     const data = response.success();
  //     yield put(doUpdateOrderSuccess(data));
  //     const orderNextresponse = yield call(doOrderNext, token);
  //     if (orderNextresponse.isSuccess()) {
  //       const data = orderNextresponse.success();
  //       const cartData = yield call(getdata, data);
  //       yield put(getCartDetailSuccess(cartData));
  //       yield put(doOrderNextSuccess(data));
  //       const Ordercompleteresponse = yield call(doOrderComplete, token);
  //       if (Ordercompleteresponse.isSuccess()) {
  //         const data = Ordercompleteresponse.success();
  //         if (payload.payload.route) {
  //           yield put(doOrderCompleteSuccess(data.data));
  //           yield put(doOrderTokenFailed());
  //           yield put(doCartEmpty());
  //           payload.payload.route.push('/thankyou');
  //         }
  //       } else {
  //         const failed = Ordercompleteresponse.fail();
  //         toast.error(checkError(failed));
  //         yield put(doOrderCompleteFailure(failed));
  //       }
  //     } else {
  //       const failed = orderNextresponse.fail();
  //       toast.error(checkError(failed));
  //       yield put(doOrderNextFailure(failed));
  //     }
  //   } else {
  //     const failed = response.fail();
  //     toast.error(checkError(failed));
  //     yield put(doUpdateOrderFailure(failed));
  //   }
  // } catch (error) {
  //   toast.error('Server error');
  //   yield put(doOrderCompleteFailure(error));
  // }
}
export function* getReturnOrderReasonsSaga() {
  try {
    const data = yield call(getReasons);
    if (data.reasons && data.reasons.length) {
      yield put(doOrderReturnReasonsSuccess(data.reasons));
    } else {
      yield put(doOrderReturnReasonsFailure(data));
    }
  } catch (error) {
    yield put(doOrderReturnReasonsFailure(error));
  }
}

export function* getReturnOrderSaga(payload) {
  try {
    const token = yield select(getToken);
    const response = yield call(returnOrder, token, payload.payload);
    if (response.error) {
      toast.error(response.error);
      return;
    }
    if (response.data && !response.data.returned_items) {
      toast.error(response.data);
    } else {
      toast.success('your return item is done successfully');
      if (payload.payload.route) {
        payload.payload.route.push('/account/order-history');
      }
    }
  } catch (error) {
    toast.error('Server error');
    yield put(doGetReturnedItemsFailure(error));
  }
}

async function getInvoice(token, payload) {
  const data = await fetch(
    `${process.env.API_URL}api/v2/storefront/bookkeeping_documents/${payload.invoice_id}`,
    {
      method: 'GET',
      headers: {
        'Content-Type': 'application/pdf',
        Authorization: `Bearer ${token.bearerToken}`,
      },
    }
  );
  return await data.text();
}

export function* getInvoiceSaga({ payload }) {
  try {
    const token = yield select(getToken);
    const invoiceData = yield call(getInvoice, token, payload);
    if (invoiceData.includes('error')) {
      toast.error(JSON.parse(invoiceData).error);
      yield put(doGetInvoiceFailure(JSON.parse(invoiceData).error));
    } else {
      const blob = new Blob([invoiceData], { type: 'application/pdf' });
      const href = window.URL.createObjectURL(blob);
      const theLink = document.createElement('a');
      theLink.href = href;
      theLink.download = payload.invoice_id + '.pdf';
      document.body.appendChild(theLink);
      theLink.click();
      document.body.removeChild(theLink);
      yield put(doGetInvoiceSuccess(invoiceData));
    }
  } catch (error) {
    toast.error(error?.error);
    yield put(doGetInvoiceFailure(error));
  }
}
export async function getWishList(token) {
  const data = await httpService.get('/wishlist', {}, token);
  return await data.data;
  // return client.wishlists.list(token, {
  //   include:
  //     'wished_items,wished_items.variant,wished_items.variant.images,wished_items.variant.product,wished_items.variant.product.images',
  // });
  // const data = await fetch(
  //   `${process.env.API_URL}api/v2/storefront/wishlists?include=wished_items,wished_items.variant,wished_items.variant.images,wished_items.variant.product,wished_items.variant.product.images`,
  //   {
  //     method: 'GET',
  //     headers: {
  //       'Content-Type': 'application/json',
  //       Authorization: `Bearer ${token.bearerToken || token}`,
  //     },
  //   }
  // );
  // return await data.json();
}

// export function wishProductData(response) {
//   let wished_products = response?.data[0]?.relationships?.wished_items?.data;
//   wished_products.map((item) => {
//     let wishProdIndex = response?.included?.findIndex(
//       (resp) => resp.id === item.id && resp.type === 'wished_item'
//     );
//     let variantIndex = response?.included?.findIndex(
//       (ele) =>
//         ele.id ===
//         response.included[wishProdIndex].relationships.variant.data.id &&
//         ele.type === 'variant'
//     );
//     item['variant_id'] =
//       response.included[wishProdIndex].relationships.variant.data.id;
//     let productIndex = response?.included?.findIndex(
//       (ele) =>
//         ele.id ===
//         response.included[variantIndex].relationships.product.data.id &&
//         ele.type === 'product'
//     );
//     item['product_attributes'] = response.included[productIndex].attributes;
//     item['product_id'] = response.included[productIndex].id;
//     if (
//       response.included[productIndex] &&
//       response.included[productIndex].relationships?.images.data.length
//     ) {
//       response?.included?.map((element) => {
//         if (
//           element.id ===
//           response.included[productIndex].relationships?.images.data[0].id &&
//           element.type === 'image'
//         ) {
//           item['original_url'] = element?.attributes?.original_url;
//         }
//       });
//     }
//   });
//   return response;
// }
export function* getWishListDataSaga() {
  try {
    const token = yield select(getToken);
    const response = yield call(getWishList, token.bearerToken);
    if (response.status) {
      yield put(getWishListDataSuccess(response));
    } else {
      const failed = response;
      toast.error(failed);
      yield put(getWishListDataFailure(failed));
    }
  } catch (error) {
    toast.error(error);
    yield put(getWishListDataFailure(error));
  }
}

async function addProductToWishlist(token, payload) {
  const data = await httpService.post('/wishlist', {}, payload, token);
  return await data.data;
  // return client.wishlists.addWishedItem(token, accessHash, {
  //   variant_id: payload?.wished_product?.variant_id,
  //   quantity: payload?.wished_product?.quantity,
  //   include: 'variant,variant.product',
  // });
  // const data = await fetch(
  //   `${process.env.API_URL}api/v2/storefront/wishlists/${accessHash}/wished_products?include=variant,variant.product`,
  //   {
  //     method: 'POST',
  //     headers: {
  //       'Content-Type': 'application/json',
  //       Authorization: `Bearer ${token.bearerToken}`,
  //     },
  //     body: JSON.stringify(payload),
  //   }
  // );
  // return await data.json();
}

export function* addProductToWishListSaga({ payload }) {
  try {
    const token = yield select(getToken);
    const response = yield call(
      addProductToWishlist,
      token.bearerToken,
      payload
    );
    if (response) {
      // const token = yield select(getToken);
      // const accessHash = yield select(getAccessHash);
      // const response = yield call(
      //   addProductToWishlist,
      //   token,
      //   accessHash,
      //   payload
      // );
      // if (response.isSuccess()) {
      //   let wishData = response.success()?.data?.relationships?.variant?.data?.id;
      //   let variantIndex = response
      //     .success()
      //     ?.included.findIndex(
      //       (item) => item.id === wishData && item.type === 'variant'
      //     );
      //   let productId =
      //     response.success().included[variantIndex].relationships.product.data.id;
      //   response.success()?.data['propduct_id'] = productId
      yield put(addProductToWishlistSuccess(response));
      if (payload.isDelete) {
        yield put(removeProductToWishlistSuccess(payload));
      }
    } else {
      const failed = response;
      toast.error(failed);
      yield put(addProductToWishlistFailure(failed));
    }
  } catch (error) {
    yield put(addProductToWishlistFailure(error));
  }
}
// async function removeProductWihlist(token, payload) {
//   const data = await httpService.post('/wishlist', {}, payload, token);
//   return await data.data;
// return client.wishlists.removeWishedItem(token, accessHash, payload);
// const data = await fetch(
//   `${process.env.API_URL}api/v2/storefront/wishlists/${accessHash}/wished_products/${payload}?include=variant`,
//   {
//     method: 'DELETE',
//     headers: {
//       'Content-Type': 'application/json',
//       Authorization: `Bearer ${token.bearerToken}`,
//     },
//   }
// );
// return await data.json();
// }
// export function* removeProductToWishlistSaga({ payload }) {
//   try {
//     const token = yield select(getToken);
//     const response = yield call(
//       addProductToWishlist,
//       token.bearerToken,
//       payload
//     );
//     if (response) {
//       yield put(removeProductToWishlistSuccess(response));
//       toast.success('Remove product to wihlist successfully');
//     } else {
//       const failed = response;
//       toast.error(failed);
//       yield put(removeProductToWishlistFailed(failed));
//     }
//   } catch (error) {
//     yield put(removeProductToWishlistFailed(error));
//   }
// }
