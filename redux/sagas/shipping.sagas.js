import { call, put, select } from 'redux-saga/effects';
import { toast } from 'react-toastify';
import { client } from '../../common/client';
import { checkError } from '../../services/spreeerrors';
import {
  shippingDetailFailed,
  shippingDetailSuccess,
} from '../actionCreaters/shipping.actioncreater';
import { getToken } from '../selectors/auth';
import httpService from '../../common/http.service';

async function getShippingdata() {
  const data = await httpService.get('/shippingcategory');
  return data.data;
  // return client.checkout.shippingMethods(token, {
  //   include: 'shipping_rates,stock_location',
  // });
}
export function* shippingSaga({}) {
  try {
    const response = yield call(getShippingdata);
    if (response.status) {
      const data = response.data;
      yield put(shippingDetailSuccess(data));
    } else {
      const failed = response;
      toast.error(failed);
      yield put(shippingDetailFailed(failed));
    }
  } catch (error) {
    toast.error(error);
    yield put(shippingDetailFailed(error));
  }
}
