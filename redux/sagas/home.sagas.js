import { call, put } from 'redux-saga/effects';
import httpService from '../../common/http.service';
import {
  getHomeDetailFailed,
  getHomeDetailSuccess,
} from '../actionCreaters/home.actioncreater';

async function getHomeDetail() {
  const data = await httpService.get('/home');
  return data.data;
}

export function* getHomesaga() {
  try {
    const response = yield call(getHomeDetail);
    if (response.status) {
      yield put(getHomeDetailSuccess(response));
    }
  } catch (error) {
    yield put(getHomeDetailFailed(error));
  }
}
