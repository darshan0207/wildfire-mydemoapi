import { call, put, select } from 'redux-saga/effects';
import {
  doAccountInfoSuccess,
  doAccountInfoFailure,
  doAccountUpdateSuccess,
  doAccountUpdateFailure,
  doForgotPasswordSuccess,
  doForgotPasswordFailed,
  doResetPasswordSuccess,
  doResetPasswordFailed,
} from '../actionCreaters/account.actioncreater';
import { toast } from 'react-toastify';
import { getToken } from '../selectors/auth';
import axios from 'axios';
import httpService from '../../common/http.service';

async function doAccountInfo(token) {
  const data = await httpService.get('/user/singleuser', {}, token.bearerToken);
  return await data.data;
}

export function getProfileImage(data) {
  if (data?.included) {
    let imageindex = data.included.findIndex(
      (resp) => resp.type === 'user_image'
    );
    if (imageindex !== -1) {
      let imagedata = data.included[imageindex];
      if (imagedata) {
        data?.data?.attributes?.user_image =
          imagedata?.attributes?.original_url;
      }
    }
    return data
  }
}

export function* AccountInfoSaga() {
  try {
    const token = yield select(getToken);
    const response = yield call(doAccountInfo,token);
    if (response.status) {
      const data = response.data;
      // const ImageData = yield call(getProfileImage, data);
      yield put(doAccountInfoSuccess(data));
    } else {
      toast.error(response);
      yield put(doAccountInfoFailure(failed));
    }
  } catch (error) {
        yield put(doAccountInfoFailure(error));
  }
}

async function doUpdateAccountInfo(payload, token) {
  const data = await httpService.put(`/user`,{}, payload, token.bearerToken);
  return await data.data;
}

export function* UpdateAccountInfoSaga({payload}) {
  try {
    const token = yield select(getToken);
    const response = yield call(doUpdateAccountInfo, payload, token);
    if (response.status) {
      const data = response.data;
      // const ImageData = yield call(getProfileImage, data);
      yield put(doAccountInfoSuccess(data));
      let message = 'User Info Updated successfully';
      if (payload.ship_address_id || payload.bill_address_id)
        message = 'Primary address set successfully';
      toast.success(message);
    } else {
      const failed = response;
      toast.error((failed));
      yield put(doAccountUpdateFailure(failed));
    }
  } catch (error) {
    yield put(doAccountUpdateFailure(error));
  }
}

async function forgotPassword(payload) {
  const data = await httpService.post('/auth/forgetpassword', {}, { email: payload.user.email});
  return await data.data;
}

export function* forgotPasswordSaga({ payload }) {
  try {
    const response = yield call(forgotPassword, payload);
    if (response?.status) {
      yield put(doForgotPasswordSuccess(response));
      payload.reset();
      toast.success(response.data);
    } else {
      const failed = response?.message;
      toast.error(failed);
      yield put(doForgotPasswordFailed(failed));
    }
  } catch (error) {
    console.log(error);
    toast.error(error);
    yield put(doForgotPasswordFailed(error));
  }
}

async function resetPassword(payload) {
  let body = {   password: payload.password.user.password,}
  const data = await httpService.post(`/auth/resetpassword/${payload.password.user.uid}`,{}, body);
  return data.data;
  // const data = await fetch(`http://localhost:3002/password-reset/${payload.password.user.uid}`,
  //   {
  //     method: 'POST',
  //     headers: {
  //       'Content-Type': 'application/json',
  //     },
  //     body: JSON.stringify({
  //       password: payload.password.user.password,
  //     }),
  //   }
  // );
  // return await data.json();
  // return client.account.resetPassword(payload.token, payload.password);
}

export function* resetPasswordSaga({ payload }) {
  try {
    const response = yield call(resetPassword, payload);
    console.log(response);
    if (response.status) {
      const token = yield select(getToken);
      if (Object.keys(token).length) {
        payload.router.push('/guest');
      } else {
        payload.router.push('/');
      }
      toast.success('password updated successfully');
    } else {
      toast.error(response.error);
    }
    yield put(doResetPasswordSuccess(response));
  } catch (error) {
    toast.error('Server error');
    yield put(doResetPasswordFailed(error));
  }
}

async function addAccountImage(token, payload) {
  let config = {
    headers: {
      Authorization: `Bearer ${token.bearerToken}`,
      'Content-Type': 'multipart/form-data'
    }
  }
  const formData = new FormData();
  formData.append('image', payload);
  const response = await axios.post(`${process.env.API_URL}api/v2/storefront/account/profile`, formData, config);
  return response;
}


export function* AddAccountProfileSaga({ payload }) {
  try {
    const token = yield select(getToken);
    const response = yield call(addAccountImage, token, payload)
    if (response?.data) {
      yield call(AccountInfoSaga)
    } else {
      toast.error(response?.error);
    }
  } catch (error) {
    toast.error(error);
  }
}