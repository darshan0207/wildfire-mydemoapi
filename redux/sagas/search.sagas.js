import { call, put } from 'redux-saga/effects';
import {
  productFilterFailure,
  productFilterSuccess,
  searchProductFailure,
  searchProductSuccess,
} from '../actionCreaters/search.actioncreater';
import { toast } from 'react-toastify';
import httpService from '../../common/http.service';

async function searchData(payload) {
  const data = await httpService.get(`/product`, payload);
  return data.data;
}

export function* searchSaga({ payload }) {
  try {
    const response = yield call(searchData, payload);
    if (response.status) {
      let data = response.data;
      if (response && response.data && response.data.length) {
        yield put(searchProductSuccess(response));
      } else {
        yield put(searchProductFailure(response));
      }
    } else {
      yield put(searchProductFailure(response));
    }
  } catch (error) {
    // toast.error('Server Error');
    yield put(searchProductFailure(error));
  }
}

async function productFilterData(payload) {
  const data = await httpService.get('/product/filters', { payload });
  return data.data;
}

export function* productFilterSaga({ payload }) {
  try {
    const response = yield call(productFilterData, payload);
    if (response.status) {
      let data = response.data;
      if (response && response.data && response.data.length) {
        yield put(productFilterSuccess(data));
      }
    } else {
      yield put(productFilterFailure(response));
    }
  } catch (error) {
    // toast.error('Server Error');
    yield put(productFilterFailure(error));
  }
}
