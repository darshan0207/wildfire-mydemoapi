import { call, put, select } from 'redux-saga/effects';
import {
  doAddressesListSuccess,
  doAddressesListFailure,
  doRemoveAddressSuccess,
  doRemoveAddressFailure,
  doCreateAddressFailure,
  doCreateAddressSuccess,
  doUpdateAddressSuccess,
  doUpdateAddressFailure,
} from '../actionCreaters/address.actioncreater';
import { toast } from 'react-toastify';
import { getToken } from '../selectors/auth';
import httpService from '../../common/http.service';

async function doAddressesList(token) {
  const tokens = token.bearerToken || token.orderToken;
  const data = await httpService.get('/address', {}, tokens);
  return await data.data;
}

export function* addressesListSaga() {
  try {
    const token = yield select(getToken);
    const response = yield call(doAddressesList, token);
    if (response) {
      const data = response;
      yield put(doAddressesListSuccess(data));
    } else {
      const failed = response;
      toast.error(failed);
      yield put(doAddressesListFailure(failed));
    }
  } catch (error) {
    toast.error('Server error');
    yield put(doAddressesListFailure(error));
  }
}

async function doCreateAddress(payload, token) {
  const data = await httpService.post(
    '/address',
    {},
    payload,
    token.bearerToken
  );
  return await data.data;
}

export function* createAddressSaga({ payload }) {
  console.log(payload);
  try {
    const token = yield select(getToken);
    const response = yield call(doCreateAddress, payload.payload, token);
    if (response) {
      const data = response;
      if (payload.hide) {
        payload.hide();
      }
      toast.success('Address add successfully');
      if (payload.isnextpage) {
        data.data.isnextpage = payload.isnextpage;
      }
      yield put(doCreateAddressSuccess(data));
    } else {
      const failed = response;
      toast.error(failed);
      yield put(doCreateAddressFailure(failed));
    }
  } catch (error) {
    toast.error('Server error');
    yield put(doCreateAddressFailure(error));
  }
}

async function doUpdateAddress(payload, token) {
  const data = await httpService.put(
    `/address/${payload.id}`,
    {},
    payload.payload,
    token.bearerToken
  );
  return await data.data;
}

export function* updateAddressSaga({ payload }) {
  try {
    const token = yield select(getToken);
    const response = yield call(doUpdateAddress, payload, token);
    if (response.status) {
      const data = response;
      if (payload.hide) {
        payload.hide();
      }
      toast.success('Address update successfully');
      yield put(doUpdateAddressSuccess(data.data));
    } else {
      const failed = response;
      toast.error(failed);
      yield put(doUpdateAddressFailure(failed));
    }
  } catch (error) {
    toast.error('Server error');
    yield put(doUpdateAddressFailure(error));
  }
}

async function doRemoveAddress(payload, token) {
  const data = await httpService.delete(
    `/address/${payload.id}`,
    {},
    token.bearerToken
  );
  return await data.data;
}

export function* removeAddressSaga({ payload }) {
  try {
    const token = yield select(getToken);
    const response = yield call(doRemoveAddress, payload, token);
    if (response.status) {
      toast.success('Address delete successfully');
      yield put(doRemoveAddressSuccess(payload));
    } else {
      const failed = response;
      toast.error(failed);
      yield put(doRemoveAddressFailure(failed));
    }
  } catch (error) {
    toast.error('Server error');
    yield put(doRemoveAddressFailure(error));
  }
}
