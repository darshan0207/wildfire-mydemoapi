import { call, put } from 'redux-saga/effects';
import { toast } from 'react-toastify';
import {
  doCountrieFailure,
  doCountrieSuccess,
  doRegionFailure,
  doRegionSuccess,
} from '../actionCreaters/countrie.actioncreater';
import httpService from '../../common/http.service';

async function doCountrieList() {
  const data = await httpService.get('/country');
  return await data.data;
}

export function* countrieListSaga() {
  try {
    const response = yield call(doCountrieList);
    if (response.status) {
      yield put(doCountrieSuccess(response));
    } else {
      const failed = response;
      toast.error(failed);
      yield put(doCountrieFailure(failed));
    }
  } catch (error) {
    yield put(doCountrieFailure(error));
  }
}

async function doRegionList() {
  const data = await httpService.get('/state');
  return await data.data;
}

export function* regionListSaga() {
  try {
    const response = yield call(doRegionList);
    if (response.status) {
      yield put(doRegionSuccess(response));
    } else {
      const failed = response;
      toast.error(failed);
      yield put(doRegionFailure(failed));
    }
  } catch (error) {
    yield put(doRegionFailure(error));
  }
}
