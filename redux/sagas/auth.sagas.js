import { call, put, delay, select } from 'redux-saga/effects';
import {
  doLoginUserSuccess,
  doLoginUserFailure,
  doLogoutUserSuccess,
  doRefreshTokenGenerateSuccess,
  doRegisterUserSuccess,
  doRegisterUserFailure,
  doGuestUserSuccess,
  doRefreshTokenGenerate,
  doRefreshTokenGenerateFailure,
} from '../actionCreaters/auth.actioncreater';
import { toast } from 'react-toastify';
import { client } from '../../common/client';
import { checkError } from '../../services/spreeerrors';
import {
  doCartEmpty,
  getCartDetail,
} from '../actionCreaters/cart.actioncreater';
import { getRefreshToken, getToken, getTokenExpiry } from '../selectors/auth';
import { AUTH_REFRESH_TOKEN_REQUESTED } from '../actionTypes/auth.actiontypes';
import { doWishListEmpty } from '../actionCreaters/order.actioncreater';
import httpService from '../../common/http.service';

async function doRegister(payload) {
  const data = await httpService.post('/auth/signup', {}, payload.user);
  return data.data;
}

async function sdkrefreshToken(refresh_token) {
  return await client.authentication.refreshToken({
    refresh_token,
  });
}

export function* registerSaga({ payload }) {
  try {
    const response = yield call(doRegister, payload);
    if (response.status) {
      // const register = response.success();
      toast.success('User Register Successfully');
      payload.onHide.setShow(false);
      yield put(doRegisterUserSuccess(response.data));
      if (payload.checkout && payload.route) {
        payload.route.push('/guest');
      }
    }
  } catch (error) {
    toast.error(error.error);
    yield put(doRegisterUserFailure(error));
  }
}

async function doLogin(payload) {
  const data = await httpService.post('/auth/login', {}, payload);
  return await data.data;
}

async function getAssociate(token, orderToken) {
  const data = await httpService.post(
    '/cart/associate',
    {},
    { ordertoken: orderToken },
    token
  );
  return data.data;
}

export function* loginSaga({ payload }) {
  try {
    const orderToken = yield select(getToken);
    const response = yield call(doLogin, payload);
    console.log(response);
    if (response.result) {
      const token = response.result;
      if (orderToken.bearerToken || orderToken.orderToken) {
        const tokens = orderToken.bearerToken || orderToken.orderToken;
        yield call(getAssociate, token.token, tokens);
      }
      token.access_token = token.token;
      toast.success('User Login Successfully');
      yield put(doLoginUserSuccess(token));
      yield put(getCartDetail());
      if (payload.route) payload.route.push('/checkout/address');
    } else {
      const failed = response.message;
      toast.error(failed);
      yield put(doLoginUserFailure(failed));
    }
  } catch (error) {
    toast.error(error.error);
    yield put(doLoginUserFailure(error));
  }
}
export function* guestUserSaga({ payload }) {
  try {
    const orderToken = yield select(getToken);
    const response = yield call(doLogin, payload);
    if (response.result) {
      const token = response.result;
      if (orderToken.orderToken) {
        yield call(getAssociate, token.token, orderToken.orderToken);
      }
      token.access_token = token.token;
      toast.success(response.message);
      yield put(doGuestUserSuccess(token));
      if (payload.route) {
        payload.route.push('/checkout/address');
      }
    } else {
      const failed = response.error;
      toast.error(failed);
    }
  } catch (error) {
    toast.error(error.error);
  }
  // yield put(doGuestUserSuccess(payload));
  // if (payload.route) {
  //   payload.route.push('/checkout/address');
  // }
}
export function* logoutSaga() {
  yield put(doLogoutUserSuccess());
  // yield delay(500);
  yield put(doCartEmpty());
  yield put(doWishListEmpty());
  // yield put(getWishListData());
}

export function* refreshTokenSaga() {
  const isServer = typeof window === 'undefined';
  if (!isServer) {
    const tokenExpiry = yield select(getTokenExpiry);

    const now = new Date();
    const twentyMinutes = 1000 * 60 * 20;
    if (tokenExpiry > 0 && tokenExpiry - now.getTime() < twentyMinutes) {
      // actually get refresh token
      const refreshToken = yield select(getRefreshToken);
      const response = yield call(sdkrefreshToken, refreshToken);
      if (response.isSuccess()) {
        const data = response.success();
        yield put(doRefreshTokenGenerateSuccess(data));
      } else {
        const failed = response.fail();
        toast.error(checkError(failed));
        yield put(doRefreshTokenGenerateFailure(failed));
      }
    }
    yield delay(tokenExpiry > 0 ? twentyMinutes : 100);
    yield call(refreshTokenSaga);
  }
}
