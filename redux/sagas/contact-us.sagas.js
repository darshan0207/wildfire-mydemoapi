import { call, put } from 'redux-saga/effects';
import { toast } from 'react-toastify';
import {
  doContactUsSuccess,
  doContactUsFailure,
} from '../actionCreaters/contact-us.actioncreater';

async function contact_us(payload) {
  const data = await fetch(
    `${process.env.API_URL}api/v2/storefront/contact_us`,
    {
      method: 'POST',
      body: JSON.stringify({
        contact: payload.contact,
      }),
      headers: {
        'Content-Type': 'application/json',
      },
    }
  );
  return await data.json();
}

export function* contactUsSaga({ payload }) {
  try {
    const contactusData = yield call(contact_us, payload);
    if (contactusData?.error) {
      toast.error(contactusData.error);
      yield put(doContactUsFailure(contactusData.error));
    } else {
      toast.success(contactusData.notice);
      payload.resetvalue();
      yield put(doContactUsSuccess(contactusData));
    }
  } catch (error) {
    toast.error(error?.error);
    yield put(doContactUsFailure(error));
  }
}
