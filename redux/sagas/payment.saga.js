import { call, put, select } from 'redux-saga/effects';
import { toast } from 'react-toastify';
import { client } from '../../common/client';
import { checkError } from '../../services/spreeerrors';
import { getToken } from '../selectors/auth';
import {
  getCardDetailsSuccess,
  getPaymentDetailFailed,
  getPaymentDetailSuccess,
  removeCardFailed,
  removeCardSuccess,
} from '../actionCreaters/payment.actioncreater';
import { getdata } from './cart.sagas';
import {
  getCartDetail,
  getCartDetailSuccess,
} from '../actionCreaters/cart.actioncreater';
import httpService from '../../common/http.service';

async function getPayment(token) {
  const data = await httpService.get('/paymentmethod');
  return data.data;

  // return client.checkout.paymentMethods(token);
}

export function* getPaymentDetailSaga() {
  try {
    // const token = yield select(getToken);
    const response = yield call(getPayment);
    if (response.status) {
      const data = response;
      yield put(getPaymentDetailSuccess(data));
    } else {
      const failed = response;
      toast.error(failed);
      yield put(getPaymentDetailFailed(failed));
    }
  } catch (error) {
    toast.error(error);
    yield put(getPaymentDetailFailed(error));
  }
}

async function setPromocode(token, payload) {
  return client.cart.applyCouponCode(token, {
    coupon_code: payload.payload.coupon_code,
    include:
      'line_items,variants,variants.images,billing_address,shipping_address,shipments,promotions,variants.product.images',
  });
}

export function* setPromoCodeSaga(payload) {
  try {
    const token = yield select(getToken);
    const response = yield call(setPromocode, token, payload);
    if (response.isSuccess()) {
      yield put(getCartDetail());
      toast.success('Apply coupon code successfully');
    } else {
      const failed = response.fail();
      toast.error(checkError(failed));
    }
  } catch (error) {
    toast.error(error);
  }
}

async function removePromocode(token, payload) {
  return client.cart.removeCouponCode(token, payload.payload.coupon_code, {
    include:
      'line_items,variants,variants.images,billing_address,shipping_address,shipments,promotions,variants.product.images',
  });
}

export function* removePromoCodeSaga(payload) {
  try {
    const token = yield select(getToken);
    const response = yield call(removePromocode, token, payload);
    if (response.isSuccess()) {
      const data = response.success();
      // const cartData = yield call(getdata, data);
      // yield put(getCartDetailSuccess(cartData));
      toast.success('Remove coupon code successfully');
    } else {
      const failed = response.fail();
      toast.error(checkError(failed));
    }
  } catch (error) {
    toast.error(error);
  }
}

async function getCard(token) {
  return client.account.creditCardsList(token);
}

export function* getCardDetailsSaga() {
  try {
    const token = yield select(getToken);
    const response = yield call(getCard, token);
    if (response.isSuccess()) {
      const data = response.success();
      yield put(getCardDetailsSuccess(data));
    } else {
      const failed = response.fail();
      toast.error(checkError(failed));
    }
  } catch (error) {
    toast.error(error);
  }
}

async function removeCard(token, payload) {
  return fetch(
    `${process.env.API_URL}api/v2/storefront/account/credit_cards/${payload}`,
    {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token.bearerToken}`,
      },
    }
  )
    .then((response) => {
      if (response.status === 204) {
        return response.status;
      }
      return response.json();
    })
    .catch((error) => {
      return error;
    });
}

export function* removeCardSaga({ payload }) {
  try {
    const token = yield select(getToken);
    const response = yield call(removeCard, token, payload);
    if (response.error) {
      toast.error(response.error);
      yield put(removeCardFailed(response.error));
    } else {
      toast.success('Remove card detail successfully');
      yield put(removeCardSuccess(payload));
    }
  } catch (error) {
    toast.error(error);
  }
}
