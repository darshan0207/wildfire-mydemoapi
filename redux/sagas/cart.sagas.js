import { call, select, put } from 'redux-saga/effects';
import { toast } from 'react-toastify';
import {
  doOrderTokenSuccess,
  getCartDetailFailed,
  getCartDetailSuccess,
  doOrderTokenFailed,
  updateCartQuantitysucess,
  cartItemDeleteSucess,
} from '../actionCreaters/cart.actioncreater';
import { getToken } from '../selectors/auth';
import { checkError } from '../../services/spreeerrors';
import httpService from '../../common/http.service';

async function getOrderToken() {
  const data = await httpService.get('/auth/getOrderId', {});
  return await data.data;
}

export function* orderTokenSaga(token) {
  try {
    if (!token.orderToken) {
      const response = yield call(getOrderToken);
      if (response.status) {
        const orderToken = response.orderId;
        if (!token.bearerToken) {
          yield put(doOrderTokenSuccess(orderToken));
          return orderToken;
        }
      }
    }
  } catch (error) {
    toast.error(error);
    yield put(doOrderTokenFailed(error));
  }
}

async function getCartData(token) {
  const tokens = token.bearerToken || token.orderToken;
  const data = await httpService.get('/cart', {}, tokens);
  return data.data;
}

export function* cartSaga({}) {
  try {
    const token = yield select(getToken);
    if (Object.keys(token).length) {
      const response = yield call(getCartData, token);
      if (response.status) {
        const data = response.data;
        yield put(getCartDetailSuccess(data));
      } else {
        const failed = response.error;
        yield put(getCartDetailFailed(failed));
      }
    }
  } catch (error) {
    toast.error(error);
    yield put(getCartDetailFailed(error));
  }
}

async function updateQuantity(token, payload) {
  const tokens = token.bearerToken || token.orderToken;
  const data = await httpService.put(
    `/cart/setquantity/${payload.line_item_id}`,
    {},
    { quantity: payload?.quantity },
    tokens
  );
  return data.data;
}

export function* updateQuantitySaga({ payload }) {
  let token;
  try {
    token = yield select(getToken);
    const response = yield call(updateQuantity, token, payload);
    if (response.status) {
      const data = response.data;
      yield put(updateCartQuantitysucess(data));
    } else {
      const failed = response.error;
      toast.error(failed);
      yield put(getCartDetailFailed(failed));
    }
  } catch (error) {
    toast.error(error);
    yield put(getCartDetailFailed(error));
  }
}

async function addToCartItem(token, payload) {
  const tokens = token.bearerToken || token.orderToken;
  const data = await httpService.post('/cart', {}, payload, tokens);
  return data.data;
}
export function* addItemToCartSaga({ payload }) {
  let token;
  try {
    token = yield select(getToken);
    if (!Object.keys(token).length) {
      yield call(orderTokenSaga, token);
    }
    token = yield select(getToken);
    const response = yield call(addToCartItem, token, payload);
    if (response.status) {
      toast.success('Added to cart successfully');
      const data = response.data;
      yield put(getCartDetailSuccess(data));
    } else {
      const failed = response;
      toast.error(checkError(failed));
    }
  } catch (error) {
    toast.error(error);
    toast.error(checkError(error));
  }
}

async function deleteCart(token, payload) {
  const tokens = token.bearerToken || token.orderToken;
  const data = await httpService.delete(
    `/cart/deleteitem/${payload}`,
    {},
    tokens
  );
  return data.data;
}

export function* cartDeleteSaga({ payload }) {
  try {
    let token = yield select(getToken);
    const response = yield call(deleteCart, token, payload);
    if (response.status) {
      toast.success('Cart Item is removed');
      const data = response.data ? response.data : response;
      data.cartId = payload;
      yield put(cartItemDeleteSucess(data));
    } else {
      const failed = response.error;
      toast.error(failed);
    }
  } catch (error) {
    toast.error(error);
  }
}
