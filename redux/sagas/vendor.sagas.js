import { call, put } from 'redux-saga/effects';
import { toast } from 'react-toastify';
import {
  getVendorFailure,
  getVendorProductFailure,
  getVendorProductSuccess,
  getVendorSuccess,
  getVendorVideoFailure,
  getVendorVideoSuccess,
  getVideoDetailsFailure,
  getVideoDetailsSuccess,
  getVideoReviewFailure,
  getVideoReviewSuccess,
  getWatchTvVideoFailure,
  getWatchTvVideoSuccess,
} from '../actionCreaters/vendor.actioncreater';

async function getVendor(name) {
  const data = await fetch(
    `${process.env.API_URL}api/v2/storefront/vendors/${name}?include=image,banner`,
    { method: 'GET' }
  );
  return await data.json();
}
async function getVendorVideo(id, page = 1) {
  const data = await fetch(
    `${process.env.API_URL}api/v2/storefront/videos?include=upload_video,taxons,thumbnail&per_page=6&page=${page}&filter[vendor_ids]=${id}`,
    { method: 'GET' }
  );
  return await data.json();
}
async function getVendorProducts(id, page = 1) {
  const data = await fetch(
    `${process.env.API_URL}api/v2/storefront/products?include=images,taxons,primary_variant.images&per_page=6&page=${page}&filter[vendor_ids]=${id}`,
    { method: 'GET' }
  );
  return await data.json();
}
async function getVideoDetails(id) {
  const data = await fetch(
    `${process.env.API_URL}api/v2/storefront/videos/${id}?include=upload_video,products,vendor,products.images,taxons,vendor.banner,thumbnail`,
    { method: 'GET' }
  );
  return await data.json();
}
async function getVideoReview(id, per_page = 3) {
  const data = await fetch(
    `${process.env.API_URL}api/v2/storefront/video_reviews?filter[video_ids]=${id}&filter[approved]=true&page=1&per_page=${per_page}&include=user,user.image`,
    { method: 'GET' }
  );
  return await data.json();
}

async function videos(data) {
  return await data.data.map((item) => {
    let taxons = item.relationships.taxons.data.map((taxon) => taxon.id);
    item['taxonId'] = taxons;
    item['attributes']['taxon'] = [];
    item['attributes']['products'] = [];
    data.included.map((includedItem) => {
      if (
        item.relationships.upload_video.data &&
        includedItem.id == item.relationships.upload_video.data.id &&
        includedItem.type == 'upload_video'
      ) {
        item['attributes']['video'] = includedItem.attributes.original_url;
      }
      if (
        includedItem.type == 'taxon' &&
        item['taxonId'].indexOf(includedItem.id) > -1
      ) {
        item['attributes']['taxon'].push(includedItem.attributes.name);
      }
      if (includedItem.type == 'vendor_banner') {
        item['attributes']['banner'] = includedItem.attributes.original_url;
      }
      if (includedItem.type == 'vendor') {
        item['attributes']['slug'] = includedItem.attributes.slug;
      }
      if (
        item.relationships.thumbnail.data &&
        includedItem.id == item.relationships?.thumbnail?.data?.id &&
        includedItem.type == 'thumbnail'
      ) {
        item['attributes']['thumbnail'] = includedItem.attributes.original_url;
      }
      if (
        includedItem.type == 'product' &&
        includedItem.relationships?.images?.data[0]?.id
      ) {
        var productImage = data.included.find(
          (product) =>
            includedItem.relationships?.images?.data[0]?.id === product?.id
        );
        includedItem['imageURL'] = productImage?.attributes?.original_url;
        item['attributes']['products'].push(includedItem);
      }
      return item;
    });
  });
}

async function products(data) {
  return await data.data.map((item) => {
    let taxons = item.relationships.taxons.data.map((taxon) => taxon.id);
    item['taxonId'] = taxons;
    item['attributes']['taxon'] = [];
    data.included.map((includedItem) => {
      if (
        includedItem.id == item.relationships.images.data[0].id &&
        includedItem.type == 'image'
      ) {
        item['attributes']['image'] = includedItem.attributes.original_url;
      }
      if (
        includedItem.type == 'taxon' &&
        item['taxonId'].indexOf(includedItem.id) > -1
      ) {
        item['attributes']['taxon'].push(includedItem.attributes.name);
      }
      return item;
    });
  });
}

export function* vendorSaga({ payload }) {
  try {
    const response = yield call(getVendor, payload);
    if (response.data && response.data.id) {
      yield put(
        getVendorSuccess({
          ...response,
          ...{
            videos: { data: [], meta: {} },
            product: { data: [], meta: {} },
          },
        })
      );
    } else {
      yield put(getVendorFailure(response));
    }
  } catch (error) {
    yield put(getVendorFailure(error));
  }
}

export function* vendorVideoSaga({ payload }) {
  try {
    const videoResponse = yield call(getVendorVideo, payload.id, payload.page);
    if (videoResponse.data) {
      yield call(videos, videoResponse);
      yield put(getVendorVideoSuccess(videoResponse));
    } else {
      yield put(getVendorVideoFailure(videoResponse));
    }
  } catch (error) {
    yield put(getVendorVideoFailure(error));
  }
}
export function* vendorProductSaga({ payload }) {
  try {
    const productData = yield call(getVendorProducts, payload.id, payload.page);
    if (productData.data) {
      yield call(products, productData);
      yield put(getVendorProductSuccess(productData));
    } else {
      yield put(getVendorProductFailure(productData));
    }
  } catch (error) {
    yield put(getVendorProductFailure(error));
  }
}

export function* getVideoDetailsSaga({ payload }) {
  try {
    var response = yield call(getVideoDetails, payload);
    if (response.data) {
      yield call(videos, {
        data: new Array(1).fill(response.data),
        included: response.included,
      });
      yield put(
        getVideoDetailsSuccess({
          ...response,
          ...{
            review: { data: [], meta: {} },
          },
        })
      );
    } else {
      yield put(getVideoDetailsFailure(response));
    }
  } catch (error) {
    yield put(getVideoDetailsFailure(error));
  }
}

export function* getVideoReviewSaga({ payload }) {
  try {
    var response = yield call(getVideoReview, payload.id, payload.per_page);
    if (response && response.data && response.data.length) {
      response.data.map((item) => {
        let itemIndex = response.included.findIndex(
          (resp) =>
            resp.id === item.relationships.user.data.id && resp.type === 'user'
        );
        if (itemIndex >= 0) {
          if (response.included[itemIndex].relationships.image.data) {
            let imageindex = response.included.findIndex(
              (resp) =>
                resp.id ===
                  response.included[itemIndex].relationships.image.data.id &&
                resp.type === 'user_image'
            );
            if (imageindex >= 0) {
              let imagedata =
                response.included[imageindex].attributes.original_url;
              item['original_url'] = imagedata;
            }
          }
        }
      });
    }
    if (response.data) {
      yield put(getVideoReviewSuccess(response));
    } else {
      yield put(getVideoReviewFailure(response));
    }
  } catch (error) {
    yield put(getVideoReviewFailure(error));
  }
}
async function getwatchTvVideo(payload) {
  const data = await fetch(
    `${process.env.API_URL}api/v2/storefront/videos?filter[name]=${
      payload?.name ? payload?.name : ''
    }&per_page=4&page=${
      payload?.page ? payload?.page : 1
    }&include=upload_video,products,products.images,vendor,primary_product.images,thumbnail`,
    {
      method: 'GET',
    }
  );
  return await data.json();
}

export function* watchTvVideoSaga({ payload }) {
  try {
    const response = yield call(getwatchTvVideo, payload);
    if (response?.data) {
      response.data.map((item) => {
        let productarr = [];
        response.included.map((element) => {
          if (
            element?.id === item?.relationships?.upload_video?.data?.id &&
            element?.type === 'upload_video'
          ) {
            item['video_url'] = element?.attributes?.original_url;

            let thumbnailIndex = response.included.findIndex(
              (ele) =>
                ele.id === item?.relationships?.thumbnail?.data?.id &&
                ele.type === 'thumbnail'
            );
            if (thumbnailIndex) {
              let thumbnaildata = response.included[thumbnailIndex]?.attributes;
              if (thumbnaildata?.original_url) {
                item['thumbnail'] = thumbnaildata?.original_url;
              }
            }
          }
        });
        if (item?.relationships?.primary_product?.data) {
          let primaryproductid = item?.relationships?.primary_product?.data?.id;
          let primaryProductIndex = response.included.findIndex(
            (ele) => ele.id === primaryproductid && ele.type === 'product'
          );
          if (primaryProductIndex) {
            let primaryproductdata = response.included[primaryProductIndex];
            if (
              response.included[primaryProductIndex].relationships?.images?.data
                ?.length
            ) {
              let primaryimagesarr =
                response.included[primaryProductIndex].relationships?.images
                  ?.data;
              let primaryimageindex = response.included.findIndex(
                (ele) =>
                  ele.id === primaryimagesarr[0].id && ele.type === 'image'
              );
              if (primaryimageindex) {
                let primaryimagedata =
                  response.included[primaryimageindex]?.attributes
                    ?.original_url;
                primaryproductdata['original_url'] = primaryimagedata;
              }
            }
            productarr.push(primaryproductdata);
          }
        }

        if (item?.relationships?.secondary_products?.data?.length) {
          item?.relationships?.secondary_products?.data?.map((element) => {
            if (element?.id !== primaryproductid) {
              let secondaryProductIndex = response.included.findIndex(
                (ele) => ele.id === element.id && ele.type === 'product'
              );
              if (secondaryProductIndex) {
                let secondaryproductdata =
                  response.included[secondaryProductIndex];
                if (
                  response.included[secondaryProductIndex].relationships?.images
                    ?.data?.length
                ) {
                  let secondaryimagesarr =
                    response.included[secondaryProductIndex].relationships
                      ?.images?.data;
                  let secondaryimageindex = response.included.findIndex(
                    (ele) =>
                      ele.id === secondaryimagesarr[0].id &&
                      ele.type === 'image'
                  );
                  if (secondaryimageindex) {
                    let secondarimagedata =
                      response.included[secondaryimageindex]?.attributes
                        ?.original_url;
                    secondaryproductdata['original_url'] = secondarimagedata;
                  }
                }
                productarr.push(secondaryproductdata);
              }
            }
          });
        }
        item['product'] = productarr;
      });
      yield put(getWatchTvVideoSuccess(response));
    } else {
      yield put(getWatchTvVideoFailure(response.data));
    }
  } catch (error) {
    yield put(getWatchTvVideoFailure(error));
  }
}
