import { call, put, select } from 'redux-saga/effects';
import {
  singleProductFailure,
  singleProductSuccess,
  productVariantSuccess,
  getReviewRatingSuccess,
  getReviewRatingFailed,
  addReviewRatingSuccess,
  addReviewRatingFailed,
} from '../actionCreaters/product.actioncreater';
import { getToken } from '../selectors/auth';
import { toast } from 'react-toastify';
import httpService from '../../common/http.service';

async function singleProductData(payload) {
  const data = await httpService.get(`/product/${payload}`, {});
  return data.data;
}

export function* singleProduct({ payload }) {
  try {
    const response = yield call(singleProductData, payload);
    if (response.data) {
      const data = response.data;
      if (data?.optiontype?.length) {
        data?.optiontype?.map((item, index) => {
          item.optionvalue = [];
          item.position = index + 1;
          if (data?.variants?.length) {
            data?.variants?.map((variantdata) => {
              if (variantdata && variantdata.optionvalueids.length) {
                let data1 = variantdata.optionvalueids.find(
                  (data) => data.optiontypeid == item._id
                );
                if (data1) {
                  item.optionvalue.push(data1);
                  const uniqueBy = (x, f) =>
                    Object.values(x.reduce((a, b) => ((a[f(b)] = b), a), {}));
                  item.optionvalue = uniqueBy(item.optionvalue, (v) => v._id);
                }
              }
            });
          }
        });
      }
      yield put(singleProductSuccess(data));
    } else {
      yield put(singleProductFailure(response.error));
    }
  } catch (error) {
    yield put(singleProductFailure(error));
  }
}

export function* productVariantSaga({ payload }) {
  yield put(productVariantSuccess(payload));
}

async function getReviewRating(payload) {
  // return fetch(
  //   `${process.env.API_URL}api/v2/storefront/reviews?filter[product_slugs]=${payload.slug}&filter[approved]=true&per_page=${payload.per_page}&page=${payload.page}&include=user,product,user.image`,
  //   {
  //     method: 'GET',
  //     headers: {
  //       'Content-Type': 'application/json',
  //     },
  //   }
  // )
  //   .then((response) => response.json())
  //   .then((data) => {
  //     return data;
  //   })
  //   .catch((error) => {
  //     return error;
  //   });
}

export function* getRatingReviewSaga({ payload }) {
  try {
    const data = yield call(getReviewRating, payload);
    if (data && data.data && data.data.length) {
      data.data.map((item) => {
        let itemIndex = data.included.findIndex(
          (resp) =>
            resp.id === item.relationships.user.data.id && resp.type === 'user'
        );
        if (itemIndex >= 0) {
          if (data.included[itemIndex].relationships.image.data) {
            let imageindex = data.included.findIndex(
              (resp) =>
                resp.id ===
                  data.included[itemIndex].relationships.image.data.id &&
                resp.type === 'user_image'
            );
            if (imageindex >= 0) {
              let imagedata = data.included[imageindex].attributes.original_url;
              item['original_url'] = imagedata;
            }
          }
        }
      });
    }
    if (data) {
      yield put(getReviewRatingSuccess(data));
    } else {
      yield put(getReviewRatingFailed(data));
    }
  } catch (error) {
    yield put(getReviewRatingFailed(error));
  }
}

async function addReviewRating(token, payload) {
  var api = 'reviews',
    payload = {
      review: payload.review,
    };
  if (payload.review.video) {
    api = 'video_reviews';
    payload = {
      video_review: payload.review,
    };
  }
  return fetch(`${process.env.API_URL}api/v2/storefront/${api}`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token.bearerToken}`,
    },
    body: JSON.stringify(payload),
  })
    .then((response) => response.json())
    .then((data) => {
      return data;
    })
    .catch((error) => {
      return error;
    });
}

export function* addReviewRatingSaga({ payload }) {
  try {
    const token = yield select(getToken);
    const response = yield call(addReviewRating, token, payload);
    if (response.error) {
      toast.error(response?.error);
      yield put(addReviewRatingFailed(response.error));
    } else {
      toast.success('Add review successfully');
      payload.Show(false);
      payload.resetvalue();
      yield put(addReviewRatingSuccess(response));
    }
  } catch (error) {
    toast.error(error?.error);
    yield put(addReviewRatingFailed(error.error));
  }
}
