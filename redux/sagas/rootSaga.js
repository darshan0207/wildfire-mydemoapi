import { takeLatest, fork } from 'redux-saga/effects';
import {
  guestUserSaga,
  loginSaga,
  logoutSaga,
  refreshTokenSaga,
  registerSaga,
} from './auth.sagas';
import {
  AUTH_LOGIN_REQUESTED,
  AUTH_LOGOUT_REQUESTED,
  AUTH_REGISTER_REQUESTED,
  GUEST_USER,
  AUTH_REFRESH_TOKEN_REQUESTED,
} from '../actionTypes/auth.actiontypes';
import {
  SEARCH_PRODUCT_REQUESTED,
  SEARCH_MORE_PRODUCT_REQUESTED,
  PRODUCT_FILTER_REQUESTED,
} from '../actionTypes/search.actiontypes';
import { productFilterSaga, searchSaga } from './search.sagas';
import {
  AccountInfoSaga,
  AddAccountProfileSaga,
  forgotPasswordSaga,
  resetPasswordSaga,
  UpdateAccountInfoSaga,
} from './account.sagas';
import {
  ACCOUNT_INFO_REQUESTED,
  ACCOUNT_UPDATE_REQUESTED,
  ADD_PROFILE_IMAGE_REQUESTED,
  FORGOT_PASSWORD_REQUESTED,
  RESET_PASSWORD_REQUESTED,
} from '../actionTypes/account.actiontypes';
import {
  addressesListSaga,
  createAddressSaga,
  removeAddressSaga,
  updateAddressSaga,
} from './address.sagas';
import {
  ADDRESSES_LIST_REQUESTED,
  CREATE_ADDRESSES_REQUESTED,
  REMOVE_ADDRESSES_REQUESTED,
  UPDATE_ADDRESSES_REQUESTED,
} from '../actionTypes/address.actiontypes';
import {
  addProductToWishListSaga,
  completedOrdersListSaga,
  completeOrderSaga,
  getInvoiceSaga,
  getReturnOrderReasonsSaga,
  getReturnOrderSaga,
  getSingleOrderDetailSaga,
  getWishListDataSaga,
  // removeProductToWishlistSaga,
  updateOrdersListSaga,
} from './order.sagas';
import {
  ADD_PRODUCT_TO_WISHLIST_REQUESTED,
  COMPLETED_MORE_ORDER_REQUESTED,
  COMPLETED_ORDER_LIST_REQUESTED,
  GET_INVOICE_REQUESTED,
  GET_SINGLE_ORDER_REQUESTED,
  GET_WISHLIST_DATALIST_REQUESTED,
  ORDER_COMPLETE_REQUESTED,
  ORDER_RETURN_REASONS_REQUESTED,
  ORDER_RETURN_REQUESTED,
  UPDATE_ORDER_REQUESTED,
} from '../actionTypes/order.actiontypes';
import {
  ADD_REVIEW_RATING_REQUESTED,
  GET_PRODUCT_RATING_REVIEW_REQUESTED,
  GET_SINGLE_PRODUCT_REQUESTED,
  PRODUCT_VARIANT_REQUESTED,
  RELATED_PRODUCT_REQUESTED,
} from '../actionTypes/product.actiontypes';
import {
  addReviewRatingSaga,
  getRatingReviewSaga,
  productVariantSaga,
  singleProduct,
} from './product.sagas';
import {
  ADD_CART_DETAILS_REQUESTED,
  DELETE_CART_DETAILS_REQUESTED,
  GET_CART_DETAILS_REQUESTED,
  UPDATE_CART_QUANTITY_REQUESTED,
} from '../actionTypes/cart.actiontypes';
import {
  addItemToCartSaga,
  cartDeleteSaga,
  cartSaga,
  updateQuantitySaga,
} from './cart.sagas';
import {
  GET_SHIPPING_DETAIL_REQUESTED,
  SET_SHIPPING_UPDATEORDER_REQUESTED,
} from '../actionTypes/shipping.actiontypes';
import { shippingSaga } from './shipping.sagas';
import {
  GET_CARD_DETAILS_REQUESTED,
  GET_PAYMENT_DETAILED_REQUESTED,
  REMOVE_CARD_REQUESTED,
  REMOVE_COUPONCODE_REQUESTED,
  SET_COUPONCODE_REQUESTED,
} from '../actionTypes/payment.actionstypes';
import {
  getCardDetailsSaga,
  getPaymentDetailSaga,
  removeCardSaga,
  removePromoCodeSaga,
  setPromoCodeSaga,
} from './payment.saga';
import { HOME_DETAIL_REQUESTED } from '../actionTypes/home.actiontypes';
import { getHomesaga } from './home.sagas';
import {
  getVideoDetailsSaga,
  getVideoReviewSaga,
  vendorProductSaga,
  vendorSaga,
  vendorVideoSaga,
  watchTvVideoSaga,
} from './vendor.sagas';
import {
  GET_VENDOR_PRODUCTS_REQUESTED,
  GET_VENDOR_REQUESTED,
  GET_VENDOR_VIDEO_REQUESTED,
  GET_VIDEODETAILS_REQUESTED,
  GET_VIDEO_REVIEW_REQUESTED,
  GET_WATCH_TV_VIDEOS_REQUESTED,
} from '../actionTypes/vendor.actiontypes';
import { CONTACT_US_REQUESTED } from '../actionTypes/contact-us.actiontypes';
import { contactUsSaga } from './contact-us.sagas';
import {
  GET_COUNTRIE_REQUESTED,
  GET_REGION_REQUESTED,
} from '../actionTypes/countrie.actiontypes';
import { countrieListSaga, regionListSaga } from './countrie.sagas';

function* rootSaga() {
  yield fork(refreshTokenSaga);
  yield takeLatest(AUTH_LOGIN_REQUESTED, loginSaga);
  yield takeLatest(AUTH_LOGOUT_REQUESTED, logoutSaga);
  yield takeLatest(SEARCH_PRODUCT_REQUESTED, searchSaga);
  yield takeLatest(SEARCH_MORE_PRODUCT_REQUESTED, searchSaga);
  yield takeLatest(ACCOUNT_INFO_REQUESTED, AccountInfoSaga);
  yield takeLatest(ACCOUNT_UPDATE_REQUESTED, UpdateAccountInfoSaga);
  yield takeLatest(ADDRESSES_LIST_REQUESTED, addressesListSaga);
  yield takeLatest(CREATE_ADDRESSES_REQUESTED, createAddressSaga);
  yield takeLatest(UPDATE_ADDRESSES_REQUESTED, updateAddressSaga);
  yield takeLatest(REMOVE_ADDRESSES_REQUESTED, removeAddressSaga);
  yield takeLatest(AUTH_REGISTER_REQUESTED, registerSaga);
  yield takeLatest(COMPLETED_ORDER_LIST_REQUESTED, completedOrdersListSaga);
  yield takeLatest(COMPLETED_MORE_ORDER_REQUESTED, completedOrdersListSaga);
  yield takeLatest(GET_SINGLE_ORDER_REQUESTED, getSingleOrderDetailSaga);
  yield takeLatest(PRODUCT_VARIANT_REQUESTED, productVariantSaga);
  yield takeLatest(FORGOT_PASSWORD_REQUESTED, forgotPasswordSaga);
  yield takeLatest(RESET_PASSWORD_REQUESTED, resetPasswordSaga);
  yield takeLatest(GET_CART_DETAILS_REQUESTED, cartSaga);
  yield takeLatest(UPDATE_CART_QUANTITY_REQUESTED, updateQuantitySaga);
  yield takeLatest(ADD_CART_DETAILS_REQUESTED, addItemToCartSaga);
  yield takeLatest(GUEST_USER, guestUserSaga);
  yield takeLatest(UPDATE_ORDER_REQUESTED, updateOrdersListSaga);
  yield takeLatest(GET_SHIPPING_DETAIL_REQUESTED, shippingSaga);
  yield takeLatest(GET_PAYMENT_DETAILED_REQUESTED, getPaymentDetailSaga);
  yield takeLatest(SET_COUPONCODE_REQUESTED, setPromoCodeSaga);
  yield takeLatest(ORDER_COMPLETE_REQUESTED, completeOrderSaga);
  yield takeLatest(REMOVE_COUPONCODE_REQUESTED, removePromoCodeSaga);
  yield takeLatest(AUTH_REFRESH_TOKEN_REQUESTED, refreshTokenSaga);
  yield takeLatest(DELETE_CART_DETAILS_REQUESTED, cartDeleteSaga);
  yield takeLatest(HOME_DETAIL_REQUESTED, getHomesaga);
  yield takeLatest(GET_VENDOR_REQUESTED, vendorSaga);
  yield takeLatest(GET_VENDOR_VIDEO_REQUESTED, vendorVideoSaga);
  yield takeLatest(GET_VENDOR_PRODUCTS_REQUESTED, vendorProductSaga);
  yield takeLatest(ORDER_RETURN_REASONS_REQUESTED, getReturnOrderReasonsSaga);
  yield takeLatest(ORDER_RETURN_REQUESTED, getReturnOrderSaga);
  yield takeLatest(GET_CARD_DETAILS_REQUESTED, getCardDetailsSaga);
  yield takeLatest(REMOVE_CARD_REQUESTED, removeCardSaga);
  yield takeLatest(GET_PRODUCT_RATING_REVIEW_REQUESTED, getRatingReviewSaga);
  yield takeLatest(ADD_REVIEW_RATING_REQUESTED, addReviewRatingSaga);
  yield takeLatest(GET_INVOICE_REQUESTED, getInvoiceSaga);
  yield takeLatest(CONTACT_US_REQUESTED, contactUsSaga);
  yield takeLatest(GET_WISHLIST_DATALIST_REQUESTED, getWishListDataSaga);
  yield takeLatest(ADD_PRODUCT_TO_WISHLIST_REQUESTED, addProductToWishListSaga);
  yield takeLatest(GET_VIDEODETAILS_REQUESTED, getVideoDetailsSaga);
  yield takeLatest(GET_VIDEO_REVIEW_REQUESTED, getVideoReviewSaga);
  yield takeLatest(GET_WATCH_TV_VIDEOS_REQUESTED, watchTvVideoSaga);
  yield takeLatest(ADD_PROFILE_IMAGE_REQUESTED, AddAccountProfileSaga);
  yield takeLatest(GET_COUNTRIE_REQUESTED, countrieListSaga);
  yield takeLatest(GET_REGION_REQUESTED, regionListSaga);
  yield takeLatest(PRODUCT_FILTER_REQUESTED, productFilterSaga);
  yield takeLatest(GET_SINGLE_PRODUCT_REQUESTED, singleProduct);
}
export default rootSaga;
