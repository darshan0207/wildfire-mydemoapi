import { handleActions } from 'redux-actions';
import produce from 'immer';
import * as actionCreators from '../actionCreaters/product.actioncreater.js';

const defaultState = {
  data: {},
  message: '',
};

const singleProductReducer = handleActions(
  {
    [actionCreators.singleProduct]: produce((draft) => {
      draft.data = {};
      draft.message = '';
    }),
    [actionCreators.singleProductSuccess]: produce((draft, { payload }) => {
      draft.data = payload;
    }),
    [actionCreators.singleProductFailure]: produce((draft, { payload }) => {
      draft.message = payload;
      draft.data = {};
    }),
    [actionCreators.productVariant]: produce((draft) => {
      draft.variant = [];
    }),
    [actionCreators.productVariantSuccess]: produce((draft, { payload }) => {
      if (draft.data?.optiontype?.length) {
        draft.data.optiontype.slice(payload.position).map((options) => {
          options.optionvalue.map((item, index) => {
            // if (options.position !== 1) {
            if (!payload.optiontype.includes(item._id)) {
              item.isactive = true;
            } else {
              delete item.isactive;
            }
            // }
          });
        });
      }
      draft.variant = draft.data;
    }),
    // [actionCreators.getRelatedProduct]: produce((draft) => {
    //   draft.relatedProduct = [];
    //   draft.message = '';
    // }),
    // [actionCreators.getRelatedProductSuccess]: produce(
    //   (draft, { payload: { data } }) => {
    //     draft.relatedProduct = data;
    //   }
    // ),
    // [actionCreators.getRelatedProductFailed]: produce((draft, { payload }) => {
    //   draft.message = payload;
    // }),
    // [actionCreators.getReviewRating]: produce((draft) => {
    //   // draft.ratingReview = [];
    //   draft.meta = {};
    //   draft.message = '';
    // }),
    // [actionCreators.getReviewRatingSuccess]: produce((draft, { payload }) => {
    //   draft.ratingReview = payload.data;
    //   draft.meta = payload.meta;
    // }),
    // [actionCreators.getReviewRatingFailed]: produce((draft, { payload }) => {
    //   draft.message = payload;
    // }),
    // [actionCreators.addReviewRating]: produce((draft) => {
    //   draft.message = '';
    // }),
    // [actionCreators.addReviewRatingSuccess]: produce(
    //   (draft, { payload: { data, meta } }) => {
    //     // draft.ratingReview.unshift(data);
    //     // draft.meta = meta;
    //   }
    // ),
    // [actionCreators.addReviewRatingFailed]: produce((draft, { payload }) => {
    //   draft.message = payload;
    // }),
  },
  defaultState
);

export default singleProductReducer;
