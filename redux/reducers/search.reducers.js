import { handleActions } from 'redux-actions';
import produce from 'immer';

import * as actionCreators from '../actionCreaters/search.actioncreater.js';

const defaultState = {
  data: [],
  included: [],
  meta: [],
  message: '',
  isloader: true,
  paginate: [],
};

const searchReducer = handleActions(
  {
    [actionCreators.searchProduct]: produce((draft) => {
      draft.data = [];
      draft.isloader = true;
    }),
    [actionCreators.searchProductSuccess]: produce(
      (draft, { payload: { data, paginate } }) => {
        draft.data.push(...data);
        // draft.included = included;
        draft.paginate = paginate;
        draft.isloader = false;
      }
    ),
    [actionCreators.searchProductFailure]: produce((draft, { payload }) => {
      draft.message = payload;
      draft.isloader = false;
    }),
    [actionCreators.productFilter]: produce((draft) => {
      draft.meta = [];
      draft.isloader = true;
    }),
    [actionCreators.productFilterSuccess]: produce((draft, { payload }) => {
      draft.meta = payload;
      draft.isloader = false;
    }),
    [actionCreators.productFilterFailure]: produce((draft, { payload }) => {
      draft.message = payload;
      draft.isloader = false;
    }),
  },
  defaultState
);

export default searchReducer;
