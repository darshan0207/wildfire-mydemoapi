import { handleActions } from 'redux-actions';
import produce from 'immer';
import * as actionCreators from '../actionCreaters/vendor.actioncreater';

const defaultState = {
  vendor: {},
  videoDetails: {},
  watchTv: {},
  message: '',
  isLoader: false,
};

const vendorReducer = handleActions(
  {
    [actionCreators.getVendor]: produce((draft) => {
      draft.vendor = {};
    }),
    [actionCreators.getVendorSuccess]: produce((draft, { payload }) => {
      draft.vendor = payload;
    }),
    [actionCreators.getVendorFailure]: produce((draft, { payload }) => {
      draft.vendor['error'] = payload;
    }),
    [actionCreators.getVendorVideoSuccess]: produce((draft, { payload }) => {
      draft.vendor['videos']['data'].push(...payload.data);
      draft.vendor['videos']['meta'] = payload.meta;
    }),
    [actionCreators.getVendorProductSuccess]: produce((draft, { payload }) => {
      draft.vendor['product']['data'].push(...payload.data);
      draft.vendor['product']['meta'] = payload.meta;
    }),
    [actionCreators.getVideoDetails]: produce((draft) => {
      draft.videoDetails = {};
    }),
    [actionCreators.getVideoDetailsSuccess]: produce((draft, { payload }) => {
      draft.videoDetails = payload;
    }),
    [actionCreators.getVideoDetailsFailure]: produce((draft, { payload }) => {
      draft.vendor['error'] = payload;
    }),
    // [actionCreators.getVideoReview]: produce((draft) => {
    //   draft.videoDetails["review"]["data"] = [];
    //   draft.videoDetails["review"]["meta"] = {};
    // }),
    [actionCreators.getVideoReviewSuccess]: produce((draft, { payload }) => {
      draft.videoDetails['review']['data'].push(...payload.data);
      draft.videoDetails['review']['meta'] = payload.meta;
    }),
    [actionCreators.getVideoReviewFailure]: produce((draft, { payload }) => {
      draft.videoDetails['review'] = payload;
    }),
    [actionCreators.getWatchTvVideo]: produce((draft) => {
      draft.watchTv = {};
      draft.isLoader = true;
    }),
    [actionCreators.getWatchTvVideoSuccess]: produce((draft, { payload }) => {
      draft.watchTv['data'] = payload?.data?.filter((data) => data.video_url);
      draft.watchTv['meta'] = payload.meta;
      draft.isLoader = false;
    }),
    [actionCreators.getWatchTvVideoFailure]: produce((draft, { payload }) => {
      draft.message = payload;
      draft.isLoader = false;
    }),
  },
  defaultState
);

export default vendorReducer;
