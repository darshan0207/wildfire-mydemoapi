import { handleActions } from 'redux-actions';
import * as actionCreators from '../actionCreaters/payment.actioncreater';
import produce from 'immer';

const defaultState = {
  data: [],
  included: [],
  message: '',
  isLoader: false,
  card: {},
};

const paymentReducer = handleActions(
  {
    [actionCreators.getPaymentDetail]: produce((draft) => {
      draft.data = [];
      draft.isLoader = true;
      draft.card = {};
    }),
    [actionCreators.getPaymentDetailSuccess]: produce(
      (draft, { payload: { data } }) => {
        draft.data = data;
        draft.isLoader = false;
        draft.card = {};
      }
    ),
    [actionCreators.getPaymentDetailFailed]: produce((draft, { payload }) => {
      draft.message = payload;
      draft.isLoader = false;
      draft.card = {};
    }),
    [actionCreators.doCardDetails]: produce((draft, { payload }) => {
      draft.card = payload;
    }),
    [actionCreators.getCardDetails]: produce((draft) => {
      draft.cardDetail = [];
    }),
    [actionCreators.getCardDetailsSuccess]: produce(
      (draft, { payload: { data } }) => {
        draft.cardDetail = data;
      }
    ),
    [actionCreators.getCardDetailsFailed]: produce((draft, { payload }) => {
      draft.message = payload;
    }),
    [actionCreators.removeCardSuccess]: produce((draft, { payload }) => {
      draft.cardDetail.splice(
        draft.cardDetail.findIndex((data) => data.id === payload),
        1
      );
    }),
    [actionCreators.removeCardFailed]: produce((draft, { payload }) => {
      draft.message = payload;
    }),
  },
  defaultState
);
export default paymentReducer;
