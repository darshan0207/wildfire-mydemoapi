import { handleActions } from 'redux-actions';
import produce from 'immer';
import * as actionCartCreators from '../actionCreaters/cart.actioncreater';
import * as actionCreators from '../actionCreaters/auth.actioncreater';
import * as actionOrderCreater from '../actionCreaters/order.actioncreater';

const defaultState = {
  isAuthenticated: false,
  isUserLoggingIn: false,
  isUserLoggingOut: false,
  isGuestUser: false,
  authenticatedUserObject: {},
  accessToken: '',
  tokenType: '',
  expiresIn: 0,
  refreshToken: '',
  createdAt: 0,
  message: null,
  email: '',
  store_credits: '',
  completed_orders: '',
  orderToken: '',
  hashToken: '',
  wishlist: [],
};

const authReducer = handleActions(
  {
    [actionCreators.doRegisterUser]: produce((draft) => {
      draft.isUserLoggingIn = true;
    }),
    [actionCreators.doRegisterUserSuccess]: produce((draft, { payload }) => {
      draft.isUserLoggingIn = false;
      draft.email = payload.attributes.email;
      draft.store_credits = payload.attributes.store_credits;
      draft.completed_orders = payload.attributes.completed_orders;
      // draft.orderToken = '';
    }),
    [actionCreators.doRegisterUserFailure]: produce((draft, { payload }) => {
      draft.message = payload;
    }),
    [actionCreators.doLoginUser]: produce((draft) => {
      draft.isUserLoggingIn = true;
      draft.isAuthenticated = false;
    }),
    [actionCreators.doLoginUserSuccess]: produce(
      (draft, { payload: { access_token, createdAt, email, wishlist } }) => {
        draft.isUserLoggingIn = false;
        draft.isAuthenticated = true;
        draft.isGuestUser = false;
        draft.accessToken = access_token;
        draft.email = email;
        draft.createdAt = createdAt;
        draft.wishlist = wishlist;
      }
    ),
    [actionCreators.doLoginUserFailure]: produce((draft, { payload }) => {
      draft.isAuthenticated = false;
      draft.isUserLoggingIn = false;
      draft.message = payload;
    }),
    [actionCreators.doLogoutUser]: produce((draft) => {
      draft.isUserLoggingOut = true;
    }),
    [actionCreators.doLogoutUserSuccess]: produce((draft) => {
      draft.isUserLoggingOut = false;
      draft.isAuthenticated = false;
      draft.accessToken = '';
      draft.tokenType = '';
      draft.expiresIn = 0;
      draft.refreshToken = '';
      draft.email = '';
      draft.createdAt = 0;
      draft.email = '';
      draft.hashToken = '';
      draft.orderToken = '';
      draft.isGuestUser = false;
      draft.wishlist = [];
    }),
    [actionCreators.doLogoutUserFailure]: produce((draft) => {
      draft.isUserLoggingOut = false;
      draft.isAuthenticated = false;
      draft.authenticatedUserObject = {};
    }),
    [actionCartCreators.doOrderTokenSuccess]: produce((draft, { payload }) => {
      draft.orderToken = payload;
    }),
    [actionCartCreators.doOrderTokenFailed]: produce((draft) => {
      draft.orderToken = '';
      draft.isGuestUser = false;
      draft.accessToken = '';
    }),
    [actionCartCreators.doRemoveOrderToken]: produce((draft) => {
      draft.orderToken = '';
      draft.isGuestUser = false;
    }),
    [actionCreators.doGuestUserSuccess]: produce((draft, { payload }) => {
      draft.email = payload.email;
      draft.accessToken = payload.access_token;
      draft.isGuestUser = true;
    }),
    [actionCreators.doRefreshTokenGenerateSuccess]: produce(
      (draft, { payload }) => {
        const {
          access_token,
          token_type,
          expires_in,
          refresh_token,
          created_at,
          email,
        } = payload;
        draft.isUserLoggingIn = false;
        draft.isAuthenticated = true;
        draft.accessToken = access_token;
        draft.tokenType = token_type;
        draft.expiresIn = expires_in;
        draft.refreshToken = refresh_token;
        draft.createdAt = created_at;
        draft.isGuestUser = false;
      }
    ),
    [actionCreators.doRefreshTokenGenerateFailure]: produce(
      (draft, { payload }) => {
        draft.isUserLoggingOut = false;
        draft.isAuthenticated = false;
        draft.accessToken = '';
        draft.tokenType = '';
        draft.expiresIn = 0;
        draft.refreshToken = '';
        draft.email = '';
        draft.createdAt = 0;
        draft.email = '';
      }
    ),
    [actionOrderCreater.addProductToWishlistSuccess]: produce(
      (draft, { payload }) => {
        draft.wishlist = payload.wishlist;
      }
    ),
  },
  defaultState
);

export default authReducer;
