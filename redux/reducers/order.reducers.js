import { handleActions } from 'redux-actions';
import produce from 'immer';

import * as actionCreators from '../actionCreaters/order.actioncreater';

const defaultState = {
  orderList: [],
  orderMeta: {},
  orderDetails: {},
  message: null,
  included: [],
  isloader: false,
  recentOrder: {},
  orderReturnReasons: [],
  returnItem: {},
  wishList: [],
};

const orderReducer = handleActions(
  {
    [actionCreators.doCompletedOrdersList]: produce((draft) => {
      draft.orderList = [];
      draft.orderMeta = {};
      draft.message = null;
      draft.isloader = true;
    }),
    [actionCreators.doCompletedOrdersListSuccess]: produce(
      (draft, { payload: { data, paginate } }) => {
        draft.orderList.push(...data);
        draft.orderMeta = paginate;
        draft.isloader = false;
      }
    ),
    [actionCreators.doCompletedOrdersListFailure]: produce(
      (draft, { payload }) => {
        draft.message = payload;
        draft.isloader = false;
      }
    ),
    [actionCreators.getSingleOrderDetail]: produce((draft) => {
      draft.orderDetails = {};
      draft.isloader = true;
    }),
    [actionCreators.getSingleOrderDetailSuccess]: produce(
      (draft, { payload }) => {
        draft.orderDetails = payload;
        draft.isloader = false;
        draft.returnItem = {};
      }
    ),
    [actionCreators.getSingleOrderDetailFailed]: produce(
      (draft, { payload }) => {
        draft.message = payload;
        draft.isloader = false;
      }
    ),
    [actionCreators.doUpdateOrder]: produce((draft) => {
      draft.orderDetails = {};
      draft.isloader = true;
    }),
    [actionCreators.doUpdateOrderSuccess]: produce(
      (draft, { payload: { data } }) => {
        draft.orderDetails = data;
        draft.isloader = false;
      }
    ),
    [actionCreators.doUpdateOrderFailure]: produce((draft, { payload }) => {
      draft.message = payload;
      draft.isloader = false;
    }),
    [actionCreators.doOrderNextSuccess]: produce(
      (draft, { payload: { data, included } }) => {
        draft.orderDetails = data;
        draft.included = included;
        draft.isloader = false;
      }
    ),

    [actionCreators.doOrderComplete]: produce((draft) => {
      draft.recentOrder = {};
    }),
    [actionCreators.doOrderCompleteSuccess]: produce((draft, { payload }) => {
      draft.recentOrder = payload;
    }),
    [actionCreators.doOrderCompleteFailure]: produce((draft) => {
      draft.recentOrder = {};
    }),
    [actionCreators.doOrderReturnReasonsSuccess]: produce(
      (draft, { payload }) => {
        draft.orderReturnReasons = payload;
      }
    ),
    [actionCreators.doOrderReturnReasonsFailure]: produce((draft) => {
      draft.orderReturnReasons = [];
    }),
    [actionCreators.doOrderReturnUpdateItem]: produce((draft, { payload }) => {
      draft.orderDetails.relationships.line_items.data[payload.index][
        payload.slug
      ] = payload.value;
    }),
    [actionCreators.doGetReturnedItemsFailure]: produce(
      (draft, { payload }) => {
        draft.returnItem = payload;
      }
    ),
    [actionCreators.getWishListData]: produce((draft) => {
      draft.wishList = [];
    }),
    [actionCreators.getWishListDataSuccess]: produce(
      (draft, { payload: { data } }) => {
        draft.wishList = data;
      }
    ),
    [actionCreators.getWishListDataFailure]: produce((draft, { payload }) => {
      draft.message = payload;
    }),
    [actionCreators.removeProductToWishlistSuccess]: produce(
      (draft, { payload }) => {
        const index = draft.wishList.findIndex(
          (data) => data._id === payload.productId
        );
        if (index != -1) {
          draft.wishList.splice(index, 1);
        }
      }
    ),
    [actionCreators.removeProductToWishlistFailed]: produce(
      (draft, { payload }) => {
        draft.message = payload;
      }
    ),
    [actionCreators.doWishListEmpty]: produce((draft) => {
      draft.wishList = {};
    }),
  },
  defaultState
);

export default orderReducer;
