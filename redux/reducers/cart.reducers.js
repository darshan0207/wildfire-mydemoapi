import { handleActions } from 'redux-actions';
import * as actionCreaters from '../actionCreaters/cart.actioncreater';
import produce from 'immer';

const defaultState = {
  data: [],
  itemtotal: 0,
  message: null,
  isLoader: true,
  paymentMethod: '',
};

const cartReducer = handleActions(
  {
    [actionCreaters.getCartDetail]: produce((draft) => {
      draft.isLoader = true;
      draft.paymentMethod = '';
    }),
    [actionCreaters.getCartDetailSuccess]: produce(
      (draft, { payload: { data, itemtotal } }) => {
        draft.data = data;
        draft.itemtotal = itemtotal;
        draft.isLoader = false;
      }
    ),
    [actionCreaters.getCartDetailFailed]: produce((draft, { payload }) => {
      draft.message = payload;
      draft.isLoader = false;
    }),
    [actionCreaters.doCartEmpty]: produce((draft) => {
      draft.data = [];
      draft.message = '';
      draft.itemtotal = '';
    }),
    [actionCreaters.doPaymentMethod]: produce((draft, { payload }) => {
      draft.paymentMethod = payload;
    }),
    [actionCreaters.updateCartQuantitysucess]: produce(
      (draft, { payload: { data, itemtotal } }) => {
        draft.data = data;
        draft.itemtotal = itemtotal;
      }
    ),
    [actionCreaters.cartItemDeleteSucess]: produce(
      (draft, { payload: { data, itemtotal, cartId } }) => {
        // draft.data = data;
        draft.data.splice(
          draft.data.findIndex((data) => data._id === cartId),
          1
        );
        draft.itemtotal = itemtotal;
      }
    ),
  },
  defaultState
);

export default cartReducer;
