import { handleActions } from 'redux-actions';
import * as actionCreators from '../actionCreaters/home.actioncreater';
import produce from 'immer';

const defaultState = {
  data: [],
  included: [],
  message: '',
};

const homeReducer = handleActions(
  {
    [actionCreators.getHomeDetail]: produce((draft) => {
      draft.data = [];
    }),
    [actionCreators.getHomeDetailSuccess]: produce(
      (draft, { payload: { data } }) => {
        draft.data = data;
      }
    ),
    [actionCreators.getHomeDetailFailed]: produce((draft, { payload }) => {
      draft.message = 'Error';
    }),
  },
  defaultState
);
export default homeReducer;
