import { handleActions } from 'redux-actions';
import produce from 'immer';
import * as actionCreators from '../actionCreaters/contact-us.actioncreater';

const defaultState = {
  contactus: {},
  error: '',
  isloader: false,
};

const contactUsReducer = handleActions(
  {
    [actionCreators.doContactUs]: produce((draft) => {
      draft.contactus = {};
      draft.error = '';
      draft.isloader = true;
    }),
    [actionCreators.doContactUsSuccess]: produce((draft, { payload }) => {
      draft.contactus = payload;
      draft.error = '';
      draft.isloader = false;
    }),
    [actionCreators.doContactUsFailure]: produce((draft, { payload }) => {
      draft.error = payload;
      draft.isloader = false;
    }),
  },
  defaultState
);

export default contactUsReducer;
