import { handleActions } from 'redux-actions';
import * as actionCreators from '../actionCreaters/shipping.actioncreater';
import produce from 'immer';

const defaultState = {
  data: [],
  included: [],
  message: '',
  isloader: true,
};

const shippingReducer = handleActions(
  {
    [actionCreators.shippingDetail]: produce((draft) => {
      draft.data = [];
      draft.isloader = true;
    }),
    [actionCreators.shippingDetailSuccess]: produce((draft, { payload }) => {
      draft.data = payload;
      draft.isloader = false;
    }),
    [actionCreators.shippingDetailFailed]: produce((draft, { payload }) => {
      draft.message = payload;
      draft.isloader = false;
    }),
  },
  defaultState
);
export default shippingReducer;
