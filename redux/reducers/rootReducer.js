import { combineReducers } from 'redux';
import accountReducer from './account.reducers';
import authReducer from './auth.reducers';
import searchReducer from './search.reducers';
import addressReducer from './address.reducers';
import orderReducer from './order.reducers';
import singleProductReducer from './product.reducers';
import cartReducer from './cart.reducers';
import shippingReducer from './shipping.reducers';
import paymentReducer from './payment.reducers';
import homeReducer from './home.reducers';
import vendorReducer from './vendor.reducers';
import contactUsReducer from './contact-us.reducers';
import countrieReducer from './countrie.reducers';

const rootReducer = combineReducers({
  auth: authReducer,
  search: searchReducer,
  account: accountReducer,
  address: addressReducer,
  order: orderReducer,
  singleProduct: singleProductReducer,
  cart: cartReducer,
  shipping: shippingReducer,
  payment: paymentReducer,
  home: homeReducer,
  vendor: vendorReducer,
  contactus: contactUsReducer,
  countrie: countrieReducer,
});

export default rootReducer;
