import { handleActions } from 'redux-actions';
import produce from 'immer';

import * as actionCreators from '../actionCreaters/address.actioncreater';

const defaultState = {
  addressesList: {
    data: [],
    links: {},
    meta: {
      count: 0,
      total_count: 0,
      total_pages: 0,
    },
  },
  message: null,
  isloader: true,
};

const addressReducer = handleActions(
  {
    [actionCreators.doAddressesList]: produce((draft) => {
      draft.addressesList = {
        data: [],
        links: {},
        meta: {
          count: 0,
          total_count: 0,
          total_pages: 0,
        },
      };
      draft.message = null;
      draft.isloader = true;
    }),
    [actionCreators.doAddressesListSuccess]: produce((draft, { payload }) => {
      // draft.addressesList = payload;
      draft.addressesList.data = payload.data;
      draft.isloader = false;
      draft.isnextpage = false;
    }),
    [actionCreators.doAddressesListFailure]: produce((draft, { payload }) => {
      draft.message = payload;
      draft.isloader = false;
    }),
    [actionCreators.doCreateAddressSuccess]: produce(
      (draft, { payload: { data } }) => {
        draft.addressesList.data.push({ ...data });
      }
    ),
    [actionCreators.doCreateAddressFailure]: produce((draft, { payload }) => {
      draft.message = payload;
    }),
    [actionCreators.doUpdateAddress]: produce((draft) => {
      draft.message = null;
    }),
    [actionCreators.doUpdateAddressSuccess]: produce((draft, { payload }) => {
      const index = draft.addressesList.data.findIndex(
        (data) => data._id === payload._id
      );
      draft.addressesList.data[index] = payload;
    }),
    [actionCreators.doUpdateAddressFailure]: produce((draft, { payload }) => {
      draft.message = payload;
    }),

    [actionCreators.doRemoveAddress]: produce((draft) => {
      draft.message = null;
    }),
    [actionCreators.doRemoveAddressSuccess]: produce((draft, { payload }) => {
      draft.addressesList.data.splice(
        draft.addressesList.data.findIndex((data) => data._id === payload.id),
        1
      );
    }),
    [actionCreators.doRemoveAddressFailure]: produce((draft, { payload }) => {
      draft.message = payload;
    }),
  },
  defaultState
);

export default addressReducer;
