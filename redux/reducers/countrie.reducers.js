import { handleActions } from 'redux-actions';
import * as actionCreators from '../actionCreaters/countrie.actioncreater';
import produce from 'immer';

const defaultState = {
  data: [],
  message: '',
  stateList: [],
};

const countrieReducer = handleActions(
  {
    [actionCreators.doCountrieList]: produce((draft) => {
      document.body.classList.add('loading-indicator');
      draft.data = [];
    }),
    [actionCreators.doCountrieSuccess]: produce(
      (draft, { payload: { data } }) => {
        document.body.classList.remove('loading-indicator');
        draft.data = data;
      }
    ),
    [actionCreators.doCountrieFailure]: produce((draft, { payload }) => {
      document.body.classList.remove('loading-indicator');
      draft.message = 'Error';
    }),
    [actionCreators.doRegionList]: produce((draft) => {
      document.body.classList.add('loading-indicator');
      draft.stateList = [];
    }),
    [actionCreators.doRegionSuccess]: produce((draft, { payload }) => {
      document.body.classList.remove('loading-indicator');
      draft.stateList = payload.data;
    }),
    [actionCreators.doRegionFailure]: produce((draft, { payload }) => {
      document.body.classList.remove('loading-indicator');
      draft.message = payload.message;
    }),
  },
  defaultState
);
export default countrieReducer;
