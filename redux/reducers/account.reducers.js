import { handleActions } from 'redux-actions';
import produce from 'immer';

import * as actionCreators from '../actionCreaters/account.actioncreater';

const defaultState = {
  accountInfo: {},
  addressesList: {},
  message: null,
  updateaccountInfo: {},
  isloader: false,
};

const accountReducer = handleActions(
  {
    [actionCreators.doAccountInfo]: produce((draft) => {
      draft.accountInfo = {};
      draft.message = null;
    }),
    [actionCreators.doAccountInfoSuccess]: produce((draft, { payload }) => {
      draft.accountInfo = payload;
    }),
    [actionCreators.doAccountInfoFailure]: produce((draft, { payload }) => {
      draft.message = payload;
    }),
    // [actionCreators.doAddressesList]: produce((draft) => {
    //   draft.addressesList = {};
    //   draft.message = null;
    // }),
    // [actionCreators.doAddressesListSuccess]: produce((draft, { payload }) => {
    //   draft.addressesList = payload;
    // }),
    // [actionCreators.doAddressesListFailure]: produce((draft, { payload }) => {
    //   draft.message = payload;
    // }),

    [actionCreators.doAccountUpdate]: produce((draft) => {
      draft.updateaccountInfo = {};
      draft.message = null;
    }),
    [actionCreators.doAccountUpdateSuccess]: produce((draft, { payload }) => {
      draft.updateaccountInfo = payload;
    }),
    [actionCreators.doAccountUpdateFailure]: produce((draft, { payload }) => {
      draft.message = payload;
    }),
    [actionCreators.doForgotPassword]: produce((draft) => {
      draft.isloader = true;
      draft.message = null;
    }),
    [actionCreators.doForgotPasswordSuccess]: produce((draft, { payload }) => {
      draft.isloader = false;
    }),
    [actionCreators.doForgotPasswordFailed]: produce((draft, { payload }) => {
      draft.message = payload;
      draft.isloader = false;
    }),
    [actionCreators.doResetPassword]: produce((draft) => {
      draft.isloader = true;
      draft.message = null;
    }),
    [actionCreators.doResetPasswordSuccess]: produce((draft, { payload }) => {
      draft.isloader = false;
    }),
    [actionCreators.doResetPasswordFailed]: produce((draft, { payload }) => {
      draft.message = payload;
      draft.isloader = false;
    }),
  },
  defaultState
);

export default accountReducer;
